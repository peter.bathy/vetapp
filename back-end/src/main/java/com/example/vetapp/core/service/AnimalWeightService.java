package com.example.vetapp.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.AnimalWeight;
import com.example.vetapp.core.repository.AnimalWeightRepository;

@Service
@Transactional
public class AnimalWeightService {

	private AnimalWeightRepository animalWeightRepository;

	@Autowired
	public AnimalWeightService(AnimalWeightRepository animalWeightRepository) {
		this.animalWeightRepository = animalWeightRepository;
	}

	public List<AnimalWeight> getAllWeightDatasByAnimalId(Long animalId) {

		return animalWeightRepository.findAllAnimalWeightDatas(animalId);
	}
	
	public void saveWeight(AnimalWeight animalWeightData) {
		animalWeightRepository.save(animalWeightData);
	}

	public void deleteWeightDataById(Long id) {
		animalWeightRepository.deleteById(id);
	}

}
