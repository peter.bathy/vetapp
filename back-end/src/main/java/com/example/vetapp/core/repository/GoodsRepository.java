package com.example.vetapp.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.payload.out.GoodListItem;
import com.example.vetapp.core.payload.out.SimpleGoodItem;

@Repository
public interface GoodsRepository extends JpaRepository<Goods, Long> {
	
	@Query("SELECT g FROM Goods g WHERE g.active = true and 'oltás' MEMBER OF g.keyWords  ORDER BY g.name ")
	List<Goods> getAllActiveVaccines();

	@Query("SELECT g FROM Goods g WHERE g.active = true and ?2 MEMBER OF g.keyWords and g.name  LIKE %?1% ORDER BY g.name ASC ")
	Page<Goods> searchGoodsDetailsInTypeLimit20(String searchWord, String goodsType, Pageable firstTwenty);

	@Query("SELECT new com.example.vetapp.core.payload.out.GoodListItem(g) FROM Goods g ORDER BY g.name ASC")
	List<GoodListItem> findByOrderByName();
	
	@Query("SELECT new com.example.vetapp.core.payload.out.SimpleGoodItem(g) FROM Goods g WHERE g.active = true ORDER BY g.name ASC")
	List<SimpleGoodItem> findAllActiveGoods();
}
