package com.example.vetapp.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.domain.OwnerDebt;
import com.example.vetapp.core.service.OwnerDebtService;

@RestController
@RequestMapping("/api/owner-debt")
public class OwnerDebtController {

	private OwnerDebtService ownerDebtService;

	@Autowired
	public OwnerDebtController(OwnerDebtService ownerDebtService) {
		this.ownerDebtService = ownerDebtService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getOwnerDebtDatasById(@PathVariable Long id) {
		
		return new ResponseEntity<>(ownerDebtService.getAllDebtDatasByOwnerId(id), HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveDebt(@RequestBody OwnerDebt debtData) {

		ownerDebtService.saveDebt(debtData);

		return new ResponseEntity<>( HttpStatus.OK);
	}
}
