package com.example.vetapp.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.AppConfig;

@Repository
public interface AppConfigRepository extends JpaRepository<AppConfig, Long> {

	Optional<AppConfig> findBySettingName(String settingName);

}
