package com.example.vetapp.core.payload.out;

import java.util.ArrayList;
import java.util.List;

import com.example.vetapp.core.domain.Breed;
import com.example.vetapp.core.domain.Species;

public class SpeciesListItem {

	private Long id;
	private String speciesName;
	private List<BreedListItem> breeds;

	public SpeciesListItem() {
	}

	public SpeciesListItem(Species species) {
		if (species != null) {
			this.id = species.getId();

			this.speciesName = species.getSpeciesName();

			List<BreedListItem> breedList = new ArrayList<BreedListItem>();

			for (Breed breeds : species.getBreeds()) {
				breedList.add(new BreedListItem(breeds));
			}

			breedList.sort((n1, n2) -> n1.getBreedName().compareTo(n2.getBreedName()));

			this.breeds = breedList;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpeciesName() {
		return speciesName;
	}

	public void setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
	}

	public List<BreedListItem> getBreeds() {
		return breeds;
	}

	public void setBreeds(List<BreedListItem> breeds) {
		this.breeds = breeds;
	}

}
