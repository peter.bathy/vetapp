package com.example.vetapp.core.payload.in;

import com.example.vetapp.core.domain.Owner;

public class SimpleOwnerIdentifier {

    private Long id;
    private String name;

    public SimpleOwnerIdentifier(Owner owner) {
        this.id = owner.getId();
        this.name = owner.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
