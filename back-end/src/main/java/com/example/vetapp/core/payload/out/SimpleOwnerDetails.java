package com.example.vetapp.core.payload.out;
import com.example.vetapp.core.domain.Owner;

public class SimpleOwnerDetails {
	private Long ownerId;
	private String ownerName;
	private String zipCode;
	private String city;
	private String address;

	public SimpleOwnerDetails() {
	}

	public SimpleOwnerDetails(Owner owner) {
		this.ownerId = owner.getId();
		this.ownerName = owner.getName();
		this.zipCode = owner.getZipCode();
		this.city = owner.getCity();
		this.address = owner.getAddress();
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
