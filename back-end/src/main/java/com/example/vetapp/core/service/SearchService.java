package com.example.vetapp.core.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.domain.GoodsTemplate;
import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.payload.in.CustomSearchValues;
import com.example.vetapp.core.payload.out.AnimalListItem;
import com.example.vetapp.core.payload.out.SimpleGoodsTemplateItem;
import com.example.vetapp.core.payload.out.SimpleOwnerDetailsWithAnimals;
import com.example.vetapp.core.repository.AnimalRepository;
import com.example.vetapp.core.repository.GoodsRepository;
import com.example.vetapp.core.repository.GoodsTemplateRepository;
import com.example.vetapp.core.repository.OwnerRepository;

@Service
@Transactional
public class SearchService {

	private AnimalRepository animalRepository;
	private OwnerRepository ownerRepository;
	private GoodsRepository goodsRepository;
	private GoodsTemplateRepository goodsTemplateRepository;

	@Autowired
	public SearchService(AnimalRepository animalRepository, OwnerRepository ownerRepository,
			GoodsRepository goodsRepository, GoodsTemplateRepository goodsTemplateRepository) {
		this.animalRepository = animalRepository;
		this.ownerRepository = ownerRepository;
		this.goodsRepository = goodsRepository;
		this.goodsTemplateRepository = goodsTemplateRepository;
	}

	public List<AnimalListItem> searchAnimalOwnerDetails(String searchWord) {

		Pageable firstTwenty = PageRequest.of(0, 20);
		Page<Animal> result = animalRepository.searchAnimalOwnerDetailsLimit20(searchWord, firstTwenty);
		List<AnimalListItem> finalList = new ArrayList<>();
		for (Animal animal : result) {
			finalList.add(new AnimalListItem(animal));
		}

		return finalList;
	}

	public List<SimpleOwnerDetailsWithAnimals> searchOwnerDetails(String searchWord) {

		Pageable firstTwenty = PageRequest.of(0, 20);
		Page<Owner> result = ownerRepository.searchOwnerDetailsLimit20(searchWord, firstTwenty);
		List<SimpleOwnerDetailsWithAnimals> finalList = new ArrayList<>();
		for (Owner owner : result) {
			finalList.add(new SimpleOwnerDetailsWithAnimals(owner));
		}

		return finalList;
	}

	public List<Goods> searchGoodsDetails(String searchWord, String goodsType) {

		Pageable firstTwenty = PageRequest.of(0, 20);
		Page<Goods> result = goodsRepository.searchGoodsDetailsInTypeLimit20(searchWord, goodsType, firstTwenty);

		return result.getContent();
	}

	public List<SimpleGoodsTemplateItem> searchTemplates(String searchWord) {

		Pageable firstTwenty = PageRequest.of(0, 20);
		Page<GoodsTemplate> result = goodsTemplateRepository.searchTemplateLimit20(searchWord, firstTwenty);

		List<SimpleGoodsTemplateItem> formedList = new ArrayList<>();

		for (GoodsTemplate templateItem : result.getContent()) {
			formedList.add(new SimpleGoodsTemplateItem(templateItem));
		}

		return formedList;
	}

	public List<AnimalListItem> searchAnimalDetails(String searchWord) {
		Pageable firstTwenty = PageRequest.of(0, 20);
		Page<Animal> result = animalRepository.searchAnimalDetailsLimit20(searchWord, firstTwenty);
		List<AnimalListItem> finalList = new ArrayList<>();
		for (Animal animal : result) {
			finalList.add(new AnimalListItem(animal));
		}

		return finalList;
	}

	public List<SimpleOwnerDetailsWithAnimals> detailedSearch(CustomSearchValues searchValues) {

		int counter = 0;

		List<Animal> result = animalRepository.searchExtendedDetails(searchValues.getOwnerName(),
				searchValues.getAddress(), searchValues.getEmail(), searchValues.getAnimalName(),
				searchValues.getIdOfVaccinationBook(), searchValues.getPassportId());

		List<SimpleOwnerDetailsWithAnimals> finalList = new ArrayList<>();
		Owner currentOwner = null;
		List<Animal> currentAnimalList = new ArrayList<Animal>();

		for (Animal animal : result) {
			if (counter == 201) {
				break;
			}
			if (!animal.getOwner().equals(currentOwner)) {
				if (currentOwner != null) {
					currentOwner.setAnimals(currentAnimalList);
					finalList.add(new SimpleOwnerDetailsWithAnimals(currentOwner));
				}
				currentOwner = animal.getOwner();
				currentAnimalList.clear();
				if (!searchValues.getBreed().isBlank() || !searchValues.getSpecies().isBlank()) {
					if (!searchValues.getBreed().isBlank()) {
						if (animal.getBreed() != null
								&& searchValues.getBreed().equals(animal.getBreed().getBreedName())) {
							currentAnimalList.add(animal);
						}
					} else if (!searchValues.getSpecies().isBlank() && animal.getBreed() != null
							&& animal.getBreed().getSpecies() != null
							&& searchValues.getSpecies().equals(animal.getBreed().getSpecies().getSpeciesName())) {
						currentAnimalList.add(animal);
						counter = counter + 1;
					}
				} else {
					currentAnimalList.add(animal);
					counter = counter + 1;
				}
			} else {
				if (!searchValues.getBreed().isBlank() || !searchValues.getSpecies().isBlank()) {
					if (!searchValues.getBreed().isBlank()) {
						if (animal.getBreed() != null
								&& searchValues.getBreed().equals(animal.getBreed().getBreedName())) {
							currentAnimalList.add(animal);
						}
					} else if (!searchValues.getSpecies().isBlank() && animal.getBreed() != null
							&& animal.getBreed().getSpecies() != null
							&& searchValues.getSpecies().equals(animal.getBreed().getSpecies().getSpeciesName())) {
						currentAnimalList.add(animal);
						counter = counter + 1;
					}
				} else {
					currentAnimalList.add(animal);
					counter = counter + 1;
				}
			}
		}

		if (!currentAnimalList.isEmpty()) {
			currentOwner.setAnimals(currentAnimalList);
			finalList.add(new SimpleOwnerDetailsWithAnimals(currentOwner));
		}

		return finalList;
	}
}
