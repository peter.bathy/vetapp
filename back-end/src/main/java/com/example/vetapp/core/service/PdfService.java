package com.example.vetapp.core.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.MedicalRecord;
import com.example.vetapp.core.domain.MedicalRecordAttachement;
import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.payload.MedicalRecordWithAttachedImages;
import com.example.vetapp.core.repository.MedicalRecordRepository;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

@Service
@Transactional
public class PdfService {

	private TemplateEngine templateEngine;
	private MedicalRecordRepository medicalRecordRepository;
	private AnimalService animalService;

	@Value("${data.dataPath}")
	private String dataPath;

	@Autowired
	public PdfService(TemplateEngine templateEngine, MedicalRecordRepository medicalRecordRepository,
			AnimalService animalService) {
		this.templateEngine = templateEngine;
		this.medicalRecordRepository = medicalRecordRepository;
		this.animalService = animalService;
	}

	public byte[] generatePatientHistory(Long animalId, List<Long> medicalRecordIds, Boolean embedImages,
			Boolean embedPdfs, boolean anonymized) throws Exception {

		Animal animal = animalService.getAnimalById(animalId);

		List<MedicalRecordWithAttachedImages> medicalRecords = new ArrayList<>();
		List<MedicalRecord> rawList = medicalRecordRepository.findByIdInOrderBySessionDateDesc(medicalRecordIds);
		List<String> pdfList = new ArrayList<>();

		for (MedicalRecord mRecord : rawList) {
			MedicalRecordWithAttachedImages newRecord = new MedicalRecordWithAttachedImages(mRecord);
			List<String> currentImages = new ArrayList<>();

			for (MedicalRecordAttachement currentAttachement : mRecord.getAttachements()) {
				if (isPdf(currentAttachement.getFileName()) == true) {
					pdfList.add(currentAttachement.getFileName());
				} else {
					byte[] fileContent = FileUtils.readFileToByteArray(
							new File(System.getProperty("user.home") + dataPath + "/attachements/" + currentAttachement.getFileName()));
					String encodedString = Base64.getEncoder().encodeToString(fileContent);
					currentImages.add(encodedString);
				}
			}

			if (embedImages) {
				newRecord.setImageStrings(currentImages);
			}

			medicalRecords.add(newRecord);
		}

		byte[] logo = IOUtils.toByteArray(new ClassPathResource("static/images/logo.png").getInputStream());
		String logoAsString = Base64.getEncoder().encodeToString(logo);

		Owner owner = animal.getOwner();

		if (anonymized) {
			owner.setName("");
			owner.setZipCode("");
			owner.setCity("");
			owner.setAddress("");
			owner.setMobileNumber("");
			owner.setPhoneNumber("");
			owner.setEmail("");
		}

		Context thymeleafContext = new Context();
		thymeleafContext.setVariable("owner", owner);
		thymeleafContext.setVariable("animal", animal);
		thymeleafContext.setVariable("medicalRecords", medicalRecords);
		thymeleafContext.setVariable("logo", logoAsString);

		String htmlBody = templateEngine.process("pdf/patient-history", thymeleafContext);

		ByteArrayOutputStream target = new ByteArrayOutputStream();

		ConverterProperties converterProperties = new ConverterProperties();
		converterProperties.setFontProvider(new DefaultFontProvider(false, true, false));

		HtmlConverter.convertToPdf(htmlBody, target, converterProperties);

		List<InputStream> inputPdfList = new ArrayList<InputStream>();

		inputPdfList.add(new ByteArrayInputStream(target.toByteArray()));

		if (embedPdfs) {
			for (String pdfName : pdfList) {
				inputPdfList.add(
						new FileInputStream(System.getProperty("user.home") + dataPath + "/attachements/" + pdfName));
			}
		}

		mergePdfFiles(inputPdfList, target);

		return target.toByteArray();

	}

	private boolean isPdf(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1).equals("pdf") ? true : false;
	}

	private void mergePdfFiles(List<InputStream> inputPdfList, OutputStream outputStream) throws Exception {

		// Create document and pdfReader objects.
		Document document = new Document();
		List<PdfReader> readers = new ArrayList<PdfReader>();
		int totalPages = 0;

		// Create pdf Iterator object using inputPdfList.
		Iterator<InputStream> pdfIterator = inputPdfList.iterator();

		// Create reader list for the input pdf files.
		while (pdfIterator.hasNext()) {
			InputStream pdf = pdfIterator.next();
			PdfReader pdfReader = new PdfReader(pdf);
			readers.add(pdfReader);
			totalPages = totalPages + pdfReader.getNumberOfPages();
		}

		// Create writer for the outputStream
		PdfWriter writer = PdfWriter.getInstance(document, outputStream);

		// Open document.
		document.open();

		// Contain the pdf data.
		PdfContentByte pageContentByte = writer.getDirectContent();

		PdfImportedPage pdfImportedPage;
		int currentPdfReaderPage = 1;
		Iterator<PdfReader> iteratorPDFReader = readers.iterator();

		// Iterate and process the reader list.
		while (iteratorPDFReader.hasNext()) {
			PdfReader pdfReader = iteratorPDFReader.next();
			// Create page and add content.
			while (currentPdfReaderPage <= pdfReader.getNumberOfPages()) {
				document.newPage();
				pdfImportedPage = writer.getImportedPage(pdfReader, currentPdfReaderPage);
				pageContentByte.addTemplate(pdfImportedPage, 0, 0);
				currentPdfReaderPage++;
			}
			currentPdfReaderPage = 1;
		}

		// Close document and outputStream.
		outputStream.flush();
		document.close();
		outputStream.close();
	}
}
