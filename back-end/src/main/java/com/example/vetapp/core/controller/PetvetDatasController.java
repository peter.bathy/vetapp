package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.domain.PetvetDatas;
import com.example.vetapp.core.service.PetvetDatasService;

@RestController
@RequestMapping("/api/petvetdata")
public class PetvetDatasController {

	private PetvetDatasService petvetDatasService;

	@Autowired
	public PetvetDatasController(PetvetDatasService petvetDatasService) {
		this.petvetDatasService = petvetDatasService;
	}
	
	@GetMapping
    public ResponseEntity<List<PetvetDatas>> getAllUndoneChanges() {
        return new ResponseEntity<>(petvetDatasService.getAllUndoneChanges(), HttpStatus.OK);
    }
	
	@PostMapping("/save")
	public ResponseEntity<?> saveOwner(@RequestBody PetvetDatas changes) {

		petvetDatasService.saveChanges(changes);

		return new ResponseEntity<>( HttpStatus.OK);
	}
	
	@GetMapping("/old-changes/{limitValue}")
	public ResponseEntity<?> searchTemplates(@PathVariable Integer limitValue) {

		return new ResponseEntity<>(petvetDatasService.searchGoodsDetails(limitValue), HttpStatus.OK);
	}
}
