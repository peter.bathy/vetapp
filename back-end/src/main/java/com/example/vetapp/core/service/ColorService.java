package com.example.vetapp.core.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Color;
import com.example.vetapp.core.payload.out.ColorListItem;
import com.example.vetapp.core.repository.ColorRepository;

@Service
@Transactional
public class ColorService {

	private ColorRepository colorRepository;

	@Autowired
	public ColorService(ColorRepository colorRepository) {
		this.colorRepository = colorRepository;
	}

	public List<ColorListItem> getAllColor() {
		
		List<Color> colors = colorRepository.findByOrderByColorNameAsc();
		
		List<ColorListItem> colorList = new ArrayList<ColorListItem>();

		colors.forEach(color -> {
			colorList.add(new ColorListItem(color));
		});

		return colorList;
	}


}
