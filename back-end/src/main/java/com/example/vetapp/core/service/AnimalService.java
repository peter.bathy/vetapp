package com.example.vetapp.core.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.Breed;
import com.example.vetapp.core.domain.Color;
import com.example.vetapp.core.domain.MedicalRecord;
import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.domain.Sex;
import com.example.vetapp.core.domain.Species;
import com.example.vetapp.core.payload.in.AnimalMergeRequest;
import com.example.vetapp.core.payload.out.AnimalDetails;
import com.example.vetapp.core.payload.out.AnimalListItem;
import com.example.vetapp.core.repository.AnimalRepository;
import com.example.vetapp.core.repository.ColorRepository;
import com.example.vetapp.core.repository.SpeciesRepository;

@Service
@Transactional
public class AnimalService {

	private static final Logger logger = Logger.getLogger("AnimalService.class");

	private AnimalRepository animalRepository;
	private OwnerService ownerService;
	private MedicalRecordService medicalRecordService;
	private ColorRepository colorRepository;
	private SpeciesRepository speciesRepository;

	@Autowired
	public AnimalService(AnimalRepository animalRepository, OwnerService ownerService, ColorRepository colorRepository,
			SpeciesRepository speciesRepository, MedicalRecordService medicalRecordService) {
		this.animalRepository = animalRepository;
		this.ownerService = ownerService;
		this.colorRepository = colorRepository;
		this.speciesRepository = speciesRepository;
		this.medicalRecordService = medicalRecordService;
	}

	public List<AnimalListItem> getAllAnimals() {

		List<Animal> animals = getAllAnimalsRaw();

		List<AnimalListItem> animalList = new ArrayList<AnimalListItem>();

		animals.forEach(animal -> {
			if (animal != null) {
				animalList.add(new AnimalListItem(animal));
			}
		});

		return animalList;
	}

	public List<Animal> getAllAnimalsRaw() {
		return animalRepository.findAll();
	}

	public AnimalDetails getAnimalDetailsById(Long id) {

		Optional<Animal> optionalAnimal = animalRepository.findById(id);

		if (optionalAnimal.isPresent()) {
			return new AnimalDetails(optionalAnimal.get());
		}
		return null;
	}

	public Animal getAnimalById(Long id) {

		Optional<Animal> optionalAnimal = animalRepository.findById(id);

		if (optionalAnimal.isPresent()) {
			return optionalAnimal.get();
		}
		return null;
	}

	private void setAnimalColor(Animal animal, String colorName, List<Color> colorList) {
		Optional<Color> matchingColor = colorList.stream().filter(a -> a.getColorName().equals(colorName)).findFirst();

		if (matchingColor.isPresent()) {
			animal.setColor(matchingColor.get());
		} else {
			animal.setColor(null);
		}
	}

	private void setAnimalBreedSpecies(Animal animal, String breed, String species, List<Species> speciesList) {
		Optional<Species> matchingSpecies = speciesList.stream().filter(a -> a.getSpeciesName().equals(species))
				.findFirst();

		if (matchingSpecies.isPresent()) {
			List<Breed> breedList = matchingSpecies.get().getBreeds();
			if (breedList.size() == 1) {
				animal.setBreed(matchingSpecies.get().getBreeds().get(0));
			} else {
				Optional<Breed> matchingBrieds = breedList.stream().filter(s -> s.getBreedName().equals(breed))
						.findFirst();
				if (matchingBrieds.isPresent()) {
					animal.setBreed(matchingBrieds.get());
				} else {
					animal.setBreed(null);
				}
			}
		} else {
			animal.setBreed(null);
		}
	}

	private void setOwner(long ownerId, Animal animal, List<Owner> ownerList) {

		Optional<Owner> matchingOwner = ownerList.stream().filter(a -> a.getId().equals(ownerId)).findFirst();

		if (matchingOwner.isPresent()) {
			animal.setOwner(matchingOwner.get());
		}
	}

	private void setSexAndSpayedValues(String databaseValue, Animal animal) {
		Boolean isSpayed = false;
		Sex sex = null;

		switch (databaseValue) {
		case " SZUKA":
		case "SZUKA":
		case "nőstény":
		case "tojó":
			sex = Sex.FEMALE;
			isSpayed = false;
			break;
		case "kan":
		case "kandúr":
		case "kos":
		case "kakas":
		case "hím":
		case "bak":
			sex = Sex.MALE;
			isSpayed = false;
			break;
		case "herélt.kan":
		case "ivart.hím":
			sex = Sex.MALE;
			isSpayed = true;
			break;
		case "ivart.nőst":
		case "ivart.szuk":
			sex = Sex.FEMALE;
			isSpayed = true;
			break;
		default:
			sex = Sex.UNKNOWN;
			isSpayed = false;
		}

		animal.setSex(sex);
		animal.setIsSpayed(isSpayed);
	}

	private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		if (dateToConvert != null) {
			return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} else {
			return null;
		}
	}

	public Animal saveAnimal(AnimalDetails animal) {
		List<Color> colorList = colorRepository.findAll();
		List<Species> speciesList = speciesRepository.findAll();

		Animal rawAnimal;
		if (animal.getAnimalId() == 0L) {
			rawAnimal = new Animal();
		} else {
			rawAnimal = animalRepository.getById(animal.getAnimalId());
		}
		rawAnimal.setActive(true);
		rawAnimal.setComment(animal.getComment());
		setAnimalBreedSpecies(rawAnimal, animal.getBreed(), animal.getSpecies(), speciesList);
		rawAnimal.setIdOfMicrochip(animal.getIdOfMicrochip());
		rawAnimal.setIdOfVaccinationBook(animal.getIdOfVaccinationBook());
		rawAnimal.setName(animal.getAnimalName());
		rawAnimal.setIsSpayed(animal.getIsSpayed());
		rawAnimal.setBirthDate(LocalDate.parse(animal.getBirthDate()));
		setAnimalColor(rawAnimal, animal.getColor(), colorList);
		rawAnimal.setOwner(ownerService.getOwnerById(animal.getOwnerId()));
		rawAnimal.setId(animal.getAnimalId());
		rawAnimal.setBirthDateIsEstimated(animal.getBirthDateIsEstimated());
		rawAnimal.setPassportId(animal.getPassportId());
		rawAnimal.setPedigree(animal.getPedigree());
		if (animal.getDateOfSpaying() != null) {
			rawAnimal.setDateOfSpaying(LocalDate.parse(animal.getDateOfSpaying()));
			rawAnimal.setIsSpayed(true);
		} else if (animal.getDateOfSpaying() == null) {
			rawAnimal.setDateOfSpaying(null);
		}
		if (animal.getDateOfDeath() != null) {
			rawAnimal.setDateOfDeath(LocalDate.parse(animal.getDateOfDeath()));
		} else if (animal.getDateOfDeath() == null) {
			rawAnimal.setDateOfDeath(null);
		}
		switch (animal.getSex()) {
		case "hím":
			rawAnimal.setSex(Sex.MALE);
			break;
		case "nőstény":
			rawAnimal.setSex(Sex.FEMALE);
			break;
		default:
			rawAnimal.setSex(Sex.UNKNOWN);
		}

		return animalRepository.save(rawAnimal);
	}

	public void updateDeadAnimals(List<MedicalRecord> findBySearchWord) {

		List<Animal> animals = new ArrayList<>();

		for (MedicalRecord medicalRecord : findBySearchWord) {
			Optional<Animal> optAnimal = animalRepository.findById(medicalRecord.getAnimal().getId());
			if (optAnimal.isPresent()) {
				Animal currentAnimal = optAnimal.get();
				currentAnimal.setDateOfDeath(medicalRecord.getSessionDate().toLocalDate());
				animals.add(currentAnimal);
			}
		}
		animalRepository.saveAll(animals);
	}

	public void mergeAnimal(AnimalMergeRequest request) {
		Animal sourceAnimal = animalRepository.getById(request.getSourceAnimalId());
		Animal goalAnimal = animalRepository.getById(request.getGoalAnimalId());

		List<MedicalRecord> movableMedicalRecords = sourceAnimal.getMedicalRecords();
		medicalRecordService.moveMedicalRecordsToNewAnimal(movableMedicalRecords, goalAnimal);

		sourceAnimal.setActive(false);
		animalRepository.save(sourceAnimal);
	}
}
