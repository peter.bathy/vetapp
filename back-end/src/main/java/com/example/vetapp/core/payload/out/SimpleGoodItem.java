package com.example.vetapp.core.payload.out;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.domain.GoodsTemplate;

public class SimpleGoodItem {

	private Long id;
	private String name;
	private Double sellPrice;
	private String baseQuantityWithUnit;
	private List<String> keyWords;
	private String comment;
	private Set<SimpleGoodsTemplateItem> templates;

	public SimpleGoodItem() {
	}

	public SimpleGoodItem(Goods good) {
		this.id = good.getId();
		if (good.getManufacturer().isBlank()) {
			this.name = good.getName();
		} else {
			this.name = good.getManufacturer() + " " + good.getName();
		}
		this.sellPrice = good.getSellPrice();
		this.baseQuantityWithUnit = good.getBaseQuantity() + " " + good.getQuantityUnit();
		this.comment = good.getComment();
		this.keyWords = good.getKeyWords();

		Set<SimpleGoodsTemplateItem> templateSet = new HashSet<SimpleGoodsTemplateItem>();

		for (GoodsTemplate template : good.getTemplates()) {
			templateSet.add(new SimpleGoodsTemplateItem(template));
		}

		this.templates = templateSet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getBaseQuantityWithUnit() {
		return baseQuantityWithUnit;
	}

	public void setBaseQuantityWithUnit(String baseQuantityWithUnit) {
		this.baseQuantityWithUnit = baseQuantityWithUnit;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<String> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(List<String> keyWords) {
		this.keyWords = keyWords;
	}

	public Set<SimpleGoodsTemplateItem> getTemplates() {
		return templates;
	}

	public void setTemplates(Set<SimpleGoodsTemplateItem> templates) {
		this.templates = templates;
	}

}
