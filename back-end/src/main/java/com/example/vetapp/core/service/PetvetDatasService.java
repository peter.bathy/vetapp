package com.example.vetapp.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.PetvetDatas;
import com.example.vetapp.core.repository.PetvetDatasRepository;

@Service
@Transactional
public class PetvetDatasService {

	private PetvetDatasRepository petvetDatasRepository;

	@Autowired
	public PetvetDatasService(PetvetDatasRepository petvetDatasRepository) {
		this.petvetDatasRepository = petvetDatasRepository;
	}

	public List<PetvetDatas> getAllUndoneChanges() {
		return petvetDatasRepository.getAllUndoneChanges();
	}

	public void saveChanges(PetvetDatas changes) {
		petvetDatasRepository.save(changes);
	}

	public List<PetvetDatas> searchGoodsDetails(Integer limitValue) {

		Pageable neededSize = PageRequest.of(0, limitValue);
		Page<PetvetDatas> result = petvetDatasRepository.getOldChangesWithLimit(neededSize);

		return result.getContent();
	}
	
}
