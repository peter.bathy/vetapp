package com.example.vetapp.core.domain;

public enum AttachementType {
    ULTRASOUND("ultrasound"),
    XRAY("xray"),
	PHOTO("photo"),
	CITOLOGY("citology"),
	LAB("lab"),
	HISTOLOGY("histology"),
	OTHER("other");

    private String name;

    AttachementType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
