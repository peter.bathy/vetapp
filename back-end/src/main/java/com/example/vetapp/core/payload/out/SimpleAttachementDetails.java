package com.example.vetapp.core.payload.out;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.vetapp.core.domain.MedicalRecordAttachement;

public class SimpleAttachementDetails {

	private String attachementType;
	private String filePath;

	public SimpleAttachementDetails() {
	}

	public SimpleAttachementDetails(MedicalRecordAttachement attachement) {
		this.attachementType = attachement.getAttachementType().getName();

		this.filePath = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/medicalrecord/attachement/")
				.path(attachement.getFileName()).toUriString();
	}

	public String getAttachementType() {
		return attachementType;
	}

	public void setAttachementType(String attachementType) {
		this.attachementType = attachementType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
