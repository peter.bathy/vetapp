package com.example.vetapp.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.domain.GoodsTemplate;
import com.example.vetapp.core.payload.out.GoodListItem;
import com.example.vetapp.core.payload.out.SimpleGoodItem;
import com.example.vetapp.core.payload.out.SimpleGoodsTemplateItem;
import com.example.vetapp.core.payload.out.VaccineListItem;
import com.example.vetapp.core.repository.GoodsRepository;
import com.example.vetapp.core.repository.GoodsTemplateRepository;

@Service
@Transactional
public class GoodsService {

	private GoodsRepository goodsRepository;
	private GoodsTemplateRepository goodsTemplateRepository;

	@Autowired
	public GoodsService(GoodsRepository goodsRepository, GoodsTemplateRepository goodsTemplateRepository) {
		this.goodsRepository = goodsRepository;
		this.goodsTemplateRepository = goodsTemplateRepository;
	}

	public List<GoodListItem> getAllGoods() {
		return goodsRepository.findByOrderByName();
	}

	public List<SimpleGoodItem> getAllActiveGoods() {
		return goodsRepository.findAllActiveGoods();
	}

	public void saveGoods(List<Goods> details) {
		goodsRepository.saveAll(details);
	}

	public List<VaccineListItem> getAllActiveVaccines() {
		List<Goods> aviableVaccines = goodsRepository.getAllActiveVaccines();
		List<VaccineListItem> vaccines = new ArrayList<>();

		for (Goods currentVaccine : aviableVaccines) {
			VaccineListItem newItem = new VaccineListItem();
			newItem.setVaccineName(currentVaccine.getName());
			newItem.setManufacturer(currentVaccine.getManufacturer());
			newItem.setId(currentVaccine.getId());
			newItem.setIsRabiesVaccination(currentVaccine.getKeyWords().contains("veszettség"));
			vaccines.add(newItem);
		}
		return vaccines;
	}

	public List<SimpleGoodsTemplateItem> getAllTemplates() {
		return goodsTemplateRepository.findAllTemplates();
	}

	public void saveTemplate(GoodsTemplate template) {
		goodsTemplateRepository.save(template);	
	}

	public void deleteTemplate(GoodsTemplate template) {
		
		Optional<GoodsTemplate> currentTemplateOpt = goodsTemplateRepository.findById(template.getId());
		
		if(currentTemplateOpt.isPresent()) {
			GoodsTemplate currentTemplate = currentTemplateOpt.get();
			List<Goods> saveableGoods = new ArrayList<>();
			for (Goods good : currentTemplate.getGoods()) {
				good.getTemplates().remove(currentTemplate);
				saveableGoods.add(good);
			}
			goodsRepository.saveAll(saveableGoods);
			goodsTemplateRepository.delete(template);
		}else {
			
		}
		
		
	}
}
