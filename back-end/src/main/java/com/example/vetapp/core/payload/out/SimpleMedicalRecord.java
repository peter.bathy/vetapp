package com.example.vetapp.core.payload.out;

import java.time.LocalDateTime;

import com.example.vetapp.core.domain.MedicalRecord;

public class SimpleMedicalRecord {

	private Long id;
	private LocalDateTime dateOfTreatment;
	private String vaccination;
	private Boolean isRabies;
	private String anamnesis;
	private String symptoms;
	private String therapy;
	private String otherDetails;

	public SimpleMedicalRecord() {
	}

	public SimpleMedicalRecord(MedicalRecord medicalRecord) {
		this.id = medicalRecord.getId();
		this.dateOfTreatment = medicalRecord.getSessionDate();
		if (medicalRecord.getVaccinationName() != null && !medicalRecord.getVaccinationName().isBlank()) {
			this.vaccination = medicalRecord.getVaccinationName();
		} else {
			this.vaccination = "";
		}
		if (medicalRecord.getAnamnesis() != null && !medicalRecord.getAnamnesis().isBlank()) {
			this.anamnesis = medicalRecord.getAnamnesis();
		} else {
			this.anamnesis = "";
		}
		if (medicalRecord.getSymptoms() != null && !medicalRecord.getSymptoms().isBlank()) {
			this.symptoms = medicalRecord.getSymptoms();
		} else {
			this.symptoms = "";
		}
		if (medicalRecord.getTherapy() != null && !medicalRecord.getTherapy().isBlank()) {
			this.therapy = medicalRecord.getTherapy();
		} else {
			this.therapy = "";
		}
		if (medicalRecord.getOtherDetails() != null && !medicalRecord.getOtherDetails().isBlank()) {
			this.otherDetails = medicalRecord.getOtherDetails();
		} else {
			this.otherDetails = "";
		}
		this.isRabies = medicalRecord.getIsVaccination();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateOfTreatment() {
		return dateOfTreatment;
	}

	public void setDateOfTreatment(LocalDateTime dateOfTreatment) {
		this.dateOfTreatment = dateOfTreatment;
	}

	public String getVaccination() {
		return vaccination;
	}

	public void setVaccination(String vaccination) {
		this.vaccination = vaccination;
	}

	public String getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getTherapy() {
		return therapy;
	}

	public void setTherapy(String therapy) {
		this.therapy = therapy;
	}

	public Boolean getIsRabies() {
		return isRabies;
	}

	public void setIsRabies(Boolean isRabies) {
		this.isRabies = isRabies;
	}

	public String getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}

}
