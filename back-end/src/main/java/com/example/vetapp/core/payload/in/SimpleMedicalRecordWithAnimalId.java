package com.example.vetapp.core.payload.in;

import com.example.vetapp.core.payload.out.SimpleMedicalRecord;

public class SimpleMedicalRecordWithAnimalId extends SimpleMedicalRecord{

	private Long animalId;
//
	public SimpleMedicalRecordWithAnimalId() {
	}
	
	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

}
