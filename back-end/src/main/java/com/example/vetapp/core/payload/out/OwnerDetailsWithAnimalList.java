package com.example.vetapp.core.payload.out;

import java.util.ArrayList;
import java.util.List;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.Owner;

public class OwnerDetailsWithAnimalList extends OwnerDetails {
	private List<AnimalListItem> animals;

	public OwnerDetailsWithAnimalList() {
	}

	public OwnerDetailsWithAnimalList(Owner owner) {
		super(owner);

		List<AnimalListItem> animalList = new ArrayList<>();

		for (Animal animal : owner.getAnimals()) {
			if (animal.getActive() == true) {
				animalList.add(new AnimalListItem(animal));
			};
		}

		this.animals = animalList;
	}

	public List<AnimalListItem> getAnimals() {
		return animals;
	}

	public void setAnimals(List<AnimalListItem> animals) {
		this.animals = animals;
	}
}
