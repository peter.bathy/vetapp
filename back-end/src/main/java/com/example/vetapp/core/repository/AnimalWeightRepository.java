package com.example.vetapp.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.AnimalWeight;
import com.example.vetapp.core.payload.out.AnimalListItem;

@Repository
public interface AnimalWeightRepository extends JpaRepository<AnimalWeight, Long> {
	@Query("SELECT a FROM AnimalWeight a where a.animalId = ?1")
	List<AnimalWeight> findAllAnimalWeightDatas(Long animalId);

}
