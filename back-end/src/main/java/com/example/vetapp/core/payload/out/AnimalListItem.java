package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.Animal;

public class AnimalListItem {

	private Long animalId;
	private String ownerName;
	private String species;
	private String breed;
	private String animalName;
	private String birthDate;
	private String dateOfDeath;
	private String chipCode;
	private String city;
	private String address;
	private String zipCode;

	public AnimalListItem() {
	}

	public AnimalListItem(Animal animal) {
		if (animal != null && animal.getOwner() != null) {
			this.animalId = animal.getId();
			this.ownerName = animal.getOwner().getName();
			if (animal.getBreed() != null) {
				this.species = animal.getBreed().getBreedName();
				this.breed = animal.getBreed().getSpecies().getSpeciesName();
			} else {
				this.species = "";
				this.breed = "";
			}
			if (animal.getBirthDate() != null) {
				this.birthDate = animal.getBirthDate().toString();
			}
			if (animal.getDateOfDeath() != null) {
				this.dateOfDeath = animal.getDateOfDeath().toString();
			}
			if (animal.getIdOfMicrochip() != null) {
				this.chipCode = animal.getIdOfMicrochip().toString();
			}
			this.animalName = animal.getName();
			this.city = animal.getOwner().getCity();
			this.address = animal.getOwner().getAddress();
			this.zipCode = animal.getOwner().getZipCode();
		}
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public String getChipCode() {
		return chipCode;
	}

	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}
}
