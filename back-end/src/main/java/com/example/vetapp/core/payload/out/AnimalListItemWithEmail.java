package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.Animal;

public class AnimalListItemWithEmail extends AnimalListItem {

	private String email;

	public AnimalListItemWithEmail() {
	}

	public AnimalListItemWithEmail(Animal animal) {
		super(animal);
		this.email = animal.getOwner().getEmail();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
