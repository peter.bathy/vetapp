package com.example.vetapp.core.payload.out;

public class StatsData {
	private String date;
	private Long count;
	
	public StatsData() {
	}

	public StatsData(String date, Long count) {
		this.date = date;
		this.count = count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
	
}
