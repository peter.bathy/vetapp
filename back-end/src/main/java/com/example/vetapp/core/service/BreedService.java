package com.example.vetapp.core.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Breed;
import com.example.vetapp.core.domain.Species;
import com.example.vetapp.core.payload.in.BreedDatas;
import com.example.vetapp.core.payload.out.BreedListItem;
import com.example.vetapp.core.payload.out.SpeciesListItem;
import com.example.vetapp.core.repository.BreedRepository;
import com.example.vetapp.core.repository.SpeciesRepository;

@Service
@Transactional
public class BreedService {

	private BreedRepository breedRepository;
	private SpeciesRepository speciesRepository;

	@Autowired
	public BreedService(BreedRepository breedRepository, SpeciesRepository speciesRepository) {
		this.breedRepository = breedRepository;
		this.speciesRepository = speciesRepository;
	}

	public List<SpeciesListItem> getAllBreedAndSpecies() {

		List<Species> rawSpeciesList = speciesRepository.findAllByOrderBySpeciesNameAsc();

		List<SpeciesListItem> speciesList = new ArrayList<SpeciesListItem>();

		rawSpeciesList.forEach(species -> {
			speciesList.add(new SpeciesListItem(species));
		});

		return speciesList;
	}

	public void saveSpeciesAndBreed(BreedDatas datas) {
		if (datas.getSpeciesId() == 0L) {
			Species newSpecies = speciesRepository.save(new Species(datas.getSpeciesName()));
			breedRepository.save(new Breed(newSpecies, ""));

			if (!datas.getBreedName().isBlank()) {
				breedRepository.save(new Breed(newSpecies, datas.getBreedName()));
			}
		} else if (datas.getBreedId() == 0L && !datas.getBreedName().isBlank()) {
			Species currentSpecies = speciesRepository.getById(datas.getSpeciesId());
			breedRepository.save(new Breed(currentSpecies, datas.getBreedName()));
		} else if (datas.getBreedId() > 0L) {
			Breed currentBreed = breedRepository.getById(datas.getBreedId());
			currentBreed.setBreedName(datas.getBreedName());
			breedRepository.save(currentBreed);
		}
	}

}
