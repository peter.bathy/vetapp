package com.example.vetapp.core.domain;

public enum ChangeEventType {
	CHIP("chip"),
	DEATH("death"),
	SPAYING("spaying"),
	ANIMALDATACHANGE("animalDataChange"),
	OWNERDATACHANGE("ownerDataChange"),
	PASSPORT("passport"),
	OTHER("other"),
	NEWOWNER("newOwner");

	private String name;

	private ChangeEventType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
