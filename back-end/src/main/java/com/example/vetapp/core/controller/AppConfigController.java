package com.example.vetapp.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.payload.in.SaveSettingRequest;
import com.example.vetapp.core.service.AppConfigService;

@RestController
@RequestMapping("/api/config")
public class AppConfigController {

	private AppConfigService appConfigService;

	@Autowired
	public AppConfigController(AppConfigService appConfigService) {
		this.appConfigService = appConfigService;
	}

	@GetMapping("/getValues")
	public ResponseEntity<?> getSettings() {
		return new ResponseEntity<>(appConfigService.getConfigValues(), HttpStatus.OK);
	}

	@PostMapping("/saveValues")
	public ResponseEntity<?> saveSettings(@RequestBody SaveSettingRequest list) {

		try {
			appConfigService.saveConfigValues(list.getConfigList());
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
