package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.payload.in.EventIntervalRequest;
import com.example.vetapp.core.payload.out.EventDetails;
import com.example.vetapp.core.service.CalendarEventService;

@RestController
@RequestMapping("/api/calendar")
public class CalendarEventController {

	private CalendarEventService calendarEventService;

	@Autowired
	public CalendarEventController(CalendarEventService calendarEventService) {
		this.calendarEventService = calendarEventService;
	}

	@PostMapping()
	public ResponseEntity<?> getEventsByInterval(@RequestBody EventIntervalRequest request) {

		List<EventDetails> events = calendarEventService.getEventsByInterval(request);

		return new ResponseEntity<>(events, HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<?> saveEvent(@RequestBody EventDetails event) {

		EventDetails savedEvent = calendarEventService.saveEvent(event);

		return new ResponseEntity<>(savedEvent, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Long> deletePost(@PathVariable Long id) {

        Boolean isRemoved = calendarEventService.deleteEvent(id);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
	
}
