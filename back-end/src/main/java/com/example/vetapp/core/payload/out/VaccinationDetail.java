package com.example.vetapp.core.payload.out;

import java.time.LocalDate;

import com.example.vetapp.core.domain.MedicalRecord;

public class VaccinationDetail {
 
    private String animalName;
    private String species;
    private String breed;
    private String numberOfMicrochip;
    private Long animalId;
    private LocalDate animalDateOfBirth;
    private String ownerName;
    private String vaccinationName;
    private LocalDate vaccinationDate;

    public VaccinationDetail() {
    }

	public VaccinationDetail(MedicalRecord medicalRecord) {
		this.animalName = medicalRecord.getAnimal().getName();
		if (medicalRecord.getAnimal().getBreed() != null) {
			this.breed = medicalRecord.getAnimal().getBreed().getBreedName();
			this.species = medicalRecord.getAnimal().getBreed().getSpecies().getSpeciesName();
		} else {
			this.species = "";
			this.breed = "";
		}
		this.numberOfMicrochip = medicalRecord.getAnimal().getIdOfMicrochip();
		this.animalId = medicalRecord.getAnimal().getId();
		this.animalDateOfBirth = medicalRecord.getAnimal().getBirthDate();
		this.ownerName = medicalRecord.getAnimal().getOwner().getName();
		this.vaccinationName = medicalRecord.getVaccinationName();
		this.vaccinationDate = medicalRecord.getSessionDate().toLocalDate();
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getNumberOfMicrochip() {
		return numberOfMicrochip;
	}

	public void setNumberOfMicrochip(String numberOfMicrochip) {
		this.numberOfMicrochip = numberOfMicrochip;
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public LocalDate getAnimalDateOfBirth() {
		return animalDateOfBirth;
	}

	public void setAnimalDateOfBirth(LocalDate animalDateOfBirth) {
		this.animalDateOfBirth = animalDateOfBirth;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getVaccinationName() {
		return vaccinationName;
	}

	public void setVaccinationName(String vaccinationName) {
		this.vaccinationName = vaccinationName;
	}

	public LocalDate getVaccinationDate() {
		return vaccinationDate;
	}

	public void setVaccinationDate(LocalDate vaccinationDate) {
		this.vaccinationDate = vaccinationDate;
	}
}
