package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.MedicalRecord;

public class ExtendedAnimalListItemWithTreatmentTime extends AnimalListItem {

	private String treatmentTime;

	public ExtendedAnimalListItemWithTreatmentTime() {
	}

	public ExtendedAnimalListItemWithTreatmentTime(MedicalRecord medicalRecord) {
		super(medicalRecord.getAnimal());
		this.treatmentTime = medicalRecord.getSessionDate().toString();
	}

	public String getTreatmentTime() {
		return treatmentTime;
	}

	public void setTreatmentTime(String treatmentTime) {
		this.treatmentTime = treatmentTime;
	}

	
}
