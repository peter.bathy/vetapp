package com.example.vetapp.core.payload.out;


public class VaccineListItem {

	private Long id;
	private String vaccineName;
	private Boolean isRabiesVaccination;
	private String manufacturer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVaccineName() {
		return vaccineName;
	}

	public void setVaccineName(String vaccineName) {
		this.vaccineName = vaccineName;
	}

	public Boolean getIsRabiesVaccination() {
		return isRabiesVaccination;
	}

	public void setIsRabiesVaccination(Boolean isRabiesVaccination) {
		this.isRabiesVaccination = isRabiesVaccination;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

}
