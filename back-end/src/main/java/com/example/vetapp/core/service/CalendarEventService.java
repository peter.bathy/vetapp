package com.example.vetapp.core.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.CalendarEvent;
import com.example.vetapp.core.payload.in.EventIntervalRequest;
import com.example.vetapp.core.payload.out.EventDetails;
import com.example.vetapp.core.repository.CalendarEventRepository;

@Service
@Transactional
public class CalendarEventService {
	
	private CalendarEventRepository calendarEventRepository;
	private AnimalService animalService;
	private NotificationService emailService;

	@Autowired
	public CalendarEventService(CalendarEventRepository calendarEventRepository, AnimalService animalService,
			NotificationService emailService) {
		this.calendarEventRepository = calendarEventRepository;
		this.animalService = animalService;
		this.emailService = emailService;
	}

	public List<EventDetails> getEventsByInterval(EventIntervalRequest request) {

		LocalDateTime beginning = request.getIntervalStart();
		LocalDateTime ending = request.getIntervalEnd();

		return calendarEventRepository.findAllByInterval(beginning, ending);
	}

	public EventDetails saveEvent(EventDetails event) {
		CalendarEvent newEvent = new CalendarEvent();
		newEvent.setAllDay(event.getAllDay());
		
		if (event.getAnimal() != null) {
			Animal animal = animalService.getAnimalById(event.getAnimal().getAnimalId());
			newEvent.setAnimal(animal);
		} else {
			newEvent.setAnimal(null);
		}
		newEvent.setEventTypes(event.getEventTypes());
		newEvent.setStart(event.getStart());
		newEvent.setEnd(event.getEnd());
		newEvent.setTitle(event.getTitle());
		if (event.getId() != null) {
			newEvent.setId(event.getId());
			emailService.deleteReminder(event.getId());
		}
		CalendarEvent savedEvent = calendarEventRepository.save(newEvent);
		return new EventDetails(savedEvent);
	}

	public Boolean deleteEvent(Long id) {

		if (calendarEventRepository.findById(id).isPresent()) {
			calendarEventRepository.deleteById(id);
			emailService.deleteReminder(id);
			return true;
		} else {
			return false;
		}
	}
}
