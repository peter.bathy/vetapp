package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.Animal;

public class AnimalItemWithContacts {

	private Long animalId;
	private String ownerName;
	private String species;
	private String breed;
	private String animalName;
	private String phone;
	private String mobile;
	private String email;

	public AnimalItemWithContacts() {
	}

	public AnimalItemWithContacts(Animal animal) {
		if (animal != null) {
			this.animalId = animal.getId();
			this.ownerName = animal.getOwner().getName();
			if (animal.getBreed() != null) {
				this.breed = animal.getBreed().getBreedName();
				this.species = animal.getBreed().getSpecies().getSpeciesName();
			} else {
				this.species = "";
				this.breed = "";
			}
			this.animalName = animal.getName();
			this.phone = animal.getOwner().getPhoneNumber();
			this.mobile = animal.getOwner().getMobileNumber();
			this.email = animal.getOwner().getEmail();
		}
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
