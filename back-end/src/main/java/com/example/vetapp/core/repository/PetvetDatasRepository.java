package com.example.vetapp.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.PetvetDatas;

@Repository
public interface PetvetDatasRepository extends JpaRepository<PetvetDatas, Long> {
	
	@Query("SELECT p FROM PetvetDatas p WHERE p.registrationDate = null  ORDER BY p.eventDate ")
	List<PetvetDatas> getAllUndoneChanges();
	
	@Query("SELECT p FROM PetvetDatas p WHERE p.registrationDate <> null  ORDER BY p.registrationDate DESC")
	Page<PetvetDatas> getOldChangesWithLimit(Pageable limitValue);

}
