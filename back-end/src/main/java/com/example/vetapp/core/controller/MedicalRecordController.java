package com.example.vetapp.core.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.vetapp.core.payload.in.EventIntervalRequest;
import com.example.vetapp.core.payload.in.MedicalRecordIdsWithAnimalId;
import com.example.vetapp.core.payload.in.SimpleMedicalRecordWithAnimalId;
import com.example.vetapp.core.payload.in.VaccineFromDate;
import com.example.vetapp.core.payload.out.AnimalListItem;
import com.example.vetapp.core.payload.out.ApiResponse;
import com.example.vetapp.core.payload.out.ExtendedAnimalListItemWithTreatmentTime;
import com.example.vetapp.core.payload.out.SimpleMedicalRecord;
import com.example.vetapp.core.payload.out.SimpleMedicalRecordWithAttachements;
import com.example.vetapp.core.payload.out.VaccinationDetail;
import com.example.vetapp.core.service.MedicalRecordService;
import com.example.vetapp.core.service.PdfService;

@RestController
@RequestMapping("/api/medicalrecord")
public class MedicalRecordController {

	private MedicalRecordService medicalRecordService;
	private PdfService pdfService;

	@Autowired
	public MedicalRecordController(MedicalRecordService medicalRecordService, PdfService pdfService) {
		this.medicalRecordService = medicalRecordService;
		this.pdfService = pdfService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getMedicalRecordByAnimalId(@PathVariable Long id) {

		List<SimpleMedicalRecordWithAttachements> records = medicalRecordService.getMedicalRecordsByAnimalId(id);

		return new ResponseEntity<>(records, HttpStatus.OK);
	}

	@PostMapping("/pdf")
	public ResponseEntity<?> generatePdf(@RequestBody MedicalRecordIdsWithAnimalId requestValues) {

		try {
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
					.body(pdfService.generatePatientHistory(requestValues.getAnimalId(),
							requestValues.getMedicalRecordIds(), requestValues.getAttacheImages(),
							requestValues.getAttachePdfs(), requestValues.getAnonymized()));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/lastrecords/{id}")
	public ResponseEntity<?> getLastThreeMedicalRecordsByAnimalId(@PathVariable Long id) {

		List<SimpleMedicalRecord> records = medicalRecordService.getLastThreeMedicalRecordsByAnimalId(id);

		if (records.size() > 0) {
			return new ResponseEntity<>(records, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ApiResponse(false, "Medical records are not found with given animal!"),
				HttpStatus.NOT_FOUND);
	}

	@PostMapping("/dayInterval")
	public ResponseEntity<?> getRecordByInterval(@RequestBody EventIntervalRequest requestinterval) {

		List<ExtendedAnimalListItemWithTreatmentTime> animals = medicalRecordService.getEventsByInterval(requestinterval);

		return new ResponseEntity<>(animals, HttpStatus.OK);

	}

	@PostMapping("/vaccination/outdated")
	public ResponseEntity<?> getOutdatedVaccinationsByMonth(@RequestBody VaccineFromDate fromDate) {

		List<AnimalListItem> animals = medicalRecordService.getOutdatedVaccinationsByMonth(fromDate.getFromDate());

		if (animals.size() > 0) {
			return new ResponseEntity<>(animals, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ApiResponse(false, "Not found animals in the given month"),
				HttpStatus.NOT_FOUND);
	}

	@PostMapping("/vaccination/new")
	public ResponseEntity<?> getNewVaccinationsFromDate(@RequestBody VaccineFromDate fromDate) {

		List<VaccinationDetail> animals = medicalRecordService.getNewVaccinationsFromDate(fromDate.getFromDate());

		if (animals.size() > 0) {
			return new ResponseEntity<>(animals, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ApiResponse(false, "Not found animals"), HttpStatus.NOT_FOUND);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveMedicalRecord(@RequestBody SimpleMedicalRecordWithAnimalId newRecord) {

		medicalRecordService.saveMedicalRecord(newRecord);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/saveAttachement")
	public ResponseEntity<?> uploadWithExtraParams(@RequestParam("file") MultipartFile[] files,
			@RequestParam String attachementType, Long medicalRecordId) {

		try {
			medicalRecordService.saveAttachement(files, attachementType, medicalRecordId);
		} catch (Exception e) {
			System.out.println("E");
			System.out.println(e);
			e.printStackTrace();
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/attachement/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = medicalRecordService.loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			System.out.print("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@GetMapping("/attachement/delete/{fileName}")
	public ResponseEntity<Resource> deleteFile(@PathVariable String fileName) {

		medicalRecordService.deleteFile(fileName);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/delete")
	public ResponseEntity<?> deleteVaccine(@RequestBody SimpleMedicalRecordWithAnimalId newRecord) {

		medicalRecordService.deleteMedicalRecord(newRecord);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/event-stats")
	public ResponseEntity<?> getStatsByInterval(@RequestBody EventIntervalRequest requestinterval) {

		return new ResponseEntity<>(medicalRecordService.getStatsInInterval(requestinterval), HttpStatus.OK);

	}
}
