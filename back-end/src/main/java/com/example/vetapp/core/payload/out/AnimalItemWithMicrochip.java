package com.example.vetapp.core.payload.out;

import java.time.LocalDate;

public class AnimalItemWithMicrochip {
 
    private String animalName;
    private String species;
    private String breed;
    private String numberOfMicrochip;
    private Long animalId;
    private LocalDate animalDateOfBirth;
    private String ownerName;
    
    public AnimalItemWithMicrochip() {
    }

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getNumberOfMicrochip() {
		return numberOfMicrochip;
	}

	public void setNumberOfMicrochip(String numberOfMicrochip) {
		this.numberOfMicrochip = numberOfMicrochip;
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public LocalDate getAnimalDateOfBirth() {
		return animalDateOfBirth;
	}

	public void setAnimalDateOfBirth(LocalDate animalDateOfBirth) {
		this.animalDateOfBirth = animalDateOfBirth;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
}
