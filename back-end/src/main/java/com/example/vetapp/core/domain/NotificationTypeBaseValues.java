package com.example.vetapp.core.domain;

public enum NotificationTypeBaseValues {
	EXAMINATIONDETAILS("examination", "Állatorvosi vizsgálat időpont információk", "info@erzsebetvet.hu"),
	OPERATIONDETAILS("operation", "Műtét időpont információk", "info@erzsebetvet.hu"),
	CONSULTATIONDETAILS("consultation", "Konzultációs időpont információk", "info@erzsebetvet.hu"),
	TREATMENTDETAILS("treatment", "Állatorvosi kezelés időpont információk", "info@erzsebetvet.hu"),
	EVENTREMINDER("eventReminder", "Állatorvosi időpont emlékeztető", "info@erzsebetvet.hu"),
	VACCINATIONSUBSC("vaccinationReminderSubscription", "Veszettségoltás emlékeztető", "info@erzsebetvet.hu"),
	VACCINATIONREMINDER("vaccinationReminder", "Hamarosan lejár kedvencének a veszettség oltása", "info@erzsebetvet.hu");

	private String name;
	private String subject;
	private String from;

	private NotificationTypeBaseValues(String name, String subject, String from) {
		this.name = name;
		this.subject = subject;
		this.from = from;
	}

	public String getName() {
		return name;
	}

	public String getSubject() {
		return subject;
	}

	public String getFrom() {
		return from;
	}
}
