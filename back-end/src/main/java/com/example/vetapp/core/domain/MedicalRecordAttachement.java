package com.example.vetapp.core.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class MedicalRecordAttachement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
    @Column(length = 10)
	private AttachementType attachementType;
	
	private String fileName;
	
	@ManyToOne
	private MedicalRecord medicalRecord;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AttachementType getAttachementType() {
		return attachementType;
	}

	public void setAttachementType(AttachementType attachementType) {
		this.attachementType = attachementType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MedicalRecord getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(MedicalRecord medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	@Override
	public String toString() {
		return "MedicalRecordAttachement [id=" + id + ", attachementType=" + attachementType + ", fileName=" + fileName
				+ ", medicalRecord=" + medicalRecord + "]";
	}
	
	
	
}