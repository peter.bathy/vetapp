package com.example.vetapp.core.domain;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
