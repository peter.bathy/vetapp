package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.payload.out.ColorListItem;
import com.example.vetapp.core.service.ColorService;

@RestController
@RequestMapping("/api/color")
public class ColorController {

	private ColorService colorService;

	@Autowired
	public ColorController(ColorService colorService) {
		this.colorService = colorService;
	}
	
	@GetMapping
    public ResponseEntity<List<ColorListItem>> getAllColor() {
        return new ResponseEntity<>(colorService.getAllColor(), HttpStatus.OK);
    }
}
