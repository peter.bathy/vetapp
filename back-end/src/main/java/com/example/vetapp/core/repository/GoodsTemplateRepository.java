package com.example.vetapp.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.GoodsTemplate;
import com.example.vetapp.core.payload.out.SimpleGoodsTemplateItem;

@Repository
public interface GoodsTemplateRepository extends JpaRepository<GoodsTemplate, Long> {

	@Query("SELECT new com.example.vetapp.core.payload.out.SimpleGoodsTemplateItem(g) FROM GoodsTemplate g")
	List<SimpleGoodsTemplateItem> findAllTemplates();
	
	@Query("SELECT g FROM GoodsTemplate g WHERE g.templateName LIKE %?1% ORDER BY g.templateName ASC ")
	Page<GoodsTemplate> searchTemplateLimit20(String searchWord, Pageable firstTwenty);
	
}
