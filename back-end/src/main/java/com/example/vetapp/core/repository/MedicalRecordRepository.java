package com.example.vetapp.core.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.MedicalRecord;
import com.example.vetapp.core.payload.out.SimpleMedicalRecordWithAttachements;
import com.example.vetapp.core.payload.out.StatsData;

@Repository
public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Long> {

	public MedicalRecord findFirstByOrderByIdDesc();

	public MedicalRecord findMedicalRecordByAnimalIdAndSessionDate(Long animalId, LocalDateTime sessionDate);

	@Query("SELECT new com.example.vetapp.core.payload.out.SimpleMedicalRecordWithAttachements(m) FROM MedicalRecord m WHERE m.animal = ?1 ORDER BY m.sessionDate DESC")
	public List<SimpleMedicalRecordWithAttachements> findAllByIdAnimalOrderBySessionDateDesc(Animal id);

	@Query("SELECT m FROM MedicalRecord m WHERE m.sessionDate > ?1 AND isVaccination = TRUE ORDER BY m.sessionDate ASC")
	public List<MedicalRecord> findAllWithSessionDateAfter(LocalDateTime fromDate);

	public List<MedicalRecord> findFirst3ByAnimalIdOrderBySessionDateDesc(Long id);

	@Query("SELECT m FROM MedicalRecord m WHERE m.sessionDate >= ?1 AND m.sessionDate <= ?2 ORDER BY m.sessionDate ASC")
	public List<MedicalRecord> findAllByInterval(LocalDateTime beginning, LocalDateTime ending);

	@Query("SELECT m FROM MedicalRecord m WHERE m.anamnesis LIKE CONCAT('%',?1 ,'%') OR m.symptoms LIKE CONCAT('%',?1 ,'%') OR m.therapy LIKE CONCAT('%',?1 ,'%')")
	public List<MedicalRecord> findBySearchWord(String searchWord);
	
	@Query("SELECT new com.example.vetapp.core.payload.out.StatsData(function('date_format', m.sessionDate, '%Y-%m-%d'), count(m.id)) FROM MedicalRecord AS m WHERE m.sessionDate >= ?1 AND m.sessionDate <= ?2 GROUP BY function('date_format', m.sessionDate, '%Y-%m-%d')")
		       public List<StatsData> getStatInInterval(LocalDateTime beginning, LocalDateTime ending);

	public List<MedicalRecord> findByIdInOrderBySessionDateDesc(List<Long> medicalRecordIds);
}
