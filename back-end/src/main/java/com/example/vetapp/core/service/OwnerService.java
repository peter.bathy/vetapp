package com.example.vetapp.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.payload.out.OwnerDetails;
import com.example.vetapp.core.payload.out.SimpleOwnerDetails;
import com.example.vetapp.core.repository.OwnerRepository;

@Service
@Transactional
public class OwnerService {

	private static final Logger logger = Logger.getLogger("OwnerService.class");

	private OwnerRepository ownerRepository;

	@Autowired
	public OwnerService(OwnerRepository ownerRepository) {
		this.ownerRepository = ownerRepository;
	}

	public Owner getOwnerById(long ownerId) {
		Optional<Owner> optOwner = ownerRepository.findById(ownerId);

		return (optOwner.isPresent() ? optOwner.get() : null);
	}

	public OwnerDetails saveOwner(OwnerDetails owner) {

		Owner currentOwner;

		if (owner.getOwnerId() == 0L) {
			currentOwner = new Owner();
			// currentOwner.setOldOwnerId(0L);
		} else {
			currentOwner = ownerRepository.getById(owner.getOwnerId());
		}
		currentOwner.setActive(true);
		currentOwner.setName(owner.getOwnerName());
		currentOwner.setZipCode(owner.getZipCode());
		currentOwner.setCity(owner.getCity());
		currentOwner.setAddress(owner.getAddress());
		currentOwner.setPhoneNumber(owner.getPhoneNumber());
		currentOwner.setMobileNumber(owner.getMobileNumber());
		currentOwner.setEmail(owner.getEmail());
		currentOwner.setEnableVaccinationReminder(owner.getEnableVaccinationReminder());
		currentOwner.setNote(owner.getOwnerNotification());

		OwnerDetails savedOwner = new OwnerDetails(ownerRepository.save(currentOwner));

		return savedOwner;
	}

	public List<Owner> findAll() {

		return ownerRepository.findAll();
	}

	public List<SimpleOwnerDetails> getAllOwners() {

		List<Owner> owners = findAll();

		List<SimpleOwnerDetails> ownerList = new ArrayList<SimpleOwnerDetails>();

		owners.forEach(owner -> {
			ownerList.add(new SimpleOwnerDetails(owner));
		});

		return ownerList;
	}

	public void savePlainOwnerDatas(Owner owner) {
		ownerRepository.save(owner);
	}
}
