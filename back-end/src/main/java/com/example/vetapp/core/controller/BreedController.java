package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.payload.in.BreedDatas;
import com.example.vetapp.core.payload.out.ApiResponse;
import com.example.vetapp.core.payload.out.SpeciesListItem;
import com.example.vetapp.core.service.BreedService;

@RestController
@RequestMapping("/api/breed")
public class BreedController {

	private BreedService breedService;

	@Autowired
	public BreedController(BreedService breedService) {
		this.breedService = breedService;
	}

	@GetMapping
	public ResponseEntity<List<SpeciesListItem>> getAllBreedAndSpecies() {
		return new ResponseEntity<>(breedService.getAllBreedAndSpecies(), HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveBreedAndSpecies(@RequestBody BreedDatas datas) {

		try {
			breedService.saveSpeciesAndBreed(datas);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ApiResponse(false, e.toString()), HttpStatus.BAD_REQUEST);
		}
	}
}
