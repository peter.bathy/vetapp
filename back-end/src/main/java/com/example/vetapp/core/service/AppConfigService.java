package com.example.vetapp.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.AppConfig;
import com.example.vetapp.core.repository.AppConfigRepository;

@Service
@Transactional
public class AppConfigService {

	private AppConfigRepository appConfigRepository;
	private OwnerService ownerService;
	private AnimalService animalService;
	private MedicalRecordService medicalRecordService;

	@Autowired
	public AppConfigService(AppConfigRepository appConfigRepository, OwnerService ownerService,
			AnimalService animalService, MedicalRecordService medicalRecordService) {
		super();
		this.appConfigRepository = appConfigRepository;
		this.ownerService = ownerService;
		this.animalService = animalService;
		this.medicalRecordService = medicalRecordService;
	}

	public List<AppConfig> getConfigValues() {
		return appConfigRepository.findAll();

	}

	public void saveConfigValues(List<AppConfig> list) {
		appConfigRepository.saveAll(list);
	}

}
