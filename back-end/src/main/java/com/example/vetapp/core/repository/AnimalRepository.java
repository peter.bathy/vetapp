package com.example.vetapp.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.payload.out.AnimalListItem;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {
	@Query("SELECT new com.example.vetapp.core.payload.out.AnimalListItem(a) FROM Animal a WHERE a.active = TRUE")
	List<AnimalListItem> findAllAnimals();

	@Query("SELECT a FROM Animal a, Owner o WHERE a.active = TRUE AND a.dateOfDeath = NULL AND o.id = a.owner AND CONCAT( a.idOfMicrochip , ' ', a.id, ' ', a.name, ' ', o.name, ' ', o.zipCode, ' ', o.city, ' ', o.address  )  LIKE %?1% ORDER BY a.id ")
	Page<Animal> searchAnimalOwnerDetailsLimit20(String searchWord, Pageable pageable);

	@Query("SELECT a FROM Animal a WHERE a.active = TRUE AND CONCAT( a.idOfMicrochip , ' ', a.id, ' ', a.name )  LIKE %?1% ORDER BY a.id ")
	Page<Animal> searchAnimalDetailsLimit20(String searchWord, Pageable firstTwenty);

	@Query("SELECT a FROM Animal a, Owner o WHERE a.active = TRUE AND a.dateOfDeath = NULL AND o.id = a.owner AND o.name LIKE %?1% AND CONCAT(o.zipCode,' ',o.city,' ',o.address) like %?2% AND o.email like %?3% AND a.name like %?4% AND a.idOfVaccinationBook like %?5% AND a.passportId LIKE %?6% ORDER BY a.owner ")
	List<Animal> searchExtendedDetails(String ownerName, String address, String email, String animalName, String idOfVaccinationBook, String passportId);

	
	//CONCAT( a.idOfMicrochip , ' ', a.id, ' ', a.name, ' ', o.name, ' ', o.zipCode, ' ', o.city, ' ', o.address  )  LIKE %?1%
	// String breed;
	// String species;

}
