package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.CalendarEvent;

import java.time.LocalDateTime;
import java.util.List;

public class EventDetails {

	private Long id;

	private String title;

	private Boolean allDay;

	private LocalDateTime start;

	private LocalDateTime end;

	private List<String> eventTypes;

	private AnimalItemWithContacts animal;

	public EventDetails() {
	}

	public EventDetails(CalendarEvent event) {
		this.id = event.getId();
		this.title = event.getTitle();
		this.allDay = event.getAllDay();
		this.start = event.getStart();
		this.end = event.getEnd();
		this.eventTypes = event.getEventTypes();
		if (event.getAnimal() != null) {
			this.animal = new AnimalItemWithContacts(event.getAnimal());
		} else {
			this.animal = null;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getAllDay() {
		return allDay;
	}

	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	public List<String> getEventTypes() {
		return eventTypes;
	}

	public void setEventTypes(List<String> eventTypes) {
		this.eventTypes = eventTypes;
	}

	public AnimalItemWithContacts getAnimal() {
		return animal;
	}

	public void setAnimal(AnimalItemWithContacts animal) {
		this.animal = animal;
	}
}
