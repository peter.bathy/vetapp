package com.example.vetapp.core.payload.in;

import java.time.LocalDateTime;

public class EventIntervalRequest {

	private LocalDateTime intervalStart;
	private LocalDateTime intervalEnd;

	public LocalDateTime getIntervalStart() {
		return intervalStart;
	}

	public void setIntervalStart(LocalDateTime intervalStart) {
		this.intervalStart = intervalStart;
	}

	public LocalDateTime getIntervalEnd() {
		return intervalEnd;
	}

	public void setIntervalEnd(LocalDateTime intervalEnd) {
		this.intervalEnd = intervalEnd;
	}
}
