package com.example.vetapp.core.payload.in;

import com.example.vetapp.core.domain.Animal;

public class SimpleAnimalIdentifier {

    private Long id;
    private String name;

    public SimpleAnimalIdentifier() {
	}

	public SimpleAnimalIdentifier(Animal animal) {
        this.id = animal.getId();
        this.name = animal.getName();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    
}
