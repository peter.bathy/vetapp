package com.example.vetapp.core.payload.in;

import java.util.List;

public class MedicalRecordIdsWithAnimalId {

	private Long animalId;

	private List<Long> medicalRecordIds;

	private Boolean attacheImages;

	private Boolean attachePdfs;
	
	private Boolean anonymized;

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public List<Long> getMedicalRecordIds() {
		return medicalRecordIds;
	}

	public void setMedicalRecordIds(List<Long> medicalRecordIds) {
		this.medicalRecordIds = medicalRecordIds;
	}

	public Boolean getAttacheImages() {
		return attacheImages;
	}

	public void setAttacheImages(Boolean attacheImages) {
		this.attacheImages = attacheImages;
	}

	public Boolean getAttachePdfs() {
		return attachePdfs;
	}

	public void setAttachePdfs(Boolean attachePdfs) {
		this.attachePdfs = attachePdfs;
	}

	public Boolean getAnonymized() {
		return anonymized;
	}

	public void setAnonymized(Boolean anonymized) {
		this.anonymized = anonymized;
	}
	
}
