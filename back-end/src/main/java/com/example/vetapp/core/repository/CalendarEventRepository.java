package com.example.vetapp.core.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.CalendarEvent;
import com.example.vetapp.core.payload.out.EventDetails;

@Repository
public interface CalendarEventRepository extends JpaRepository<CalendarEvent, Long> {

	@Query("SELECT new com.example.vetapp.core.payload.out.EventDetails(e) FROM CalendarEvent e WHERE e.end >= ?1 AND e.start <= ?2")
	public List<EventDetails> findAllByInterval(LocalDateTime beginning, LocalDateTime ending);

}
