package com.example.vetapp.core.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class PetvetDatas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private ChangeEventType changeType;

	private Long animalId;
	private String animalName;
	private String idOfMicrochip;
	private Long ownerId;
	private String ownerName;
	private LocalDate eventDate;
	private LocalDate registrationDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ChangeEventType getChangeType() {
		return changeType;
	}

	public void setChangeType(ChangeEventType changeType) {
		this.changeType = changeType;
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getIdOfMicrochip() {
		return idOfMicrochip;
	}

	public void setIdOfMicrochip(String idOfMicrochip) {
		this.idOfMicrochip = idOfMicrochip;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public LocalDate getEventDate() {
		return eventDate;
	}

	public void setEventDate(LocalDate eventDate) {
		this.eventDate = eventDate;
	}

	public LocalDate getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDate registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

}
