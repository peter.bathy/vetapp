package com.example.vetapp.core.payload;

import java.time.LocalDateTime;
import java.util.List;

import com.example.vetapp.core.domain.MedicalRecord;

public class MedicalRecordWithAttachedImages{

	private Long id;
	private LocalDateTime sessionDate;
	private String vaccinationName;
	private String anamnesis;
	private String symptoms;
	private String therapy;
	private String otherDetails;
	private List<String> imageStrings;

	public MedicalRecordWithAttachedImages() {
	}

	public MedicalRecordWithAttachedImages(MedicalRecord medicalRecord) {
		this.id = medicalRecord.getId();
		this.sessionDate = medicalRecord.getSessionDate();
		this.vaccinationName = medicalRecord.getVaccinationName();
		this.anamnesis = medicalRecord.getAnamnesis();
		this.symptoms = medicalRecord.getSymptoms();
		this.therapy = medicalRecord.getTherapy();
		this.otherDetails = medicalRecord.getOtherDetails();
		this.imageStrings = null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getSessionDate() {
		return sessionDate;
	}

	public void setSessionDate(LocalDateTime sessionDate) {
		this.sessionDate = sessionDate;
	}

	public String getVaccinationName() {
		return vaccinationName;
	}

	public void setVaccinationName(String vaccinationName) {
		this.vaccinationName = vaccinationName;
	}

	public String getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getTherapy() {
		return therapy;
	}

	public void setTherapy(String therapy) {
		this.therapy = therapy;
	}

	public List<String> getImageStrings() {
		return imageStrings;
	}

	public void setImageStrings(List<String> imageStrings) {
		this.imageStrings = imageStrings;
	}

	public String getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}
}
