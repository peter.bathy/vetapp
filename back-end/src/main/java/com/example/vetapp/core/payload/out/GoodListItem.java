package com.example.vetapp.core.payload.out;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.domain.GoodsTemplate;

public class GoodListItem {

	private Long id;
	private Boolean active;
	private String name;
	private String manufacturer;
	private Double packPrice;
	private Double numberPerPack;
	private Double nettoPrice;
	private Float tax;
	private Float margin;
	private Double bruttoPrice;
	private Double sellPrice;
	private String quantityUnit;
	private Integer baseQuantity;
	private String comment;
	private List<String> keyWords;
	private Set<SimpleGoodsTemplateItem> templates;

	public GoodListItem() {
	}

	public GoodListItem(Goods good) {
		this.id = good.getId();
		this.active = good.getActive();
		this.name = good.getName();
		this.manufacturer = good.getManufacturer();
		this.packPrice = good.getPackPrice();
		this.numberPerPack = good.getNumberPerPack();
		this.nettoPrice = good.getNettoPrice();
		this.tax = good.getTax();
		this.margin = good.getMargin();
		this.bruttoPrice = good.getBruttoPrice();
		this.sellPrice = good.getSellPrice();
		this.quantityUnit = good.getQuantityUnit();
		this.baseQuantity = good.getBaseQuantity();
		this.comment = good.getComment();
		this.keyWords = good.getKeyWords();
		Set<SimpleGoodsTemplateItem> templates = new HashSet<>();
		for (GoodsTemplate currentTemplate : good.getTemplates()) {
			templates.add(new SimpleGoodsTemplateItem(currentTemplate));
		}
		this.templates = templates;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Double getPackPrice() {
		return packPrice;
	}

	public void setPackPrice(Double packPrice) {
		this.packPrice = packPrice;
	}

	public Double getNumberPerPack() {
		return numberPerPack;
	}

	public void setNumberPerPack(Double numberPerPack) {
		this.numberPerPack = numberPerPack;
	}

	public Double getNettoPrice() {
		return nettoPrice;
	}

	public void setNettoPrice(Double nettoPrice) {
		this.nettoPrice = nettoPrice;
	}

	public Float getTax() {
		return tax;
	}

	public void setTax(Float tax) {
		this.tax = tax;
	}

	public Float getMargin() {
		return margin;
	}

	public void setMargin(Float margin) {
		this.margin = margin;
	}

	public Double getBruttoPrice() {
		return bruttoPrice;
	}

	public void setBruttoPrice(Double bruttoPrice) {
		this.bruttoPrice = bruttoPrice;
	}

	public Double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getQuantityUnit() {
		return quantityUnit;
	}

	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public Integer getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(Integer baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<String> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(List<String> keyWords) {
		this.keyWords = keyWords;
	}

	public Set<SimpleGoodsTemplateItem> getTemplates() {
		return templates;
	}

	public void setTemplates(Set<SimpleGoodsTemplateItem> templates) {
		this.templates = templates;
	}

}
