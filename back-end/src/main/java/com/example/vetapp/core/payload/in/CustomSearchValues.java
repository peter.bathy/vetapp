package com.example.vetapp.core.payload.in;

public class CustomSearchValues {
	private String ownerName;
	private String address;
	private String email;
	private String animalName;
	private String breed;
	private String species;
	private String color;
	private String idOfVaccinationBook;
	private String passportId;

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIdOfVaccinationBook() {
		return idOfVaccinationBook;
	}

	public void setIdOfVaccinationBook(String idOfVaccinationBook) {
		this.idOfVaccinationBook = idOfVaccinationBook;
	}

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	@Override
	public String toString() {
		return "CustomSearchValues [ownerName=" + ownerName + ", address=" + address + ", email=" + email
				+ ", animalName=" + animalName + ", breed=" + breed + ", species=" + species + ", color=" + color
				+ ", idOfVaccinationBook=" + idOfVaccinationBook + ", passportId=" + passportId + "]";
	}
	
	

}
