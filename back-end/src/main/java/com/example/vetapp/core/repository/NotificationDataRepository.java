package com.example.vetapp.core.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.NotificationData;

@Repository
public interface NotificationDataRepository extends JpaRepository<NotificationData, Long> {

	@Query("SELECT a FROM NotificationData a WHERE a.animalId = ?1 AND a.letterSent = true ORDER BY sendingTime DESC")
	public List<NotificationData> findAllByAnimalIdOrderBySendingTimeDesc(Long id);

	@Query("SELECT a FROM NotificationData a WHERE a.sendingTime < CURDATE() AND a.letterSent = false ORDER BY sendingTime ASC")
	Page<NotificationData> findTodaySendablesFirtstSix(Pageable pageable);

	List<NotificationData> findByEventId(Long eventId);

	@Query("SELECT a FROM NotificationData a WHERE a.sendingTime > ?1 AND a.sendingTime < ?2 AND a.emailBaseDatas = com.example.vetapp.core.domain.NotificationTypeBaseValues.VACCINATIONREMINDER")
	List<NotificationData> findIfSchedulingIsDoneForThisWeek(LocalDateTime lastSaturDay, LocalDateTime nextMonday);

}
