package com.example.vetapp.core.payload.in;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class VaccineFromDate {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime fromDate;
	
	public VaccineFromDate() {
	}

	public VaccineFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}
}
