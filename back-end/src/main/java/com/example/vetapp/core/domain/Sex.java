package com.example.vetapp.core.domain;

public enum Sex {
    FEMALE("nőstény"),
    MALE("hím"),
	UNKNOWN("ismeretlen");

    private String sexName;

    Sex(String sexName) {
        this.sexName = sexName;
    }
    
    public String getSexName() {
		return sexName;
	}

	public void setSexName(String sexName) {
		this.sexName = sexName;
	}

}
