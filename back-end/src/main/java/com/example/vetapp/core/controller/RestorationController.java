package com.example.vetapp.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.service.RestorationService;

@RestController
@RequestMapping("/api/restoration")
public class RestorationController {

	private RestorationService restorationService;

	@Autowired
	public RestorationController(RestorationService restorationService) {
		this.restorationService = restorationService;
	}

	@GetMapping("/restore-database")
	public ResponseEntity<?> restoreDatabase() {

		restorationService.restoreDatabase();

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/restore-attachements")
	public ResponseEntity<?> restoreAttachements() {

		restorationService.restoreAttachements();

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
