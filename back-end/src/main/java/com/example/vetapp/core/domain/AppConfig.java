package com.example.vetapp.core.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AppConfig {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String settingName;
	private String settingValue;
	private Boolean isPassword;
	
	public AppConfig() {
	}

	public AppConfig(Long id, String settingName, String settingValue, Boolean isPassword) {
		this.id = id;
		this.settingName = settingName;
		this.settingValue = settingValue;
		this.isPassword = isPassword;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSettingName() {
		return settingName;
	}

	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

	public Boolean getIsPassword() {
		return isPassword;
	}

	public void setIsPassword(Boolean isPassword) {
		this.isPassword = isPassword;
	}

}
