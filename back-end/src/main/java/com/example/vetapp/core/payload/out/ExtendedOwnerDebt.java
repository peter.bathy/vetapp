package com.example.vetapp.core.payload.out;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.*;

import com.example.vetapp.core.domain.OwnerDebt;

public class ExtendedOwnerDebt {

	private Long id;
	private Long ownerId;
	private String transactionDate;
	private Integer debtValue;
	private String debtType;
	
	public ExtendedOwnerDebt() {
	}

	public ExtendedOwnerDebt(OwnerDebt ownerDebt, String type) {
		this.id = ownerDebt.getId();
		this.ownerId = ownerDebt.getOwnerId();
		this.transactionDate = ownerDebt.getTransactionDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		this.debtValue = ownerDebt.getDebtValue();
		this.debtType = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getDebtValue() {
		return debtValue;
	}

	public void setDebtValue(Integer debtValue) {
		this.debtValue = debtValue;
	}

	public String getDebtType() {
		return debtType;
	}

	public void setDebtType(String debtType) {
		this.debtType = debtType;
	}

}
