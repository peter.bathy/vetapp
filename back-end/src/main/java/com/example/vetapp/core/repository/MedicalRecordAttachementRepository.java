package com.example.vetapp.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.MedicalRecordAttachement;

@Repository
public interface MedicalRecordAttachementRepository extends JpaRepository<MedicalRecordAttachement, Long> {

}
