package com.example.vetapp.core.domain;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class OwnerDebt {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long ownerId;

	private LocalDate transactionDate;

	private Integer debtValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public LocalDate getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDate transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getDebtValue() {
		return debtValue;
	}

	public void setDebtValue(Integer debtValue) {
		this.debtValue = debtValue;
	}

}
