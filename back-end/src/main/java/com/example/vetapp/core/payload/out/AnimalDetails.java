package com.example.vetapp.core.payload.out;

import java.time.format.DateTimeFormatter;

import com.example.vetapp.core.domain.Animal;

public class AnimalDetails {
	private Long animalId;
	private String sex;
	private Long ownerId;
	private String comment;
	private String species;
	private String breed;
	private String idOfMicrochip;
	private String idOfVaccinationBook;
	private String passportId;
	private String pedigree;
	private String animalName;
	private Boolean isSpayed;
	private String birthDate;
	private String dateOfSpaying;
	private String dateOfDeath;
	private Boolean birthDateIsEstimated;
	private String fur;
	private String color;
	private String pattern;

	public AnimalDetails() {
	}

	public AnimalDetails(Animal animal) {
		this.animalId = animal.getId();
		this.sex = animal.getSex().getSexName();
		this.isSpayed = animal.getIsSpayed();
		this.ownerId = animal.getOwner().getId();
		if (animal.getComment() != null) {
			this.comment = animal.getComment();
		}
		if (animal.getBreed() != null) {
			this.breed = animal.getBreed().getBreedName();
			this.species = animal.getBreed().getSpecies().getSpeciesName();
		} else {
			this.species = "";
			this.breed = "";
		}
		if (animal.getIdOfMicrochip() != null) {
			this.idOfMicrochip = animal.getIdOfMicrochip();
		} else {
			this.idOfMicrochip = "";
		}
		if (animal.getIdOfVaccinationBook() != null) {
			this.idOfVaccinationBook = animal.getIdOfVaccinationBook();
		} else {
			this.idOfVaccinationBook = "";
		}
		if (animal.getPassportId() != null) {
			this.passportId = animal.getPassportId();
		} else {
			this.passportId = "";
		}
		if (animal.getPedigree() != null) {
			this.pedigree = animal.getPedigree();
		} else {
			this.pedigree = "";
		}
		if (animal.getComment() != null) {
			this.comment = animal.getComment();
		} else {
			this.comment = "";
		}
		this.animalName = animal.getName();
		if (animal.getBirthDate() != null) {
			this.birthDate = animal.getBirthDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		if (animal.getDateOfDeath() != null) {
			this.dateOfDeath = animal.getDateOfDeath().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		if (animal.getDateOfSpaying() != null) {
			this.dateOfSpaying = animal.getDateOfSpaying().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		if (animal.getBirthDateIsEstimated() == null) {
			this.birthDateIsEstimated = false;
		} else {
			this.birthDateIsEstimated = animal.getBirthDateIsEstimated();
		}
		if (animal.getFur() != null) {
			this.fur = animal.getFur();
		} else {
			this.fur = "";
		}
		if (animal.getColor() != null) {
			this.color = animal.getColor().getColorName().toString();
		} else {
			this.color = "";
		}
		if (animal.getPattern() != null) {
			this.pattern = animal.getPattern();
		} else {
			this.pattern = "";
		}
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getIdOfMicrochip() {
		return idOfMicrochip;
	}

	public void setIdOfMicrochip(String idOfMicrochip) {
		this.idOfMicrochip = idOfMicrochip;
	}

	public String getIdOfVaccinationBook() {
		return idOfVaccinationBook;
	}

	public void setIdOfVaccinationBook(String idOfVaccinationBook) {
		this.idOfVaccinationBook = idOfVaccinationBook;
	}

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	public String getPedigree() {
		return pedigree;
	}

	public void setPedigree(String pedigree) {
		this.pedigree = pedigree;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getDateOfSpaying() {
		return dateOfSpaying;
	}

	public void setDateOfSpaying(String dateOfSpaying) {
		this.dateOfSpaying = dateOfSpaying;
	}

	public Boolean getIsSpayed() {
		return isSpayed;
	}

	public void setIsSpayed(Boolean isSpayed) {
		this.isSpayed = isSpayed;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public Boolean getBirthDateIsEstimated() {
		return birthDateIsEstimated;
	}

	public void setBirthDateIsEstimated(Boolean birthDateIsEstimated) {
		this.birthDateIsEstimated = birthDateIsEstimated;
	}

	public String getFur() {
		return fur;
	}

	public void setFur(String fur) {
		this.fur = fur;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
