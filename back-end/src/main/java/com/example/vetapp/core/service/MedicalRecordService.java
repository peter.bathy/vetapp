package com.example.vetapp.core.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.AttachementType;
import com.example.vetapp.core.domain.MedicalRecord;
import com.example.vetapp.core.domain.MedicalRecordAttachement;
import com.example.vetapp.core.payload.in.EventIntervalRequest;
import com.example.vetapp.core.payload.in.SimpleMedicalRecordWithAnimalId;
import com.example.vetapp.core.payload.out.AnimalListItem;
import com.example.vetapp.core.payload.out.AnimalListItemWithEmail;
import com.example.vetapp.core.payload.out.ExtendedAnimalListItemWithTreatmentTime;
import com.example.vetapp.core.payload.out.SimpleMedicalRecord;
import com.example.vetapp.core.payload.out.SimpleMedicalRecordWithAttachements;
import com.example.vetapp.core.payload.out.StatsData;
import com.example.vetapp.core.payload.out.VaccinationDetail;
import com.example.vetapp.core.repository.MedicalRecordAttachementRepository;
import com.example.vetapp.core.repository.MedicalRecordRepository;

@Service
@Transactional
public class MedicalRecordService {

	private static final Logger logger = Logger.getLogger("MedicalRecordService.class");

	private MedicalRecordRepository medicalRecordRepository;
	private MedicalRecordAttachementRepository attachementRepository;
	private AnimalService animalService;

	@Value("${data.dataPath}")
	private String dataPath;

	@Autowired
	@Lazy
	public MedicalRecordService(MedicalRecordRepository medicalRecordRepository, AnimalService animalService,
			MedicalRecordAttachementRepository attachementRepository) {
		this.medicalRecordRepository = medicalRecordRepository;
		this.animalService = animalService;
		this.attachementRepository = attachementRepository;
	}

	public MedicalRecordService() {

	}

	LocalDateTime convertHourMinute(LocalDateTime baseDate, BigDecimal hourMinuteRaw) {

		Long hour = hourMinuteRaw.round(new MathContext(3, RoundingMode.DOWN)).longValue();
		Long minute = Math.round((hourMinuteRaw.remainder(BigDecimal.ONE)).doubleValue() * 100);

		return baseDate.plusHours(hour).plusMinutes(minute);
	}

	public List<SimpleMedicalRecordWithAttachements> getMedicalRecordsByAnimalId(Long id) {

		return medicalRecordRepository.findAllByIdAnimalOrderBySessionDateDesc(animalService.getAnimalById(id));
	}

	public List<SimpleMedicalRecord> getLastThreeMedicalRecordsByAnimalId(Long id) {

		List<MedicalRecord> lastRecords = medicalRecordRepository.findFirst3ByAnimalIdOrderBySessionDateDesc(id);
		List<SimpleMedicalRecord> simpleRecords = new ArrayList<SimpleMedicalRecord>();

		for (MedicalRecord medicalRecord : lastRecords) {
			simpleRecords.add(new SimpleMedicalRecord(medicalRecord));
		}

		return simpleRecords;
	}

	public List<ExtendedAnimalListItemWithTreatmentTime> getEventsByInterval(EventIntervalRequest request) {

		LocalDateTime beginning = request.getIntervalStart();
		LocalDateTime ending = request.getIntervalEnd();

		List<MedicalRecord> treatmentsInInterval = medicalRecordRepository.findAllByInterval(beginning, ending);

		List<ExtendedAnimalListItemWithTreatmentTime> animalsInDay = new ArrayList<>();

		for (MedicalRecord medicalRecord : treatmentsInInterval) {
			animalsInDay.add(new ExtendedAnimalListItemWithTreatmentTime(medicalRecord));
		}

		return animalsInDay;
	}

	public List<AnimalListItem> getOutdatedVaccinationsByMonth(LocalDateTime fromDate) {

		LocalDateTime toDate = fromDate.plusMonths(1);

		List<MedicalRecord> outdatedVaccinations = medicalRecordRepository.findAllWithSessionDateAfter(fromDate);

		List<Animal> animals = new ArrayList<>();

		for (MedicalRecord medicalRecord : outdatedVaccinations) {
			if (medicalRecord.getSessionDate().isBefore(toDate) && medicalRecord.getAnimal().getBreed() != null
					&& medicalRecord.getAnimal().getBreed().getSpecies() != null
					&& medicalRecord.getAnimal().getBreed().getSpecies().getSpeciesName().equals("eb")
					&& medicalRecord.getAnimal().getDateOfDeath() == null) {
				animals.add(medicalRecord.getAnimal());
			} else {
				if (animals.contains(medicalRecord.getAnimal())) {
					animals.remove(medicalRecord.getAnimal());
				}
			}
		}

		List<AnimalListItem> animalsInMonth = new ArrayList<>();

		for (Animal animal : animals) {
			animalsInMonth.add(new AnimalListItem(animal));
		}

		return animalsInMonth;
	}

	public List<VaccinationDetail> getNewVaccinationsFromDate(LocalDateTime fromDate) {

		List<MedicalRecord> newVaccinations = medicalRecordRepository.findAllWithSessionDateAfter(fromDate);

		List<VaccinationDetail> result = new ArrayList<>();

		for (MedicalRecord record : newVaccinations) {
			if (record.getAnimal().getIdOfMicrochip() != null && !record.getAnimal().getIdOfMicrochip().isBlank()) {
				result.add(new VaccinationDetail(record));
			}
		}

		return result;
	}

	public void saveMedicalRecord(SimpleMedicalRecordWithAnimalId newRecord) {
		MedicalRecord currentRecord;

		if (newRecord.getId() != null) {
			currentRecord = medicalRecordRepository.getById(newRecord.getId());
		} else {
			currentRecord = new MedicalRecord();
			currentRecord.setAnimal(animalService.getAnimalById(newRecord.getAnimalId()));
		}

		currentRecord.setSessionDate(newRecord.getDateOfTreatment());
		currentRecord.setVaccinationName(newRecord.getVaccination());
		currentRecord.setIsVaccination(newRecord.getIsRabies());
		currentRecord.setAnamnesis(newRecord.getAnamnesis());
		currentRecord.setSymptoms(newRecord.getSymptoms());
		currentRecord.setTherapy(newRecord.getTherapy());
		currentRecord.setOtherDetails(newRecord.getOtherDetails());

		medicalRecordRepository.save(currentRecord);
	}

	public void deleteMedicalRecord(SimpleMedicalRecordWithAnimalId newRecord) {
		medicalRecordRepository.deleteById(newRecord.getId());
	}

	private Path checkAndCreateFolder() {

		Path path = Paths.get(System.getProperty("user.home") + dataPath + "/attachements/");
		try {
			Files.createDirectories(path);
			return path;
		} catch (IOException e) {
			logger.warning("Could not initialize folder for upload!");
			return null;
		}
	}

	public Boolean saveAttachement(MultipartFile[] files, String attachementType, Long medicalRecordId) {

		Optional<MedicalRecord> optmedicalRecord = medicalRecordRepository.findById(medicalRecordId);

		if (optmedicalRecord.isPresent()) {

			MedicalRecord currentMedicalRecord = optmedicalRecord.get();
			Long animalId = currentMedicalRecord.getAnimal().getId();
			List<MedicalRecordAttachement> attachements = new ArrayList<MedicalRecordAttachement>();

			attachements = currentMedicalRecord.getAttachements();

			int startingLength = attachements.size();

			int index = 1;

			Path fileBasePath = checkAndCreateFolder();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmss");

			for (MultipartFile file : files) {
				String fileType = getFileExtension(file.getOriginalFilename()).toLowerCase();

				if (fileType.equals("jpeg")) {
					fileType = "jpg";
				}

				try {
					MedicalRecordAttachement newAttachement = new MedicalRecordAttachement();

					String fileName = animalId + "_" + attachementType + "_"
							+ currentMedicalRecord.getSessionDate().format(formatter) + "_"
							+ LocalDateTime.now().format(formatter) + "_" + index + "." + fileType;
					index++;

					Path path = fileBasePath.resolve(fileName);

					if (fileType.matches("jpg|gif|png")) {
						BufferedImage i = ImageIO.read(file.getInputStream());
						if (i.getWidth() > 2000 || i.getHeight() > 2000)
							i = Scalr.resize(i, Scalr.Method.QUALITY, 2000);
						ImageIO.write(i, fileType.toUpperCase(), new File(path.toString()));
					} else {
						Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
					}
					switch (attachementType) {
					case "ultrasound":
						newAttachement.setAttachementType(AttachementType.ULTRASOUND);
						break;
					case "xray":
						newAttachement.setAttachementType(AttachementType.XRAY);
						break;
					case "photo":
						newAttachement.setAttachementType(AttachementType.PHOTO);
						break;
					case "lab":
						newAttachement.setAttachementType(AttachementType.LAB);
						break;
					default:
						newAttachement.setAttachementType(AttachementType.OTHER);
					}
					newAttachement.setFileName(fileName);
					newAttachement.setMedicalRecord(currentMedicalRecord);
					attachements.add(newAttachement);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (startingLength < attachements.size()) {
				currentMedicalRecord.setAttachements(attachements);
				medicalRecordRepository.save(currentMedicalRecord);
			}

			return true;
		}
		return false;
	}

	public Resource loadFileAsResource(String fileName) {
		try {
			Path fileBasePath = checkAndCreateFolder();
			Path filePath = fileBasePath.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				System.out.println("hiba történt");
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private String getFileExtension(String file) {
		String fileName = file.toString();

		int index = fileName.lastIndexOf('.');
		if (index > 0) {
			return fileName.substring(index + 1);
		}
		return null;
	}

	public void deleteFile(String fileName) {

		String[] fileInfos = fileName.split("_");
		Long animalId = Long.valueOf(fileInfos[0]);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmss");
		LocalDateTime sessionDate = LocalDateTime.parse(fileInfos[2], formatter);

		MedicalRecord record = medicalRecordRepository.findMedicalRecordByAnimalIdAndSessionDate(animalId, sessionDate);

		if (record != null) {

			Optional<MedicalRecordAttachement> matchingAttachement = record.getAttachements().stream()
					.filter(a -> a.getFileName().equals(fileName)).findFirst();

			if (matchingAttachement.isPresent()) {
				MedicalRecordAttachement attachement = matchingAttachement.get();

				attachementRepository.deleteById(attachement.getId());
				record.getAttachements().remove(attachement);
				medicalRecordRepository.save(record);

				Path fileBasePath = checkAndCreateFolder();
				Path filePath = fileBasePath.resolve(fileName).normalize();

				try {
					Files.deleteIfExists(filePath);
				} catch (NoSuchFileException e) {
					System.out.println("No such file/directory exists");
				} catch (DirectoryNotEmptyException e) {
					System.out.println("Directory is not empty.");
				} catch (IOException e) {
					System.out.println("Invalid permissions.");
				}
				System.out.println("Deletion successful.");
			} else {
				System.out.println("No such file/directory exists");
			}
		}
	}

	public List<AnimalListItemWithEmail> getExpiringVaccinationsByTwoWeeksBefore(LocalDateTime lastSaturDay,
			LocalDateTime nextMonday) {
		LocalDateTime fromDate = lastSaturDay.minusYears(1).plusWeeks(2);
		LocalDateTime toDate = nextMonday.minusYears(1).plusWeeks(2);

		List<MedicalRecord> expiringVaccinations = medicalRecordRepository.findAllWithSessionDateAfter(fromDate);

		List<Animal> animals = new ArrayList<>();

		for (MedicalRecord medicalRecord : expiringVaccinations) {
			if (medicalRecord.getSessionDate().isBefore(toDate) && medicalRecord.getAnimal().getBreed() != null
					&& medicalRecord.getAnimal().getBreed().getSpecies() != null
					&& medicalRecord.getAnimal().getBreed().getSpecies().getSpeciesName().equals("eb")
					&& medicalRecord.getAnimal().getDateOfDeath() == null
					&& medicalRecord.getAnimal().getOwner().getEnableVaccinationReminder() != null
					&& medicalRecord.getAnimal().getOwner().getEnableVaccinationReminder() == true
					&& medicalRecord.getAnimal().getOwner().getEmail() != null
					&& !medicalRecord.getAnimal().getOwner().getEmail().isBlank()) {
				animals.add(medicalRecord.getAnimal());
			} else {
				if (animals.contains(medicalRecord.getAnimal())) {
					animals.remove(medicalRecord.getAnimal());
				}
			}
		}

		List<AnimalListItemWithEmail> animalsInDay = new ArrayList<>();

		for (Animal animal : animals) {
			animalsInDay.add(new AnimalListItemWithEmail(animal));
		}

		System.out.println("Oltási emlékeztetőre felvett címek száma: " + animalsInDay.size());

		return animalsInDay;
	}

	public List<StatsData> getStatsInInterval(EventIntervalRequest requestinterval) {

		List<StatsData> dataList = medicalRecordRepository.getStatInInterval(requestinterval.getIntervalStart(),
				requestinterval.getIntervalEnd());

		Comparator<StatsData> byDate = Comparator.comparing(StatsData::getDate);

		Collections.sort(dataList, byDate);

		return dataList;
	}

	public void moveMedicalRecordsToNewAnimal(List<MedicalRecord> movableMedicalRecords, Animal goalAnimal) {
		for (MedicalRecord medicalRecord : movableMedicalRecords) {
			medicalRecord.setAnimal(goalAnimal);
		}
		medicalRecordRepository.saveAll(movableMedicalRecords);
	}
}
