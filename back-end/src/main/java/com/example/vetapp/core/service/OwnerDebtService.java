package com.example.vetapp.core.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.OwnerDebt;
import com.example.vetapp.core.payload.out.ExtendedOwnerDebt;
import com.example.vetapp.core.repository.OwnerDebtRepository;

@Service
@Transactional
public class OwnerDebtService {

	private OwnerDebtRepository ownerDebtRepository;

	@Autowired
	public OwnerDebtService(OwnerDebtRepository ownerDebtRepository) {
		this.ownerDebtRepository = ownerDebtRepository;
	}

	public List<ExtendedOwnerDebt> getAllDebtDatasByOwnerId(Long ownerId) {

		List<ExtendedOwnerDebt> extendedList = new ArrayList<ExtendedOwnerDebt>();

		List<OwnerDebt> debtList = ownerDebtRepository.findAllOwnerDebtDatas(ownerId);

		for (OwnerDebt ownerDebt : debtList) {
			extendedList.add(new ExtendedOwnerDebt(ownerDebt, "debt"));
		}

		/*List<Document> documentList = billingoService.getDocumentsByOwnerIdAndPaymentStatus(ownerId);

		if (documentList != null) {
			for (Document document : documentList) {
				ExtendedOwnerDebt newDebt = new ExtendedOwnerDebt();
				newDebt.setDebtType("invoice");
				newDebt.setDebtValue(document.getGrossTotal().intValue());
				newDebt.setTransactionDate(document.getDueDate().toString());
				extendedList.add(newDebt);
			}
		}*/

		return extendedList;
	}

	public void saveDebt(OwnerDebt ownerDebt) {
		ownerDebtRepository.save(ownerDebt);
	}

}
