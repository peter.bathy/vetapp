package com.example.vetapp.core.payload.in;

public class AnimalMergeRequest {

    private Long sourceAnimalId;
    private Long goalAnimalId;
    
	public Long getSourceAnimalId() {
		return sourceAnimalId;
	}
	public void setSourceAnimalId(Long sourceAnimalId) {
		this.sourceAnimalId = sourceAnimalId;
	}
	public Long getGoalAnimalId() {
		return goalAnimalId;
	}
	public void setGoalAnimalId(Long goalAnimalId) {
		this.goalAnimalId = goalAnimalId;
	}
}
