package com.example.vetapp.core.domain;

import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "goods")
public class Goods {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Boolean active;
	private String name;
	private String manufacturer;
	private Double nettoPrice;
	private Double packPrice;
	private Double numberPerPack;
	private Float tax;
	private Float margin;
	private Double bruttoPrice;
	private Double sellPrice;
	private String quantityUnit;
	private Integer baseQuantity;
	private String comment;
	@ElementCollection
	private List<String> keyWords;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "goods_template_ids", joinColumns = @JoinColumn(name = "goods_id"), inverseJoinColumns = @JoinColumn(name = "goods_template_id"))
	private Set<GoodsTemplate> templates;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Double getPackPrice() {
		return packPrice;
	}

	public void setPackPrice(Double packPrice) {
		this.packPrice = packPrice;
	}

	public Double getNumberPerPack() {
		return numberPerPack;
	}

	public void setNumberPerPack(Double numberPerPack) {
		this.numberPerPack = numberPerPack;
	}

	public Double getNettoPrice() {
		return nettoPrice;
	}

	public void setNettoPrice(Double nettoPrice) {
		this.nettoPrice = nettoPrice;
	}

	public Float getTax() {
		return tax;
	}

	public void setTax(Float tax) {
		this.tax = tax;
	}

	public Float getMargin() {
		return margin;
	}

	public void setMargin(Float margin) {
		this.margin = margin;
	}

	public Double getBruttoPrice() {
		return bruttoPrice;
	}

	public void setBruttoPrice(Double bruttoPrice) {
		this.bruttoPrice = bruttoPrice;
	}

	public Double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getQuantityUnit() {
		return quantityUnit;
	}

	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public Integer getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(Integer baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public List<String> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(List<String> keyWords) {
		this.keyWords = keyWords;
	}

	public Set<GoodsTemplate> getTemplates() {
		return templates;
	}

	public void setTemplates(Set<GoodsTemplate> templates) {
		this.templates = templates;
	}

	public void addTemplate(GoodsTemplate template) {
		templates.add(template);
		template.getGoods().add(this);
	}

	public void removeTemplate(GoodsTemplate template) {
		templates.remove(template);
		template.getGoods().remove(this);
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Goods))
			return false;
		return id != null && id.equals(((Goods) o).getId());
	}
}
