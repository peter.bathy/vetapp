package com.example.vetapp.core.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "animals")
public class Animal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private Sex sex;

	@ManyToOne
	private Owner owner;

	@Column(columnDefinition = "TEXT")
	private String comment;

	@ManyToOne
	private Breed breed;

	private String idOfMicrochip;
	private String idOfVaccinationBook;
	private String passportId;
	private String pedigree;
	private String name;
	private Boolean isSpayed;
	private LocalDate birthDate;
	private LocalDate dateOfSpaying;
	private LocalDate dateOfDeath;
	private Boolean birthDateIsEstimated;
	private Boolean active;
	private String fur;

	@ManyToOne
	private Color color;
	private String pattern;
	@OneToMany(mappedBy = "animal", cascade = CascadeType.ALL)
	private List<MedicalRecord> medicalRecords;

	@OneToMany(mappedBy = "animal", cascade = CascadeType.ALL)
	private List<CalendarEvent> events;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Breed getBreed() {
		return breed;
	}

	public void setBreed(Breed breed) {
		this.breed = breed;
	}

	public String getIdOfMicrochip() {
		return idOfMicrochip;
	}

	public void setIdOfMicrochip(String idOfMicrochip) {
		this.idOfMicrochip = idOfMicrochip;
	}

	public String getIdOfVaccinationBook() {
		return idOfVaccinationBook;
	}

	public void setIdOfVaccinationBook(String idOfVaccinationBook) {
		this.idOfVaccinationBook = idOfVaccinationBook;
	}

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	public String getPedigree() {
		return pedigree;
	}

	public void setPedigree(String pedigree) {
		this.pedigree = pedigree;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateOfSpaying() {
		return dateOfSpaying;
	}

	public void setDateOfSpaying(LocalDate dateOfSpaying) {
		this.dateOfSpaying = dateOfSpaying;
	}

	public Boolean getIsSpayed() {
		return isSpayed;
	}

	public void setIsSpayed(Boolean isSpayed) {
		this.isSpayed = isSpayed;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public LocalDate getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(LocalDate dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public Boolean getBirthDateIsEstimated() {
		return birthDateIsEstimated;
	}

	public void setBirthDateIsEstimated(Boolean birthDateIsEstimated) {
		this.birthDateIsEstimated = birthDateIsEstimated;
	}

	public String getFur() {
		return fur;
	}

	public void setFur(String fur) {
		this.fur = fur;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public List<MedicalRecord> getMedicalRecords() {
		return medicalRecords;
	}

	public void setMedicalRecords(List<MedicalRecord> medicalRecords) {
		this.medicalRecords = medicalRecords;
	}

	public List<CalendarEvent> getEvents() {
		return events;
	}

	public void setEvents(List<CalendarEvent> events) {
		this.events = events;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
