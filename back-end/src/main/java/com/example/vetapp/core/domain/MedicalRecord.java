package com.example.vetapp.core.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class MedicalRecord {

	@Id
	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sequence", allocationSize = 10)
	private Long id;
	private LocalDateTime sessionDate;
	private Boolean isVaccination;
	private String vaccinationName;
	@Column(columnDefinition = "TEXT")
	private String anamnesis;
	@Column(columnDefinition = "TEXT")
	private String symptoms;
	@Column(columnDefinition = "TEXT")
	private String therapy;
	@Column(columnDefinition = "TEXT")
	private String otherDetails;
	@ManyToOne
	private Animal animal;

	@OneToMany(mappedBy = "medicalRecord", cascade = CascadeType.ALL)
	private List<MedicalRecordAttachement> attachements;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getSessionDate() {
		return sessionDate;
	}

	public void setSessionDate(LocalDateTime sessionDate) {
		this.sessionDate = sessionDate;
	}

	public Boolean getIsVaccination() {
		return isVaccination;
	}

	public void setIsVaccination(Boolean isVaccination) {
		this.isVaccination = isVaccination;
	}

	public String getVaccinationName() {
		return vaccinationName;
	}

	public void setVaccinationName(String vaccinationName) {
		this.vaccinationName = vaccinationName;
	}

	public String getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getTherapy() {
		return therapy;
	}

	public void setTherapy(String therapy) {
		this.therapy = therapy;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public List<MedicalRecordAttachement> getAttachements() {
		return attachements;
	}

	public void setAttachements(List<MedicalRecordAttachement> attachements) {
		this.attachements = attachements;
	}

	public String getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}

	@Override
	public String toString() {
		return "MedicalRecord [id=" + id + ", sessionDate=" + sessionDate + ", isVaccination=" + isVaccination
				+ ", vaccinationName=" + vaccinationName + ", anamnesis=" + anamnesis + ", symptoms=" + symptoms
				+ ", therapy=" + therapy + ", otherDetails=" + otherDetails + ", animal=" + animal + ", attachements="
				+ attachements + "]";
	}
	
	

}
