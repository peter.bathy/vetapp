package com.example.vetapp.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.OwnerDebt;

@Repository
public interface OwnerDebtRepository extends JpaRepository<OwnerDebt, Long> {
	@Query("SELECT o FROM OwnerDebt o where o.ownerId = ?1")
	List<OwnerDebt> findAllOwnerDebtDatas(Long ownerId);

}
