package com.example.vetapp.core.payload.out;

import java.util.ArrayList;
import java.util.List;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.Owner;

public class SimpleOwnerDetailsWithAnimals extends SimpleOwnerDetails {
	private List<SimpleAnimalDetails> animalList;

	public SimpleOwnerDetailsWithAnimals(Owner owner) {
		super(owner);

		List<SimpleAnimalDetails> newList = new ArrayList<>();

		for (Animal animal : owner.getAnimals()) {
			if(animal.getActive()== true) {
				newList.add(new SimpleAnimalDetails(animal));
			}
		}

		this.animalList = newList;
	}

	public List<SimpleAnimalDetails> getAnimalList() {
		return animalList;
	}

	public void setAnimalList(List<SimpleAnimalDetails> animalList) {
		this.animalList = animalList;
	}

}
