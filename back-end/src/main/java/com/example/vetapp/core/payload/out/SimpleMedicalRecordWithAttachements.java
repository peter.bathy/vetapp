package com.example.vetapp.core.payload.out;

import java.util.ArrayList;
import java.util.List;

import com.example.vetapp.core.domain.MedicalRecord;
import com.example.vetapp.core.domain.MedicalRecordAttachement;

public class SimpleMedicalRecordWithAttachements extends SimpleMedicalRecord {

	private List<SimpleAttachementDetails> attachements;

	public SimpleMedicalRecordWithAttachements() {
	}

	public SimpleMedicalRecordWithAttachements(MedicalRecord medicalRecord) {
		super(medicalRecord);
		List<SimpleAttachementDetails> attachementList = new ArrayList<>();
		for (MedicalRecordAttachement currentAttachement : medicalRecord.getAttachements()) {
			attachementList.add(new SimpleAttachementDetails(currentAttachement));
		}
		this.attachements = attachementList;

	}

	public List<SimpleAttachementDetails> getAttachements() {
		return attachements;
	}

	public void setAttachements(List<SimpleAttachementDetails> attachements) {
		this.attachements = attachements;
	}
}
