package com.example.vetapp.core.controller;

import com.example.vetapp.core.payload.in.AnimalMergeRequest;
import com.example.vetapp.core.payload.in.CustomSearchValues;
import com.example.vetapp.core.payload.out.AnimalDetails;
import com.example.vetapp.core.payload.out.AnimalListItem;
import com.example.vetapp.core.payload.out.ApiResponse;
import com.example.vetapp.core.payload.out.SimpleOwnerDetailsWithAnimals;
import com.example.vetapp.core.service.AnimalService;
import com.example.vetapp.core.service.SearchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/animal")
public class AnimalController {

	private AnimalService animalService;
	private SearchService searchService;

	@Autowired
	public AnimalController(AnimalService animalService, SearchService searchService) {
		this.animalService = animalService;
		this.searchService = searchService;
	}

	/**
	 * @GetMapping public ResponseEntity<List<AnimalListItem>> getAllAnimal() {
	 *             return new ResponseEntity<>(animalService.getAllAnimals(),
	 *             HttpStatus.OK); }
	 */

	@GetMapping("/{id}")
	public ResponseEntity<?> getAnimalById(@PathVariable Long id) {

		AnimalDetails animal = animalService.getAnimalDetailsById(id);

		if (animal != null) {
			return new ResponseEntity<>(animal, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ApiResponse(false, "Animal is not found with given id!"), HttpStatus.NOT_FOUND);
	}

	@GetMapping("/search-full/{searchWord}")
	public ResponseEntity<List<AnimalListItem>> searchWithOwnerDetails(@PathVariable String searchWord) {

		return new ResponseEntity<>(searchService.searchAnimalOwnerDetails(searchWord), HttpStatus.OK);
	}

	@GetMapping("/search/{searchWord}")
	public ResponseEntity<List<AnimalListItem>> search(@PathVariable String searchWord) {

		return new ResponseEntity<>(searchService.searchAnimalDetails(searchWord), HttpStatus.OK);
	}

	@PostMapping("/detailed-search")
	public ResponseEntity<List<SimpleOwnerDetailsWithAnimals>> detailedSearch(@RequestBody CustomSearchValues searchValues) {
		return new ResponseEntity<>(searchService.detailedSearch(searchValues), HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveAnimal(@RequestBody AnimalDetails animal) {

		AnimalDetails savedAnimal = new AnimalDetails(animalService.saveAnimal(animal));

		return new ResponseEntity<>(savedAnimal, HttpStatus.OK);
	}

	@PostMapping("/merge")
	public ResponseEntity<?> mergeAnimals(@RequestBody AnimalMergeRequest request) {

		animalService.mergeAnimal(request);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
