package com.example.vetapp.core.repository;

import com.example.vetapp.core.domain.Species;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpeciesRepository extends JpaRepository<Species, Long>{
	
	List<Species> findAllByOrderBySpeciesNameAsc();
    
}
