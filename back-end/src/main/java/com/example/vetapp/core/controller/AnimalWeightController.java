package com.example.vetapp.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.domain.AnimalWeight;
import com.example.vetapp.core.service.AnimalWeightService;

@RestController
@RequestMapping("/api/animal-weight")
public class AnimalWeightController {

	private AnimalWeightService animalWeightService;

	@Autowired
	public AnimalWeightController(AnimalWeightService animalWeightService) {
		this.animalWeightService = animalWeightService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getAnimalWeightDatasById(@PathVariable Long id) {

		return new ResponseEntity<>(animalWeightService.getAllWeightDatasByAnimalId(id), HttpStatus.OK);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<?> deleteWeightDataById(@PathVariable Long id) {

		animalWeightService.deleteWeightDataById(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveAnimal(@RequestBody AnimalWeight weightData) {

		animalWeightService.saveWeight(weightData);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
