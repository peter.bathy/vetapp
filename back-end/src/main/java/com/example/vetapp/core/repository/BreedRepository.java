package com.example.vetapp.core.repository;

import com.example.vetapp.core.domain.Breed;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BreedRepository extends JpaRepository<Breed, Long>{
    
}
