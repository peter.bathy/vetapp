package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.Owner;

public class OwnerDetails extends SimpleOwnerDetails {
	private String email;
	private Boolean enableVaccinationReminder;
	private String mobileNumber;
	private String phoneNumber;
	private String ownerNote;

	public OwnerDetails() {
	}

	public OwnerDetails(Owner owner) {

		super(owner);
		this.enableVaccinationReminder = owner.getEnableVaccinationReminder();
		this.email = owner.getEmail();
		this.mobileNumber = owner.getMobileNumber();
		this.phoneNumber = owner.getPhoneNumber();
		this.ownerNote = owner.getNote();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getOwnerNotification() {
		return ownerNote;
	}

	public void setOwnerNotification(String ownerNotification) {
		this.ownerNote = ownerNotification;
	}

	public Boolean getEnableVaccinationReminder() {
		return enableVaccinationReminder;
	}

	public void setEnableVaccinationReminder(Boolean enableVaccinationReminder) {
		this.enableVaccinationReminder = enableVaccinationReminder;
	}

	public String getOwnerNote() {
		return ownerNote;
	}

	public void setOwnerNote(String ownerNote) {
		this.ownerNote = ownerNote;
	}
	
	

}
