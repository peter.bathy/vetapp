package com.example.vetapp.core.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.NotificationData;
import com.example.vetapp.core.domain.NotificationTypeBaseValues;
import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.payload.in.ReminderEmailDatas;
import com.example.vetapp.core.payload.out.AnimalListItemWithEmail;
import com.example.vetapp.core.payload.out.OwnerDetails;
import com.example.vetapp.core.repository.NotificationDataRepository;

@Service
@Transactional
public class NotificationService {

	private TemplateEngine templateEngine;
	private JavaMailSender mailSender;
	private NotificationDataRepository notificationDataRepository;
	private OwnerService ownerService;
	private AnimalService animalService;
	private MedicalRecordService medicalRecordService;

	@Autowired
	public NotificationService(JavaMailSender mailSender, TemplateEngine templateEngine,
			NotificationDataRepository notificationDataRepository, OwnerService ownerService,
			AnimalService animalService, MedicalRecordService medicalRecordService) {
		this.templateEngine = templateEngine;
		this.mailSender = mailSender;
		this.notificationDataRepository = notificationDataRepository;
		this.ownerService = ownerService;
		this.animalService = animalService;
		this.medicalRecordService = medicalRecordService;
	}

	public void sendReminder(ReminderEmailDatas reminderDatas) throws Exception {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");

		// Send the information about booked event

		Context thymeleafContext = new Context();
		String customEmailText = reminderDatas.getEmailText().replaceAll("(\r\n|\n)", "<br />");
		thymeleafContext.setVariable("date", customEmailText);

		String htmlBody = templateEngine.process(selectTemplate(reminderDatas.getMessageType()), thymeleafContext);

		helper.setSubject("Állatorvosi időpont emlékeztető");
		helper.setFrom("info@erzsebetvet.hu");
		helper.setTo(reminderDatas.getEmail());

		helper.setText(htmlBody, true);

		mailSender.send(message);

		// Add records to scheduler

		List<NotificationData> emails = new ArrayList<>();

		NotificationData data1 = new NotificationData();
		data1.setAnimalId(reminderDatas.getAnimalId());
		data1.setEventId(reminderDatas.getEventId());
		switch (reminderDatas.getMessageType()) {
		case "operation":
			data1.setEmailBaseDatas(NotificationTypeBaseValues.OPERATIONDETAILS);
			break;
		case "examination":
			data1.setEmailBaseDatas(NotificationTypeBaseValues.EXAMINATIONDETAILS);
			break;
		case "treatment":
			data1.setEmailBaseDatas(NotificationTypeBaseValues.TREATMENTDETAILS);
			break;
		case "consultation":
			data1.setEmailBaseDatas(NotificationTypeBaseValues.CONSULTATIONDETAILS);
			break;
		}
		data1.setEmailTo(reminderDatas.getEmail());
		data1.setSendingTime(LocalDateTime.now());
		data1.setLetterSent(true);
		data1.setEmailBody(reminderDatas.getEmailText());
		emails.add(data1);

		if (LocalDateTime.now().plusDays(1).isBefore(reminderDatas.getEventStart().minusDays(1))) {
			NotificationData data2 = new NotificationData();
			data2.setAnimalId(reminderDatas.getAnimalId());
			data2.setEventId(reminderDatas.getEventId());
			data2.setEmailBaseDatas(NotificationTypeBaseValues.EVENTREMINDER);
			data2.setEmailTo(reminderDatas.getEmail());

			data2.setSendingTime(reminderDatas.getEventStart().minusDays(2L));
			data2.setLetterSent(false);
			data2.setEmailBody(reminderDatas.getEmailText());

			emails.add(data2);
		}

		notificationDataRepository.saveAll(emails);

		// Save owner's email address if animal is attached and email value is empty or
		// different

		if (reminderDatas.getAnimalId() != null && reminderDatas.getAnimalId() != 0L) {
			Animal animal = animalService.getAnimalById(reminderDatas.getAnimalId());
			if (animal != null) {
				Owner owner = animal.getOwner();
				if (!owner.getEmail().equals(reminderDatas.getEmail())) {
					owner.setEmail(reminderDatas.getEmail());
					ownerService.savePlainOwnerDatas(owner);
				}
			}
		}
	}

	public void vaccineReminderRegistration(OwnerDetails ownerDetails) throws Exception {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
		Context context = new Context();
		String htmlBody = templateEngine.process(selectTemplate("vaccinationRegistration"), context);

		helper.setSubject("Oltás-emlékeztető feliratkozás");
		helper.setFrom("info@erzsebetvet.hu");
		helper.setTo(ownerDetails.getEmail());

		helper.setText(htmlBody, true);

		mailSender.send(message);

		List<NotificationData> emails = new ArrayList<>();

		NotificationData data1 = new NotificationData();
		data1.setEmailBaseDatas(NotificationTypeBaseValues.VACCINATIONSUBSC);
		data1.setEmailTo(ownerDetails.getEmail());
		data1.setSendingTime(LocalDateTime.now());
		data1.setLetterSent(true);
		emails.add(data1);

		notificationDataRepository.saveAll(emails);
	}

	public void scheduleVaccinationNotification() {

		LocalDateTime today = LocalDateTime.now();
		LocalDateTime lastSaturDay = today.with(DayOfWeek.MONDAY).minusDays(1);
		LocalDateTime nextMonday = today.with(DayOfWeek.SUNDAY).plusDays(1);
		
		if (notificationDataRepository.findIfSchedulingIsDoneForThisWeek(lastSaturDay, nextMonday).size() == 0) {
			
			List<AnimalListItemWithEmail> schedulableItems = medicalRecordService
					.getExpiringVaccinationsByTwoWeeksBefore(lastSaturDay, nextMonday);

			Set<String> emails = new HashSet<>();
			
			for (AnimalListItemWithEmail animalListItemWithEmail : schedulableItems) {
				emails.add(animalListItemWithEmail.getEmail());
			}
			
			List<NotificationData> notifications = new ArrayList<>();

			for (String email : emails) {
				NotificationData data = new NotificationData();
				data.setEmailBaseDatas(NotificationTypeBaseValues.VACCINATIONREMINDER);
				data.setEmailTo(email);
				data.setSendingTime(LocalDateTime.now());
				data.setLetterSent(false);

				notifications.add(data);
			}
			notificationDataRepository.saveAll(notifications);
		}
	};

	//@Scheduled(fixedDelay = 1800000, initialDelay = 150000)
	public void scheduledEmailSending() {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
		Pageable firstSix = PageRequest.of(0, 6);
		Page<NotificationData> results = notificationDataRepository.findTodaySendablesFirtstSix(firstSix);

		if (results.hasContent()) {
			for (NotificationData emailData : results) {

				try {
					Context thymeleafContext = new Context();
					if(emailData.getEmailBody() != null) {
						String customEmailText = emailData.getEmailBody().replaceAll("(\r\n|\n)", "<br />");
					thymeleafContext.setVariable("date", customEmailText);
					}

					String htmlBody = templateEngine.process(selectTemplate(emailData.getEmailBaseDatas().getName()),
							thymeleafContext);

					helper.setSubject(emailData.getEmailBaseDatas().getSubject());
					helper.setFrom("info@erzsebetvet.hu");
					helper.setTo(emailData.getEmailTo());

					helper.setText(htmlBody, true);

					mailSender.send(message);

					TimeUnit.SECONDS.sleep(30L);
				} catch (Exception e) {
					emailData.setSendingError(e.getMessage());
				} finally {
					emailData.setLetterSent(true);
					emailData.setSendingTime(LocalDateTime.now());
					notificationDataRepository.save(emailData);
				}
			}
		}
	}

	public List<NotificationData> getEmailsByAnimalId(Long animalId) {

		return notificationDataRepository.findAllByAnimalIdOrderBySendingTimeDesc(animalId);
	}

	public void deleteReminder(Long eventId) {
		List<NotificationData> datas = notificationDataRepository.findByEventId(eventId);

		notificationDataRepository.deleteAll(datas);
	}

	private String selectTemplate(String messageType) {
		String emailType = null;
		switch (messageType) {
		case "operation":
			emailType = "email/operation-reminder";
			break;
		case "examination":
			emailType = "email/examination-reminder";
			break;
		case "treatment":
			emailType = "email/treatment-reminder";
			break;
		case "consultation":
			emailType = "email/consultation-reminder";
			break;
		case "eventReminder":
			emailType = "email/event-reminder";
			break;
		case "vaccinationReminder":
			emailType = "email/vaccination-reminder";
			break;
		case "vaccinationRegistration":
			emailType = "email/vaccination-subscription";
			break;
		default:
			emailType = null;
		}
		return emailType;
	}
}
