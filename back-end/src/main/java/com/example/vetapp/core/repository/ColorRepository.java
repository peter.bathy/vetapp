package com.example.vetapp.core.repository;

import com.example.vetapp.core.domain.Color;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColorRepository extends JpaRepository<Color, Long>{

	List<Color> findByOrderByColorNameAsc();
    
}
