package com.example.vetapp.core.service;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.vetapp.core.domain.Animal;
import com.example.vetapp.core.domain.AnimalWeight;
import com.example.vetapp.core.domain.AttachementType;
import com.example.vetapp.core.domain.Breed;
import com.example.vetapp.core.domain.CalendarEvent;
import com.example.vetapp.core.domain.ChangeEventType;
import com.example.vetapp.core.domain.Color;
import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.domain.MedicalRecord;
import com.example.vetapp.core.domain.MedicalRecordAttachement;
import com.example.vetapp.core.domain.NotificationData;
import com.example.vetapp.core.domain.NotificationTypeBaseValues;
import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.domain.OwnerDebt;
import com.example.vetapp.core.domain.PetvetDatas;
import com.example.vetapp.core.domain.Sex;
import com.example.vetapp.core.domain.Species;
import com.example.vetapp.core.repository.AnimalRepository;
import com.example.vetapp.core.repository.AnimalWeightRepository;
import com.example.vetapp.core.repository.BreedRepository;
import com.example.vetapp.core.repository.CalendarEventRepository;
import com.example.vetapp.core.repository.ColorRepository;
import com.example.vetapp.core.repository.GoodsRepository;
import com.example.vetapp.core.repository.MedicalRecordAttachementRepository;
import com.example.vetapp.core.repository.MedicalRecordRepository;
import com.example.vetapp.core.repository.NotificationDataRepository;
import com.example.vetapp.core.repository.OwnerDebtRepository;
import com.example.vetapp.core.repository.OwnerRepository;
import com.example.vetapp.core.repository.PetvetDatasRepository;
import com.example.vetapp.core.repository.SpeciesRepository;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

@Service
@Transactional
public class RestorationService {

	private static final Logger logger = Logger.getLogger("RestorationService.class");

	private AnimalRepository animalRepository;
	private MedicalRecordRepository medicalRecordRepository;
	private MedicalRecordAttachementRepository medicalRecordAttachementRepository;
	private ColorRepository colorRepository;
	private BreedRepository breedRepository;
	private SpeciesRepository speciesRepository;
	private OwnerRepository ownerRepository;
	private OwnerDebtRepository ownerDebtRepository;
	private AnimalWeightRepository animalWeightRepository;
	private CalendarEventRepository calendarEventRepository;
	private PetvetDatasRepository petvetDataRepository;
	private GoodsRepository goodsEventRepository;
	private NotificationDataRepository notificationRepository;

	private String dataPath = "/var/lib/data";

	final String delimiter = "#";
	private Map<String, List<String>> eventTypesMap = new HashMap<>();
	private Map<String, List<String>> goodsKeyMap = new HashMap<>();

	@Autowired
	public RestorationService(AnimalRepository animalRepository, ColorRepository colorRepository,
			BreedRepository breedRepository, SpeciesRepository speciesRepository,
			MedicalRecordRepository medicalRecordRepository,
			MedicalRecordAttachementRepository medicalRecordAttachementRepository, OwnerRepository ownerRepository,
			OwnerDebtRepository ownerDebtRepository, AnimalWeightRepository animalWeightRepository,
			CalendarEventRepository calendarEventRepository, PetvetDatasRepository petvetDataRepository,
			GoodsRepository goodsEventRepository, NotificationDataRepository notificationRepository) {
		this.animalRepository = animalRepository;
		this.colorRepository = colorRepository;
		this.breedRepository = breedRepository;
		this.speciesRepository = speciesRepository;
		this.medicalRecordRepository = medicalRecordRepository;
		this.medicalRecordAttachementRepository = medicalRecordAttachementRepository;
		this.ownerRepository = ownerRepository;
		this.ownerDebtRepository = ownerDebtRepository;
		this.animalWeightRepository = animalWeightRepository;
		this.calendarEventRepository = calendarEventRepository;
		this.petvetDataRepository = petvetDataRepository;
		this.goodsEventRepository = goodsEventRepository;
		this.notificationRepository = notificationRepository;
	}

	private CSVParser parser = new CSVParserBuilder().withSeparator('#').build();
	private String FOLDER_PATH = System.getProperty("user.home") + dataPath + "/backup-files/";

	public void restoreDatabase() {

		System.out.println(FOLDER_PATH);
		File directoryPath = new File(FOLDER_PATH);
		// List of all files and directories
		File filesList[] = directoryPath.listFiles();
		Arrays.sort(filesList, (a, b) -> Integer.valueOf((a.getName().split("_")[0]))
				.compareTo(Integer.valueOf((b.getName().split("_")[0]))));
		for (File file : filesList) {
			System.out.println("File name: " + file.getName());
			int firstIndex = file.getName().indexOf("_");
			int lastIndex = file.getName().lastIndexOf("_");

			String tableName = file.getName().substring(firstIndex + 1, lastIndex);

			switch (tableName) {

			case "species":
				restoreSpecies(file);
				break;
			case "breed":
				restoreBreeds(file);
				break;
			case "colors":
				restoreColors(file);
				break;
			case "owners":
				restoreOwners(file);
				break;
			case "owner_debt":
				restoreOwnerDebts(file);
				break;
			case "animals":
				restoreAnimals(file);
				break;
			case "animal_weight":
				restoreAnimalWeights(file);
				break;
			case "calendar_event":
				restoreCalendarEvents(file);
				break;
			case "calendar_event_event_types":
				restoreCalendarEventTypes(file);
				break;
			case "petvet_datas":
				restorePetvetDatas(file);
				break;
			case "goods":
				restoreGoods(file);
				break;
			case "goods_key_words":
				restoreGoodsKeyWords(file);
				break;
			case "medical_record":
				restoreMedicalRecords(file);
				break;
			case "notification_data":
				restoreNotificationDatas(file);
				break;
			default:
				break;
			}
		}
	}

	public void restoreAttachements() {

		// String FOLDER_PATH = System.getProperty("user.home") + path;
		// //"/Asztal/vetapp4-táblák/backup-files";

		// Creating a File object for directory
		File directoryPath = new File(FOLDER_PATH);
		// List of all files and directories
		File filesList[] = directoryPath.listFiles();
		Arrays.sort(filesList, (a, b) -> Integer.valueOf((a.getName().split("_")[0]))
				.compareTo(Integer.valueOf((b.getName().split("_")[0]))));
		for (File file : filesList) {
			System.out.println("File name: " + file.getName());
			int firstIndex = file.getName().indexOf("_");
			int lastIndex = file.getName().lastIndexOf("_");

			String tableName = file.getName().substring(firstIndex + 1, lastIndex);

			switch (tableName) {

			case "medical_record_attachement":
				restoreAttachementFileNames(file);
				break;
			default:
				break;
			}
		}

	}

	private void restoreSpecies(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<Species> speciesList = new ArrayList<Species>();
			while ((result = reader.readNext()) != null) {
				Species newSpecies = new Species();
				newSpecies.setId(Long.valueOf(result[0]));
				newSpecies.setSpeciesName(result[1]);
				speciesList.add(newSpecies);
			}
			reader.close();
			speciesRepository.saveAll(speciesList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreBreeds(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<Breed> breedList = new ArrayList<Breed>();
			while ((result = reader.readNext()) != null) {
				Optional<Species> optionalSpecies = speciesRepository.findById(Long.valueOf(result[2]));
				if (optionalSpecies.isPresent()) {
					Breed newBreed = new Breed();
					newBreed.setId(Long.valueOf(result[0]));
					newBreed.setBreedName(result[1]);
					newBreed.setSpecies(optionalSpecies.get());
					breedList.add(newBreed);
				}
			}
			reader.close();
			breedRepository.saveAll(breedList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreColors(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<Color> colorList = new ArrayList<Color>();
			while ((result = reader.readNext()) != null) {
				Color newColor = new Color();
				newColor.setId(Long.valueOf(result[0]));
				newColor.setColorName(result[1]);
				colorList.add(newColor);
			}
			reader.close();
			colorRepository.saveAll(colorList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreNotificationDatas(File file) {

		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<NotificationData> notificationList = new ArrayList<NotificationData>();
			List<Animal> animalList = animalRepository.findAll();
			List<Owner> ownerList = ownerRepository.findAll();

			while ((result = reader.readNext()) != null) {
				NotificationData newNotificationData = new NotificationData();

				if (result[1].isBlank()) {
					newNotificationData.setAnimalId(null);
				} else {
					newNotificationData.setAnimalId(Long.valueOf(result[1]));
				}

				newNotificationData.setEmailBaseDatas(convertStringToNotificationType(result[2]));

				if (result[3].isBlank()) {
					newNotificationData.setEmailBody("");
				} else {
					newNotificationData.setEmailBody(result[3]);
				}
				newNotificationData.setEmailTo(result[4]);

				if (result[5].isBlank()) {
					newNotificationData.setEventId(null);
				} else {
					newNotificationData.setEventId(Long.valueOf(result[5]));
				}

				if (result[6].equals("1")) {
					newNotificationData.setLetterSent(true);
				} else {
					newNotificationData.setLetterSent(false);
				}

				if (result[7].isBlank()) {
					newNotificationData.setOwnerId(null);
				} else {
					newNotificationData.setOwnerId(Long.valueOf(result[7]));
				}
				if (result[8].isBlank()) {
					newNotificationData.setSendingError(null);
				} else {
					newNotificationData.setSendingError(result[8]);
				}
				newNotificationData.setSendingTime(convertStringToLocalDateTime(result[9]));

				notificationList.add(newNotificationData);
			}
			reader.close();
			notificationRepository.saveAll(notificationList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreOwners(File file) {

		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<Owner> ownerList = new ArrayList<Owner>();
			while ((result = reader.readNext()) != null) {
				Owner newOwner = new Owner();
				newOwner.setId(Long.valueOf(result[0]));
				if (result[1].equals("1")) {
					newOwner.setActive(true);
				} else {
					newOwner.setActive(false);
				}
				if (result[2].isBlank()) {
					newOwner.setAddress("");
				} else {
					newOwner.setAddress(result[2]);
				}
				if (result[3].isBlank()) {
					newOwner.setCity("");
				} else {
					newOwner.setCity(result[3]);
				}
				if (result[5].isBlank()) {
					newOwner.setEnableVaccinationReminder(null);
				} else {
					if (result[5].equals("1")) {
						newOwner.setEnableVaccinationReminder(true);
					} else {
						newOwner.setEnableVaccinationReminder(false);
					}
				}
				if (result[4].isBlank()) {
					newOwner.setEmail("");
				} else {
					newOwner.setEmail(result[4]);
				}
				if (result[6].isBlank()) {
					newOwner.setMobileNumber("");
				} else {
					newOwner.setMobileNumber(result[6]);
				}
				if (result[7].isBlank()) {
					newOwner.setName("");
				} else {
					newOwner.setName(result[7]);
				}
				if (result[8].isBlank()) {
					newOwner.setNote("");
				} else {
					newOwner.setNote(result[8]);
				}
				if (result[9].isBlank()) {
					newOwner.setPhoneNumber("");
				} else {
					newOwner.setPhoneNumber(result[9]);
				}
				if (result[10].isBlank()) {
					newOwner.setZipCode("");
				} else {
					newOwner.setZipCode(result[10]);
				}
				ownerList.add(newOwner);
			}
			reader.close();
			ownerRepository.saveAll(ownerList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreOwnerDebts(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<OwnerDebt> debtList = new ArrayList<OwnerDebt>();
			while ((result = reader.readNext()) != null) {
				if (!result[1].isBlank()) {
					OwnerDebt newDebt = new OwnerDebt();
					newDebt.setId(Long.valueOf(result[0]));
					newDebt.setDebtValue(Integer.valueOf(result[1]));
					newDebt.setOwnerId(Long.valueOf(result[2]));
					newDebt.setTransactionDate(convertStringToLocalDate(result[3]));
					debtList.add(newDebt);
				}
			}
			reader.close();
			ownerDebtRepository.saveAll(debtList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreAnimals(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<Animal> animalList = new ArrayList<Animal>();
			List<Color> colorList = colorRepository.findAll();
			List<Owner> ownerList = ownerRepository.findAll();
			List<Breed> breedList = breedRepository.findAll();

			while ((result = reader.readNext()) != null) {
				Animal newAnimal = new Animal();
				newAnimal.setId(Long.valueOf(result[0]));

				if (result[1].equals("1")) {
					newAnimal.setActive(true);
				} else {
					newAnimal.setActive(false);
				}
				if (result[2].isBlank()) {
					newAnimal.setBirthDate(null);
				} else {
					newAnimal.setBirthDate(convertStringToLocalDate(result[2]));
				}
				if (result[3].equals("1")) {
					newAnimal.setBirthDateIsEstimated(true);
				} else {
					newAnimal.setBirthDateIsEstimated(false);
				}
				if (result[4].isBlank()) {
					newAnimal.setComment("");
				} else {
					newAnimal.setComment(result[4]);
				}
				if (result[5].isBlank()) {
					newAnimal.setDateOfDeath(null);
				} else {
					newAnimal.setDateOfDeath(convertStringToLocalDate(result[5]));
				}
				if (result[6].isBlank()) {
					newAnimal.setDateOfSpaying(null);
				} else {
					newAnimal.setDateOfSpaying(convertStringToLocalDate(result[6]));
				}
				if (result[7].isBlank()) {
					newAnimal.setFur("");
				} else {
					newAnimal.setFur(result[7]);
				}
				if (result[8].isBlank()) {
					newAnimal.setIdOfMicrochip("");
				} else {
					newAnimal.setIdOfMicrochip(result[8]);
				}
				if (result[9].isBlank()) {
					newAnimal.setIdOfVaccinationBook("");
				} else {
					newAnimal.setIdOfVaccinationBook(result[9]);
				}
				if (result[10].equals("1")) {
					newAnimal.setIsSpayed(true);
				} else {
					newAnimal.setIsSpayed(false);
				}
				if (result[11].isBlank()) {
					newAnimal.setName("");
				} else {
					newAnimal.setName(result[11]);
				}
				if (result[12].isBlank()) {
					newAnimal.setPassportId("");
				} else {
					newAnimal.setPassportId(result[12]);
				}
				if (result[13].isBlank()) {
					newAnimal.setPattern("");
				} else {
					newAnimal.setPattern(result[13]);
				}
				if (result[14].isBlank()) {
					newAnimal.setPedigree("");
				} else {
					newAnimal.setPedigree(result[14]);
				}
				if (result[15].isBlank()) {
					newAnimal.setSex(null);
				} else {
					newAnimal.setSex(convertStringToSex(result[15]));
				}
				if (result[16].isBlank()) {
					newAnimal.setBreed(null);
				} else {
					newAnimal.setBreed(convertStringToBreed(result[16], breedList));
				}
				if (result[17].isBlank()) {
					newAnimal.setColor(null);
				} else {
					newAnimal.setColor(convertStringToColor(result[17], colorList));
				}
				if (result[18].isBlank()) {
					newAnimal.setOwner(null);
				} else {
					newAnimal.setOwner(convertStringToOwner(result[18], ownerList));
				}
				animalList.add(newAnimal);
			}
			reader.close();
			animalRepository.saveAll(animalList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreAnimalWeights(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<AnimalWeight> weightList = new ArrayList<AnimalWeight>();
			while ((result = reader.readNext()) != null) {
				if (!result[2].isBlank()) {
					AnimalWeight newWeighting = new AnimalWeight();
					newWeighting.setId(Long.valueOf(result[0]));
					newWeighting.setAnimalId(Long.valueOf(result[1]));
					newWeighting.setWeight(Float.valueOf(result[2]));
					newWeighting.setWeightingDate(convertStringToLocalDate(result[3]));
					weightList.add(newWeighting);
				}
			}
			reader.close();
			animalWeightRepository.saveAll(weightList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Store event types in a HashMap to add to the events in the next step
	private void restoreCalendarEventTypes(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			while ((result = reader.readNext()) != null) {
				List<String> listOfValues = eventTypesMap.get(result[0]);
				if (listOfValues != null) {
					listOfValues.add(result[1]);
				} else {
					listOfValues = new ArrayList<>();
					listOfValues.add(result[1]);
					eventTypesMap.put(result[0], listOfValues);
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreCalendarEvents(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<CalendarEvent> eventList = new ArrayList<CalendarEvent>();
			List<Animal> animalList = animalRepository.findAll();
			Long currentId = 0L;
			while ((result = reader.readNext()) != null) {
				CalendarEvent newEvent = new CalendarEvent();
				newEvent.setId(currentId);

				if (result.length > 1 && result[1].equals("1")) {
					newEvent.setAllDay(true);
				} else {
					newEvent.setAllDay(false);
				}
				if (result.length > 2) {
					newEvent.setEnd(convertStringToLocalDateTime(result[2]));
				}
				if (result.length > 3) {
					newEvent.setStart(convertStringToLocalDateTime(result[3]));
				}
				if (result.length > 4) {
					newEvent.setTitle(result[4]);
				}
				newEvent.setEventTypes(null);
				newEvent.setEventTypes(eventTypesMap.get(result[0]));
				if (result.length > 5) {
					if (result[5].isBlank()) {
						newEvent.setAnimal(null);
					} else {
						newEvent.setAnimal(convertStringToAnimal(result[5], animalList));
					}
				} else {
					newEvent.setAnimal(null);
				}
				eventList.add(newEvent);
				currentId++;
			}
			reader.close();
			calendarEventRepository.saveAll(eventList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restorePetvetDatas(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<PetvetDatas> petDataList = new ArrayList<PetvetDatas>();
			Long idCounter = 1L;
			while ((result = reader.readNext()) != null) {
				PetvetDatas newData = new PetvetDatas();
				newData.setId(idCounter);
				newData.setAnimalId(Long.valueOf(result[1]));
				newData.setAnimalName(result[2]);
				newData.setChangeType(convertStringToChangeType(result[3]));
				newData.setEventDate(convertStringToLocalDate(result[4]));
				newData.setIdOfMicrochip(result[5]);
				newData.setOwnerId(Long.valueOf(result[6]));
				newData.setOwnerName(result[7]);
				if (!result[8].isBlank()) {
					newData.setRegistrationDate(convertStringToLocalDate(result[8]));
				} else {
					newData.setRegistrationDate(null);
				}

				petDataList.add(newData);
				idCounter++;
			}
			reader.close();
			petvetDataRepository.saveAll(petDataList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Store goods keywords in a HashMap to add to the events in the next step
	private void restoreGoodsKeyWords(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			while ((result = reader.readNext()) != null) {
				List<String> listOfValues = goodsKeyMap.get(result[0]);
				if (listOfValues != null) {
					listOfValues.add(result[1]);
				} else {
					listOfValues = new ArrayList<>();
					listOfValues.add(result[1]);
					goodsKeyMap.put(result[0], listOfValues);
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreGoods(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<Goods> goodsList = new ArrayList<Goods>();
			while ((result = reader.readNext()) != null) {
				Goods newGoodsItem = new Goods();
				newGoodsItem.setId(Long.valueOf(result[0]));
				if (result[1].equals("1")) {
					newGoodsItem.setActive(true);
				} else {
					newGoodsItem.setActive(false);
				}
				newGoodsItem.setBaseQuantity(Integer.valueOf(result[2]));
				newGoodsItem.setBruttoPrice(Double.valueOf(result[3]));
				newGoodsItem.setComment(result[4]);
				newGoodsItem.setManufacturer(result[5]);
				newGoodsItem.setMargin(Float.valueOf(result[6]));
				newGoodsItem.setName(result[7]);
				if (result[8].isBlank()) {
					newGoodsItem.setNettoPrice(null);
				} else {
					newGoodsItem.setNettoPrice(Double.valueOf(result[8]));
				}
				if (result[9].isBlank()) {
					newGoodsItem.setNumberPerPack(null);
				} else {
					newGoodsItem.setNumberPerPack(Double.valueOf(result[9]));
				}
				if (result[10].isBlank()) {
					newGoodsItem.setPackPrice(null);
				} else {
					newGoodsItem.setPackPrice(Double.valueOf(result[10]));
				}
				newGoodsItem.setQuantityUnit(result[11]);
				if (result[12].isBlank()) {
					newGoodsItem.setSellPrice(null);
				} else {
					newGoodsItem.setSellPrice(Double.valueOf(result[12]));
				}
				if (result.length < 13 || result[13].isBlank()) {
					newGoodsItem.setTax(null);
				} else {
					newGoodsItem.setTax(Float.valueOf(result[13]));
				}
				if (goodsKeyMap.containsKey(result[0])) {
					newGoodsItem.setKeyWords(goodsKeyMap.get(result[0]));
				}
				goodsList.add(newGoodsItem);
			}
			reader.close();
			goodsEventRepository.saveAll(goodsList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreMedicalRecords(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<MedicalRecord> eventList = new ArrayList<MedicalRecord>();
			List<Animal> animalList = animalRepository.findAll();
			Long currentId = 0L;
			while ((result = reader.readNext()) != null) {
				MedicalRecord newRecord = new MedicalRecord();
				newRecord.setId(currentId);
				try {
					if (result[1].isBlank()) {
						newRecord.setAnamnesis(null);
					} else {
						newRecord.setAnamnesis(result[1]);
					}
					if (result[2].equals("1")) {
						newRecord.setIsVaccination(true);
					} else {
						newRecord.setIsVaccination(false);
					}
					newRecord.setOtherDetails(result[3]);
					newRecord.setSessionDate(convertStringToLocalDateTime(result[4]));
					newRecord.setSymptoms(result[5]);
					newRecord.setTherapy(result[6]);
					newRecord.setVaccinationName(result[7]);
					newRecord.setAnimal(convertStringToAnimal(result[8], animalList));
					eventList.add(newRecord);

				} catch (Exception e) {
					System.out.println("Hibás bejegyzés. id: " + currentId + ", állat azonosító: " + result[8]);
				}
				eventList.add(newRecord);
				currentId++;
			}
			reader.close();

			int totalObjects = eventList.size();
			int batchSize = 1000;

			for (int i = 0; i < totalObjects; i = i + batchSize) {
				if (i + batchSize > totalObjects) {
					List<MedicalRecord> eventList1 = eventList.subList(i, totalObjects - 1);
					medicalRecordRepository.saveAll(eventList1);
					break;
				}
				List<MedicalRecord> eventList1 = eventList.subList(i, i + batchSize);
				medicalRecordRepository.saveAll(eventList1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void restoreAttachementFileNames(File file) {
		try {
			CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withSkipLines(1).withCSVParser(parser)
					.build();
			String[] result;
			List<String> attachementList = new ArrayList<String>();
			while ((result = reader.readNext()) != null) {
				if (attachementList != null) {
					attachementList.add(result[2]);
				}
			}
			reader.close();

			attachementList.sort(Comparator.naturalOrder());

			MedicalRecord currentMedicalRecord = new MedicalRecord();

			for (String attachement : attachementList) {

				String[] attachementDetails = attachement.split("_");
				Long animalId = Long.valueOf(attachementDetails[0]);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmss");
				LocalDateTime sessionDate = LocalDateTime.parse(attachementDetails[2], formatter);

				try {
					if (currentMedicalRecord != null && currentMedicalRecord.getId() != null
							&& currentMedicalRecord.getAnimal().getId().equals(animalId)
							&& currentMedicalRecord.getSessionDate().equals(sessionDate)) {
						handleAttachementListExpansion(currentMedicalRecord, attachement, attachementDetails);
					} else {
						if (currentMedicalRecord != null && currentMedicalRecord.getId() != null) {
							medicalRecordRepository.save(currentMedicalRecord);
						}

						currentMedicalRecord = medicalRecordRepository
								.findMedicalRecordByAnimalIdAndSessionDate(animalId, sessionDate);

						if (currentMedicalRecord != null) {
							handleAttachementListExpansion(currentMedicalRecord, attachement, attachementDetails);
						}
					}
				} catch (Exception e) {

					e.printStackTrace();
					System.out.println(attachement);
				}
			}
			if (currentMedicalRecord != null && currentMedicalRecord.getId() != null) {
				medicalRecordRepository.save(currentMedicalRecord);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void handleAttachementListExpansion(MedicalRecord currentMedicalRecord, String attachement,
			String[] attachementDetails) {
		List<MedicalRecordAttachement> recordAttachementList = currentMedicalRecord.getAttachements();

		if (recordAttachementList == null) {
			recordAttachementList = new ArrayList<MedicalRecordAttachement>();
		}
		MedicalRecordAttachement newAttachementToRecord = new MedicalRecordAttachement();

		switch (attachementDetails[1]) {
		case "ultrasound":
			newAttachementToRecord.setAttachementType(AttachementType.ULTRASOUND);
			break;
		case "xray":
			newAttachementToRecord.setAttachementType(AttachementType.XRAY);
			break;
		case "photo":
			newAttachementToRecord.setAttachementType(AttachementType.PHOTO);
			break;
		case "lab":
			newAttachementToRecord.setAttachementType(AttachementType.LAB);
			break;
		default:
			newAttachementToRecord.setAttachementType(AttachementType.OTHER);
		}

		newAttachementToRecord.setFileName(attachement);
		newAttachementToRecord.setMedicalRecord(currentMedicalRecord);
		recordAttachementList.add(newAttachementToRecord);

		currentMedicalRecord.setAttachements(recordAttachementList);
	}

	private LocalDate convertStringToLocalDate(String dateString) {
		int firstIndex = dateString.indexOf(" ");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		return LocalDate.parse(dateString.substring(0, firstIndex), formatter);
	}

	private LocalDateTime convertStringToLocalDateTime(String dateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		int firstIndex = dateString.indexOf(".");

		if (firstIndex != -1) {
			dateString = dateString.substring(0, firstIndex);
		}

		return LocalDateTime.parse(dateString, formatter);
	}

	private Sex convertStringToSex(String sexString) {
		switch (sexString) {
		case "MALE":
			return Sex.MALE;
		case "FEMALE":
			return Sex.FEMALE;
		default:
			return Sex.UNKNOWN;
		}
	}

	private NotificationTypeBaseValues convertStringToNotificationType(String typeString) {
		switch (typeString) {
		case "CONSULTATIONDETAILS":
			return NotificationTypeBaseValues.CONSULTATIONDETAILS;
		case "EVENTREMINDER":
			return NotificationTypeBaseValues.EVENTREMINDER;
		case "EXAMINATIONDETAILS":
			return NotificationTypeBaseValues.EXAMINATIONDETAILS;
		case "OPERATIONDETAILS":
			return NotificationTypeBaseValues.OPERATIONDETAILS;
		case "TREATMENTDETAILS":
			return NotificationTypeBaseValues.TREATMENTDETAILS;
		case "VACCINATIONREMINDER":
			return NotificationTypeBaseValues.VACCINATIONREMINDER;
		case "VACCINATIONSUBSC":
			return NotificationTypeBaseValues.VACCINATIONSUBSC;
		default:
			return null;
		}
	}

	private ChangeEventType convertStringToChangeType(String changeType) {
		switch (changeType) {
		case "CHIP":
			return ChangeEventType.CHIP;
		case "DEATH":
			return ChangeEventType.DEATH;
		case "SPAYING":
			return ChangeEventType.SPAYING;
		case "ANIMALDATACHANGE":
			return ChangeEventType.ANIMALDATACHANGE;
		case "OWNERDATACHANGE":
			return ChangeEventType.OWNERDATACHANGE;
		case "PASSPORT":
			return ChangeEventType.PASSPORT;
		case "NEWOWNER":
			return ChangeEventType.NEWOWNER;
		default:
			return ChangeEventType.OTHER;
		}
	}

	private Color convertStringToColor(String colorId, List<Color> colorList) {
		Long currentId = Long.valueOf(colorId);
		Optional<Color> matchingColor = colorList.stream().filter(a -> a.getId().equals(currentId)).findFirst();

		if (matchingColor.isPresent()) {
			return matchingColor.get();
		} else {
			return null;
		}
	}

	private Owner convertStringToOwner(String ownerId, List<Owner> ownerList) {
		Long currentId = Long.valueOf(ownerId);
		Optional<Owner> matchingOwner = ownerList.stream().filter(o -> o.getId().equals(currentId)).findFirst();

		if (matchingOwner.isPresent()) {
			return matchingOwner.get();
		} else {
			return null;
		}
	}

	private Animal convertStringToAnimal(String animalId, List<Animal> animalList) {

		if (animalId.isBlank() || animalId == null) {
			return null;
		}

		Long currentId = Long.valueOf(animalId);
		Optional<Animal> matchingAnimal = animalList.stream().filter(a -> a.getId().equals(currentId)).findFirst();

		if (matchingAnimal.isPresent()) {
			return matchingAnimal.get();
		} else {
			return null;
		}
	}

	private Breed convertStringToBreed(String breedId, List<Breed> breedList) {
		Long currentId = Long.valueOf(breedId);
		Optional<Breed> matchingBreed = breedList.stream().filter(b -> b.getId().equals(currentId)).findFirst();

		if (matchingBreed.isPresent()) {
			return matchingBreed.get();
		} else {
			return null;
		}
	}

}