package com.example.vetapp.core.payload.in;

import java.util.ArrayList;

import com.example.vetapp.core.domain.AppConfig;

public class SaveSettingRequest {

	private ArrayList<AppConfig> configList;

	public ArrayList<AppConfig> getConfigList() {
		return configList;
	}

	public void setConfigList(ArrayList<AppConfig> configList) {
		this.configList = configList;
	}

	@Override
	public String toString() {
		return "SaveSettingRequest [configList=" + configList+ "]";
	}

	
}
