package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.Breed;

public class BreedListItem {

	private Long id;
	private String breedName;

	public BreedListItem() {
	}

	public BreedListItem(Breed breed) {
		if (breed != null) {
			this.id = breed.getId();
			this.breedName = breed.getBreedName();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBreedName() {
		return breedName;
	}

	public void setBreedName(String breedName) {
		this.breedName = breedName;
	}

}
