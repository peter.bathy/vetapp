package com.example.vetapp.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vetapp.core.domain.Owner;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {

	@Query("SELECT o FROM Owner o WHERE o.active = TRUE AND CONCAT( o.name, ' ', o.city, ' ', o.address, ' ', o.zipCode  )  LIKE %?1% ORDER BY o.name ")
	Page<Owner> searchOwnerDetailsLimit20(String searchWord, Pageable pageable);
   
}
