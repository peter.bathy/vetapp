package com.example.vetapp.core.payload.out;

import java.time.format.DateTimeFormatter;

import com.example.vetapp.core.domain.Animal;

public class SimpleAnimalDetails {
	private Long animalId;
	private String species;
	private String breed;
	private String animalName;
	private String dateOfDeath;

	public SimpleAnimalDetails() {
	}

	public SimpleAnimalDetails(Animal animal) {
		this.animalId = animal.getId();
		if (animal.getBreed() != null) {
			this.species = animal.getBreed().getBreedName();
			this.breed = animal.getBreed().getSpecies().getSpeciesName();
		} else {
			this.species = "";
			this.breed = "";
		}
		this.animalName = animal.getName();
		if (animal.getDateOfDeath() != null) {
			this.dateOfDeath = animal.getDateOfDeath().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

}
