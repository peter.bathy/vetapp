package com.example.vetapp.core.domain;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class AnimalWeight {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long animalId;

	private LocalDate weightingDate;

	private Float weight;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public LocalDate getWeightingDate() {
		return weightingDate;
	}

	public void setWeightingDate(LocalDate weightingDate) {
		this.weightingDate = weightingDate;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

}
