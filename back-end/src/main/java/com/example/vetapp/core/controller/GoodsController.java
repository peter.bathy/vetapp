package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.domain.Goods;
import com.example.vetapp.core.domain.GoodsTemplate;
import com.example.vetapp.core.payload.out.GoodListItem;
import com.example.vetapp.core.payload.out.SimpleGoodItem;
import com.example.vetapp.core.payload.out.SimpleGoodsTemplateItem;
import com.example.vetapp.core.payload.out.VaccineListItem;
import com.example.vetapp.core.service.GoodsService;
import com.example.vetapp.core.service.SearchService;

@RestController
@RequestMapping("/api/goods")
public class GoodsController {

	private GoodsService goodsService;
	private SearchService searchService;

	@Autowired
	public GoodsController(GoodsService goodsService, SearchService searchService) {
		this.goodsService = goodsService;
		this.searchService = searchService;
	}

	@GetMapping
	public ResponseEntity<List<GoodListItem>> getAllGoods() {
		return new ResponseEntity<>(goodsService.getAllGoods(), HttpStatus.OK);
	}

	@GetMapping("/vaccines")
	public ResponseEntity<List<VaccineListItem>> getAllActiveVaccines() {
		return new ResponseEntity<>(goodsService.getAllActiveVaccines(), HttpStatus.OK);
	}

	@GetMapping("/templates")
	public ResponseEntity<List<SimpleGoodsTemplateItem>> getAllTemplates() {
		return new ResponseEntity<>(goodsService.getAllTemplates(), HttpStatus.OK);
	}

	@GetMapping("/active-goods")
	public ResponseEntity<List<SimpleGoodItem>> getAllActiveGoods() {
		return new ResponseEntity<>(goodsService.getAllActiveGoods(), HttpStatus.OK);
	}

	@PostMapping("/save-template")
	public ResponseEntity<?> saveTemplate(@RequestBody GoodsTemplate template) {

		goodsService.saveTemplate(template);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/delete-template")
	public ResponseEntity<?> deleteTemplate(@RequestBody GoodsTemplate template) {

		goodsService.deleteTemplate(template);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveOwner(@RequestBody List<Goods> details) {

		goodsService.saveGoods(details);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/search/{searchValues}")
	public ResponseEntity<List<Goods>> search(@PathVariable(value = "searchValues") String searchType,
			String searchWord) {

		return new ResponseEntity<>(searchService.searchGoodsDetails(searchWord, searchType), HttpStatus.OK);
	}

	@GetMapping("/search/templates/{searchWord}")
	public ResponseEntity<?> searchTemplates(@PathVariable String searchWord) {

		return new ResponseEntity<>(searchService.searchTemplates(searchWord), HttpStatus.OK);
	}
}
