package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.GoodsTemplate;

public class SimpleGoodsTemplateItem {

	private Long id;
	private String templateName;
	private String templateBody;

	public SimpleGoodsTemplateItem() {
	}

	public SimpleGoodsTemplateItem(GoodsTemplate template) {
		this.id = template.getId();
		this.templateName = template.getTemplateName();
		this.templateBody = template.getTemplateBody();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateBody() {
		return templateBody;
	}

	public void setTemplateBody(String templateBody) {
		this.templateBody = templateBody;
	}
}
