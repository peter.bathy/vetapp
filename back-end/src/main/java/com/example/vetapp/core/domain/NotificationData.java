package com.example.vetapp.core.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class NotificationData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long animalId;
	private Long ownerId;
	private Long eventId;
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private NotificationTypeBaseValues emailBaseDatas;
	private String emailTo;
	private LocalDateTime sendingTime;
	private Boolean letterSent;
	@Column(columnDefinition = "TEXT")
	private String emailBody;
	@Column(columnDefinition = "TEXT")
	private String sendingError;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAnimalId() {
		return animalId;
	}

	public void setAnimalId(Long animalId) {
		this.animalId = animalId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public LocalDateTime getSendingTime() {
		return sendingTime;
	}

	public void setSendingTime(LocalDateTime sendingTime) {
		this.sendingTime = sendingTime;
	}

	public Boolean getLetterSent() {
		return letterSent;
	}

	public void setLetterSent(Boolean letterSent) {
		this.letterSent = letterSent;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public NotificationTypeBaseValues getEmailBaseDatas() {
		return emailBaseDatas;
	}

	public void setEmailBaseDatas(NotificationTypeBaseValues emailBaseDatas) {
		this.emailBaseDatas = emailBaseDatas;
	}

	public String getSendingError() {
		return sendingError;
	}

	public void setSendingError(String sendingError) {
		this.sendingError = sendingError;
	}
	
	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	@Override
	public String toString() {
		return "EmailData [id=" + id + ", animalId=" + animalId + ", eventId=" + eventId + ", emailBaseDatas="
				+ emailBaseDatas + ", emailTo=" + emailTo + ", sendingTime=" + sendingTime + ", letterSent="
				+ letterSent + ", emailBody=" + emailBody + "]";
	}

}
