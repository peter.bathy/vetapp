package com.example.vetapp.core.payload.out;

import com.example.vetapp.core.domain.Color;

public class ColorListItem {

	private Long colorId;
	private String colorName;

	public ColorListItem() {
	}

	public ColorListItem(Color color) {
		if (color != null) {
			this.colorId = color.getId();
			this.colorName = color.getColorName();
		}
	}

	public Long getColorId() {
		return colorId;
	}

	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}

	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

}
