package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.domain.NotificationData;
import com.example.vetapp.core.payload.in.ReminderEmailDatas;
import com.example.vetapp.core.payload.out.ApiResponse;
import com.example.vetapp.core.payload.out.OwnerDetails;
import com.example.vetapp.core.service.NotificationService;

@RestController
@RequestMapping("/api/email")
public class EmailController {

	private NotificationService emailService;

	@Autowired
	public EmailController(NotificationService emailService) {
		this.emailService = emailService;
	}

	@PostMapping("/eventReminder")
	public ResponseEntity<?> sendReminder(@RequestBody ReminderEmailDatas reminderDatas) {

		try {
			emailService.sendReminder(reminderDatas);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ApiResponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping("/vaccination-reminder-registration")
	public ResponseEntity<?> registerVaccinationReminder(@RequestBody OwnerDetails ownerDetails) {
		try {
			emailService.vaccineReminderRegistration(ownerDetails);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ApiResponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/emails-by-animal/{id}")
	public ResponseEntity<?> getEmailsByAnimalId(@PathVariable Long id) {
		return new ResponseEntity<>(emailService.getEmailsByAnimalId(id), HttpStatus.OK);
	}
}
