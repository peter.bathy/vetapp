package com.example.vetapp.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vetapp.core.domain.Owner;
import com.example.vetapp.core.payload.out.ApiResponse;
import com.example.vetapp.core.payload.out.OwnerDetails;
import com.example.vetapp.core.payload.out.OwnerDetailsWithAnimalList;
import com.example.vetapp.core.payload.out.SimpleOwnerDetails;
import com.example.vetapp.core.payload.out.SimpleOwnerDetailsWithAnimals;
import com.example.vetapp.core.service.OwnerService;
import com.example.vetapp.core.service.SearchService;

@RestController
@RequestMapping("/api/owner")
public class OwnerController {

	private OwnerService ownerService;
	private SearchService searchService;

	@Autowired
	public OwnerController(OwnerService ownerService, SearchService searchService) {
		this.ownerService = ownerService;
		this.searchService = searchService;
	}
	
	@GetMapping
    public ResponseEntity<List<SimpleOwnerDetails>> getAllOwner() {
        return new ResponseEntity<>(ownerService.getAllOwners(), HttpStatus.OK);
    }

	@GetMapping("/{id}")
	public ResponseEntity<?> getOwnerById(@PathVariable Long id) {

		Owner owner = ownerService.getOwnerById(id);

		if (owner != null) {
			return new ResponseEntity<>(new OwnerDetails(owner), HttpStatus.OK);
		}
		return new ResponseEntity<>(new ApiResponse(false, "Owner is not found with given id!"), HttpStatus.NOT_FOUND);
	}

	@GetMapping("/withlist/{id}")
	public ResponseEntity<?> getOwnerByIdWithList(@PathVariable Long id) {

		Owner owner = ownerService.getOwnerById(id);

		if (owner != null) {
			return new ResponseEntity<>(new OwnerDetailsWithAnimalList(owner), HttpStatus.OK);
		}
		return new ResponseEntity<>(new ApiResponse(false, "Owner is not found with given id!"), HttpStatus.NOT_FOUND);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveOwner(@RequestBody OwnerDetails owner) {

		OwnerDetails savedOwner = ownerService.saveOwner(owner);

		return new ResponseEntity<>(savedOwner, HttpStatus.OK);
	}
	
	@GetMapping("/search/{searchWord}")
	public ResponseEntity<List<SimpleOwnerDetailsWithAnimals>> search(@PathVariable String searchWord) {

			return new ResponseEntity<>(searchService.searchOwnerDetails(searchWord), HttpStatus.OK);
	}
	
}
