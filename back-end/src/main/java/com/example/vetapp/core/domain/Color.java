package com.example.vetapp.core.domain;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "colors")
public class Color {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String colorName;

	@OneToMany(mappedBy = "color", cascade = CascadeType.ALL)
	private List<Animal> animals;

	public Color() {
	}

	public Color(String colorName) {
		this.id = null;
		this.animals = null;
		this.colorName = colorName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	public List<Animal> getAnimals() {
		return animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}
	
}
