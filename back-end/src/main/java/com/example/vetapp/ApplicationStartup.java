package com.example.vetapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.vetapp.core.domain.Breed;
import com.example.vetapp.core.domain.Color;
import com.example.vetapp.core.domain.Role;
import com.example.vetapp.core.domain.RoleName;
import com.example.vetapp.core.domain.Species;
import com.example.vetapp.core.domain.User;
import com.example.vetapp.core.exception.AppException;
import com.example.vetapp.core.repository.BreedRepository;
import com.example.vetapp.core.repository.ColorRepository;
import com.example.vetapp.core.repository.RoleRepository;
import com.example.vetapp.core.repository.SpeciesRepository;
import com.example.vetapp.core.repository.UserRepository;
import com.example.vetapp.core.service.AnimalService;
import com.example.vetapp.core.service.AppConfigService;
import com.example.vetapp.core.service.MedicalRecordService;
import com.example.vetapp.core.service.NotificationService;
import com.example.vetapp.core.service.OwnerService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

	private static final Logger logger = Logger.getLogger("ApplicationStartup.class");

	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private ColorRepository colorRepository;
	private PasswordEncoder passwordEncoder;
	private NotificationService notificationService;

	@Autowired
	public ApplicationStartup(UserRepository userRepository, RoleRepository roleRepository,
			ColorRepository colorRepository, PasswordEncoder passwordEncoder, NotificationService notificationService) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.colorRepository = colorRepository;
		this.passwordEncoder = passwordEncoder;
		this.notificationService = notificationService;
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {

		Role newRole = new Role();
		newRole.setId(1L);
		newRole.setName(RoleName.ROLE_ADMIN);
		roleRepository.save(newRole);

		newRole.setId(2L);
		newRole.setName(RoleName.ROLE_USER);
		roleRepository.save(newRole);

		if (isUserTableEmpty() == true) {
			User user = new User("Pesterzsébeti Állatorvosi Rendelő", "vetapp", "info@erzsebetvet.hu", "123456");

			user.setPassword(passwordEncoder.encode(user.getPassword()));

			Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
					.orElseThrow(() -> new AppException("User Role not set."));

			user.setRoles(Collections.singleton(userRole));

			userRepository.save(user);
		}

		if (isNoColors() == true) {
			List<String> colors = new ArrayList<String>(Arrays.asList("só-bors", "rozsdavörös", "drapp", "harlekin",
					"zöld", "szürke", "vaddisznó", "mahagóni", "tricolor", "cameo", "fókabarna", "barna-spriccelt",
					"sárga", "barna-fehér", "mályva", "vadas", "fekete-cser", "fekete-szürke", "sárga-zöld",
					"gold-sable", "fehér-kék", "arany-fehér", "barna-szürke", "sárga-fehér", "őzbarna", "zöldes-barna",
					"zobel", "acélkék-cser", "bicolor", "vörös-cirmos", "ólomszürke", "kék", "fehér-barna tarka",
					"szürkés-zöld", "zsemle", "arany", "teknőctarka", "ezüst", "barna-vörös", "rajzos", "chok. and tan",
					"apricot", "márvány", "rajzos-csíkos", "ordas", "fehér", "palaszürke", "vörös-fehér", "vörös",
					"acélcolorpoint", "csokoládé", "barna", "cirmos", "fehér-szürke", "fakó", "barna-ordas", "krém",
					"zöld-sárga-fekete", "fehér-vörös", "fekete-sárga", "szürke-cirmos", "szürke-fehér", "fehér-ordas",
					"fehér-cirmos", "fekete-barna", "fehér-fekete", "zsemlesárga", "fekete-vörös", "black and tan",
					"fekete-fehér", "blauschimmel", "kék-krém", "mogyoró", "blue-tan", "fekete", "cifra", "fawn",
					"red grizly", "cirmos-fehér", "füst", "blue-merle", "kék-fehér", "szürke-ordas", "fekete-spriccelt",
					"fehér-fekete pettyes", "fekete-tigriscsíkos", "orange-schimmel", "sorell", "fekete-ezüst",
					"orange-belton", "homok", "maszkosfakó", "beige", "tigriscsíkos-fehér", "tarka", "blue", "kávé",
					"tigriscsíkos", "fóka jegyű", "csíkos", "orange", "red merle", "rózsaszín", "lila",
					"dalmát tricolor"));
			List<Color> newColorList = new ArrayList<Color>();

			for (String color : colors) {
				newColorList.add(new Color(color));
			}
			colorRepository.saveAll(newColorList);
		}
		
		logger.info("Inícializálás befejeződött.");
		
		notificationService.scheduleVaccinationNotification();
	}

	boolean isUserTableEmpty() {
		return (userRepository.count() == 0);
	}

	boolean isNoColors() {
		return (colorRepository.count() == 0);
	}
}
