package com.example.vetapp.core.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MedicalRecordServiceTest {

	private MedicalRecordService mrc;

	@BeforeEach
	void setup() {
		mrc = new MedicalRecordService();
	}

	@Test
	@DisplayName("Óra perc átalakítás")
	void testHourMinuteConvert() {

		String str = "2020-02-02 00:00";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime baseTime = LocalDateTime.parse(str, formatter);
		BigDecimal hourMinuteRaw = new BigDecimal("11.30");
		assertEquals("2020-02-02T11:30", mrc.convertHourMinute(baseTime, hourMinuteRaw).toString());
	}
}
