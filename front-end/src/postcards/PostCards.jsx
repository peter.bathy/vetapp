import React from 'react';
import PostCardDataListing from './PostCardDataListing';
import PostCardDateSelector from './PostCardDateSelector';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

const steps = ['Hónapválasztás', 'Címlista ellenőrzése, nyomtatás'];

export default function PostCards() {

    const [activeStep, setActiveStep] = React.useState(0);
    const [animalList, setAnimalList] = React.useState([]);

    const handleNext = () => {
        setActiveStep(activeStep + 1);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

   /* const printList = () => {
        window.print();
    }
*/
    const deleteItem = (index) => {

        const newList = [...animalList];
        newList.splice(index, 1);
        setAnimalList(newList);
    }

    const getStepContent = (step) => {
        switch (step) {
            case 0:
                return <PostCardDateSelector handleNext={handleNext} setAnimalList={setAnimalList} />;
            case 1:
                return <PostCardDataListing animalList={animalList} setAnimalList={setAnimalList} deleteItem={deleteItem}
                />;
            default:
                throw new Error('Unknown step');
        }
    }

    return (
        <div>
            <div className={'d-print-none'}>
                <div className="d-flex align-items-end justify-content-between">
                    <div>
                        <span className="mb-0 h5"> Levelezőlapok</span><br />
                        <span className="mb-0 text-secondary"> {steps[activeStep]}</span>
                    </div>
                    {activeStep === 1 &&
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button type="button" className="btn btn-outline-secondary btn-sm" onClick={handleBack} ><FontAwesomeIcon icon={faArrowLeft} /> Vissza</button>
                            {/*<button type="button" className="btn btn-outline-secondary btn-sm" onClick={printList}><FontAwesomeIcon icon={faPrint} /> Nyomtatás</button>*/}
                        </div>
                    }
                </div>
                <hr className='mt-2 mb-4' />
            </div>
            {getStepContent(activeStep)}
        </div>
    )
}