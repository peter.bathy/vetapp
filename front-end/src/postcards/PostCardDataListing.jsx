import React from 'react';
import { toast } from 'react-toastify';
import moment from 'moment';
import ListSkeleton from '../common/skeletons/ListSkeleton';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfo, faListUl, faTrash } from '@fortawesome/free-solid-svg-icons';

export default function PostCardDataListing(props) {

    //const [printedList, setPrintedList] = React.useState([]);

    const [treatmentDetails, setTreatmentDetails] = React.useState({
        animalName: '',
        animalId: 1,
        lastTreatments: [
            {
                dateOfTreatment: '2021.01.01. - 00:00',
                treatmentDetails: ''
            },
            {
                dateOfTreatment: '2021.01.01. - 00:00',
                treatmentDetails: ''
            }, {
                dateOfTreatment: '2021.01.01. - 00:00',
                treatmentDetails: ''
            }
        ]
    })

    /*useEffect(() => {
        const newList = props.animalList.filter((v, i, a) => a.findIndex(t => (t.ownerName === v.ownerName && t.address === v.address)) === i);

        setPrintedList(newList);
    }, [props.animalList]);*/

    const handleClose = () => {
        setTreatmentDetails({
            animalName: '',
            animalId: 1,
            lastTreatments: [
                {
                    dateOfTreatment: '2021.01.01. - 00:00',
                    treatmentDetails: ''
                },
                {
                    dateOfTreatment: '2021.01.01. - 00:00',
                    treatmentDetails: ''
                }, {
                    dateOfTreatment: '2021.01.01. - 00:00',
                    treatmentDetails: ''
                }
            ]
        })
    };

    const getLastTreatment = animal => {

        axiosCall.get('/medicalrecord/lastrecords/' + animal.animalId
        )
            .then(response => {
                setTreatmentDetails({
                    animalName: animal.animalName,
                    animalId: animal.animalId,
                    lastTreatments: response.data
                });

            }).catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }

    /*const capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + (string.slice(1)).toLowerCase();
    }*/

    return (
        <div>
            <div className={'d-print-none'}>
                <p>
                    A lista elemeire kattintva meg lehet nézni az utolsó kórelőzményt az X-re kattintva pedig törölhetőek
                </p>
                <br />
            </div>
            <div className={'d-print-none'}>
                <>
                    {props.animalList.length === 0 ?
                        <ListSkeleton />
                        :
                        <ul className="list-group list-group-flush">
                            {props.animalList.map((animal, index) => {
                                return (
                                    <li className='list-group-item list-group-item-action' key={index}>
                                        <div className="d-flex align-items-end justify-content-between">
                                            <div>
                                                <span>{animal.animalName + ', ' + animal.species + ' ' + animal.breed}</span><br />
                                                <small className='text-secondary'>{animal.ownerName + ', ' + animal.zipCode + ' ' + animal.city + ' ' + animal.address}</small>
                                            </div>
                                            <div>
                                                <button type="button" className="btn btn-light btn-sm me-2" data-bs-toggle="modal" data-bs-target='#treatmentDetailsModal' onClick={() => getLastTreatment(animal)}><FontAwesomeIcon icon={faInfo} /></button>
                                                <button type="button" className="btn btn-light btn-sm" onClick={() => props.deleteItem(index)}><FontAwesomeIcon icon={faTrash} /></button>
                                            </div>
                                        </div>
                                    </li>
                                );
                            })}
                        </ul>
                    }
                </>
            </div>

            {/*<div className={'justPrint'}>
                <div className={'postCard'}>
                    <List>
                        {printedList.map((animal, index) => {
                            return (
                                <React.Fragment key={animal.animalId}>
                                    <ListItem dense button
                                        className={'postCardBreak'}
                                        onClick={() => getLastTreatment(animal)}
                                    >
                                        <div className={'postCardHeader'}>Pesterzsébeti Állatorvosi Rendelő<br />
                                            1202 Budapest, Ipolyság u. 2.
                                        </div>
                                        <ListItemText>
                                            <Typography
                                                className={'ownerName'}
                                            >
                                                {animal.ownerName}
                                            </Typography>
                                            <Typography
                                                className={'zipCode'}
                                            >
                                                {animal.zipCode}
                                            </Typography>
                                            <Typography
                                                className={'city'}
                                            >
                                                {capitalizeFirstLetter(animal.city)}
                                            </Typography>
                                            <Typography
                                                className={'address'}
                                            >
                                                {animal.address}
                                            </Typography>
                                        </ListItemText>
                                    </ListItem>
                                </React.Fragment>
                            );
                        })}
                    </List>
                </div>
                    </div>*/}
            <div className="modal fade" id="treatmentDetailsModal" tabIndex="-1" aria-labelledby="treatmentDetailsModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="treatmentDetailsModalLabel">{treatmentDetails.animalName}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}></button>
                        </div>
                        <div className="modal-body">
                            <div className='mb-3'>
                                <strong>{moment(treatmentDetails.lastTreatments[0].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                <hr className='mt-1' />
                                {treatmentDetails.lastTreatments[0].vaccination &&
                                    <div>
                                        {treatmentDetails.lastTreatments[0].vaccination}
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].anamnesis &&
                                    <div>
                                        <small className='text-secondary'>Anamnézis</small><br />
                                        <span className='ps-2'> {treatmentDetails.lastTreatments[0].anamnesis}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].symptoms &&
                                    <div>
                                        <small className='text-secondary'>Szimptóma</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].symptoms}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].therapy &&
                                    <div>
                                        <small className='text-secondary'>Kezelés</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].therapy}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].otherDetails &&
                                    <div>
                                        <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].otherDetails}</span>
                                    </div>
                                }
                                <div className="collapse" id={"moreTreatmentCollapse"} >
                                    {treatmentDetails.lastTreatments[1] &&
                                        <div className='mt-3'>
                                            <strong>{moment(treatmentDetails.lastTreatments[1].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                            <hr className='mt-2' />
                                            {treatmentDetails.lastTreatments[1].vaccination &&
                                                <div>
                                                    {treatmentDetails.lastTreatments[1].vaccination}
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].anamnesis &&
                                                <div>
                                                    <small className='text-secondary'>Anamnézis</small><br />
                                                    <span className='ps-2'> {treatmentDetails.lastTreatments[1].anamnesis}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].symptoms &&
                                                <div>
                                                    <small className='text-secondary'>Szimptóma</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].symptoms}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].therapy &&
                                                <div>
                                                    <small className='text-secondary'>Kezelés</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].therapy}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].otherDetails &&
                                                <div>
                                                    <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].otherDetails}</span>
                                                </div>
                                            }
                                        </div>}
                                    {treatmentDetails.lastTreatments[2] &&
                                        <div className='mt-3'>
                                            <strong> {moment(treatmentDetails.lastTreatments[2].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                            <hr className='mt-2' />
                                            {treatmentDetails.lastTreatments[2].vaccination &&
                                                <div>
                                                    {treatmentDetails.lastTreatments[2].vaccination}
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].anamnesis &&
                                                <div>
                                                    <small className='text-secondary'>Anamnézis</small><br />
                                                    <span className='ps-2'> {treatmentDetails.lastTreatments[2].anamnesis}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].symptoms &&
                                                <div>
                                                    <small className='text-secondary'>Szimptóma</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].symptoms}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].therapy &&
                                                <div>
                                                    <small className='text-secondary'>Kezelés</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].therapy}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].otherDetails &&
                                                <div>
                                                    <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].otherDetails}</span>
                                                </div>
                                            }
                                        </div>}
                                </div>
                            </div>
                            <div className="d-flex align-items-end justify-content-end">
                                <div>
                                    <button type="button" className="btn btn-outline-secondary btn-sm mt-2" data-bs-toggle="collapse" data-bs-target={"#moreTreatmentCollapse"} aria-expanded="false" aria-controls={"#moreTreatmentCollapse"}><FontAwesomeIcon icon={faListUl} /> Előzmény</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </div >
    )
}