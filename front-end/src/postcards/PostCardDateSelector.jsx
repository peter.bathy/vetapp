import React, { useEffect } from 'react';
import moment from 'moment';
import 'moment/locale/hu';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';

export default function PostCardDateSelector(props) {
    const [months, setMonths] = React.useState([{
        givenMonth: '',
        monthNumber: 1,
        givenYear: 2020
    }
    ]);

    useEffect(() => {
        const fillDateArray = () => {

            moment.locale('hu');

            let monthsArray = [];

            for (let i = 5; i > -1; i--) {

                let newDate = moment().subtract(i, 'months').subtract(1, 'year');

                monthsArray.push({
                    givenMonth: newDate.startOf("month").format('MMMM'),
                    monthNumber: newDate.startOf("month").format('MM'),
                    givenYear: newDate.startOf("year").format('YYYY')
                })
            }
            setMonths(monthsArray);
        };
        fillDateArray();
    }, []);

    const downloadGivenMonthList = (month) => {

        const bodyParameters = {
            fromDate: month.givenYear + '-' + month.monthNumber + '-01 00:00:00'
        };

        axiosCall.post('/medicalrecord/vaccination/outdated',
            bodyParameters
        )
            .then(response => {

                let sortedResponse = [...response.data].sort(compare);

                props.setAnimalList(sortedResponse);

            }).then(
                props.handleNext()
            ).catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
            .finally(
            //setLoading(false)
        )
    }

    const compare = (a, b) => {
        if (a.ownerName < b.ownerName) {
            return -1;
        }
        if (a.ownerName > b.ownerName) {
            return 1;
        }
        return 0;
    }

    return (
        <div>
            <p>
                Válaszd ki, melyik hónapra vonatkozó értesítéseket szeretnéd kinyomtatni:
            </p>
            <br />
            <div className="container-fluid ps-0 pe-0">
                <div className="row g-3 row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-xxl-6">
                    {months.map((month, index) =>
                        <div className="col" key={index}>
                            <button className="btn btn-light h-100 w-100 shadow-sm text-start" type="button" onClick={() => downloadGivenMonthList(month)} >
                                <span className='text-secondary'                            >
                                    {month.givenYear}
                                </span>
                                <h3>
                                    {month.givenMonth}
                                </h3>
                            </button>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}