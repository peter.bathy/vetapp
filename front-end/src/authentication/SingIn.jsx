import React, { Fragment } from 'react';
import { Helmet } from "react-helmet";
import { axiosCall, handleError } from '../utils/ApiService';
import { setWithExpiry } from '../utils/utils';
import { ACCESS_TOKEN } from "../utils/constants";
import { toast } from 'react-toastify';
import logo from '../allatdoki-logo.png';
import background from '../vetapp-hatter.jpg';
import { useNavigate } from "react-router-dom";
import { ToastContainer } from 'react-toastify';

export default function SignIn() {

    let navigate = useNavigate();
    const [userName, setUserName] = React.useState('');
    const [password, setPassword] = React.useState('');

    const postDataHandler = (event) => {

        event.preventDefault();

        axiosCall.post('/auth/signin', {
            usernameOrEmail: userName,
            password: password,
        })
            .then(response => {
                setWithExpiry(ACCESS_TOKEN, response.data.accessToken, 50400000)

                navigate(`/`);
            })
            .catch(error => {
                toast.error(handleError(error))
            });
    }

    return (
        <Fragment>
            <Helmet>
                <title>VetApp</title>
            </Helmet>
            <main style={{
                backgroundImage: `url(${background})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
                minHeight: '100vh',
            }}>
                <div className="card shadow p-3 mb-5 bg-body rounded" style={{ "--bs-bg-opacity": 0.6 }}>
                    <div className="card-body">
                        <img className="loginScreenLogo" src={logo} alt="logo" />
                        <br /><br />
                        <form autoComplete="off" noValidate onSubmit={postDataHandler}>
                            <p className='mb-0 text-start text-white'>Felhasználónév</p>
                            <input
                                className="form-control "
                                name='userName'
                                value={userName}
                                autoFocus
                                onChange={(e) => {
                                    setUserName(e.target.value);
                                }}
                            />
                            <p className='mb-0 mt-3 text-white text-start'>Jelszó</p>
                            <input
                                className="form-control "
                                name='password'
                                type='password'
                                value={password}
                                onChange={(e) => {
                                    setPassword(e.target.value);
                                }}
                            />
                            <button type="submit" onClick={postDataHandler} className="btn btn-outline-dark w-100 mt-4">
                                Bejelentkezés
                            </button>
                        </form>
                    </div>
                </div>
            </main>
            <ToastContainer />
        </Fragment>
    )
}