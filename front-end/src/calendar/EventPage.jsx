import React, { useEffect } from 'react';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarCheck, faEnvelope, faExternalLinkAlt, faFloppyDisk, faHeartbeat, faIdCard, faLaptopMedical, faRadiation, faTooth, faTrash } from '@fortawesome/free-solid-svg-icons'
import AnimalSearchComponent from '../common/AnimalSearchComponent';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';

export default function EventPage({ handlePopupClose, localEvent, setLocalEvent, setActiveSubpage }) {

    const history = useNavigate();

    const [deleteIntent, setDeleteIntent] = React.useState(false);

    const addAnimalToEvent = animal => {
        let currentEvent = { ...localEvent };
        currentEvent.animal = animal;

        setLocalEvent(currentEvent);
    }

    const changeEventDetails = e => {

        const newEvent = { ...localEvent }

        if (e.target.name === 'start' || e.target.name === 'end') {
            if (e.target.name === 'start' && !newEvent.allDay) {
                let minuteDiff = moment(localEvent.end).diff(moment(localEvent.start), 'minutes');
                newEvent.start = moment(e.target.value).format('YYYY-MM-DDTHH:mm:ss');
                newEvent.end = moment(newEvent.start).add(minuteDiff, 'minutes');
            } else if (e.target.name === 'end' && !newEvent.allDay) {
                let oldDate = moment(localEvent.end).format('YYYY-MM-DD');
                newEvent.end = moment(oldDate + 'T' + e.target.value + ':00');
            } else {
                if (e.target.name === 'start') {
                    newEvent.start = moment(e.target.value).format('YYYY-MM-DD');
                } else if (e.target.name === 'end') {
                    newEvent.end = moment(e.target.value).format('YYYY-MM-DD');
                }
            }
        } else {
            newEvent[e.target.name] = e.target.value;
        }

        setLocalEvent(newEvent);
    }

    const handleSave = (sendNotify) => {

        if (!localEvent.id) {
            if (new Date(localEvent.start).getHours() === 0) {
                localEvent.allDay = true;
            }
        }

        const bodyParameters = {
            id: localEvent.id,
            title: localEvent.title,
            animal: localEvent.animal,
            description: localEvent.description,
            allDay: localEvent.allDay,
            start: moment(localEvent.start).format('YYYY-MM-DDTHH:mm:ss'),
            end: moment(localEvent.end).format('YYYY-MM-DDTHH:mm:ss'),
            eventTypes: localEvent.eventTypes,
        };
        axiosCall.post('/calendar/save',
            bodyParameters
        )
            .then(response => {
                if (sendNotify) {
                    setLocalEvent(response.data);
                    setActiveSubpage(1);
                } else {
                    handlePopupClose();
                    setDeleteIntent(false);
                }
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az esemény mentése közben'))
            })
    };

    const handleDelete = () => {

        axiosCall.delete('/calendar/delete/' + localEvent.id
        )
            .then(() => {
                handlePopupClose();
                setDeleteIntent(false);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az esemény törlése közben'))
            })
    };

    const checkeventType = type => {
        if (localEvent.eventTypes) {
            return localEvent.eventTypes.includes(type) ? 'btn-primary' : 'btn-outline-secondary';
        } else {
            return 'btn-outline-secondary';
        }
    };

    const changeType = type => {
        let currentTypes = [];
        if (localEvent.eventTypes) {
            currentTypes = [...localEvent.eventTypes]
            if (!currentTypes.includes(type)) {
                currentTypes.push(type);
            } else {
                const index = currentTypes.indexOf(type);
                if (index > -1) {
                    currentTypes.splice(index, 1);
                }
            }
        } else {
            currentTypes.push(type);
        }

        let currentEvent = { ...localEvent };
        currentEvent.eventTypes = currentTypes;
        setLocalEvent(currentEvent);
    }

    const deleteAnimal = () => {
        let currentEvent = { ...localEvent };
        currentEvent.animal = null;
        setLocalEvent(currentEvent);
    }

    const navigateToAnimal = (destination) => {
        handlePopupClose();
        setDeleteIntent(false);
        if (destination === 'samePage') {
            history('/animal/' + localEvent.animal.animalId);
        } else {
            window.open('/animal/' + localEvent.animal.animalId, "_blank")?.focus();
        }
    }

    return (
        <>
            <div className="modal-header">
                <h5 className="modal-title" id="eventModalLabel">Esemény {localEvent.id ? 'szerkesztése' : 'létrehozása'}</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handlePopupClose}></button>
            </div>
            <div className="modal-body">
                <form autoComplete="off">
                    {localEvent.allDay ?

                        <div className="container ps-0 pe-0 mb-2">
                            <div className="row gx-3">
                                <div className="col-6">
                                    <small className='text-secondary'>Kezdődátum</small>
                                    <input
                                        className="form-control"
                                        id='start1'
                                        name='start'
                                        key='start1'
                                        type='date'
                                        value={moment(localEvent.start).format('YYYY-MM-DD')}
                                        onChange={changeEventDetails}
                                        max="9999-12-31"
                                    />
                                </div>
                                <div className="col-6">
                                    <small className='text-secondary'>Esemény vége</small>
                                    <input
                                        className="form-control"
                                        name='end'
                                        key='end1'
                                        id='end1'
                                        type='date'
                                        value={moment(localEvent.end).format('YYYY-MM-DD')}
                                        onChange={changeEventDetails}
                                        max="9999-12-31"
                                    />
                                </div>
                            </div>
                        </div>
                        :
                        <div className="container ps-0 pe-0 mb-2">
                            <div className="row gx-3">
                                <div className="col-8">
                                    <small className='text-secondary'>Kezdés</small>
                                    <input
                                        className="form-control"
                                        name='start'
                                        key='start2'
                                        id='start2'
                                        type='datetime-local'
                                        value={moment(localEvent.start).format('YYYY-MM-DDTHH:mm')}
                                        onChange={changeEventDetails}
                                        max="9999-12-31"
                                    />
                                </div>
                                <div className="col-4">
                                    <small className='text-secondary'>Vége</small>
                                    <input
                                        className="form-control"
                                        name='end'
                                        key='end2'
                                        id='end2'
                                        type='time'
                                        value={moment(localEvent.end).format('HH:mm')}
                                        onChange={changeEventDetails}
                                        max="9999-12-31"
                                    />
                                </div>
                            </div>
                        </div>
                    }
                    <div className="mb-2">
                        <small className='text-secondary'>Leírás</small>
                        <textarea
                            className="form-control"
                            name='title'
                            rows='2'
                            autoFocus
                            value={localEvent.title || ''}
                            onChange={changeEventDetails}
                        />
                    </div>
                    {!localEvent.allDay ?
                        <>
                            <div className="mb-3">
                                <small className='text-secondary'>Kategória</small><br />
                                <button type="button" className={"btn me-2 mb-2 rounded-pill btn-sm " + checkeventType('tooth')} onClick={() => changeType('tooth')}><FontAwesomeIcon icon={faTooth} /> Fogazás</button>
                                <button type="button" className={"btn me-2 mb-2 rounded-pill btn-sm " + checkeventType('xray')} onClick={() => changeType('xray')}><FontAwesomeIcon icon={faRadiation} />  Röntgen</button>
                                <button type="button" className={"btn me-2 mb-2 rounded-pill btn-sm " + checkeventType('ultrasound')} onClick={() => changeType('ultrasound')}><FontAwesomeIcon icon={faLaptopMedical} />  Ultrahang</button>
                                <button type="button" className={"btn me-2 mb-2 rounded-pill btn-sm " + checkeventType('surgery')} onClick={() => changeType('surgery')}><FontAwesomeIcon icon={faHeartbeat} />  Műtét</button>
                                <button type="button" className={"btn me-2 mb-2 rounded-pill btn-sm " + checkeventType('consultation')} onClick={() => changeType('consultation')}><FontAwesomeIcon icon={faCalendarCheck} />  Konzultáció</button>
                            </div>
                            <div className="mb-2">
                                {localEvent.animal ?
                                    <>
                                        <div className="container-fluid ps-0 pe-0">
                                            <div className="row gx-0">
                                                <div className="col-6">
                                                    <small className='text-secondary'>Páciens adatok</small>
                                                </div>
                                                <div className="col-6 text-end">
                                                    <button type="button" className="btn btn-link btn-sm" onClick={deleteAnimal}>Törlés</button>
                                                </div>

                                            </div>
                                        </div>
                                        <div className="container-fluid ps-0 pe-0">
                                            <div className="row gx-3">
                                                <div className="col-10">
                                                    <button className="btn btn-light w-100 h-100 shadow-sm text-start py-4 mb-4" onClick={() => navigateToAnimal('samePage')} data-bs-dismiss="modal" data-bs-target="#eventModal" type="button">
                                                        <div className="container-fluid ps-0 pe-0">
                                                            <div className="row gx-1">
                                                                <div className="col-1">
                                                                    <span className='fs-4'><FontAwesomeIcon icon={faIdCard} /></span>
                                                                </div>
                                                                <div className="col-11">
                                                                    <p className='m-0'>
                                                                        {localEvent.animal.animalName}, {localEvent.animal.breed} - {localEvent.animal.species} <br />
                                                                        {localEvent.animal.ownerName} {(localEvent.animal.mobile || localEvent.animal.phone) && ' - '}{localEvent.animal.mobile} {localEvent.animal.phone}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                </div>
                                                <div className="col-2">
                                                    <button className="btn btn-light w-100 h-100 shadow-sm text-center mb-4" onClick={() => navigateToAnimal('blankPage')} data-bs-dismiss="modal" data-bs-target="#eventModal" type="button">
                                                        <span className='fs-4'><FontAwesomeIcon icon={faExternalLinkAlt} /></span>
                                                    </button>
                                                </div>

                                            </div>
                                        </div>

                                    </>
                                    :
                                    <>
                                        <small className='text-secondary'>Állat hozzáadása</small>
                                        <AnimalSearchComponent addAnimal={addAnimalToEvent} />
                                    </>
                                }
                            </div>
                        </>
                        :
                        null}
                </form>
            </div>
            <div className="modal-footer">
                {(localEvent.id && deleteIntent) &&
                    <button type="button" className="btn btn-danger btn-sm" data-bs-dismiss="modal" data-bs-target="#eventModal" onClick={handleDelete}><FontAwesomeIcon icon={faTrash} /> Törlés</button>
                }
                {(localEvent.id && !deleteIntent) &&
                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => setDeleteIntent(true)}><FontAwesomeIcon icon={faTrash} /> Törlés</button>
                }
                <button type="button" className="btn btn-primary btn-sm" data-bs-dismiss="modal" data-bs-target="#eventModal" onClick={() => handleSave(false)}><FontAwesomeIcon icon={faFloppyDisk} /> Mentés</button>
                {!localEvent.allDay && <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => handleSave(true)}><FontAwesomeIcon icon={faEnvelope} /> Értesítő</button>}
            </div>
        </>
    )
}