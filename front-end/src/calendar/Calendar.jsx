import React, { useCallback, useEffect } from 'react';
import { toast } from 'react-toastify';
import moment from 'moment';
import 'moment/locale/hu';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import CalendarDialog from './CalendarDialog';
import { axiosCall, handleError } from '../utils/ApiService';
import { Modal } from 'bootstrap';

export default function VetAppCalendar() {
    const localizer = momentLocalizer(moment);
    moment.locale('hu');
    localizer.segmentOffset = 0;

    const [openedEvent, setOpenedEvent] = React.useState({
        id: 0,
        title: '',
        animal: '',
        description: '',
        allDay: false,
        start: moment(),
        end: moment().add(1, 'hour'),
        eventTypes: [],
    });
    const [currentDate, setCurrentDate] = React.useState(new Date());
    const [currentView, setCurrentView] = React.useState('week');
    const [events, setEvents] = React.useState([
    ]);

    const getDatas = useCallback(() => {

        let startOfView = "2021-05-05T10:00:00";
        let endOfView = "2022-01-01T00:00:00";

        if (currentView === 'week') {
            startOfView = moment(currentDate).startOf('isoWeek');
            endOfView = moment(currentDate).endOf('isoWeek');
        }
        if (currentView === 'month') {
            startOfView = moment(currentDate).startOf('month').subtract(7, 'days')
            endOfView = moment(currentDate).endOf('month').add(7, 'days');
        }

        const bodyParameters = {
            intervalStart: startOfView,
            intervalEnd: endOfView
        };

        axiosCall.post('/calendar',
            bodyParameters
        )
            .then(response => {
                let appointments = response.data;

                for (let i = 0; i < appointments.length; i++) {
                    appointments[i].start = moment(appointments[i].start).toDate();
                    appointments[i].end = moment(appointments[i].end).toDate();
                }

                setEvents(appointments);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }, [currentDate, currentView]);

    useEffect(() => {
        getDatas();
    }, [getDatas]);

    useEffect(() => {
        getDatas();
    }, [currentDate, getDatas]);

    useEffect(() => {
        getDatas();
    }, [currentView, getDatas]);

    const handleSelect = (event) => {
        let currentEvent = { ...event }

        if (moment(currentEvent.start).format('HH') === '00') {
            currentEvent.allDay = true;
        }
        setOpenedEvent(currentEvent);
        const modal = new Modal('#eventModal', {
            backdrop: 'static'
        })
        modal.show();
    };

    const handleClose = () => {
        setOpenedEvent({
            id: 0,
            title: '',
            animal: '',
            description: '',
            allDay: false,
            start: moment(),
            end: moment().add(1, 'hour'),
            eventTypes: [],
        });
        getDatas();
    };

    const eventPropGetter = (event) => {
        let style = {
            //display: 'block',
        };

        let backgroundColor = '#85C8F2';
        let borderColor = '#fee440';
        let color = '#000000';

        if (event.eventTypes.length > 1) {
            backgroundColor = '#f15bb5';
            borderColor = '#8f0758'
        } else {
            switch (event.eventTypes[0]) {
                case 'surgery':
                    backgroundColor = '#fee440';
                    borderColor = '#c3a807';
                    color = '#000000';
                    break;
                case 'xray':
                    backgroundColor = '#9b5de5';
                    borderColor = '#460791';
                    break;
                case 'tooth':
                    backgroundColor = '#00f5d4';
                    borderColor = '#08a18c';
                    color = '#000000';
                    break;
                case 'ultrasound':
                    backgroundColor = '#00bbf9';
                    borderColor = '#0b8bb5';
                    color = '#000000';
                    break;
                case 'consultation':
                    backgroundColor = '#f35422';
                    borderColor = '#a36754';
                    color = '#000000';
                    break;
                default:
                    return {};
            }
        }

        style.backgroundColor = backgroundColor;
        style.borderColor = borderColor;
        style.color = color;

        return {
            style: style,
        };
    }

    const dayPropGetter = (date) => {

        /*//console.log(date)

        let today = moment();

        let style = {
            //display: 'block',
        };

        let backgroundColor = '';

        if (moment(date).isBefore(today, 'day')) {
            backgroundColor = '#eeeeee';
        } else {
            backgroundColor = '';
        }

        style.backgroundColor = backgroundColor;

        return {
            style: style,
        };*/
    }

    const CustomEvent = ({ event }) => (
        <>
            {!event.allDay && <small> {moment(event.start).format('HH:mm')} - </small>}{event.title}
        </>
    );

    return (
        <div>
            <div className="d-flex align-items-end justify-content-between">
                <span className="mb-0 h5">Naptár</span>
            </div>
            <hr className='mt-2' />
            <div style={{ height: '85vh' }}>
                <Calendar
                    selectable
                    popup
                    events={events}
                    min={new Date('2020-12-17T08:00:00')}
                    max={new Date('2020-12-17T20:00:00')}
                    step={15}
                    timeslots={4}
                    defaultView={Views.WEEK}
                    views={['week', 'month']}
                    defaultDate={moment().toDate()}
                    localizer={localizer}
                    onSelectEvent={event => handleSelect(event)}
                    onSelectSlot={event => handleSelect(event)}
                    onNavigate={(date) => {
                        setCurrentDate(date);
                    }}
                    onView={(view) => {
                        setCurrentView(view);
                    }}
                    components={{
                        event: CustomEvent,
                    }}
                    eventPropGetter={(eventPropGetter)}
                    dayPropGetter={(dayPropGetter)}
                    messages={{ next: "Következő", previous: "Előző", today: "Mai nap", week: "hét", month: "hónap" }}
                    className='mainCalendar'
                />
            </div>
            <CalendarDialog
                handleClose={handleClose}
                openedEvent={openedEvent}
            />
        </div >
    )
}