import React, { useEffect } from 'react';
import EventPage from './EventPage';
import EmailPage from './EmailPage';
import moment from 'moment';

export default function CalendarDialog({ openedEvent, handleClose }) {
    const [activeSubpage, setActiveSubpage] = React.useState(0);
    const [localEvent, setLocalEvent] = React.useState({
        id: null,
        title: '',
        animal: '',
        description: '',
        allDay: false,
        start: moment(),
        end: moment().add(1, 'hour'),
        eventTypes: [],
    });

    useEffect(() => {
        setLocalEvent({ ...openedEvent })
    }, [openedEvent]);

    const handlePopupClose = () => {
        handleClose();
        setActiveSubpage(0);
    }

    const subPages = [
        <EventPage handlePopupClose={handlePopupClose} localEvent={localEvent} setLocalEvent={setLocalEvent} setActiveSubpage={setActiveSubpage} />,
        <EmailPage handlePopupClose={handlePopupClose} localEvent={localEvent} />,
    ]

    return (
        <div className="modal fade" id="eventModal" tabIndex="-1" aria-labelledby="eventModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    {subPages[activeSubpage]}
                </div>
            </div>
        </div >
    )
}