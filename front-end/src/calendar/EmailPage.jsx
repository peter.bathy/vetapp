import React, { useEffect } from 'react';
import moment from 'moment';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';

export default function EmailPage({ localEvent, handlePopupClose }) {

    const [messageDetails, setMessageDetails] = React.useState({
        email: '',
        messageType: 'examination',
        emailText: '',
        eventId: 0
    })

    const messages = {
        "base": 'Az egyeztetett időpont: ' + moment(localEvent.start).format('YYYY. MMMM DD. HH:mm') + '.\nKérjük az időpont előtt legkésőbb tíz perccel megérkezni a rendelőbe.',
        "consultation": 'Az egyeztetett időpont: ' + moment(localEvent.start).format('YYYY. MMMM DD. HH:mm') + '.\nA konzultáció díja: 8 000 Ft.\nKedvencét nem kell elhoznia, viszont a súlyát mindenképpen mérjék meg és ezt az értéket mondják majd meg kollégánknak. Ez szükséges a gyógyszeres kúra részleteinek meghatározásához.\nKérjük az időpont előtt legkésőbb öt perccel megérkezni a rendelőbe.'
    }


    useEffect(() => {
        let mType = 'examination';
        let customText = messages.base;

        if (localEvent.eventTypes) {
            let currentTypes = [...localEvent.eventTypes];

            if (currentTypes.includes('tooth')) {
                mType = 'treatment';
            }

            if (currentTypes.includes('consultation')) {
                mType = 'consultation';
                customText = messages.consultation;
            }

            if (currentTypes.includes('surgery')) {
                mType = 'operation';
                customText = messages.base;
            }
        }

        let email = '';
        if (localEvent.animal && localEvent.animal.email) {
            email = localEvent.animal.email
        }

        setMessageDetails(
            {
                email: email,
                emailText: customText,
                messageType: mType,
            }
        )
    }, [localEvent]);

    const changeMessageDetails = e => {

        const newDetails = { ...messageDetails }
        newDetails[e.target.name] = e.target.value;

        if (e.target.name === 'messageType') {
            if (e.target.value === 'consultation') {
                newDetails.emailText = messages.consultation;
            } else {
                newDetails.emailText = messages.base;
            }
        }


        setMessageDetails(newDetails);
    }

    const sendReminder = () => {

        let animalId;

        if (localEvent.animal && localEvent.animal.animalId) {
            animalId = localEvent.animal.animalId
        }

        const bodyParameters = {
            email: messageDetails.email,
            messageType: messageDetails.messageType,
            eventStart: moment(localEvent.start).format('YYYY-MM-DDTHH:mm:ss'),
            eventId: localEvent.id,
            animalId: animalId,
            emailText: messageDetails.emailText,
        };
        axiosCall.post('/email/eventReminder',
            bodyParameters
        )
            .then(() => {
                handlePopupClose();
                toast.info('A levél el lett küldve a megadott címre')
            })
            .catch(error => {
                console.log(error.response)
                toast.error(handleError(error))
            })
    }

    return (
        <>
            <div className="modal-header">
                <h5 className="modal-title" id="eventModalLabel">Emlékeztető küldése</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handlePopupClose}></button>
            </div>
            <div className="modal-body">
                <form autoComplete="off">
                    <div className="mb-2">
                        <small className='text-secondary'>Címzett</small>
                        <input
                            className="form-control"
                            id="email"
                            type='email'
                            name='email'
                            value={messageDetails.email}
                            onChange={changeMessageDetails}
                        />
                    </div>
                    <div className="mb-2">
                        <small className='text-secondary'>Esemény típusa</small>
                        <select
                            className="form-select"
                            value={messageDetails.messageType}
                            onChange={changeMessageDetails}
                            name='messageType'
                        >
                            <option value={'operation'}>Műtét</option>
                            <option value={'treatment'}>Kezelés</option>
                            <option value={'examination'}>Vizsgálat</option>
                            <option value={'consultation'}>Konzultáció</option>
                        </select>
                    </div>
                    <div className="mb-2">
                        <small className='text-secondary'>Email szövege</small>
                        <textarea
                            rows='7'
                            name='emailText'
                            className="form-control"
                            value={messageDetails.emailText}
                            onChange={changeMessageDetails}
                        >
                        </textarea>
                    </div>
                </form>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal" data-bs-target="#eventModal" onClick={event => sendReminder(event)}><FontAwesomeIcon icon={faPaperPlane} /> Küldés</button>
            </div>
        </>
    )
}