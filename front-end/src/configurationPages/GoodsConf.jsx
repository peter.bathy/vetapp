import React, { useEffect, useCallback, useState } from 'react';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight, faCheck, faFloppyDisk, faPenToSquare, faPlus } from '@fortawesome/free-solid-svg-icons'
import Select from 'react-select';

export default function GoodsConf() {

    const [symptomsTemplate, setSymptomsTemplate] = React.useState('');
    const [therapyTemplate, setTherapyTemplate] = React.useState('');
    const [prescriptionTemplate, setPrescriptionTemplate] = React.useState('');
    const [currentDetails, setCurrentDetails] = useState({
        active: true,
        name: '',
        manufacturer: '',
        numberPerPack: 0,
        packPrice: 0,
        nettoPrice: 0,
        tax: 0.27,
        margin: 0,
        bruttoPrice: 0,
        sellPrice: 0,
        quantityUnit: 'db',
        baseQuantity: 1,
        comment: '',
        keyWords: [],
        id: -1,
    });
    const [goods, setGoods] = React.useState([]);
    const [filteredList, setFilteredList] = React.useState([]);
    const [nameSearchWord, setNameSearchWord] = React.useState('');
    const [manufacturerSearchWord, setManufacturerSearchWord] = React.useState('');

    const updateGoods = useCallback(() => {
        axiosCall.get('/goods'
        )
            .then(response => {
                setGoods(response.data);
                setFilteredList(response.data);
                setNameSearchWord('');
                setManufacturerSearchWord('');
            })
            .catch(error => {
                toast.error(handleError(error))
            });
    }, []);

    useEffect(() => {
        updateGoods();
    }, [updateGoods]);

    const handleSave = (rawArray) => {

        let bodyParameters = [];

        if (currentDetails.id > -1) {
            let attachements = [];
            if (symptomsTemplate !== '') {
                attachements.push({
                    attachementTypeNAme: 'symptomsTemplate',
                    attachementBody: symptomsTemplate
                })
            }
            if (therapyTemplate !== '') {
                attachements.push({
                    attachementTypeNAme: 'therapyTemplate',
                    attachementBody: therapyTemplate
                })
            }
            if (prescriptionTemplate !== '') {
                attachements.push({
                    attachementTypeNAme: 'prescriptionTemplate',
                    attachementBody: prescriptionTemplate
                })
            }
            currentDetails.attachements = attachements;

            bodyParameters.push(currentDetails);
        } else {
            bodyParameters = [...rawArray];
        }

        axiosCall.post('/goods/save',
            bodyParameters
        )
            .then(() => {
                handleClose();
                updateGoods();
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a termék mentése közben'))
            })
    };

    const handleClose = () => {
        setCurrentDetails({
            active: true,
            name: '',
            manufacturer: '',
            numberPerPack: 0,
            packPrice: 0,
            nettoPrice: 0,
            tax: 0.27,
            margin: 0,
            bruttoPrice: 0,
            sellPrice: 0,
            quantityUnit: 'db',
            baseQuantity: 1,
            comment: '',
            keyWords: [],
            id: -1,
        })
        setPrescriptionTemplate('');
        setSymptomsTemplate('');
        setTherapyTemplate('');
    }

    /* const copyGoods = () => {
         let rawArray = [];
         selectedRows.forEach(index => {
             let currentGoods = { ...goods[index - 1] }
             currentGoods.id = 0;
             currentGoods.name = currentGoods.name + ' (másolat)';
             rawArray.push(currentGoods);
         });
         handleSave(rawArray);
     }*/

    /*
     const handleBulkSave =()=>{
         let rawArray = [];
         selectedRows.forEach(index => {
             let currentGoods = { ...goods[index - 1] }
             rawArray.push(currentGoods);
         });
         handleSave(rawArray);
     }
     */

    const editGoodsDetails = index => {
        let goodsDetails = goods.find(element => element.id === index);

        /* goodsDetails.attachements.forEach(a => {
             switch (a.attachementTypeNAme) {
                 case 'symptomsTemplate':
                     setSymptomsTemplate(a.attachementBody);
                     break;
                 case 'therapyTemplate':
                     setTherapyTemplate(a.attachementBody);
                     break;
                 case 'prescriptionTemplate':
                     setPrescriptionTemplate(a.attachementBody);
                     break;
             }
         })*/

        setCurrentDetails(goodsDetails);
    };

    const handleChange = (event) => {
        let localDetails = { ...currentDetails }

        let key = event.target.name;
        const value = event.target.value;

        let tax = localDetails.tax < 1 ? (1 + Number(localDetails.tax)) : 1;
        let margin = localDetails.margin < 1 ? 1 + Number(localDetails.margin) : 1;

        if (key !== 'active') {
            localDetails[key] = value;
        }

        switch (key) {
            case 'packPrice':
                if (value > 0 && localDetails.numberPerPack > 0) {
                    localDetails.nettoPrice = Math.round(value / localDetails.numberPerPack);
                    localDetails.bruttoPrice = Math.round((value / localDetails.numberPerPack) * tax * margin);
                }
                break;
            case 'numberPerPack':
                if (value > 0 && localDetails.packPrice > 0) {
                    localDetails.nettoPrice = Math.round(localDetails.packPrice / value);
                    localDetails.bruttoPrice = Math.round((localDetails.packPrice / value) * tax * margin);
                }
                break;
            case 'tax':
                tax = value < 1 ? (1 + Number(value)) : 1;
                localDetails.bruttoPrice = Math.round(localDetails.nettoPrice * tax * margin);
                break;
            case 'margin':
                margin = value < 1 ? (1 + Number(value)) : 1;
                localDetails.bruttoPrice = Math.round(localDetails.nettoPrice * tax * margin);
                break;
            case 'nettoPrice':
                localDetails.bruttoPrice = Math.round(value * tax * margin);
                break;
            case 'bruttoPrice':
                localDetails.nettoPrice = Math.round(value / tax / margin);
                break;
            case 'active':
                localDetails.active = !localDetails.active;
                break;
            default:
                break;
        }

        setCurrentDetails(localDetails);
    }

    /* const handleTemplateChange = (event) => {
 
         let key = event.target.name;
         const value = event.target.value;
 
         switch (key) {
             case 'symptomsTemplate':
                 setSymptomsTemplate(value);
                 break;
             case 'therapyTemplate':
                 setTherapyTemplate(value);
                 break;
             case 'prescriptionTemplate':
                 setPrescriptionTemplate(value);
                 break;
             default:
                 break;
         }
     }
 */
    const options = [
        'oltás',
        'veszettség',
        'féregtelenítő',
        'okmány',
        'kedvezmény',
        'recept',
        'ivartalanítás',
        'kezelés',
        'vizsgálat',
        'gyógyszer',
        //'gyűjtőtétel',
    ]

    const optionsNew = [
        { value: 'oltás', label: 'oltás' },
        { value: 'veszettség', label: 'veszettség' },
        { value: 'féregtelenítő', label: 'féregtelenítő' },
        { value: 'okmány', label: 'okmány' },
        { value: 'kedvezmény', label: 'kedvezmény' },
        { value: 'recept', label: 'recept' },
        { value: 'ivartalanítás', label: 'ivartalanítás' },
        { value: 'kezelés', label: 'kezelés' },
        { value: 'vizsgálat', label: 'vizsgálat' },
        { value: 'gyógyszer', label: 'gyógyszer' },
    ]

    const copyPrice = () => {
        let localDetails = { ...currentDetails }

        localDetails.sellPrice = localDetails.bruttoPrice;

        setCurrentDetails(localDetails);
    }

    const createNew = () => {
        setCurrentDetails({
            active: true,
            name: '',
            manufacturer: '',
            numberPerPack: 0,
            packPrice: 0,
            nettoPrice: 0,
            tax: 0.27,
            margin: 0.4,
            bruttoPrice: 0,
            sellPrice: 0,
            quantityUnit: 'db',
            baseQuantity: 1,
            keyWords: [],
            id: 0,
        })
    }

    const filterList = event => {

        let currentList = [...goods];

        const name = event.target.name;
        const value = event.target.value;

        if (name === 'nameSearch') {
            setNameSearchWord(value);
            currentList = currentList.filter(item => item.name.toLowerCase().includes(value.toLowerCase()));
        } else {
            setManufacturerSearchWord(value);
            currentList = currentList.filter(item => item.manufacturer.toLowerCase().includes(value.toLowerCase()));
        }

        setFilteredList(currentList);

    }

    const onSelect = (selectedList) => {
        let details = { ...currentDetails }
        details.keyWords = selectedList
        setCurrentDetails(details);
    }

    /*const onRemove = (selectedList) => {
        let details = { ...currentDetails }
        details.keyWords = selectedList
        setCurrentDetails(details);
    }*/

    const MyMultiSelect = ({ values = [], options = [], onChange, ...props }) => (
        <Select {...props} value={values.map((value) => ({ label: value, value, }))}
            options={options.map((value) => ({ label: value, value, }))}
            onChange={(values) => {
                onChange(values.map((option) => option.value));
            }}
        />
    )

    return (
        <div>
            <div className="d-flex align-items-end justify-content-between">
                <span className="mb-0 h5">Temékek és szolgáltatások szerkesztése</span>
                <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={createNew} data-bs-toggle="modal" data-bs-target='#goodConfModal' ><FontAwesomeIcon icon={faPlus} /> Hozzáadás</button>
                    {/*selectedRows.size > 0 && <button type="button" className="btn btn-outline-secondary btn-sm" onClick={copyGoods}><FontAwesomeIcon icon={faCopy} /> Másolás</button>*/}
                </div>
            </div>
            <hr className='mt-2' />

            <table className="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col">
                            <input
                                className="form-control"
                                key='nameSearch'
                                name='nameSearch'
                                type='text'
                                placeholder='Név'
                                value={nameSearchWord}
                                onChange={filterList}
                            />
                        </th>
                        <th scope="col">
                            <input
                                className="form-control"
                                key='manufacturerSearch'
                                name='manufacturerSearch'
                                type='text'
                                placeholder='Gyártó'
                                value={manufacturerSearchWord}
                                onChange={filterList}
                            />
                        </th>
                        <th scope="col">Csomagár</th>
                        <th scope="col">Egység/csomag</th>
                        <th scope="col">Nettó egységár</th>
                        <th scope="col">Eladási ár</th>
                        <th scope="col">Alapegység</th>
                        <th scope="col">Kulcsszavak</th>
                        <th scope="col">Megjegyzés</th>
                        <th scope="col">Aktív</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {filteredList.map((data, index) => {
                        return (
                            <tr key={index}>
                                <td>{data.name}</td>
                                <td>{data.manufacturer}</td>
                                <td>{data.packPrice ? <p className='text-end m-0'>{data.packPrice} Ft.</p> : ''}</td>
                                <td>{data.numberPerPack ? <p className='text-end m-0'>{data.numberPerPack}</p> : ''}</td>
                                <td>{data.nettoPrice ? <p className='text-end m-0'>{data.nettoPrice} Ft.</p> : ''}</td>
                                <td>{data.sellPrice ? <p className='text-end m-0'>{data.sellPrice} Ft.</p> : ''}</td>
                                <td>{data.baseQuantity + ' ' + data.quantityUnit}</td>
                                <td>{data.keyWords.join(', ')}</td>
                                <td>{data.comment}</td>
                                <td>{data.active ? <FontAwesomeIcon icon={faCheck} /> : null}</td>
                                <td><button type='button' className="btn btn-light" data-bs-toggle="modal" data-bs-target='#goodConfModal' onClick={() => editGoodsDetails(data.id)}><FontAwesomeIcon icon={faPenToSquare} /> </button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div className="modal fade" id="goodConfModal" tabIndex="-1" aria-labelledby="goodConfModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="goodConfModalLabel">Termékek, szolgáltatások szerkesztése</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form autoComplete='off'>
                            <div className="modal-body">
                                <div className="container ps-0 pe-0 mb-2">
                                    <div className="row gx-3 gy-2">
                                        <div className="col-1">
                                            <small className='text-secondary'>Aktív</small><br />
                                            <input
                                                className="form-check-input"
                                                id='active'
                                                name='active'
                                                key='active'
                                                type="checkbox"
                                                value={currentDetails.active}
                                                checked={currentDetails.active}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-5">
                                            <small className='text-secondary'>Név</small>
                                            <input
                                                className="form-control"
                                                name='name'
                                                key='name'
                                                id='name'
                                                type='text'
                                                value={currentDetails.name}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-6">
                                            <small className='text-secondary'>Gyártó</small>
                                            <input
                                                className="form-control"
                                                name='manufacturer'
                                                key='manufacturer'
                                                id='manufacturer'
                                                type='text'
                                                value={currentDetails.manufacturer}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-4">
                                            <p className="mt-2">Egybe csomagolt termékek esetén a csomag nettó ára és a csomagolási darabszám:</p>
                                        </div>
                                        <div className="col-4">
                                            <small className='text-secondary'>Nettó csomagár</small>
                                            <input
                                                className="form-control"
                                                name='packPrice'
                                                key='packPrice'
                                                id='packPrice'
                                                type='number'
                                                value={currentDetails.packPrice}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-4">
                                            <small className='text-secondary'>Termék / csomag</small>
                                            <input
                                                className="form-control"
                                                name='numberPerPack'
                                                key='numberPerPack'
                                                id='numberPerPack'
                                                type='number'
                                                value={currentDetails.numberPerPack}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-2">
                                            <small className='text-secondary'>Nettó egységár</small>
                                            <input
                                                className="form-control"
                                                name='nettoPrice'
                                                key='nettoPrice'
                                                id='nettoPrice'
                                                type='number'
                                                value={currentDetails.nettoPrice}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-2">
                                            <small className='text-secondary'>ÁFA</small>
                                            <select
                                                className="form-select"
                                                name='tax'
                                                key='tax'
                                                id='tax'
                                                value={currentDetails.tax}
                                                onChange={handleChange}
                                            >
                                                <option value={0.27}>27%</option>
                                                <option value={0}>0%</option>
                                            </select>
                                        </div>
                                        <div className="col-2">
                                            <small className='text-secondary'>Árrés</small>
                                            <select
                                                className="form-select"
                                                name='margin'
                                                key='margin'
                                                id='margin'
                                                value={currentDetails.margin}
                                                onChange={handleChange}
                                            >
                                                <option value={0}>0%</option>
                                                <option value={0.1}>10%</option>
                                                <option value={0.2}>20%</option>
                                                <option value={0.3}>30%</option>
                                                <option value={0.4}>40%</option>
                                                <option value={0.5}>50%</option>
                                                <option value={0.6}>60%</option>
                                                <option value={0.7}>70%</option>
                                                <option value={0.8}>80%</option>
                                                <option value={0.9}>90%</option>
                                                <option value={1}>100%</option>
                                            </select>
                                        </div>
                                        <div className="col-2">
                                            <small className='text-secondary'>Bruttó egységár</small>
                                            <input
                                                className="form-control"
                                                name='bruttoPrice'
                                                key='bruttoPrice'
                                                id='bruttoPrice'
                                                type='number'
                                                value={currentDetails.bruttoPrice}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-1">
                                            <button type='button' className="btn btn-light mt-4 rounded-circle" onClick={copyPrice}><FontAwesomeIcon icon={faArrowRight} /></button>
                                        </div>
                                        <div className="col-3">
                                            <small className='text-secondary'>Eladási ár</small>
                                            <input
                                                className="form-control"
                                                name='sellPrice'
                                                key='sellPrice'
                                                id='sellPrice'
                                                type='number'
                                                value={currentDetails.sellPrice}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-3">
                                            <small className='text-secondary'>Egység</small>
                                            <select
                                                className="form-select"
                                                name='quantityUnit'
                                                key='quantityUnit'
                                                id='quantityUnit'
                                                value={currentDetails.quantityUnit}
                                                onChange={handleChange}
                                            >
                                                <option value=''></option>
                                                <option value='db'>db</option>
                                                <option value='ml'>ml</option>
                                                <option value='alkalom'>alkalom</option>
                                                <option value='mg'>mg</option>
                                                <option value='gramm'>gramm</option>
                                            </select>
                                        </div>
                                        <div className="col-3">
                                            <small className='text-secondary'>Alapérték</small>
                                            <input
                                                className="form-control"
                                                name='baseQuantity'
                                                key='baseQuantity'
                                                id='baseQuantity'
                                                type='number'
                                                value={currentDetails.baseQuantity}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <div className="col-6">
                                            <small className='text-secondary'>Kulcsszavak</small>
                                            <MyMultiSelect
                                                isMulti
                                                options={options}
                                                values={currentDetails.keyWords}
                                                onChange={(values) => {
                                                    onSelect(values);
                                                }}
                                            />
                                        </div>
                                        <div className="col-12">
                                            <small className='text-secondary'>Megjegyzés</small>
                                            <textarea
                                                className="form-control"
                                                name='comment'
                                                key='comment'
                                                id='comment'
                                                type='text'
                                                value={currentDetails.comment}
                                                onChange={handleChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type='button' className="btn btn-sm btn-outline-secondary" onClick={handleSave} data-bs-dismiss="modal" data-bs-target="#goodConfModal"><FontAwesomeIcon icon={faFloppyDisk} /> Mentés</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {/*
                <DialogContent>
                    <Grid container spacing={1}>
                        {(currentDetails.keyWords.indexOf('kezelés') > -1 || currentDetails.keyWords.indexOf('vizsgálat') > -1) &&
                            <>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        multiline
                                        id="symptomsTemplate"
                                        label="Vizsgálat sablon"
                                        value={symptomsTemplate}
                                        name="symptomsTemplate"
                                        margin="dense"
                                        onChange={handleTemplateChange}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        multiline
                                        id="therapyTemplate"
                                        label="Kezelés sablon"
                                        value={therapyTemplate}
                                        name="therapyTemplate"
                                        margin="dense"
                                        onChange={handleTemplateChange}
                                        variant="outlined"
                                    />
                                </Grid>
                            </>
                        }
                        {(currentDetails.keyWords.indexOf('recept') > -1) &&
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    multiline
                                    id="prescriptionTemplate"
                                    label="Recept sablon"
                                    value={prescriptionTemplate}
                                    name="prescriptionTemplate"
                                    margin="dense"
                                    onChange={handleTemplateChange}
                                    variant="outlined"
                                />
                            </Grid>
                        }
                    </Grid>

                </DialogContent>
                    </Dialog>*/}

        </div >
    )
}
