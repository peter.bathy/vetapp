import React, { useCallback, useEffect } from 'react';
import { axiosCall, handleError } from '../utils/ApiService';
import { toast } from 'react-toastify';
import { faEdit, faFloppyDisk, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function BreedSpeciesConf() {
    const [newSpeciesBreed, setNewSpeciesBreed] = React.useState(false);
    const [currentEditorDetails, setCurrentEditorDetails] = React.useState({
        breedId: 0,
        breedName: '',
        speciesId: 0,
        speciesName: ''
    });
    const [speciesList, setSpeciesList] = React.useState([
        {
            species: 0,
            speciesName: '',
            breeds: [],
        }
    ])

    const getSpeciesList = useCallback(() => {
        axiosCall.get('/breed/'
        )
            .then(response => {
                setSpeciesList(response.data);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }, []);

    useEffect(() => {
        getSpeciesList();
    }, [getSpeciesList]);

    const handleChange = event => {

        let name = event.target.name;
        let value = event.target.value;

        if (name === 'speciesName' && value === 'new') {
            setNewSpeciesBreed(true);
            setCurrentEditorDetails({
                speciesId: 0,
                speciesName: '',
                breedId: 0,
                breedName: ''
            })
        } else if (name === 'speciesName' && !newSpeciesBreed) {
            let foundSpecies = speciesList.find(species => species.speciesName === value);
            setCurrentEditorDetails({
                ...currentEditorDetails,
                speciesId: foundSpecies.id,
                speciesName: value
            })
        } else if (name === 'speciesName' && newSpeciesBreed) {
            setCurrentEditorDetails({
                ...currentEditorDetails,
                speciesName: value
            })
        } else if (name === 'breedName') {
            setCurrentEditorDetails({
                ...currentEditorDetails,
                breedName: value
            })
        }
    }

    const setEditorValues = selectedItem => {

        console.log(selectedItem)

        let foundSpecies = speciesList.find(species => species.breeds.find(b => b.id === selectedItem.id));
        console.log(foundSpecies)
        setCurrentEditorDetails({
            breedId: selectedItem.id,
            breedName: selectedItem.breedName,
            speciesId: foundSpecies.id,
            speciesName: foundSpecies.speciesName
        })
    }

    const saveDetails = () => {

        const bodyParameters = {
            breedId: currentEditorDetails.breedId,
            breedName: currentEditorDetails.breedName,
            speciesId: currentEditorDetails.speciesId,
            speciesName: currentEditorDetails.speciesName,
        };

        axiosCall.post('/breed/save',
            bodyParameters
        )
            .then(response => {
                closeEditor();
                getSpeciesList();
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }

    const closeEditor = () => {
        setNewSpeciesBreed(false);
        setCurrentEditorDetails({
            breedId: 0,
            breedName: '',
            speciesId: 0,
            speciesName: ''
        })
    }

    return (
        <div>

            <div className="d-flex align-items-end justify-content-between">
                <div>
                    <span className="mb-0 h5"> Faj, fajta beállítások</span>
                </div>
            </div>
            <hr className='mt-2' />
            <div className="mb-2 d-flex justify-content-between align-items-center">
                <div className="ps-3">
                    Faj - fajta
                </div>
                <button className="btn btn-sm btn-outline-secondary " type="button" data-bs-toggle="modal" data-bs-target="#speciesBreedModal">
                    <FontAwesomeIcon icon={faPlus} /> Hozzáadás
                </button>
            </div>
            <ul className="list-group">
                {speciesList.map((species) => {
                    return (
                        species.breeds.map(singleBreed => {
                            return (
                                <li className="list-group-item d-flex justify-content-between align-items-center" key={singleBreed.id}>
                                    <div>
                                        <span className={singleBreed.breedName === '' ? '' : 'text-secondary text-opacity-25'}>{species.speciesName}</span> {singleBreed.breedName === '' ? null : (' - ' + singleBreed.breedName)}
                                    </div>
                                    {singleBreed.breedName !== '' ?
                                        <button className="btn btn-sm btn-light " type="button" data-bs-toggle="modal" data-bs-target="#speciesBreedModal" onClick={() => setEditorValues(singleBreed)}>
                                            <FontAwesomeIcon icon={faEdit} />
                                        </button>
                                        :
                                        null}
                                </li>
                            )
                        })
                    )
                })}
            </ul>
            <div className="modal fade" id="speciesBreedModal" tabIndex="-1" aria-labelledby="speciesBreedModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="goodConfModalLabel">Faj, fajta hozzáadás, szerkesztés</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={closeEditor}></button>
                        </div>
                        <form autoComplete='off'>
                            <div className="modal-body">
                                <div className="container ps-0 pe-0 mb-2">
                                    <div className="row gx-3 gy-2">
                                        <div className="col-6">
                                            <small className='text-secondary'>Faj</small>
                                            {newSpeciesBreed ?
                                                <input
                                                    className="form-control"
                                                    name='speciesName'
                                                    key='speciesName'
                                                    id='speciesName'
                                                    type='speciesName'
                                                    value={currentEditorDetails.speciesName}
                                                    onChange={handleChange}
                                                    autoFocus
                                                />
                                                :
                                                <select
                                                    className="form-select"
                                                    name='speciesName'
                                                    key='speciesName'
                                                    id='speciesName'
                                                    value={currentEditorDetails.speciesName}
                                                    onChange={handleChange}
                                                >
                                                    <option hidden defaultValue>Kérlek válassz a lehetőségek közül</option>
                                                    <option value='new'>Új faj felvitele</option>
                                                    {speciesList.map(item => {
                                                        return (
                                                            <option value={item.speciesName} key={item.id}>{item.speciesName}</option>
                                                        )
                                                    })}
                                                </select>
                                            }
                                        </div>
                                        <div className="col-6">
                                            <small className='text-secondary'>Fajta</small>
                                            <input
                                                className="form-control"
                                                name='breedName'
                                                key='breedName'
                                                id='breedName'
                                                type='text'
                                                value={currentEditorDetails.breedName}
                                                onChange={handleChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type='button' className="btn btn-sm btn-outline-secondary" onClick={saveDetails} disabled={currentEditorDetails.speciesName === ''}
                                    data-bs-dismiss="modal" data-bs-target="#goodConfModal"><FontAwesomeIcon icon={faFloppyDisk} /> Mentés</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        </div >
    )
}