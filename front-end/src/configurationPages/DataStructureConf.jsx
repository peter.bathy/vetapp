import React, { useEffect, useCallback } from 'react';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Legend } from 'recharts';
import moment from 'moment';

export default function DataStructureConf() {
    const [statDatas, setStatDatas] = React.useState([]);

    const downloadDatas = useCallback(() => {

        const bodyParameters = {
            "intervalStart": moment().subtract(12, 'months').format('YYYY-MM-DDT00:00:00'),
            "intervalEnd": moment().format('YYYY-MM-DDT00:00:00')
        };

        axiosCall.post('/medicalrecord/event-stats',
            bodyParameters
        )
            .then(response => {

                const rawList = response.data;
                let currentDay = rawList[0].date;
                let finalList = []

                rawList.forEach(currentDateData => {
                    if (currentDay !== currentDateData.date) {
                        const dateDifference = moment(currentDateData.date).diff(moment(currentDay), 'days');
                        for (let index = 0; index < dateDifference; index++) {
                            finalList.push({
                                date: currentDay,
                                count: 0,
                                fill: 'green',
                            })
                            currentDay = moment(currentDay).add(1, 'day').format('YYYY-MM-DD');
                        }
                    }
                    if (moment(currentDay).isoWeekday() < 6) {
                        finalList.push({
                            date: currentDay,
                            count: currentDateData.count,
                            fill: 'blue'
                        })
                    } else {
                        finalList.push({
                            date: currentDay,
                            count: currentDateData.count,
                            fill: 'green',
                        })
                    }
                    currentDay = moment(currentDay).add(1, 'day').format('YYYY-MM-DD');
                });

                console.log(finalList)

                setStatDatas(finalList);
            })
            .catch(error => {
                toast.error(handleError(error))
            });
    }, []);

    useEffect(() => {
        downloadDatas();
    }, [downloadDatas]);

    return (
        <div>
            <div className="d-flex align-items-end justify-content-between">
                <div>
                    <span className="mb-0 h5">Adatok</span>
                </div>
            </div>
            <hr className='mt-2' />
            <ResponsiveContainer width={'99%'} height={500}>
                <BarChart
                    margin={{ top: 20, right: 30, left: 0, bottom: 0 }}
                    data={statDatas}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="date" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="count" />
                </BarChart>
            </ResponsiveContainer>
        </div>
    )
}