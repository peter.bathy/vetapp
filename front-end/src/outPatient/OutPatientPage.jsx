import React, { useEffect } from 'react';
import SimpleForm from './SimpleForm';
import moment from 'moment';
import UltraSoundBodyForm from './UltraSoundBodyForm';

export default function OutpatientPage() {

    const [choosenForm, setChoosenForm] = React.useState(null);
    const availableFormArray = ["Alap ambuláns lap", "Röntgen", "Hasi ultrahang", "Szívultrahang"];
    const [outpatientPageValues, setOutpatientPageValues] = React.useState({
        ownerDetails: "",
        animalDetails: "",
        date: moment().format("YYYY-MM-DDTHH:mm")

    });

    const handleChange = event => {
        let key = event.target.name;
        let value = event.target.value;
        let currentValues = structuredClone(outpatientPageValues);
        currentValues[key] = value;
        setOutpatientPageValues(currentValues);
    }

    /*const options = {
        filename: "using-function.pdf",
        page: {
          margin: 20
        }
      };
      */
    const getTargetElement = () => document.getElementById("print-container");

    const downloadPdf = () => window.print();

    return (
        <React.Fragment>
            <div className={'d-print-none'}>
                <div className="d-flex align-items-end justify-content-between">
                    <div>
                        <span className="mb-0 h5"> Ambuláns lapok</span><br />
                        <button className='btn' onClick={downloadPdf}>Download PDF</button>
                    </div>
                </div>
                <hr className='mt-2 mb-4' />
            </div>
            <div className="container-fluid ps-0 pe-0" id="print-container">
                {choosenForm ?
                    <>
                        <div className='d-none d-print-block'>
                            <div className="d-flex align-items-end justify-content-between">
                                <div>
                                    <span className="mb-0 h5">{choosenForm}</span><br />
                                </div>
                                <div>
                                    szöveg<br />
                                    szöveg<br />
                                    szöveg<br />
                                    szöveg<br />
                                </div>
                            </div>
                        </div>

                        <div className={''}>
                            <div className="d-flex align-items-end justify-content-between">
                                <div>
                                    <span className="mb-0 h5">{choosenForm}</span><br />
                                </div>
                            </div>
                            <hr className='mt-2 mb-4' />
                        </div>
                        <div className="row mb-3">
                            <div className='col-sm-2'>
                                <label className="form-label">Dátum</label>
                            </div>
                            <div className='col-sm-3'>
                                <input className="form-control" type="datetime-local" onChange={handleChange} name="date" value={outpatientPageValues?.date} ></input>

                            </div>
                            <div className='col-sm-7'>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className='col-sm-2'>
                                <label className="form-label">Tulajdonos adatai</label>
                            </div>
                            <div className='col-sm-10'>
                                <input className="form-control" type="text" onChange={handleChange} name="ownerDetails" value={outpatientPageValues?.ownerDetails} ></input>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className='col-sm-2'>
                                <label className="form-label">Állat adatai</label>
                            </div>
                            <div className='col-sm-10'>
                                <input className="form-control" type="text" onChange={handleChange} name="animalDetails" value={outpatientPageValues?.animalDetails} ></input>
                            </div>
                        </div>


                        {choosenForm === 'Alap ambuláns lap' ?
                            <SimpleForm /> :
                            null
                        }
                        {choosenForm === 'Röntgen' ?
                            <></> :
                            null
                        }
                        {choosenForm === 'Hasi ultrahang' ?
                            <UltraSoundBodyForm /> :
                            null
                        }
                        {choosenForm === 'Szívultrahang' ?
                            <></> :
                            null
                        }
                    </>


                    :
                    <div className="row g-3 row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-xxl-6">
                        {availableFormArray.map((element, index) =>
                            <div className="col" key={index}>
                                <button className="btn btn-light h-100 w-100 shadow-sm text-start" type="button" onClick={() => setChoosenForm(element)} >
                                    <h5>
                                        {element}
                                    </h5>
                                </button>
                            </div>
                        )}
                    </div>}
            </div>
        </React.Fragment >
    )
}