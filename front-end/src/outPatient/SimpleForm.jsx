import React, { useEffect } from 'react';
import moment from 'moment';
import { addAutoResize } from '../utils/utils';

export default function SimpleForm() {
    addAutoResize();

    const [formValues, setFormValues] = React.useState({
        anamnesis: "",
        symptoms: "",
        therapy: "",
        otherDetails: "",
    });

    const handleChange = event => {
        let key = event.target.name;
        let value = event.target.value;
        let currentValues = structuredClone(formValues);
        currentValues[key] = value;
        setFormValues(currentValues);
    }

    return (
        <React.Fragment>
            <hr />
            <div className="container-fluid ps-0 pe-0">
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Anamnézis</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="anamnesis" value={formValues?.anamnesis}/>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Szimptóma</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="symptoms" value={formValues?.symptoms} ></textarea>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Kezelés</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="therapy" value={formValues?.therapy} ></textarea>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Megjegyzés, javaslat</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="otherDetails" value={formValues?.otherDetails} ></textarea>
                    </div>
                </div>
            </div>
        </React.Fragment >
    )
}