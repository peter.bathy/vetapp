import React from 'react';
import { addAutoResize } from '../utils/utils';

export default function UltraSoundBodyForm() {
    addAutoResize();

    const [formValues, setFormValues] = React.useState({
        sex: "male",
        anamnesis: "",
        symptoms: "",
        therapy: "",
        otherDetails: "",
    });

    const handleChange = (event, section) => {
        let key = event.target.name;
        let value = event.target.value;
        let currentValues = structuredClone(formValues);
        if (section) {
            if (!currentValues.section) {
                currentValues[section] = {}
            }
            currentValues[section][key] = value;
        } else {
            currentValues[key] = value;
        }
        setFormValues(currentValues);
    }

    return (
        <React.Fragment>
            <hr />
            <div className="container-fluid ps-0 pe-0">
                <div className="row mb-3 d-print-none">
                    <div className='col-sm-2'>
                        <label className="form-label">Állat neme</label>
                    </div>
                    <div className='col-sm-3'>
                        <select className="form-select" onChange={event => handleChange(event, null)} name="sex" value={formValues?.sex} >
                            <option value="male">hím</option>
                            <option value="female">nőstény</option>
                            <option value="other">egyéb</option>
                        </select>
                    </div>
                    <div className='col-sm-7'>
                    </div>
                </div>
                <h6 className='fw-bold'>Máj</h6>
                <hr />
                <div className="row mb-3">
                    <div className='col-sm-2'>
                        <label className="form-label">Méret</label>
                    </div>
                    <div className='col-sm-3'>
                        <select className="form-select" onChange={event => handleChange(event, "liver")} name="size" value={formValues?.liver?.size} >
                            <option value="small++">megkisebbedett++</option>
                            <option value="small+">megkisebbedett+</option>
                            <option value="normal">normál</option>
                            <option value="bigger+">megnövekedett+</option>
                            <option value="bigger++">megnövekedett++</option>
                        </select>
                    </div>
                    <div className='col-sm-7'>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-2'>
                        <label className="form-label">Alak</label>
                    </div>
                    <div className='col-sm-3'>
                        <select className="form-select" onChange={event => handleChange(event, "liver")} name="shape" value={formValues?.liver?.shape} >
                            <option value="normal">normál</option>
                            <option value="abnormal">abnormál</option>
                        </select>
                    </div>
                    <div className='col-sm-1'>
                        <label className="form-label">megj.:</label>
                    </div>
                    <div className='col-sm-6'>
                        <textarea className="form-control" data-autoresize rows={1} onChange={event => handleChange(event, "liver")} name="liverShapeComment" value={formValues?.liver?.liverShapeComment} />
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-2'>
                        <label className="form-label">Kontúr</label>
                    </div>
                    <div className='col-sm-3'>
                        <select className="form-select" onChange={event => handleChange(event, "liver")} name="contour" value={formValues?.liver?.contour} >
                            <option value="normal">normál</option>
                            <option value="abnormal">abnormál</option>
                        </select>
                    </div>
                    <div className='col-sm-1'>
                        <label className="form-label">megj.:</label>
                    </div>
                    <div className='col-sm-6'>
                        <textarea className="form-control" data-autoresize rows={1} onChange={event => handleChange(event, "liver")} name="liverContourComment" value={formValues?.liver?.liverContourComment} />
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-2'>
                        <label className="form-label">Struktúra</label>
                    </div>
                    <div className='col-sm-3'>
                        <select className="form-select" onChange={event => handleChange(event, "liver")} name="structure" value={formValues?.liver?.structure} >
                            <option value="normal">normál</option>
                            <option value="diffuse">diffúz</option>
                            <option value="focal">gócos</option>
                        </select>
                    </div>
                    <div className='col-sm-1'>
                        <label className="form-label">megj.:</label>
                    </div>
                    <div className='col-sm-6'>
                        <textarea className="form-control" data-autoresize rows={1} onChange={event => handleChange(event, "liver")} name="liverStructureComment" value={formValues?.liver?.liverStructureComment} />
                    </div>
                </div>
                
                • Intenzitás: normál / fokozott + / fokozott ++ / csökkent
                • Májlebenyek: nem különíthetők el / elkülöníthetők
                • Májerek: normál / tágak / egyéb:
                • Epehólyag: közepesen telt / nem felkereshető / kitágult
                • Epehólyag fal: normál / megvastagodott / szövetszaporulat, megjegyzés:
                • Epevezető: normál / kitágult / echogazdag képlet hangárnyék nélkül / echogazdag képlet hangárnyékkal
                • Epeerek: normál / tágak
                • Epehólyag tartalom: echomentes / sludge / echogazdag képlet hangárnyék nélkül / echogazdag képlet hangárnyékkal
                • Egyéb:

                Lép
                • Nagyság: normál / megkisebbedett / megnövekedett+ / megnövekedett++
                • Alak: normál / abnormál, megjegyzés:
                • Kontúr: normál / abnormál, megjegyzés:
                • Szerkezet: normál, parenchymás / diffúz elváltozás / gócos elváltozás, megjegyzés:
                • Helyeződés: normál / abnormál, megjegyzés:
                • Intenzitás: normál / fokozott + / fokozott ++ / csökkent + / csökkent ++
                • Egyéb:


                {/*<div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Anamnézis</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="anamnesis" value={formValues?.anamnesis} />
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Szimptóma</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="symptoms" value={formValues?.symptoms} ></textarea>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Kezelés</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="therapy" value={formValues?.therapy} ></textarea>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className='col-sm-4'>
                        <label className="form-label">Megjegyzés, javaslat</label>
                    </div>
                    <div className='col-sm-8'>
                        <textarea className="form-control" data-autoresize rows={4} onChange={handleChange} name="otherDetails" value={formValues?.otherDetails} ></textarea>
                    </div>
                </div>
*/}






            </div>
        </React.Fragment >
    )

    /*return (
        <React.Fragment>
            <div className={'d-print-none'}>
                <div className="d-flex align-items-end justify-content-between">
                    <div>
                        <span className="mb-0 h5">Alap ambuláns lap</span><br />
                    </div>
                </div>
                <hr className='mt-2 mb-4' />
            </div>
            <div className="container-fluid ps-0 pe-0">

                <div className="row g-3">


                    <h6 className='fw-bold'>section.sectionName</h6>
                    <hr />

                    <div className="mb-3">
                        <label for="exampleFormControlInput1" className="form-label">inputTitle</label>
                        <input type="text" className="form-control" onChange={handleChange} name="ddd" value={outpatientPageValues?.inputName ?? ""}></input>
                    </div>

                    <div className="mb-3">
                        <label className="form-label">inputTitle</label>
                        <textarea className="form-control" onChange={handleChange} rows={3} name="xxx" value={outpatientPageValues?.inputName ?? ""} ></textarea>
                    </div>

                    <div className="mb-3">
                        <label className="form-label">inputTitle</label>
                        <select className="form-select" onChange={handleChange} name="zzz" value={outpatientPageValues?.inputName ?? ""} >
                            <option value="">valami</option>
                            <option value="1">valami</option>
                            <option value="2">valami</option>
                            <option value="3">valami</option>
                            <option value="4">valami</option>
                        </select>
                    </div>
                </div>
            </div>
        </React.Fragment >
    )*/
}