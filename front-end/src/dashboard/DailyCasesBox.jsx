import React, { useCallback, useEffect, useRef } from 'react';
import { toast } from 'react-toastify';
import moment from 'moment';
import 'moment/locale/hu';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExternalLinkAlt, faFolderOpen, faIdCard, faListUl, faTowerBroadcast } from '@fortawesome/free-solid-svg-icons';
import ListSkeleton from '../common/skeletons/ListSkeleton';
import { axiosCall, handleError } from '../utils/ApiService';
import { Collapse } from 'bootstrap';
import { calcAge } from '../utils/utils';

export default function DailyCasesBox() {
    const titleRef = useRef();

    const [currentDate, setCurrentDate] = React.useState(moment());
    const history = useNavigate();
    const [animalList, setAnimalList] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [treatmentDetails, setTreatmentDetails] = React.useState({
        animalName: '',
        animalId: 1,
        breed: '',
        species: '',
        ownerName: '',
        chipCode: '',
        birthDate: '',
        lastTreatments: [
            {
                dateOfTreatment: moment(),
                treatmentDetails: ''
            },
            {
                dateOfTreatment: moment(),
                treatmentDetails: ''
            }, {
                dateOfTreatment: moment(),
                treatmentDetails: ''
            }
        ]
    })

    const stepBack = () => {
        setCurrentDate(moment(currentDate).subtract(1, 'day'))
    }

    const stepForward = () => {
        setCurrentDate(moment(currentDate).add(1, 'day'))
    }

    const getDatas = useCallback(() => {

        setLoading(true);

        let startOfView = moment(currentDate).startOf('day');
        let endOfView = moment(currentDate).endOf('day');

        const bodyParameters = {
            intervalStart: startOfView,
            intervalEnd: endOfView
        };

        axiosCall.post('/medicalrecord/dayInterval',
            bodyParameters
        )
            .then(response => {
                console.log(response.data)
                setAnimalList(response.data);
                if (response.data.length > 0) {
                    titleRef.current.scrollIntoView({ block: "start", behavior: "smooth" });
                }
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
            .finally(
                setLoading(false)
            )
    }, [currentDate]);

    useEffect(() => {
        getDatas();
    }, [currentDate, getDatas]);

    const handleClose = () => {
        setTreatmentDetails({
            animalName: '',
            animalId: 1,
            breed: '',
            species: '',
            ownerName: '',
            chipCode: '',
            birthDate: '',
            lastTreatments: [
                {
                    dateOfTreatment: moment(),
                    treatmentDetails: ''
                },
                {
                    dateOfTreatment: moment(),
                    treatmentDetails: ''
                }, {
                    dateOfTreatment: moment(),
                    treatmentDetails: ''
                }
            ]
        })
    };

    const getLastTreatment = animal => {

        axiosCall.get('/medicalrecord/lastrecords/' + animal.animalId
        )
            .then(response => {
                setTreatmentDetails({
                    animalName: animal.animalName,
                    animalId: animal.animalId,
                    breed: animal.breed || '',
                    species: animal.species || '',
                    ownerName: animal.ownerName || '',
                    chipCode: animal.chipCode || '',
                    birthDate: animal.birthDate || '',
                    lastTreatments: response.data
                });

            }).catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
            .finally(
            //setLoading(false)
        )
    }

    const navigateToAnimal = (destination) => {
        handleClose();
        if (destination === 'samePage') {
            history('/animal/' + treatmentDetails.animalId);
        } else {
            window.open('/animal/' + treatmentDetails.animalId, "_blank")?.focus();
        }
    }

    useEffect(() => {
        const modalObj = document.getElementById('treatmentDetailsModal');
        modalObj.addEventListener('hide.bs.modal', function (event) {
            const bsCollapse = new Collapse('#moreTreatmentCollapse', {
                toggle: false
            })
            bsCollapse.hide();
        });
    }, [])

    return (
        <>
            <div className="d-flex align-items-end justify-content-between mb-3">

                <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={stepBack}>Előző </button>
                    {!moment(currentDate).isAfter(moment().subtract(1, 'day')) && <button type="button" className="btn btn-outline-secondary btn-sm" onClick={stepForward}> Következő</button>}
                </div>
                <div>
                    <span className="mb-0"> {moment(currentDate).format('dddd MMM DD')}</span>
                </div>
                <div></div>
            </div>
            {loading ?
                <ListSkeleton />
                :
                <div style={{ maxHeight: '94%', overflow: 'auto' }}>
                    {animalList.length === 0 ?
                        <div ref={titleRef}>
                            <h1 className='text-secondary text-center'>
                                <FontAwesomeIcon icon={faFolderOpen} />
                            </h1>
                            <p className='text-secondary text-center'>
                                Nincs az adott napon kezelés
                            </p>
                        </div>
                        :
                        <div ref={titleRef} >
                            {animalList.map((animal, index) => {
                                return (
                                    <React.Fragment key={index}>
                                        <button type="button" className="btn btn-light shadow-sm p-3 mb-3 w-100 rounded text-start position-relative" data-bs-toggle="modal" data-bs-target='#treatmentDetailsModal' onClick={() => getLastTreatment(animal)}>
                                            <div className='row'>
                                                <div className='col-auto'>
                                                    <span class="badge bg-secondary-subtle text-dark">{moment(animal.treatmentTime).format("HH:mm")}</span>
                                                </div>
                                                <div className='col'>
                                                    <strong>
                                                        {animal.animalName + ', ' + animal.breed + ' ' + animal.species}
                                                    </strong><br />
                                                    {animal.ownerName}
                                                </div>
                                            </div>
                                        </button>
                                    </React.Fragment>
                                );
                            })}
                        </div>
                    }
                </div>
            }
            <div className="modal fade" id="treatmentDetailsModal" tabIndex="-1" aria-labelledby="treatmentDetailsModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="treatmentDetailsModalLabel">Kezelések</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}></button>
                        </div>
                        <div className="modal-body">
                            <div className="container-fluid ps-0 pe-0 mb-4">
                                <div className="row gx-3">
                                    <div className="col-10">
                                        <button className="btn btn-light w-100 h-100 shadow-sm text-start py-4 mb-4" type="button" data-bs-dismiss="modal" data-bs-target="#treatmentDetailsModal" onClick={() => navigateToAnimal('samePage')}>
                                            <div className="container-fluid ps-0 pe-0">
                                                <div className="row gx-1">
                                                    <div className="col-1">
                                                        <span className='fs-4'><FontAwesomeIcon icon={faIdCard} /></span>
                                                    </div>
                                                    <div className="col-11">
                                                        <p className='m-0'>
                                                            {treatmentDetails.animalName + ', ' + treatmentDetails.breed + ' ' + treatmentDetails.species + ' - ' + treatmentDetails.animalId + ' / ' + calcAge(treatmentDetails.birthDate) + ' '}   {'  ' + treatmentDetails.chipCode.length > 1 ? <FontAwesomeIcon icon={faTowerBroadcast} /> : null}<br />
                                                            {treatmentDetails.ownerName}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </button>
                                    </div>
                                    <div className="col-2">
                                        <button className="btn btn-light w-100 h-100 shadow-sm text-center mb-4" onClick={() => navigateToAnimal('blankPage')} data-bs-dismiss="modal" data-bs-target="#eventModal" type="button">
                                            <span className='fs-4'><FontAwesomeIcon icon={faExternalLinkAlt} /></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className='mb-3'>
                                <strong>{moment(treatmentDetails.lastTreatments[0].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                <hr className='mt-1' />
                                {treatmentDetails.lastTreatments[0].vaccination &&
                                    <div>
                                        {treatmentDetails.lastTreatments[0].vaccination}
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].anamnesis &&
                                    <div>
                                        <small className='text-secondary'>Anamnézis</small><br />
                                        <span className='ps-2'> {treatmentDetails.lastTreatments[0].anamnesis}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].symptoms &&
                                    <div>
                                        <small className='text-secondary'>Szimptóma</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].symptoms}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].therapy &&
                                    <div>
                                        <small className='text-secondary'>Kezelés</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].therapy}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].otherDetails &&
                                    <div>
                                        <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].otherDetails}</span>
                                    </div>
                                }
                                <div className="collapse" id={"moreTreatmentCollapse"} >
                                    {treatmentDetails.lastTreatments[1] &&
                                        <div className='mt-3'>
                                            <strong>{moment(treatmentDetails.lastTreatments[1].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                            <hr className='mt-2' />
                                            {treatmentDetails.lastTreatments[1].vaccination &&
                                                <div>
                                                    {treatmentDetails.lastTreatments[1].vaccination}
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].anamnesis &&
                                                <div>
                                                    <small className='text-secondary'>Anamnézis</small><br />
                                                    <span className='ps-2'> {treatmentDetails.lastTreatments[1].anamnesis}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].symptoms &&
                                                <div>
                                                    <small className='text-secondary'>Szimptóma</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].symptoms}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].therapy &&
                                                <div>
                                                    <small className='text-secondary'>Kezelés</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].therapy}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].otherDetails &&
                                                <div>
                                                    <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].otherDetails}</span>
                                                </div>
                                            }
                                        </div>}
                                    {treatmentDetails.lastTreatments[2] &&
                                        <div className='mt-3'>
                                            <strong> {moment(treatmentDetails.lastTreatments[2].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                            <hr className='mt-2' />
                                            {treatmentDetails.lastTreatments[2].vaccination &&
                                                <div>
                                                    {treatmentDetails.lastTreatments[2].vaccination}
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].anamnesis &&
                                                <div>
                                                    <small className='text-secondary'>Anamnézis</small><br />
                                                    <span className='ps-2'> {treatmentDetails.lastTreatments[2].anamnesis}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].symptoms &&
                                                <div>
                                                    <small className='text-secondary'>Szimptóma</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].symptoms}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].therapy &&
                                                <div>
                                                    <small className='text-secondary'>Kezelés</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].therapy}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].otherDetails &&
                                                <div>
                                                    <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].otherDetails}</span>
                                                </div>
                                            }
                                        </div>}
                                </div>
                            </div>
                            <div className="d-flex align-items-end justify-content-end">
                                <div>
                                    <button type="button" className="btn btn-outline-secondary btn-sm mt-2" data-bs-toggle="collapse" data-bs-target={"#moreTreatmentCollapse"} aria-expanded="false" aria-controls={"#moreTreatmentCollapse"}><FontAwesomeIcon icon={faListUl} /> Előzmény</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}