import React from 'react';
import DailyCasesBox from './DailyCasesBox';
import EventsListBox from './EventsListBox';

export default function Dashboard() {

    /*function checkLuhn(cardNo) {
        let nDigits = cardNo.length;

        let nSum = 0;
        let isSecond = false;
        for (let i = nDigits - 1; i >= 0; i--) {

            let d = cardNo[i].charCodeAt() - '0'.charCodeAt();

            if (isSecond == true)
                d = d * 2;

            // We add two digits to handle
            // cases that make two digits
            // after doubling
            nSum += parseInt(d / 10, 10);
            nSum += d % 10;

            isSecond = !isSecond;
        }
        return (nSum % 10 == 0);
    }

    const [code, setCode] = React.useState('')

    const barcode = "022463576";

    const generateCheckSum = () => {
        setCode(checkDigit(barcode));
    }




    const checkDigit = (barcode) => {
        let s = 0;
        for (let i = 0; i < barcode.length; i++) {
            let c = parseInt(barcode.charAt(i), 36);
            s += c * (i % 2 == 0 ? 1 : 3);
        }
        s = (10 - s % 10) % 10;
        barcode += s;
        return barcode;
    }*/

    return (
        <div className="container-fluid ps-0 pe-0">
            <div className="row gx-4">
                <div className="col-md-6">
                    <div className='p-3 bg-white rounded'>
                        <span className="mb-0 h5">
                            Kezelések
                        </span>
                        <hr className='mt-2' />
                        <div style={{ height: '85vh' }}>
                            <DailyCasesBox />
                        </div>
                    </div>
                </div>

                <div className="col-md-6">
                    <div className='p-3 bg-white rounded'>
                        <span className="mb-0 h5">
                            Naptár
                        </span>
                        <hr className='mt-2' />
                        <div style={{ height: '85vh' }}>
                            <EventsListBox />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}