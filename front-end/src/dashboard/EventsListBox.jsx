import React, { useCallback, useEffect } from 'react';
import { toast } from 'react-toastify';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt, faHeartbeat, faIdCard, faLaptopMedical, faRadiation, faTooth } from '@fortawesome/free-solid-svg-icons'
import { axiosCall, handleError } from '../utils/ApiService';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import { Modal } from 'bootstrap';

export default function EventsListBox() {
    const localizer = momentLocalizer(moment);
    const history = useNavigate();
    moment.locale('hu');

    const [currentDate, setCurrentDate] = React.useState(moment());
    const [openedEvent, setOpenedEvent] = React.useState({});
    const [events, setEvents] = React.useState([
        {
            id: 0,
            title: '',
            allDay: true,
            start: new Date(1999, 6, 0),
            end: new Date(1999, 6, 1),
        }
    ]);

    const getDatas = useCallback(() => {

        const startOfView = moment(currentDate).startOf('day');
        const endOfView = moment(currentDate).endOf('day');

        const bodyParameters = {
            intervalStart: startOfView,
            intervalEnd: endOfView
        };

        axiosCall.post('/calendar',
            bodyParameters
        )
            .then(response => {
                let appointments = response.data;

                for (let i = 0; i < appointments.length; i++) {
                    appointments[i].start = moment(appointments[i].start).toDate();
                    if (appointments[i].allDay) {
                        moment(appointments[i].end).add(2, 'hours');
                    }
                    appointments[i].end = moment(appointments[i].end).toDate();
                }

                setEvents(appointments);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }, [currentDate]);

    useEffect(() => {
        getDatas();
    }, [currentDate]);

    const handleSelect = (event) => {
        let currentEvent = { ...event }

        if (moment(currentEvent.start).format('HH') === '00') {
            currentEvent.allDay = true;
        }
        setOpenedEvent(currentEvent)
        const modal = new Modal('#eventDetailsModal', { keyboard: false })
        modal.show()
    };

    const handleClose = () => {
        setOpenedEvent({});
    };

    const eventPropGetter = (event) => {

        let backgroundColor = '#0000ff';

        if (event.eventTypes.length < 1) {
            return {};
        } else if (event.eventTypes.length > 1) {
            backgroundColor = '#f15bb5';
        } else {
            switch (event.eventTypes[0]) {
                case 'surgery':
                    backgroundColor = '#fee440'
                    break;
                case 'xray':
                    backgroundColor = '#9b5de5'
                    break;
                case 'tooth':
                    backgroundColor = '#00f5d4'
                    break;
                case 'ultrasound':
                    backgroundColor = '#00bbf9'
                    break;
                default:
                    return {};
            }
        }

        let style = {
            backgroundColor: backgroundColor,
            color: 'black',
            display: 'block'
        };

        return {
            style: style
        };
    }

    const CustomEvent = ({ event }) => (
        <>
            {!event.allDay && <small> {moment(event.start).format('HH:mm')} - </small>}{event.title}
        </>
    );

    const navigateToAnimal = (destination) => {
        handleClose();
        if (destination === 'samePage') {
            history('/animal/' + openedEvent.animal.animalId);
        } else {
            window.open('/animal/' + openedEvent.animal.animalId, "_blank")?.focus();
        }
    }

    return (
        <>
            <div style={{ height: '85vh' }}>
                <Calendar
                    popup
                    events={events}
                    min={new Date('2020-12-17T08:00:00')}
                    max={new Date('2020-12-17T20:00:00')}
                    step={15}
                    timeslots={4}
                    defaultView={Views.DAY}
                    views={['day']}
                    defaultDate={moment().toDate()}
                    localizer={localizer}
                    onSelectEvent={event => handleSelect(event)}
                    onNavigate={(date) => {
                        setCurrentDate(date);
                    }}
                    components={{
                        event: CustomEvent,
                    }}
                    eventPropGetter={(eventPropGetter)}
                    messages={{ next: "Következő", previous: "Előző", today: "Mai nap" }}
                    className='mainCalendar'
                />
            </div>
            <div className="modal fade" id="eventDetailsModal" tabIndex="-1" aria-labelledby="eventDetailsModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="eventDetailsModalLabel">Esemény részletei</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}></button>
                        </div>
                        <div className="modal-body">
                            {openedEvent.animal ?
                                <div className="container-fluid ps-0 pe-0">
                                    <div className="row gx-3">
                                        <div className="col-10">
                                            <button className="btn btn-light w-100 h-100 shadow-sm text-start py-4 mb-4" onClick={() => navigateToAnimal('samePage')} data-bs-dismiss="modal" data-bs-target="#eventDetailsModal" type="button">
                                                <div className="container-fluid ps-0 pe-0">
                                                    <div className="row gx-1">
                                                        <div className="col-1">
                                                            <span className='fs-4'><FontAwesomeIcon icon={faIdCard} /></span>
                                                        </div>
                                                        <div className="col-11">
                                                            <p className='m-0'>
                                                                {openedEvent.animal.animalName}, {openedEvent.animal.breed} - {openedEvent.animal.species} <br />
                                                                {openedEvent.animal.ownerName} {(openedEvent.animal.mobile || openedEvent.animal.phone) && ' - '}{openedEvent.animal.mobile} {openedEvent.animal.phone}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                        <div className="col-2">
                                            <button className="btn btn-light w-100 h-100 shadow-sm text-center mb-4" onClick={() => navigateToAnimal('blankPage')} data-bs-dismiss="modal" data-bs-target="#eventModal" type="button">
                                                <span className='fs-4'><FontAwesomeIcon icon={faExternalLinkAlt} /></span>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                :
                                null
                            }
                            <p className='text-secondary mb-0 mt-4 fs-6'>Leírás:</p>
                            <p>
                                {openedEvent.title ? openedEvent.title : ''}
                            </p>
                            {!openedEvent.allDay && openedEvent.eventTypes && openedEvent.eventTypes.length > 0 &&
                                <>
                                    {openedEvent.eventTypes.includes('tooth') &&
                                        <button type="button" className="btn me-2 rounded-pill" disabled style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} ><FontAwesomeIcon icon={faTooth} /> Fogazás</button>
                                    }
                                    {openedEvent.eventTypes.includes('xray') &&
                                        <button type="button" className="btn me-2 rounded-pill" disabled style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} ><FontAwesomeIcon icon={faRadiation} />  Röntgen</button>
                                    }
                                    {openedEvent.eventTypes.includes('ultrasound') &&
                                        <button type="button" className="btn me-2 rounded-pill" disabled style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} ><FontAwesomeIcon icon={faLaptopMedical} />  Ultrahang</button>
                                    }
                                    {openedEvent.eventTypes.includes('surgery') &&
                                        <button type="button" className="btn me-2 rounded-pill" disabled style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} ><FontAwesomeIcon icon={faHeartbeat} />  Műtét</button>
                                    }
                                </>}
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}