import React, { Component } from 'react';
import './App.css';
import 'react-toastify/dist/ReactToastify.min.css';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { RequireAuth } from './common/RequireAuth';
import Dashboard from './dashboard/Dashboard';
import NotFound from './common/NotFound';
import { Helmet } from 'react-helmet';
import SingIn from './authentication/SingIn';
import Calendar from './calendar/Calendar';
import Vaccinations from './vaccination/Vaccinations';
import PricePage from './prices/PricePage';
import GoodsConf from './configurationPages/GoodsConf';
import DataStructureConf from './configurationPages/DataStructureConf';
import BreedSpeciesConf from './configurationPages/BreedSpeciesConf';
import OwnerDetails from "./owner/OwnerDetails";
import AnimalDetails from "./animal/AnimalDetails";
import PostCards from './postcards/PostCards';
import EditPageNew from './editing/EditPageNew';
import AnimalMergePage from './editing/AnimalMergePage';
import ChangeList from './petvetDatas/ChangeList';
import SearchPage from './common/SearchPage'
import OutpatientPage from './outPatient/OutPatientPage';


const router = createBrowserRouter([
    {
        path: "/animal/:id",
        element: <RequireAuth><AnimalDetails /></RequireAuth>,
    },
    {
        path: "/owner/:id",
        element: <RequireAuth><OwnerDetails /></RequireAuth>,
    },
    {
        path: "/postcards",
        element: <RequireAuth><PostCards /></RequireAuth>,
    },
    {
        path: "/prices",
        element: <RequireAuth><PricePage /></RequireAuth>,
    },
    {
        path: "/calendar",
        element: <RequireAuth><Calendar /></RequireAuth>,
    },
    {
        path: "/edit",
        element: <RequireAuth><EditPageNew /></RequireAuth>,
    },
    {
        path: "/merge",
        element: <RequireAuth><AnimalMergePage /></RequireAuth>,
    },
    {
        path: "/vaccination/expired",
        element: <RequireAuth><Vaccinations /></RequireAuth>,
    },
    {
        path: "/settings/datastructure",
        element: <RequireAuth><DataStructureConf /></RequireAuth>,
    },
    {
        path: "/settings/goods",
        element: <RequireAuth><GoodsConf /></RequireAuth>,
    },
    {
        path: "/outpatient",
        element: <RequireAuth><OutpatientPage /></RequireAuth>,
    },
    {
        path: "/settings/species",
        element: <RequireAuth><BreedSpeciesConf /></RequireAuth>,
    },
    {
        path: "/postcards",
        element: <RequireAuth><PostCards /></RequireAuth>,
    },
    {
        path: "/detailed-search",
        element: <RequireAuth><SearchPage /></RequireAuth>,
    },
    {
        path: "/data-changes",
        element: <RequireAuth><ChangeList /></RequireAuth>,
    },
    {
        path: "/signin",
        element: <SingIn />,
    },
    {
        path: "/",
        element: <RequireAuth><Dashboard /></RequireAuth>,
        errorElement: <NotFound />
    },
]);

class App extends Component {

    render() {
        return (
            <div className="App bg-light">
                <Helmet>
                    <title>VetApp</title>
                </Helmet>
                <RouterProvider router={router} />
            </div>
        );
    }
}
export default App;