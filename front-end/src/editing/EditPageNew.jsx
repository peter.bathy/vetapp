import React, { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { zipcodes } from "../utils/zipcodes";
import { toast } from 'react-toastify';
import moment from 'moment';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCross, faFloppyDisk, faIdCard, faPaw, faTowerBroadcast, faUser, faUserPlus, faVenusMars, faXmark } from '@fortawesome/free-solid-svg-icons';

export default function EditPageNew(props) {
    const history = useNavigate();
    const search = useLocation().search;
    const animalId = new URLSearchParams(search).get('animalid');
    const ownerId = new URLSearchParams(search).get('ownerid');
    const chipNumber = new URLSearchParams(search).get('chipnumber');
    const [colorList, setColorList] = React.useState([]);
    const [breedList, setBreedList] = React.useState([]);
    const [speciesList, setSpeciesList] = React.useState([]);
    const [searchOwnerList, setSearchOwnerList] = React.useState([]);
    const [ownerListOpen, setOwnerListOpen] = React.useState(false);
    const [errors, setErrors] = React.useState({});
    const [changeType, setChangeType] = React.useState('');
    const [modifyTypes] = React.useState(new Set());
    const [buttonIsDisabled, setButtonIsDisabled] = React.useState(false);

    const [animalDetails, setAnimalDetails] = React.useState({
        animalName: '',
        breed: '',
        species: '',
        birthDate: '',
        birthDateIsEstimated: false,
        dateOfSpaying: '',
        isSpayed: false,
        dateOfDeath: '',
        sex: 'ismeretlen',
        color: '',
        idOfMicrochip: '',
        idOfVaccinationBook: '',
        passportId: '',
        pedigree: '',
        animalId: 0,
        ownerId: 0,
        comment: '',
    });

    const [ownerDetails, setOwnerDetails] = React.useState(
        {
            ownerId: 0,
            ownerName: '',
            zipCode: '',
            city: '',
            address: '',
            mobileNumber: '',
            phoneNumber: '',
            email: '',
            ownerNotification: ''
        }
    )

    const getSelectedOwner = oId => {
        axiosCall.get('/owner/' + oId
        )
            .then(response => {
                setOwnerDetails(response.data);
                handleNameClose();
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a tulajdonos adatainak betöltése közben'))
            })
    }

    const nameCompare = (a, b) => {
        if (a.speciesName < b.speciesName) {
            return -1;
        }
        if (a.speciesName > b.speciesName) {
            return 1;
        }
        return 0;
    }

    useEffect(() => {

        const getBreedList = () => {
            axiosCall.get('/breed'
            )
                .then(response => {
                    const speciesList = response.data;
                    setSpeciesList(speciesList);

                    let rawBreedList = [];

                    response.data.forEach(species => {
                        species.breeds.forEach(breedItem => {
                            rawBreedList.push(breedItem.breedName)
                        })
                    })
                    rawBreedList.sort(nameCompare);
                    setBreedList(rawBreedList);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a fajták betöltése közben'))
                })
        }
        getBreedList();

        const getColorList = () => {
            axiosCall.get('/color'
            )
                .then(response => {
                    setColorList(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a színadatok betöltése közben'))
                })
        }
        getColorList();

        const getDataHandler = () => {
            if (+animalId !== 0) {
                axiosCall.get('/animal/' + animalId
                )
                    .then(response => {
                        let newAnimalDetails = response.data;
                        setAnimalDetails({
                            animalName: newAnimalDetails.animalName || '',
                            breed: newAnimalDetails.breed || '',
                            species: newAnimalDetails.species || '',
                            birthDate: newAnimalDetails.birthDate || '',
                            birthDateIsEstimated: newAnimalDetails.birthDateIsEstimated,
                            dateOfDeath: newAnimalDetails.deathDate || '',
                            dateOfSpaying: newAnimalDetails.dateOfSpaying || '',
                            isSpayed: newAnimalDetails.isSpayed,
                            sex: newAnimalDetails.sex || '',
                            color: newAnimalDetails.color || '',
                            idOfMicrochip: newAnimalDetails.idOfMicrochip || '',
                            idOfVaccinationBook: newAnimalDetails.idOfVaccinationBook || '',
                            passportId: newAnimalDetails.passportId || '',
                            pedigree: newAnimalDetails.pedigree || '',
                            animalId: newAnimalDetails.animalId || 0,
                            ownerId: newAnimalDetails.ownerId || 0,
                            comment: newAnimalDetails.comment || '',
                        });
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'AAHiba történt az állat adatainak betöltése közben'))
                    })
            }
            if (+ownerId !== 0) {
                axiosCall.get('/owner/' + ownerId
                )
                    .then(response => {
                        let newOwnerDetails = response.data;
                        setOwnerDetails(
                            {
                                ownerId: newOwnerDetails.ownerId || 0,
                                ownerName: newOwnerDetails.ownerName || '',
                                zipCode: newOwnerDetails.zipCode || '',
                                city: newOwnerDetails.city || '',
                                address: newOwnerDetails.address || '',
                                mobileNumber: newOwnerDetails.mobileNumber || '',
                                phoneNumber: newOwnerDetails.phoneNumber || '',
                                email: newOwnerDetails.email || '',
                                ownerNotification: newOwnerDetails.ownerNotification || ''
                            }
                        )
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt a tulajdonos adatainak betöltése közben'))
                    })
            }
            if (chipNumber.length === 15) {
                const currentAnimalDetails = { ...animalDetails }
                currentAnimalDetails.idOfMicrochip = chipNumber;
                setAnimalDetails(currentAnimalDetails);
            }

        };
        getDataHandler();

    }, [ownerId, animalId]);

    const clearOwner = () => {

        modifyTypes.add("ownerChange");
        setChangeType("newOwner");

        setOwnerDetails(
            {
                ownerId: 0,
                ownerName: '',
                zipCode: '',
                city: '',
                address: '',
                mobileNumber: '',
                phoneNumber: '',
                email: '',
                ownerNotification: ''
            }
        )
    }

    const saveAnimal = savedOwnerId => {
        let birthDate = animalDetails.birthDate || null;
        let deathDate = null;
        let dateOfSpaying = null;

        if (animalDetails.dateOfDeath && animalDetails.dateOfDeath !== 'Invalid date' && animalDetails.dateOfDeath !== '') {
            deathDate = animalDetails.dateOfDeath;
        }

        if (animalDetails.dateOfSpaying && animalDetails.dateOfSpaying !== 'Invalid date' && animalDetails.dateOfSpaying !== '') {
            dateOfSpaying = animalDetails.dateOfSpaying;
        }

        const bodyParameters = {
            animalName: animalDetails.animalName,
            breed: animalDetails.breed,
            species: animalDetails.species,
            birthDate: birthDate,
            birthDateIsEstimated: animalDetails.birthDateIsEstimated,
            dateOfDeath: deathDate,
            dateOfSpaying: dateOfSpaying,
            isSpayed: animalDetails.isSpayed,
            sex: animalDetails.sex,
            color: animalDetails.color,
            idOfMicrochip: animalDetails.idOfMicrochip,
            idOfVaccinationBook: animalDetails.idOfVaccinationBook,
            passportId: animalDetails.passportId,
            pedigree: animalDetails.pedigree,
            animalId: animalDetails.animalId,
            ownerId: savedOwnerId,
            comment: animalDetails.comment,
        };

        axiosCall.post('/animal/save',
            bodyParameters
        )
            .then(response => {
                if (animalDetails.idOfMicrochip !== '' && changeType !== '') {
                    saveDataChanges(response.data.animalId, savedOwnerId);
                } else {
                    history('/animal/' + response.data.animalId);
                }
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az állat adatainak mentése közben'))
            })
    }

    const saveDataChanges = (animalId, ownerId) => {

        const bodyParameters = {
            animalName: animalDetails.animalName,
            idOfMicrochip: animalDetails.idOfMicrochip,
            animalId: animalId,
            ownerName: ownerDetails.ownerName,
            ownerId: ownerId,
            changeType: changeType.toUpperCase(),
            eventDate: moment().format("YYYY-MM-DD"),
        };

        axiosCall.post('/petvetdata/save',
            bodyParameters
        )
            .then(() => {
                history('/animal/' + animalId);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a változások mentése közben'))
            })
    }

    const validate = () => {

        let hasErrors = false;

        let newErrors = { ...errors };

        if (ownerDetails.ownerName === '') {
            newErrors.ownerName = 'Tulajdonos neve kötelező!';
            hasErrors = true;
        }

        if (animalDetails.animalName === '') {
            newErrors.animalName = 'Állat neve kötelező!'
            hasErrors = true;
        }

        if ((new Date(animalDetails.birthDate) === "Invalid Date") || isNaN(new Date(animalDetails.birthDate))) {
            newErrors.birthDate = 'Születési dátum kötelező!'
            hasErrors = true;
        }

        setErrors(newErrors);
        return hasErrors;
    }

    const handleSave = () => {

        if (!validate()) {
            setButtonIsDisabled(true);
            const bodyParameters = {
                ownerId: ownerDetails.ownerId,
                ownerName: ownerDetails.ownerName,
                zipCode: ownerDetails.zipCode,
                city: ownerDetails.city,
                address: ownerDetails.address,
                mobileNumber: ownerDetails.mobileNumber,
                phoneNumber: ownerDetails.phoneNumber,
                email: ownerDetails.email,
                enableVaccinationReminder: ownerDetails.enableVaccinationReminder,
                ownerNotification: ownerDetails.ownerNotification,
            };

            axiosCall.post('/owner/save',
                bodyParameters
            )
                .then(response => {
                    saveAnimal(response.data.ownerId);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a tulajdonos adatainak mentése közben'))
                })
        }
    };

    const handleOwnerDetailChange = event => {
        let currentOwnerDetails = { ...ownerDetails };

        let key = event.target.name;
        let value = event.target.value;

        modifyTypes.add("ownerDataChange");
        setChangeType("ownerDataChange")

        if (key === 'ownerName') {
            let freshErrors = { ...errors };
            freshErrors.ownerName = null;

            setErrors(freshErrors);
        }

        if (key === 'zipCode' && value.length === 4) {
            let zipcodeObject = zipcodes.find(object => object.zipcode === +value);
            if (zipcodeObject) {
                currentOwnerDetails.city = zipcodeObject.city;
            }
        }

        if (key === 'ownerName' && value !== '') {
            const smallSearchWord = String(value).toLocaleLowerCase();
            axiosCall.get('/owner/search/' + smallSearchWord)
                .then(response => {
                    setSearchOwnerList(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a tulajdonosi lista betöltése közben'))
                })
        }

        if (key === 'ownerName' && value === '') {
            setSearchOwnerList([]);
        }

        currentOwnerDetails[key] = value;

        setOwnerDetails(currentOwnerDetails);
    }

    const handleAnimalDetailChange = event => {
        let currentAnimalDetails = { ...animalDetails };

        let key = event.target.name;
        let value = event.target.value;

        switch (key) {
            case "dateOfDeath":
                modifyTypes.add("dateOfDeathChange");
                setChangeType("death")
                break;
            case "dateOfSpaying":
                modifyTypes.add("spayingChange");
                setChangeType("spaying")
                break;
            case "isSpayed":
                modifyTypes.add("spayingChange");
                setChangeType("spaying")
                break;
            case "idOfMicrochip":
                modifyTypes.add("microchipChange");
                setChangeType("chip")
                break;
            case "passportId":
                modifyTypes.add("passportChange");
                setChangeType("passport")
                break;
            default:
                modifyTypes.add("animalDataChange");
                setChangeType("animalDataChange")
                break;
        }

        if (key === 'animalName') {
            let freshErrors = { ...errors };
            freshErrors.animalName = null;

            setErrors(freshErrors);
        }

        if (key === 'birthDate') {
            let freshErrors = { ...errors };
            freshErrors.birthDate = null;

            setErrors(freshErrors);
        }

        if (key === 'species') {
            currentAnimalDetails.breed = '';
            let species = speciesList.find(s => s.speciesName === value);
            const newBreedList = species.breeds.sort(nameCompare);
            setBreedList(newBreedList)
        }

        if (key === 'isSpayed') {
            currentAnimalDetails.isSpayed = !currentAnimalDetails.isSpayed;
        } else if (key === 'birthDateIsEstimated') {
            currentAnimalDetails.birthDateIsEstimated = !currentAnimalDetails.birthDateIsEstimated;
        } else if (key === 'birthDate') {
            currentAnimalDetails.birthDate = moment(value).format('YYYY-MM-DD')
        } else if (key === 'dateOfSpaying') {
            currentAnimalDetails.dateOfSpaying = moment(value).format('YYYY-MM-DD')
        } else if (key === 'dateOfDeath') {
            currentAnimalDetails.dateOfDeath = moment(value).format('YYYY-MM-DD');
        } else {
            currentAnimalDetails[key] = value;
        }

        setAnimalDetails(currentAnimalDetails);
    };

    const openOwnerList = () => {
        setOwnerListOpen(true);
        setSearchOwnerList([]);
    };

    const handleNameClose = () => {
        setTimeout(() => {
            setOwnerListOpen(false);
        }, "200")
    };

    const updateBreedList = () => {
        const currentSpecies = speciesList.find(item => item.speciesName === animalDetails.species)
        const newBreedList = currentSpecies.breeds.sort(nameCompare);
        setBreedList(newBreedList);
    };

    return (
        <div >
            <div className="d-flex align-items-end justify-content-between">
                <span className="mb-0 h5">Tulajdonos és állat adatainak szerkesztése</span>
                <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={handleSave}><FontAwesomeIcon icon={faFloppyDisk} disabled={buttonIsDisabled} /> Mentés</button>
                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => history(-1)}><FontAwesomeIcon icon={faXmark} /> Mégse</button>
                </div>
            </div>
            <hr className='mt-2' />
            {animalDetails.idOfMicrochip ?
                <div className="mb-4">
                    <span className="h6">Petvetdata adatmódosítás</span>
                    <hr className='mt-0' />
                    {modifyTypes.has("microchipChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'chip' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('chip')}><FontAwesomeIcon icon={faTowerBroadcast} /> Chip beültetés</button>}
                    {modifyTypes.has("passportChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'passport' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('passport')}><FontAwesomeIcon icon={faIdCard} /> Útlevél</button>}
                    {modifyTypes.has("dateOfDeathChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'death' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('death')}><FontAwesomeIcon icon={faCross} />  Elhullás</button>}
                    {modifyTypes.has("spayingChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'spaying' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('spaying')}><FontAwesomeIcon icon={faVenusMars} />  Ivartalanítás</button>}
                    {modifyTypes.has("ownerDataChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'ownerDataChange' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('ownerDataChange')}><FontAwesomeIcon icon={faUser} /> Tulaj adatváltozás</button>}
                    {modifyTypes.has("animalDataChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'animalDataChange' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('animalDataChange')}><FontAwesomeIcon icon={faPaw} />  Állat adatváltozás</button>}
                    {modifyTypes.has("ownerChange") && <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === 'newOwner' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('newOwner')}><FontAwesomeIcon icon={faUserPlus} />  Új tulaj</button>}
                    <button type="button" className={"btn me-2 rounded-pill btn-sm " + (changeType === '' ? 'btn-primary' : 'btn-outline-secondary')} onClick={() => setChangeType('')}><FontAwesomeIcon icon={faXmark} /> Nincs módosítandó</button>
                </div>
                :
                null
            }
            <form autoComplete="off">
                <div className="container-fluid ps-0 pe-0">
                    <div className="row gx-4">
                        <div className="col-xl-6">
                            <div className="d-flex justify-content-between">
                                <span className="h6">Tulajdonos adatai</span>
                                {ownerDetails.ownerId ?
                                    <div className="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" className="btn  btn-light" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={clearOwner}><FontAwesomeIcon icon={faUserPlus} /> Új tulajdonos</button>
                                    </div> :
                                    null
                                }
                            </div>
                            <hr className='mt-0' />
                            <div className="mb-2">
                                <div className='popperAnchor'>
                                    <p className='mb-0 text-secondary'><small>Név</small></p>
                                    <input
                                        className="form-control "
                                        name='ownerName'
                                        onFocus={openOwnerList}
                                        onBlur={handleNameClose}
                                        value={ownerDetails.ownerName}
                                        onChange={handleOwnerDetailChange}
                                    />
                                    {searchOwnerList.length > 0 && ownerListOpen ?
                                        <div className={'card shadow mb-5 border-top-0 border-bottom-0 popperAnchorBody'} >
                                            <div className="list-group list-group-flush">
                                                {searchOwnerList.slice(0, 20).map((owner, index) => {
                                                    return (
                                                        <button type="button" key={index} onClick={() => getSelectedOwner(owner.ownerId)} className="list-group-item list-group-item-action list-group-flush">
                                                            {owner.ownerName}<br />
                                                            <span className='text-secondary'>{owner.zipCode + ' ' + owner.city + ', ' + owner.address}</span>
                                                        </button>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        :
                                        null
                                    }
                                    {errors.ownerName ?
                                        <small className="text-danger">
                                            {errors.ownerName}
                                        </small>
                                        : null
                                    }
                                </div>
                            </div>
                            <div className="container ps-0 pe-0 mb-2">
                                <div className="row gx-2">
                                    <div className="col-3">
                                        <p className='mb-0 text-secondary'><small>Irányítószám</small></p>
                                        <input
                                            className="form-control"
                                            name='zipCode'
                                            value={ownerDetails.zipCode}
                                            onChange={handleOwnerDetailChange}
                                        />
                                    </div>
                                    <div className="col-9">
                                        <p className='mb-0 text-secondary'><small>Város</small></p>
                                        <input
                                            className="form-control"
                                            name='city'
                                            value={ownerDetails.city}
                                            onChange={handleOwnerDetailChange}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Cím</small></p>
                                <input
                                    className="form-control"
                                    name='address'
                                    value={ownerDetails.address}
                                    onChange={handleOwnerDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Mobilszám</small></p>
                                <input
                                    className="form-control"
                                    name='mobileNumber'
                                    value={ownerDetails.mobileNumber}
                                    onChange={handleOwnerDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Vezetékes telefon</small></p>
                                <input
                                    className="form-control"
                                    name='phoneNumber'
                                    value={ownerDetails.phoneNumber}
                                    onChange={handleOwnerDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Email</small></p>
                                <input
                                    type='email'
                                    className="form-control"
                                    name='email'
                                    value={ownerDetails.email}
                                    onChange={handleOwnerDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Megjegyzés tulajhoz</small></p>
                                <input
                                    className="form-control"
                                    name='ownerNotification'
                                    value={ownerDetails.ownerNotification}
                                    onChange={handleOwnerDetailChange}
                                />
                            </div>
                        </div>
                        <div className="col-xl-6">
                            <div className="d-flex justify-content-between">
                                <span className="h6">Állat adatai</span>
                            </div>
                            <hr className='mt-0' />
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Név</small></p>
                                <input
                                    className="form-control"
                                    name='animalName'
                                    id='animalName'
                                    required
                                    value={animalDetails.animalName}
                                    onChange={handleAnimalDetailChange}
                                />
                                {errors.animalName ?
                                    <small className="text-danger">
                                        {errors.animalName}
                                    </small>
                                    : null
                                }

                            </div>
                            <div className="container ps-0 pe-0 mb-2">
                                <div className="row gx-2">
                                    <div className="col-6">
                                        <p className='mb-0 text-secondary'><small>Faj</small></p>
                                        <select
                                            className="form-select"
                                            name='species'
                                            id='species'
                                            defaultValue={animalDetails.species}
                                            onChange={handleAnimalDetailChange}
                                        >
                                            <option value={animalDetails.species} disabled hidden>{animalDetails.species}</option>
                                            {
                                                speciesList.map((option, index) => {
                                                    return (
                                                        <option key={index} value={option.speciesName}>{option.speciesName}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>
                                    <div className="col-6">
                                        <p className='mb-0 text-secondary'><small>Fajta</small></p>
                                        <select
                                            className="form-select"
                                            name='breed'
                                            id='breed'
                                            onFocus={updateBreedList}
                                            defaultValue={animalDetails.breed}
                                            onChange={handleAnimalDetailChange}
                                        >
                                            <option value={animalDetails.breed} disabled hidden>{animalDetails.breed}</option>
                                            {
                                                breedList.map((option, index) => {
                                                    return (
                                                        <option key={index} value={option.breedName}>{option.breedName}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="container ps-0 pe-0 mb-2">
                                <div className="row gx-2">
                                    <div className="col-4">
                                        <p className='mb-0 text-secondary'><small>Születés dátuma</small></p>
                                        <input
                                            className="form-control"
                                            name='birthDate'
                                            id='birthDate'
                                            type='date'
                                            value={animalDetails.birthDate}
                                            onChange={handleAnimalDetailChange}
                                            max="9999-12-31"
                                        />
                                        {errors.birthDate ?
                                            <small className="text-danger">
                                                {errors.birthDate}
                                            </small>
                                            : null
                                        }
                                    </div>
                                    <div className="col-8">
                                        <div className='mt-4'>
                                            Becsült dátum &nbsp;
                                            <input className="form-check-input" type="checkbox" name="birthDateIsEstimated" checked={animalDetails.birthDateIsEstimated} onChange={handleAnimalDetailChange} />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="container ps-0 pe-0 mb-2">
                                <div className="row gx-2">
                                    <div className="col-4">
                                        <p className='mb-0 text-secondary'><small>Halál dátuma</small></p>
                                        <input
                                            className="form-control"
                                            name='dateOfDeath'
                                            type='date'
                                            value={animalDetails.dateOfDeath}
                                            onChange={handleAnimalDetailChange}
                                            max="9999-12-31"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="container ps-0 pe-0 mb-2">
                                <div className="row gx-2">
                                    <div className="col-4">
                                        <p className='mb-0 text-secondary'><small>Nem</small></p>
                                        <select
                                            className="form-select"
                                            name='sex'
                                            value={animalDetails.sex}
                                            onChange={handleAnimalDetailChange}
                                        >
                                            <option value="ismeretlen">ismeretlen</option>
                                            <option value="hím">hím</option>
                                            <option value="nőstény">nőstény</option>
                                        </select>
                                    </div>
                                    <div className="col-8">
                                        <div className='mt-4'>
                                            Ivartalanítva &nbsp;
                                            <input className="form-check-input" type="checkbox" name="isSpayed" checked={animalDetails.isSpayed} onChange={handleAnimalDetailChange} />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="container ps-0 pe-0 mb-2">
                                <div className="row gx-2">
                                    <div className="col-4">
                                        <p className='mb-0 text-secondary'><small>Ivartalanítás dátuma</small></p>
                                        <input
                                            className="form-control"
                                            name='dateOfSpaying'
                                            type='date'
                                            value={animalDetails.dateOfSpaying}
                                            onChange={handleAnimalDetailChange}
                                            max="9999-12-31"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Szín</small></p>
                                <select
                                    className="form-select"
                                    name='color'
                                    id='color'
                                    value={animalDetails.color}
                                    onChange={handleAnimalDetailChange}
                                >
                                    <option value={animalDetails.color} disabled hidden>{animalDetails.color}</option>
                                    <option value={''}>''</option>
                                    {

                                        colorList.map((option, index) => {

                                            return (
                                                <option key={index} value={option.colorName}>{option.colorName}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Chipszám</small></p>
                                <input
                                    className="form-control"
                                    name='idOfMicrochip'
                                    value={animalDetails.idOfMicrochip}
                                    onChange={handleAnimalDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Oltási könyv száma</small></p>
                                <input
                                    className="form-control"
                                    name='idOfVaccinationBook'
                                    value={animalDetails.idOfVaccinationBook}
                                    onChange={handleAnimalDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Útlevél száma</small></p>
                                <input
                                    className="form-control"
                                    name='passportId'
                                    value={animalDetails.passportId}
                                    onChange={handleAnimalDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Törzskönyv száma</small></p>
                                <input
                                    className="form-control"
                                    name='pedigree'
                                    id='pedigree'
                                    value={animalDetails.pedigree}
                                    onChange={handleAnimalDetailChange}
                                />
                            </div>
                            <div className="mb-2">
                                <p className='mb-0 text-secondary'><small>Megjegyzés állathoz</small></p>
                                <input
                                    className="form-control"
                                    name='comment'
                                    id='comment'
                                    value={animalDetails.comment}
                                    onChange={handleAnimalDetailChange}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div >
    )
}