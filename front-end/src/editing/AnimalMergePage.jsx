import React, { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCross, faFloppyDisk, faXmark } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import { axiosCall, handleError } from '../utils/ApiService';
import { calcAge } from '../utils/utils';
import AnimalSearchComponent from '../common/AnimalSearchComponent';

export default function AnimalMergePage() {

    const history = useNavigate();
    const search = useLocation().search;
    const animalId = new URLSearchParams(search).get('animalid');

    const [animalDetails, setAnimalDetails] = React.useState({
        animalName: '',
        breed: '',
        species: '',
        birthDate: moment(),
        dateOfDeath: moment(),
        dateOfSpaying: moment(),
        sex: '',
        color: '',
        idOfMicrochip: 0,
        idOfVaccinationBook: 0,
        pedigree: '',
        passportId: '',
        birthDateIsEstimated: false,
        animalId: 0,
        comment: '',
        ownerId: 0,
    });
    const [ownerDetails, setOwnerDetails] = React.useState({
        ownerId: 0,
        ownerName: '',
        zipCode: 0,
        city: '',
        address: '',
        email: '',
        mobileNumber: '',
        phoneNumber: '',
        ownerNotification: '',
    });

    const [animalGoalDetails, setAnimalGoalDetails] = React.useState({
        animalName: '',
        breed: '',
        species: '',
        birthDate: moment(),
        dateOfDeath: moment(),
        dateOfSpaying: moment(),
        sex: '',
        color: '',
        idOfMicrochip: 0,
        idOfVaccinationBook: 0,
        pedigree: '',
        passportId: '',
        birthDateIsEstimated: false,
        animalId: 0,
        comment: '',
        ownerId: 0,
    });
    const [ownerGoalDetails, setOwnerGoalDetails] = React.useState({
        ownerId: 0,
        ownerName: '',
        zipCode: 0,
        city: '',
        address: '',
        email: '',
        mobileNumber: '',
        phoneNumber: '',
        ownerNotification: '',
    });

    const addAnimal = animal => {
        console.log(animal)

        axiosCall.get('/animal/' + animal.animalId,
        )
            .then(response => {
                setAnimalGoalDetails(response.data);
                axiosCall.get('/owner/' + response.data.ownerId,
                )
                    .then(response => {
                        setOwnerGoalDetails(response.data);
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    })
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }

    useEffect(() => {
        const getDataHandler = () => {

            axiosCall.get('/animal/' + animalId,
            )
                .then(response => {
                    setAnimalDetails(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                })
        };
        getDataHandler();
    }, [animalId]);



    useEffect(() => {
        if (animalDetails.animalId) {

            const getOwnerDetails = () => {
                axiosCall.get('/owner/' + animalDetails.ownerId,
                )
                    .then(response => {
                        setOwnerDetails(response.data);
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    })
            };
            getOwnerDetails();
        }
    }, [animalDetails]);

    const handleMerge = () => {

        const bodyParameters = {
            goalAnimalId: animalGoalDetails.animalId,
            sourceAnimalId: animalDetails.animalId,
        };

        axiosCall.post('/animal/merge', bodyParameters
        )
            .then(() => {
                history('/animal/' + animalGoalDetails.animalId);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatlapok összevonása közben'))
            })
    };

    return (
        <div >
            <div className="d-flex align-items-end justify-content-between">
                <span className="mb-0 h5">Állatok adatlapjainak összevonása</span>
                <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-outline-secondary btn-sm"
                        disabled={animalGoalDetails.animalId > 0 ? false : true}
                        onClick={handleMerge}
                    ><FontAwesomeIcon icon={faFloppyDisk} /> Összevonás</button>
                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => history('/animal/' + animalDetails.animalId)}><FontAwesomeIcon icon={faXmark} /> Mégse</button>
                </div>
            </div>
            <hr className='mt-2' />
            <div className="container-fluid ps-0 pe-0">
                <div className="row gx-4">
                    <div className="col-md-6">
                        <div>
                            <span className="mb-0 h5"> {animalDetails.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>} {animalDetails.animalName}</span><br />
                            <span className="mb-0 text-secondary"> {animalDetails.breed}{animalDetails.species && ', '}  {animalDetails.species}</span>
                        </div>
                        <hr className='mt-1' />
                        <div className="container-fluid ps-0 pe-0 mb-4">
                            {!animalDetails.dateOfDeath ?
                                <div className="row gx-0">
                                    <div className="col-4">
                                        <p className='text-secondary'>
                                            Kor:
                                        </p>
                                    </div>
                                    <div className="col-8">
                                        <p>
                                            {calcAge(animalDetails.birthDate)} ({animalDetails.birthDateIsEstimated && '~ '}{moment(animalDetails.birthDate).format('YYYY. MM. DD.')})
                                        </p>
                                    </div>
                                </div>
                                :
                                <div className="row gx-0">
                                    <div className="col-4">
                                        <p className='text-secondary'>
                                            Halál dátuma:
                                        </p>
                                    </div>
                                    <div className="col-8">
                                        <p>
                                            {moment(animalDetails.dateOfDeath).format('YYYY. MM. DD.')}
                                        </p>
                                    </div>
                                </div>
                            }
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Ivar:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.dateOfSpaying && 'ivartalanított '}{animalDetails.sex}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Szín:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.color}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Chipszám:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.idOfMicrochip}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Oltási könyv száma:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.idOfVaccinationBook}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Útlevél száma:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.passportId}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Törzskönyv száma:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.pedigree}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Rendelői azonosító:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {animalDetails.animalId}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Megjegyzés:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p className='text-danger'>
                                        {animalDetails.comment}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <strong>{ownerDetails.ownerName}</strong>
                        <hr className='mt-0' />
                        <div className="container-fluid ps-0 pe-0">
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Cím:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {ownerDetails.zipCode + ' ' + ownerDetails.city + ', ' + ownerDetails.address}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Mobil:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {ownerDetails.mobileNumber}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Telefon:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {ownerDetails.phoneNumber}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Email:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p>
                                        {ownerDetails.email}
                                    </p>
                                </div>
                            </div>
                            <div className="row gx-0">
                                <div className="col-4">
                                    <p className='text-secondary'>
                                        Megjegyzés:
                                    </p>
                                </div>
                                <div className="col-8">
                                    <p className='text-danger'>
                                        {ownerDetails.ownerNotification}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        {(animalGoalDetails.animalId === 0 || animalGoalDetails.animalId === animalDetails.animalId) ?
                            <>
                                <div>
                                    <span className="mb-0 h5"> A cél adatlap keresése</span><br />
                                    <span className="mb-0 text-secondary"> A kiválasztott állat alá kerülnek a kezelések, a beolvasztott adatlap törlődik.</span>
                                </div>
                                <hr className='mt-1' />

                                {animalGoalDetails.animalId === animalDetails.animalId &&
                                    <div className="alert alert-danger" role="alert">
                                        A kiválasztott adatlap megegyezik az összevonni kívánttal!
                                    </div>}

                                <AnimalSearchComponent addAnimal={addAnimal} />
                            </>
                            :
                            <>
                                <div>
                                    <span className="mb-0 h5"> {animalGoalDetails.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>} {animalGoalDetails.animalName}</span><br />
                                    <span className="mb-0 text-secondary"> {animalGoalDetails.breed}{animalGoalDetails.species && ', '}  {animalGoalDetails.species}</span>
                                </div>
                                <hr className='mt-1' />
                                <div className="container-fluid ps-0 pe-0 mb-4">
                                    {!animalGoalDetails.dateOfDeath ?
                                        <div className="row gx-0">
                                            <div className="col-4">
                                                <p className='text-secondary'>
                                                    Kor:
                                                </p>
                                            </div>
                                            <div className="col-8">
                                                <p>
                                                    {calcAge(animalGoalDetails.birthDate)} ({animalGoalDetails.birthDateIsEstimated && '~ '}{moment(animalGoalDetails.birthDate).format('YYYY. MM. DD.')})
                                                </p>
                                            </div>
                                        </div>
                                        :
                                        <div className="row gx-0">
                                            <div className="col-4">
                                                <p className='text-secondary'>
                                                    Halál dátuma:
                                                </p>
                                            </div>
                                            <div className="col-8">
                                                <p>
                                                    {moment(animalGoalDetails.dateOfDeath).format('YYYY. MM. DD.')}
                                                </p>
                                            </div>
                                        </div>
                                    }
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Ivar:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.dateOfSpaying && 'ivartalanított '}{animalGoalDetails.sex}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Szín:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.color}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Chipszám:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.idOfMicrochip}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Oltási könyv száma:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.idOfVaccinationBook}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Útlevél száma:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.passportId}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Törzskönyv száma:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.pedigree}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Rendelői azonosító:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalGoalDetails.animalId}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Megjegyzés:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p className='text-danger'>
                                                {animalGoalDetails.comment}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <strong>{ownerGoalDetails.ownerName}</strong>
                                <hr className='mt-0' />
                                <div className="container-fluid ps-0 pe-0">
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Cím:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerGoalDetails.zipCode + ' ' + ownerGoalDetails.city + ', ' + ownerGoalDetails.address}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Mobil:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerGoalDetails.mobileNumber}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Telefon:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerGoalDetails.phoneNumber}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Email:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerGoalDetails.email}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Megjegyzés:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p className='text-danger'>
                                                {ownerGoalDetails.ownerNotification}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                </div>
            </div>
        </div >
    )
}