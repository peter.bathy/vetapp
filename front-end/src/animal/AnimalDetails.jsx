import React, { useCallback, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import moment from 'moment';
import { calcAge, replaceLineBreaks } from '../utils/utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChartPie, faCheck, faCross, faEdit, faEllipsis, faFileSignature, faFolderOpen, faMoneyBillTrendUp, faPaperclip, faPen, faPlus, faScaleUnbalanced, faQuestion, faXmark, faFilePdf, faTrash} from '@fortawesome/free-solid-svg-icons';
import MedicalRecordEditor from './MedicalRecordEditor';
import AttachementViewer from './AttachementViewer';
import AttachementBox from './AttachementBox';
import { axiosCall, handleError } from '../utils/ApiService';
import VaccinationReminderBox from './VaccinationReminderBox';
import AnimalPrintBoxNew from './AnimalPrintBoxNew';
import FinancialBox from './FinancialBox';

export default function AnimalDetails() {

    const { id } = useParams();
    const [endIndex, setEndIndex] = React.useState(-1);
    const [editorOpen, setEditorOpen] = React.useState(false);
    const [historyDetailId, setHistoryDetailId] = React.useState(0);
    const [debt, setDebt] = React.useState(0);
    const [debtOrPayBackValue, setDebtOrPayBackValue] = React.useState(0);
    //const [emailList, setEmailList] = React.useState([]);
    const [viewerContent, setViewerContent] = React.useState(
        {
            attachementType: '',
            filePaths: ['']
        },
    );
    const [currentMedicalDetails, setCurrentMedicalDetails] = React.useState({
        id: null,
        dateOfTreatment: moment(),
        vaccination: '',
        anamnesis: '',
        symptoms: '',
        therapy: '',
        otherDetails: ''
    })
    const [animalDetails, setAnimalDetails] = React.useState({
        animalName: '',
        breed: '',
        species: '',
        birthDate: moment(),
        dateOfDeath: moment(),
        dateOfSpaying: moment(),
        sex: '',
        color: '',
        idOfMicrochip: 0,
        idOfVaccinationBook: 0,
        pedigree: '',
        passportId: '',
        birthDateIsEstimated: false,
        animalId: 0,
        comment: '',
        ownerId: 0,
    });
    const [ownerDetails, setOwnerDetails] = React.useState({
        ownerId: 0,
        ownerName: '',
        zipCode: 0,
        city: '',
        address: '',
        email: '',
        mobileNumber: '',
        phoneNumber: '',
        ownerNotification: '',
    });
    const [medicalRecordList, setMedicalRecordList] = React.useState([{ id: 0 }]);

    const openAttachementBox = historyId => {
        setHistoryDetailId(historyId);
    }

    const closeAttachementBox = () => {
        getMedicalRecords();
    };

    const closeAttachementViewer = (update) => {
        if (update) {
            getMedicalRecords();
        }
    };

    useEffect(() => {
        const getDataHandler = () => {

            axiosCall.get('/animal/' + id,
            )
                .then(response => {

                    let aDetails = response.data

                    axiosCall.get('/animal-weight/' + id,
                    )
                        .then(response => {
                            console.log(response.data)
                            aDetails.weightOfAnimal = response.data;
                        })
                        .catch(error => {
                            toast.error(handleError(error, 'Hiba történt a tömegadatok betöltése közben'))
                        })

                    setAnimalDetails(aDetails);

                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                })
        };
        getDataHandler();
    }, [id]);

    const getMedicalRecords = useCallback(() => {

        axiosCall.get('/medicalrecord/' + id
        )
            .then(response => {
                //console.log(response.data)
                let recordList = response.data;
                recordList.map(element => {
                    if (element.attachements?.length > 0) {
                        let attachementList = [...element.attachements];

                        let labList = [];
                        let photoList = [];
                        let xrayList = [];
                        let ultrasoundList = [];
                        let otherList = [];
                        attachementList.forEach(currentAttachement => {
                            switch (currentAttachement.attachementType) {
                                case 'photo':
                                    photoList.push(currentAttachement.filePath)
                                    break;
                                case 'lab':
                                    labList.push(currentAttachement.filePath)
                                    break;
                                case 'xray':
                                    xrayList.push(currentAttachement.filePath)
                                    break;
                                case 'ultrasound':
                                    ultrasoundList.push(currentAttachement.filePath)
                                    break;
                                case 'other':
                                    otherList.push(currentAttachement.filePath)
                                    break;
                                default:
                                    break;
                            }
                        })
                        element.attachements = [
                            {
                                attachementType: 'photo',
                                filePaths: photoList
                            },
                            {
                                attachementType: 'lab',
                                filePaths: labList
                            },
                            {
                                attachementType: 'xray',
                                filePaths: xrayList
                            },
                            {
                                attachementType: 'ultrasound',
                                filePaths: ultrasoundList
                            },
                            {
                                attachementType: 'other',
                                filePaths: otherList
                            }
                        ]
                    }
                    return element;
                });

                setMedicalRecordList(recordList);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }, [id]);

    useEffect(() => {
        if (animalDetails.animalId) {

            getMedicalRecords();

            const getOwnerDetails = () => {
                axiosCall.get('/owner/' + animalDetails.ownerId,
                )
                    .then(response => {
                        //console.log(response.data)
                        setOwnerDetails(response.data);
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    })
            };
            getOwnerDetails();

            const getDebtDetails = () => {
                axiosCall.get('/owner-debt/' + animalDetails.ownerId,
                )
                    .then(response => {
                        let debtValue = 0;
                        response.data.forEach(element => {
                            debtValue += element.debtValue;
                        });
                        setDebt(debtValue);

                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    })
            };
            getDebtDetails();
        }
    }, [animalDetails, getMedicalRecords]);

    const openViewer = viewerContent => {
        setViewerContent(viewerContent);
    }

    const openMedicalRecordEditor = (medicalRecord, index) => {
        //console.log(medicalRecord);
        if (medicalRecord) {
            setEndIndex(index);
            setCurrentMedicalDetails({
                id: medicalRecord.id,
                dateOfTreatment: medicalRecord.dateOfTreatment,
                vaccination: medicalRecord.vaccination,
                anamnesis: medicalRecord.anamnesis,
                symptoms: medicalRecord.symptoms,
                therapy: medicalRecord.therapy,
                otherDetails: medicalRecord.otherDetails,
            });
        } else {
            setEditorOpen(true);
        }
    }

    const closeEditor = update => {
        setEndIndex(-1);
        setCurrentMedicalDetails({
            id: null,
            dateOfTreatment: moment(),
            vaccination: '',
            anamnesis: '',
            symptoms: '',
            therapy: '',
            otherDetails: '',
        });
        setEditorOpen(false);
        if (update) {
            getMedicalRecords();
        }
    }

    const clearDebt = () => {
        let currentValue = debt * -1;

        const bodyParameters = {
            ownerId: ownerDetails.ownerId,
            transactionDate: moment().format('YYYY-MM-DD'),
            debtValue: currentValue,
        };
        axiosCall.post('/owner-debt/save',
            bodyParameters
        )
            .then(() => {
                setDebt(0);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a tartozás-változás mentése közben'))
            })

    }

    const saveDebtOrPayback = (transactionType) => {

        if (debtOrPayBackValue !== 0) {
            let currentValue = debtOrPayBackValue;

            if (transactionType !== 'debt') {
                currentValue = (debtOrPayBackValue * -1);
            }
            const bodyParameters = {
                ownerId: ownerDetails.ownerId,
                transactionDate: moment().format('YYYY-MM-DD'),
                debtValue: currentValue,
            };

            axiosCall.post('/owner-debt/save',
                bodyParameters
            )
                .then(() => {
                    setDebt(Number(debt) + Number(currentValue));
                    setDebtOrPayBackValue(0);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a tartozás-változás mentése közben'))
                })
        }
    }

    const onChange = event => {
        setDebtOrPayBackValue(event.target.value);
    }

    const [weight, setWeight] = React.useState(0);


    const handleWeightChange = event => {
        setWeight(event.target.value);
    }

    const saveNewWeightData = () => {
        const bodyParameters = {
            animalId: animalDetails.animalId,
            weightingDate: moment().format('YYYY-MM-DD'),
            weight: Number(weight),
        };

        axiosCall.post('/animal-weight/save',
            bodyParameters
        )
            .then(() => {
                setWeight(0);
                let currentDetails = { ...animalDetails }
                currentDetails.weightOfAnimal.push(bodyParameters)
                setAnimalDetails(currentDetails);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a súly mentése közben'))
            })
    }


    const deleteWeight = (id) => {

        axiosCall.get('/animal-weight/delete/' + id)
            .then(() => {
                setWeight(0);
                let currentDetails = { ...animalDetails }
                currentDetails.weightOfAnimal = currentDetails.weightOfAnimal.filter(weightData => weightData.id !== id)
                setAnimalDetails(currentDetails);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a súly törlése közben'))
            })
    }

    return (
        <>
            <div className={'d-print-none'}>
                <div>
                    <div className="d-flex align-items-end justify-content-between">
                        <div>
                            <span className="mb-0 h5"> {animalDetails.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>} {animalDetails.animalName}</span><br />
                            <span className="mb-0 text-secondary"> {animalDetails.species}{animalDetails.breed && ', '}  {animalDetails.breed}</span>
                        </div>
                        <div>
                            <div className="btn-group" role="group" aria-label="Basic example">
                                <Link to={'/edit?animalid=' + id + '&ownerid=' + animalDetails.ownerId + '&chipnumber=0'} className="btn btn-outline-secondary btn-sm" role="button"><FontAwesomeIcon icon={faPen} /> Szerkesztés</Link>
                                <button type="button" className="btn btn-outline-secondary btn-sm" data-bs-toggle="modal" data-bs-target="#printBoxModal"><FontAwesomeIcon icon={faFilePdf} /> Kórlap letöltés</button>
                                <div className="btn-group" role="group">
                                    <button type="button" className="btn btn-outline-secondary btn-sm" data-bs-toggle="dropdown" aria-expanded="false">
                                        <FontAwesomeIcon icon={faEllipsis} />
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li><Link className="dropdown-item" to={'/merge?animalid=' + animalDetails.animalId}>Adatlapok összevonása</Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr className='mt-2' />
                    <div className="container-fluid ps-0 pe-0">
                        <div className="row gx-4">
                            <div className="col-md-4">
                                <div className="container-fluid ps-0 pe-0 mb-4">
                                    {!animalDetails.dateOfDeath ?
                                        <div className="row gx-0">
                                            <div className="col-4">
                                                <p className='text-secondary'>
                                                    Kor:
                                                </p>
                                            </div>
                                            <div className="col-8">
                                                <p>
                                                    {calcAge(animalDetails.birthDate)} ({animalDetails.birthDateIsEstimated && '~ '}{moment(animalDetails.birthDate).format('YYYY. MM. DD.')})
                                                </p>
                                            </div>
                                        </div>
                                        :
                                        <div className="row gx-0">
                                            <div className="col-4">
                                                <p className='text-secondary'>
                                                    Halál dátuma:
                                                </p>
                                            </div>
                                            <div className="col-8">
                                                <p>
                                                    {moment(animalDetails.dateOfDeath).format('YYYY. MM. DD.')}
                                                </p>
                                            </div>
                                        </div>
                                    }
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Ivar:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.isSpayed && 'ivartalanított '}{animalDetails.sex}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Szín:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.color}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Chipszám:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.idOfMicrochip}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Oltási könyv száma:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.idOfVaccinationBook}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Útlevél száma:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.passportId}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Törzskönyv száma:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.pedigree}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Rendelői azonosító:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {animalDetails.animalId}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Megjegyzés:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p className='text-danger'>
                                                {animalDetails.comment}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Tömeg:
                                            </p>
                                        </div>
                                        <div className="col-8 text-end">
                                            <div className="dropup-center dropup">
                                                <button type="button" data-bs-toggle="dropdown" aria-expanded="false" className=" btn-secondary btn me-1 rounded-pill" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} ><span className='me-1'><FontAwesomeIcon icon={faScaleUnbalanced} /> </span>{(animalDetails.weightOfAnimal && animalDetails.weightOfAnimal.length > 0) ? <>{animalDetails.weightOfAnimal[animalDetails.weightOfAnimal.length - 1].weight} kg</> : <FontAwesomeIcon icon={faPlus} />}</button>

                                                <div className="dropdown-menu shadow p-3" style={{ 'minWidth': '400px', 'maxWidth': '600px', 'width': '100%' }}>
                                                    <ul className="list-group  list-group-flush">
                                                        {animalDetails?.weightOfAnimal?.length === 0 ?
                                                            <>
                                                                <li className="list-group-item d-flex justify-content-between align-items-center px-0">
                                                                    Nincsenek korábbi adatok
                                                                </li>
                                                            </>
                                                            :
                                                            <>
                                                                {animalDetails?.weightOfAnimal?.map((weightRecord) => {
                                                                    return (
                                                                        <li className="list-group-item d-flex justify-content-between align-items-center px-0" key={weightRecord.id}>
                                                                            <span>{weightRecord.weightingDate} </span>
                                                                            <span>{weightRecord.weight} kg</span>
                                                                            <button type="button" className="btn btn-light btn-sm rounded-circle" onClick={() => deleteWeight(weightRecord.id)}><FontAwesomeIcon icon={faTrash} /></button>
                                                                        </li>)
                                                                })}
                                                            </>}
                                                    </ul>
                                                    <div className="input-group  input-group-sm">
                                                        <span className="input-group-text">Új érték: </span>
                                                        <input
                                                            className="form-control"
                                                            id="weight"
                                                            autoFocus
                                                            name='weight'
                                                            type='number'
                                                            min="0"
                                                            step="1"
                                                            value={weight > 0 ? weight : ''}
                                                            onChange={handleWeightChange}
                                                        />
                                                        <span className="input-group-text">kg</span>
                                                        <button className="btn btn-outline-secondary" onClick={saveNewWeightData} type="button">Hozzáadás</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex align-items-end justify-content-between">
                                    <strong><Link to={'/owner/' + animalDetails.ownerId} className="text-decoration-none">{ownerDetails.ownerName}</Link></strong>
                                    <div>

                                        <div className="dropup-center dropup">
                                            {debt !== 0 ?
                                                <button type="button" data-bs-toggle="dropdown" aria-expanded="false" className={(debt > 0 ? " btn-danger" : " btn-warning") + " btn me-1 rounded-pill"} style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} >{debt > 0 ? "Tartozás: " : "Túlfizetés: "} <strong>{debt} Ft.</strong></button>
                                                :
                                                <button type="button" data-bs-toggle="dropdown" aria-expanded="false" className="btn me-1 rounded-pill btn-light" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }}><FontAwesomeIcon icon={faMoneyBillTrendUp} /></button>
                                            }
                                            <div className="dropdown-menu shadow p-3" style={{ 'minWidth': '400px', 'maxWidth': '600px', 'width': '100%' }}>
                                                <div className="input-group input-group-sm">
                                                    <input type="number" min="0" className="form-control mb-2" placeholder="" value={debtOrPayBackValue} onChange={onChange} />
                                                    <div className="input-group-append">
                                                        <span className="input-group-text mb-2">Ft.</span>
                                                    </div>
                                                    {/*<button className="btn btn-sm  btn-outline-secondary" type="button" id="button-addon1" onClick={() => saveDebtChange(debtOrPayback)}>{debtOrPayback === "debt" ? "Hozzáad" : "Visszafizet"}</button>*/}
                                                </div>
                                                {debt !== 0 ?
                                                    <>
                                                        <button type="button" className="btn btn-primary me-1 rounded-pill" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={() => saveDebtOrPayback('payback')}><FontAwesomeIcon icon={faChartPie} /> Résztörlesztés felvitele</button>
                                                    </>
                                                    :
                                                    <>
                                                    </>
                                                }
                                                <button type="button" className="btn me-1 rounded-pill btn-danger" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={() => saveDebtOrPayback('debt')}><FontAwesomeIcon icon={faMoneyBillTrendUp} /> Tartozás felvitele</button><br />
                                                {debt !== 0 ?
                                                    <>
                                                        <hr />
                                                        <button type="button" className="btn btn-success me-1 rounded-pill" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={clearDebt}><FontAwesomeIcon icon={faCheck} /> Teljes összeg kiegyenlítve</button>
                                                    </>
                                                    :
                                                    <>
                                                    </>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr className='mt-0' />
                                <div className="container-fluid ps-0 pe-0">
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Cím:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerDetails.zipCode + ' ' + ownerDetails.city + ', ' + ownerDetails.address}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Mobil:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerDetails.mobileNumber}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Telefon:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerDetails.phoneNumber}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Email:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p>
                                                {ownerDetails.email}
                                            </p>
                                        </div>
                                    </div>
                                    {animalDetails.species === "eb" ?
                                        <div className="row gx-0">
                                            <div className="col-4">
                                                <p className='text-secondary'>
                                                    Oltási értesítő:
                                                </p>
                                            </div>
                                            <div className="col-8 text-end">
                                                <p>
                                                    <button type="button" className="btn btn-light btn-sm me-1 rounded-pill" data-bs-toggle="modal" data-bs-target="#vaccinationReminderModal" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }}>{ownerDetails.enableVaccinationReminder === null ? <><FontAwesomeIcon icon={faQuestion} /> nincs beállítva</> : ownerDetails.enableVaccinationReminder === true ? <><FontAwesomeIcon icon={faCheck} /> küldés beállítva</> : <><FontAwesomeIcon icon={faXmark} /> küldés letiltva</>}</button>
                                                </p>
                                            </div>
                                        </div>
                                        :
                                        null
                                    }
                                    <div className="row gx-0">
                                        <div className="col-4">
                                            <p className='text-secondary'>
                                                Megjegyzés:
                                            </p>
                                        </div>
                                        <div className="col-8">
                                            <p className='text-danger'>
                                                {ownerDetails.ownerNotification}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8">
                                <div className='p-3 bg-white rounded'>
                                    <div className="d-flex align-items-end justify-content-between">
                                        <div>
                                            Kórelőzmény
                                        </div>
                                        <div>
                                            {editorOpen || endIndex > -1 ? null : <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => setEditorOpen(true)}><FontAwesomeIcon icon={faFileSignature} /> Hozzáadás</button>}
                                        </div>
                                    </div>
                                    <hr className='mt-1' />

                                    {medicalRecordList.length === 0 && !editorOpen ?
                                        <>
                                            <p className='text-center fs-1'>
                                                <FontAwesomeIcon icon={faFolderOpen} />
                                            </p>
                                            <p className='text-secondary text-center'>
                                                Nincs korábbi kezelés
                                            </p>
                                        </>
                                        :
                                        <>
                                            {editorOpen && <MedicalRecordEditor closeEditor={closeEditor} animalId={animalDetails.animalId} currentMedicalDetails={currentMedicalDetails} />}
                                            {endIndex > 0 &&
                                                <>
                                                    {medicalRecordList.slice(0, endIndex).map((record, index) => {
                                                        return (
                                                            <div key={record.id} className='listItem ms-3 mb-3' style={{ fontSize: '1.1rem' }}>
                                                                <div className="d-flex align-items-end justify-content-between mb-1">
                                                                    <div className='mt-2'>
                                                                        <span className='me-2'>
                                                                            <strong>{moment(record.dateOfTreatment).format('YYYY. MM. DD. HH:mm')}</strong>
                                                                        </span>
                                                                        {record.attachements &&
                                                                            record.attachements.map((aItem) => {
                                                                                return (
                                                                                    <React.Fragment key={aItem.attachementType}>
                                                                                        {aItem.filePaths.length > 0 &&
                                                                                            <button type="button" className="btn btn-primary me-1 rounded-pill" data-bs-toggle="modal" data-bs-target="#attachementViewerModal" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={() => openViewer(aItem)}><FontAwesomeIcon icon={faPaperclip} /> {aItem.attachementType}</button>
                                                                                        }
                                                                                    </React.Fragment>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>
                                                                    <div className='listItemOptions'>
                                                                        {endIndex === -1 && editorOpen === false &&
                                                                            <>
                                                                                <div className="btn-group" role="group" aria-label="Basic example">
                                                                                    <button type="button" className="btn btn-outline-secondary" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-toggle="modal" data-bs-target='#attachementUploaderModal' onClick={() => openAttachementBox(record.id)}><FontAwesomeIcon icon={faPaperclip} /></button>
                                                                                    {/*<button type="button" className="btn btn-outline-secondary" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-toggle="modal" data-bs-target='#attachementUploaderModal' onClick={() => openAttachementBox(record.id)}><FontAwesomeIcon icon={faFlask} /></button>*/}
                                                                                    <button type="button" className="btn btn-outline-secondary" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={() => openMedicalRecordEditor(record, index)}><FontAwesomeIcon icon={faEdit} /></button>
                                                                                </div>
                                                                            </>
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <hr className='mb-0 mt-0' />
                                                                {record.vaccination && <p className='mt-0 mb-2'> {record.vaccination} </p>}
                                                                {record.anamnesis &&
                                                                    <>
                                                                        <small className='text-secondary'>Anamnézis</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.anamnesis)} </p>
                                                                    </>}
                                                                {record.symptoms &&
                                                                    <>
                                                                        <small className='text-secondary'>Szimptóma</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.symptoms)}</p>
                                                                    </>}
                                                                {record.therapy &&
                                                                    <>
                                                                        <small className='text-secondary'>Kezelés</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.therapy)} </p>
                                                                    </>}
                                                                {record.otherDetails &&
                                                                    <>
                                                                        <small className='text-secondary'>Megjegyzés, javaslat</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.otherDetails)} </p>
                                                                    </>}
                                                            </div>
                                                        );
                                                    })}
                                                </>
                                            }
                                            {endIndex >= 0 && <MedicalRecordEditor closeEditor={closeEditor} animalId={animalDetails.animalId} currentMedicalDetails={currentMedicalDetails} />}

                                            <>
                                                {medicalRecordList.slice(endIndex + 1, medicalRecordList.length).map((record, index) => {
                                                    return (
                                                        <React.Fragment key={record.id}>
                                                            <div className='listItem ms-3 mb-3' style={{ fontSize: '1.1rem' }}>
                                                                <div className="d-flex align-items-end justify-content-between mb-1">
                                                                    <div className='mt-2'>
                                                                        <span className='me-2'>
                                                                            <strong>{moment(record.dateOfTreatment).format('YYYY. MM. DD. HH:mm')}</strong>
                                                                        </span>
                                                                        {record.attachements &&
                                                                            record.attachements.map((aItem) => {
                                                                                return (
                                                                                    <React.Fragment key={aItem.attachementType}>
                                                                                        {aItem.filePaths.length > 0 &&
                                                                                            <button type="button" className="btn btn-primary me-1 rounded-pill" data-bs-toggle="modal" data-bs-target="#attachementViewerModal" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={() => openViewer(aItem)}><FontAwesomeIcon icon={faPaperclip} /> {aItem.attachementType}</button>
                                                                                        }
                                                                                    </React.Fragment>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>
                                                                    <div className='listItemOptions'>
                                                                        {endIndex === -1 && editorOpen === false &&
                                                                            <>
                                                                                <div className="btn-group" role="group" aria-label="Basic example">
                                                                                    <button type="button" className="btn btn-outline-secondary" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-toggle="modal" data-bs-target='#attachementUploaderModal' onClick={() => openAttachementBox(record.id)}><FontAwesomeIcon icon={faPaperclip} /></button>
                                                                                    {/*<button type="button" className="btn btn-outline-secondary" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-toggle="modal" data-bs-target='#attachementUploaderModal' onClick={() => openAttachementBox(record.id)}><FontAwesomeIcon icon={faFlask} /></button>*/}
                                                                                    <button type="button" className="btn btn-outline-secondary" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} onClick={() => openMedicalRecordEditor(record, index)}><FontAwesomeIcon icon={faEdit} /></button>
                                                                                </div>
                                                                            </>
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <hr className='mb-0 mt-0' />
                                                                {record.vaccination && <p className='mt-0 mb-2'> {record.vaccination} </p>}
                                                                {record.anamnesis &&
                                                                    <>
                                                                        <small className='text-secondary'>Anamnézis</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.anamnesis)} </p>
                                                                    </>}
                                                                {record.symptoms &&
                                                                    <>
                                                                        <small className='text-secondary'>Szimptóma</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.symptoms)}</p>
                                                                    </>}
                                                                {record.therapy &&
                                                                    <>
                                                                        <small className='text-secondary'>Kezelés</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.therapy)} </p>
                                                                    </>}
                                                                {record.otherDetails &&
                                                                    <>
                                                                        <small className='text-secondary'>Megjegyzés, javaslat</small>
                                                                        <p className='mt-0 mb-2 ms-2'>{replaceLineBreaks(record.otherDetails)} </p>
                                                                    </>}
                                                            </div>
                                                        </React.Fragment>
                                                    );
                                                })}
                                            </>
                                        </>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <AnimalPrintBoxNew medicalRecordList={medicalRecordList} animalDetails={animalDetails} ownerDetails={ownerDetails} />
            <VaccinationReminderBox ownerDetails={ownerDetails} setOwnerDetails={setOwnerDetails} />
            <FinancialBox ownerDetails={ownerDetails} debt={debt} setDebt={setDebt} />
            <AttachementViewer viewerContent={viewerContent} closeAttachementViewer={closeAttachementViewer} />
            <AttachementBox closeAttachementBox={closeAttachementBox} historyDetailId={historyDetailId} />
        </>
    )
}