import React, { useEffect } from 'react';
import moment from 'moment';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { addAutoResize } from '../utils/utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFloppyDisk, faListUl, faSyringe, faTrash, faXmark } from '@fortawesome/free-solid-svg-icons';
import ReactRouterPrompt from 'react-router-prompt';

export default function MedicalRecordEditor({ currentMedicalDetails, animalId, closeEditor }) {
    addAutoResize();

    const [localDetails, setLocalDetails] = React.useState({
        id: null,
        dateOfTreatment: moment(),
        vaccination: '',
        isRabies: false,
        anamnesis: '',
        symptoms: '',
        therapy: '',
        otherDetails: ''
    })
    const [vaccineRawList, setVaccineRawList] = React.useState([]);
    const [deleteApproval, setDeleteApproval] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [open, setOpen] = React.useState(false);
    const [searchList, setSearchList] = React.useState([]);
    const [activeSuggestionIndex, setActiveSuggestionIndex] = React.useState(0);
    const [startIndex, setStartIndex] = React.useState(-1);
    const [price, setPrice] = React.useState(0);
    const [currentSelection, setCurrentSelection] = React.useState();
    const [selectedProducts, setSelectedProducts] = React.useState([]);
    const [quantity, setQuantity] = React.useState(1);
    const [focusId, setFocusId] = React.useState('');
    const [blockChange, setBlockChange] = React.useState(false);
    const [isUnsaved, setIsUnsaved] = React.useState(false);

    useEffect(() => {
        setLocalDetails({ ...currentMedicalDetails })

        const getVaccineList = () => {
            axiosCall.get('/goods/vaccines'
            )
                .then(response => {
                    setVaccineRawList(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                })
        }
        getVaccineList();
    }, [currentMedicalDetails]);

    const handleSave = () => {

        let isRabies = false;

        let currentVaccineDetails = vaccineRawList.find(v => v.vaccineName === localDetails.vaccination);

        if (currentVaccineDetails) {
            if (currentVaccineDetails.isRabiesVaccination) {
                isRabies = true;
            }
        }

        const bodyParameters = {
            id: localDetails.id,
            animalId: animalId,
            dateOfTreatment: moment(localDetails.dateOfTreatment).format('YYYY-MM-DDTHH:mm:ss'),
            vaccination: localDetails.vaccination,
            isRabies: isRabies,
            anamnesis: localDetails.anamnesis,
            symptoms: localDetails.symptoms,
            therapy: localDetails.therapy,
            otherDetails: localDetails.otherDetails,
        };

        axiosCall.post('/medicalrecord/save',
            bodyParameters
        )
            .then(() => {
                closeEditor(true);
                setIsUnsaved(false);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a kórelőzmény mentése közben'))
            })
    };

    const handleDelete = () => {

        const bodyParameters = {
            id: localDetails.id,
            animalId: '',
            dateOfTreatment: '',
            vaccination: '',
            isRabies: false,
            anamnesis: '',
            symptoms: '',
            therapy: '',
            otherDetails: '',
        };

        axiosCall.post('/medicalrecord/delete', bodyParameters
        )
            .then(() => {
                closeEditor(true)
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a kórelőzmény törlése közben'))
            })
    };

    const closeSuggestions = () => {
        setAnchorEl(null);
        setOpen(false)
        setActiveSuggestionIndex(0);
        setCurrentSelection();
        setQuantity(1);
        document.getElementById(focusId).focus();
        setFocusId('');
    }

    const handleChange = (event) => {
        setIsUnsaved(true);

        if (event.nativeEvent.inputType === 'insertLineBreak' && blockChange) {
            setBlockChange(false);
        } else {
            let eventName = event.target.name;
            let eventValue = event.target.value;
            let index = -1;

            let allowedLength;

            switch (eventName) {
                case 'symptoms':
                case 'therapy':
                    allowedLength = 1500;
                    break;

                default:
                    allowedLength = 800
                    break;
            }

            if (eventValue.length > allowedLength) {
                eventValue = eventValue.substring(0, allowedLength);
            }

            if (allowedLength >= eventValue.length) {

                let currentDetails = { ...localDetails }

                if (eventName === 'symptoms') {
                    //index = eventValue.lastIndexOf('#');
                    let searchWord = '';

                    if (index > -1) {
                        searchWord = eventValue.substring(index + 1, eventValue.length).toLowerCase();
                        if (searchWord === '') {
                            setSearchList([]);
                            setActiveSuggestionIndex(0);
                        } else {
                            axiosCall.get('/goods/search/templates/' + searchWord)
                                .then(response => {
                                    setSearchList(response.data);
                                    setActiveSuggestionIndex(0);
                                })
                                .catch(error => {
                                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                                })
                        }
                    }
                }

                if (eventName === 'therapy') {
                    //index = eventValue.lastIndexOf('#');
                    let searchWord = '';

                    if (index > -1) {
                        searchWord = eventValue.substring(index + 1, eventValue.length).toLowerCase();
                        if (searchWord === '') {
                            setSearchList([]);
                            setActiveSuggestionIndex(0);
                        } else {
                            axiosCall.get('/goods/search/templates/' + searchWord)
                                .then(response => {
                                    setSearchList(response.data);
                                    setActiveSuggestionIndex(0);
                                })
                                .catch(error => {
                                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                                })
                        }
                    }
                    if (index === -1) {
                        index = eventValue.lastIndexOf('&');
                        searchWord = '';

                        if (index > -1) {
                            searchWord = eventValue.substring(index + 1, eventValue.length).toLowerCase();
                            if (searchWord === '') {
                                setSearchList([]);
                                setActiveSuggestionIndex(0);
                            } else {
                                axiosCall.get('/goods/search/gyógyszer?searchWord=' + searchWord)
                                    .then(response => {
                                        setSearchList(response.data);
                                        setActiveSuggestionIndex(0);
                                    })
                                    .catch(error => {
                                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                                    })
                            }
                        }
                    }
                }
                if (index > -1 && !open) {
                    setStartIndex(index);
                    setOpen(true);
                    setAnchorEl(event.currentTarget)
                    setFocusId(event.target.id);
                }

                if (index === -1 && open) {
                    closeSuggestions();
                }

                if (eventName === 'vaccination') {
                    let currentVaccineDetails = vaccineRawList.find(v => v.vaccineName === eventValue);

                    if (currentVaccineDetails) {
                        if (currentVaccineDetails.isRabiesVaccination) {
                            currentDetails.isRabies = true;
                        } else {
                            currentDetails.isRabies = false;
                        }
                    }
                }
                currentDetails[eventName] = eventValue;
                setLocalDetails(currentDetails);
            }
        }
    }

    const onSelect = (suggestion) => {
        if (suggestion.name) {
            setCurrentSelection(suggestion);
            setQuantity(suggestion.baseQuantity);
        } else {
            addGoodsDetails(suggestion);
            closeSuggestions();
        }
        setBlockChange(true);
    }

    document.addEventListener('keydown', function (e) {
        if (e.key === 'Enter' && open) {
            if (e.target.nodeName === 'TEXTAREA' && e.target.type === 'textarea') {
                e.preventDefault();
                return false;
            }
        }
    },  { once: true });

    const onKeyDown = (key, event) => {
        if (key.keyCode === 13 && open && !currentSelection) {
            if (searchList[activeSuggestionIndex].name) {

                setCurrentSelection(searchList[activeSuggestionIndex]);
                setQuantity(searchList[activeSuggestionIndex].baseQuantity);
            } else {
                addGoodsDetails(searchList[activeSuggestionIndex]);
                closeSuggestions();
            }
            setBlockChange(true);
        }

        if (key.keyCode === 13 && open && currentSelection) {
            addGoodsDetails(currentSelection);
            closeSuggestions();
        }

        if (key.keyCode === 40) {
            const newIndex = activeSuggestionIndex + 1;

            if (20 > newIndex) {
                setActiveSuggestionIndex(newIndex)
            }
        }

        if (key.keyCode === 38) {
            const newIndex = activeSuggestionIndex - 1;

            if (newIndex >= 0) {
                setActiveSuggestionIndex(newIndex);
            }
        }
    }

    const addGoodsDetails = selectedGoods => {
        let currentDetails = { ...localDetails }

        if (selectedGoods) {

            if (selectedGoods.name) {
                currentDetails.therapy = currentDetails.therapy.substring(0, startIndex) + selectedGoods.name + ' ' + quantity + ' ' + selectedGoods.quantityUnit;

                let addedValue = selectedGoods.sellPrice ? selectedGoods.sellPrice : selectedGoods.bruttoPrice;

                setPrice(price + (addedValue * quantity / selectedGoods.baseQuantity));

                let currentGoods = [...selectedProducts];
                selectedGoods.sellPrice = addedValue * quantity / selectedGoods.baseQuantity;
                currentGoods.push(selectedGoods)
                setSelectedProducts(currentGoods);

            } else {
                if (anchorEl.name === 'symptoms') {
                    currentDetails.symptoms = currentDetails.symptoms.substring(0, startIndex) + selectedGoods.templateBody;
                } else {
                    currentDetails.therapy = currentDetails.therapy.substring(0, startIndex) + selectedGoods.templateBody;
                }
            }
            const symptomsIndex = selectedGoods.keyWords.lastIndexOf('vizsgálat');
            const therapyIndex = selectedGoods.keyWords.lastIndexOf('kezelés');
            const drugIndex = selectedGoods.keyWords.lastIndexOf('gyógyszer');

            if (drugIndex > -1) {
                currentDetails.therapy = currentDetails.therapy.substring(0, startIndex) + selectedGoods.name + ' ' + quantity + ' ' + selectedGoods.quantityUnit;
            } else {
                let symptomsTemplate = selectedGoods.attachements.find(a => a.attachementTypeNAme === 'symptomsTemplate')
                let therapyTemplate = selectedGoods.attachements.find(a => a.attachementTypeNAme === 'therapyTemplate')

                let symptomsTemplateBody = symptomsTemplate ? symptomsTemplate.attachementBody : '';
                let therapyTemplateBody = therapyTemplate ? therapyTemplate.attachementBody : '';

                if (symptomsIndex > -1) {
                    currentDetails.symptoms = currentDetails.symptoms.substring(0, startIndex)
                }
                if (therapyIndex > -1) {
                    currentDetails.therapy = currentDetails.therapy.substring(0, startIndex)
                }
                currentDetails.symptoms += (currentDetails.symptoms.length > 0 ? ' ' : '') + symptomsTemplateBody;
                currentDetails.therapy += (currentDetails.therapy.length > 0 ? ' ' : '') + therapyTemplateBody;
            }
        }
        setLocalDetails(currentDetails);
    }

    const handleQuantityChange = event => {
        setQuantity(event.target.value);
    }

    const deleteProductFormList = index => {
        let list = [...selectedProducts];

        let minusValue = list[index].sellPrice;

        setPrice(price - minusValue);

        list.splice(index, 1);

        setSelectedProducts(list);
    }

    const suggestionBox = () => (
        <div className={'card shadow mb-5 p-2 border-top-0 border-bottom-0 popperAnchorBody'} >
            {currentSelection ?
                <div className='p-2 row'>
                    <div className="input-group">
                        {currentSelection.name}: &nbsp;
                        <input
                            className="form-control w-25 form-control-sm"
                            id="quantity"
                            autoFocus
                            name='quantity'
                            type='number'
                            min="0"
                            step="1"
                            value={quantity}
                            onChange={handleQuantityChange}
                            onKeyDown={key => onKeyDown(key)}
                        />
                        &nbsp;{currentSelection.quantityUnit}
                    </div>
                </div>
                :
                <React.Fragment>
                    <ul className="list-group list-group-flush">
                        {searchList.map((item, index) => {
                            return (
                                <React.Fragment key={index}>
                                    <li
                                        type='button'
                                        key={index}
                                        className={'list-group-item list-group-item-action d-flex justify-content-between align-items-start ' + (index === activeSuggestionIndex ? 'active' : null)}
                                        onClick={() => onSelect(item)}
                                    >
                                        <span>
                                            {item.name ? item.name : item.templateName}
                                        </span>
                                        {item.name &&
                                            <span>
                                                {item.sellPrice} Ft. / {item.baseQuantity} {item.quantityUnit}
                                            </span>
                                        }
                                    </li>
                                </React.Fragment>
                            );
                        })}
                    </ul>
                    <hr />
                    {searchList.length === 20 &&
                        <p>
                            A lista szűkítéséhez pontosítsd a keresést
                        </p>}
                </React.Fragment>
            }
        </div>
    );

    return (
        <React.Fragment>
            <ReactRouterPrompt when={isUnsaved}>
                {({ onConfirm, onCancel }) => (
                    <div className="alert alert-warning" role="alert">
                        <p>Nem mentett változtatások vannak. Az "OK" gombra kattintva a módosítások elvesznek és a kért oldal jelenik meg. A "Mégse" gombra kattintva tudod elmenteni a kórelőzményt.</p>

                        <button className='btn btn-outline-secondary btn-sm me-3' onClick={onConfirm}>Ok</button>
                        <button className='btn btn-outline-secondary btn-sm' onClick={onCancel}>Mégse</button>
                    </div>
                )}
            </ReactRouterPrompt>
            <div className="card shadow bg-body rounded" >
                <div className="card-body">
                    <form autoComplete="off">
                        <div className="d-flex align-items-start justify-content-between mb-2">
                            <div>
                                <small className='mb-0 text-secondary'>Kezelés időpontja</small>
                                <input
                                    className="form-control form-control-sm"
                                    name='dateOfTreatment'
                                    id='dateOfTreatment'
                                    type="datetime-local"
                                    value={moment(localDetails.dateOfTreatment).format('YYYY-MM-DDTHH:mm:ss')}
                                    onChange={handleChange}
                                    max="9999-12-31T23:59:59"
                                />
                            </div>
                            <div>
                                <div className="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={handleSave}><FontAwesomeIcon icon={faFloppyDisk} /> Mentés</button>
                                    {(deleteApproval && localDetails.id) ?
                                        <button type="button" className="btn btn-danger btn-sm" onClick={handleDelete}><FontAwesomeIcon icon={faTrash} /> Törlés</button>
                                        :
                                        <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => setDeleteApproval(true)}><FontAwesomeIcon icon={faTrash} /></button>
                                    }
                                    <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => closeEditor(false)}><FontAwesomeIcon icon={faXmark} /></button>
                                </div>
                            </div>
                        </div>
                        <div className="mb-2">
                            <small className='mb-0 text-secondary'>Oltás</small>
                            <div className="input-group flex-nowrap">
                                <select
                                    className='form-select'
                                    aria-label="multiple"
                                    value={localDetails.vaccination}
                                    name='vaccination'
                                    onChange={handleChange}
                                >
                                    <option value={localDetails.vaccination} disabled hidden>{localDetails.vaccination}</option>
                                    {
                                        vaccineRawList.map((vaccine, index) => {
                                            return (
                                                <option key={index} className={vaccine.isRabiesVaccination ? 'rabies-option' : ''} value={vaccine.vaccineName}>{vaccine.vaccineName} </option>
                                            )
                                        })
                                    }
                                </select>
                                <button className="btn btn-outline-secondary btn-sm" type="button" id="button-addon2"
                                    onClick={() => {
                                        let details = { ...localDetails }
                                        details.vaccination = '';
                                        details.isRabies = false;
                                        setLocalDetails(details);
                                    }}
                                >törlés</button>
                            </div>
                        </div>
                        <div className="mb-2">
                            <small className='mb-0 text-secondary'>Anamnézis</small>
                            <textarea
                                className="form-control"
                                name='anamnesis'
                                id='anamnesis'
                                data-autoresize
                                value={localDetails.anamnesis}
                                onChange={handleChange}
                            />
                            <p className='text-end text-secondary m-0'><small>{localDetails.anamnesis.length}/800</small></p>
                        </div>
                        <div className="mb-2">
                            <small className='mb-0 text-secondary'>Szimptóma</small>
                            <textarea
                                className="form-control"
                                name='symptoms'
                                id='symptoms'
                                data-autoresize
                                value={localDetails.symptoms}
                                onChange={handleChange}
                                onKeyDown={onKeyDown}
                            />
                            {(open && searchList.length > 0 && focusId === 'symptoms') ?
                                <>
                                    {suggestionBox()}
                                </>
                                :
                                null
                            }
                            <div className="d-flex align-items-start justify-content-between mb-2">
                                <div>
                                    {/*<small className='text-secondary'>Sablonok: # karakter</small>*/}
                                </div>
                                <div>
                                    <small className='text-secondary'>{localDetails.symptoms.length}/1500</small>
                                </div>
                            </div>
                        </div>
                        <div className="mb-2">
                            <div className='popperAnchor'>
                                <small className='mb-0 text-secondary'>Kezelés</small>
                                <textarea
                                    className="form-control"
                                    name='therapy'
                                    id='therapy'
                                    data-autoresize
                                    value={localDetails.therapy}
                                    onChange={handleChange}
                                    onKeyDown={onKeyDown}
                                />

                                {(open && searchList.length > 0 && focusId === 'therapy') ?
                                    <>
                                        {suggestionBox()}
                                    </>
                                    :
                                    null
                                }
                                <div className="d-flex align-items-start justify-content-between mb-2">
                                    <div>
                                    {/*<small className='text-secondary'>Sablonok: #, gyógyszerek és egyéb termékek: & karakter</small>*/}
                                        <small className='text-secondary fst-italic'>Gyógyszerek és egyéb termékek: & karakter</small>
                                    </div>
                                    <div>
                                        <small className='text-secondary'>{localDetails.therapy.length}/1500</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="mb-2">
                            <div>
                                <small className='mb-0 text-secondary'>Megjegyzés, javaslat</small>
                                <textarea
                                    className="form-control"
                                    name='otherDetails'
                                    id='otherDetails'
                                    data-autoresize
                                    value={localDetails.otherDetails}
                                    onChange={handleChange}
                                />
                                <div className="d-flex align-items-start justify-content-between mb-2">
                                    <div>
                                    </div>
                                    <div>
                                        <small className='text-secondary'>{localDetails.otherDetails.length}/800</small>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {!currentMedicalDetails.id ?
                            <>
                                <div className="container-fluid ps-0 pe-0 mb-3">
                                    <div className="row gx-2 gy-2">
                                        <div className="col-7">
                                        </div>
                                        <div className="col-4 text-end">
                                            {price} Ft.
                                        </div>
                                        <div className="col-1">
                                            <button className="btn btn-outline-primary btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                <FontAwesomeIcon icon={faListUl} />
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div className="collapse" id="collapseExample">
                                    <div className="container-fluid ps-0 pe-0">
                                        {selectedProducts.map((item, index) => {
                                            return (
                                                <div className="row gx-2 gy-6" key={index}>
                                                    <div className="col-3">
                                                    </div>
                                                    <div className="col-4 text-end">
                                                        {item.name}
                                                    </div>
                                                    <div className="col-4 text-end">
                                                        {item.sellPrice} Ft.
                                                    </div>
                                                    <div className="col-1">
                                                        <button type="button" className="btn btn-danger btn-sm" onClick={() => deleteProductFormList(index)}><FontAwesomeIcon icon={faTrash} /></button>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                            </>
                            :
                            null
                        }
                    </form>
                </div>
            </div >
        </React.Fragment >
    )
}