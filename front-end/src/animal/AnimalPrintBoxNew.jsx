import React, { useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf, faTrash } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import { toast } from 'react-toastify';
import { calcAge, replaceLineBreaks } from '../utils/utils';
import { axiosCall, handleError } from '../utils/ApiService';
import { Modal } from 'bootstrap';

export default function AnimalPrintBoxNew({ medicalRecordList, ownerDetails, animalDetails }) {

    const [medicalRecordPrintList, setMedicalRecordPrintList] = React.useState([]);
    const [startDate, setStartDate] = React.useState(moment());
    const [endDate, setEndDate] = React.useState(moment());
    const [attacheImages, setAttacheImages] = React.useState(true);
    const [attachePdfs, setAttachePdfs] = React.useState(true);
    const [anonymized, setAnonymized] = React.useState(false);

    useEffect(() => {
        let printList = [];
        const currentList = [...medicalRecordList];

        currentList.forEach(medicalRecord => {
            printList.push({
                id: medicalRecord.id,
                dateOfTreatment: medicalRecord.dateOfTreatment,
                treatmentDetails: medicalRecord.vaccination +
                    medicalRecord.anamnesis +
                    medicalRecord.symptoms +
                    medicalRecord.therapy +
                    medicalRecord.otherDetails
            })
        });

        if (printList.length > 0) {
            setStartDate(moment(printList[printList.length - 1].dateOfTreatment).format('YYYY-MM-DD'));
            setEndDate(moment(printList[0].dateOfTreatment).format('YYYY-MM-DD'));
        }
        setMedicalRecordPrintList(currentList);
    }, [medicalRecordList]);

    const deleteItem = (index) => {

        const newList = [...medicalRecordPrintList];
        newList.splice(index, 1);
        setMedicalRecordPrintList(newList);
        console.log(newList);
    }

    const changeInterval = event => {
        let newList = [];
        if (event.target.name === 'start') {
            setStartDate(moment(event.target.value).format('YYYY-MM-DD'));

            newList = medicalRecordList.filter(item => (moment(item.dateOfTreatment).format('YYYY-MM-DD') >= event.target.value)).filter(filtered => moment(filtered.dateOfTreatment).format('YYYY-MM-DD') <= endDate)
        } else {
            setEndDate(moment(event.target.value).format('YYYY-MM-DD'));
            newList = medicalRecordList.filter(item => (moment(item.dateOfTreatment).format('YYYY-MM-DD') <= event.target.value)).filter(filtered => moment(filtered.dateOfTreatment).format('YYYY-MM-DD') >= startDate)
        }
        setMedicalRecordPrintList(newList);
    }

    const callPrint = () => {

        let recordIdList = [];

        medicalRecordPrintList.forEach(record => {
            recordIdList.push(record.id);
        })

        const bodyParameters = {
            animalId: animalDetails.animalId,
            medicalRecordIds: recordIdList,
            attacheImages: attacheImages,
            attachePdfs: attachePdfs,
            anonymized: anonymized
        };

        const responseType = {
            responseType: 'blob'
        }

        axiosCall.post('/medicalrecord/pdf',
            bodyParameters,
            responseType
        )
            .then(response => {

                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', ownerDetails.ownerName + '_' + animalDetails.animalName + '_kórlap_' + moment().format('YYYY-MM-DD') + '.pdf');
                document.body.appendChild(link);
                link.click();

                handleClose();
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a pdf generálása közben'))
            })
    }


    const header = (animalDetails, ownerDetails) => {
        return (
            <table className='table table-borderless table-sm'>
                <tbody>
                    <tr>
                        <td className='p-0'>
                            Állat adatai: <br />
                            {animalDetails.animalName} - {animalDetails.breed}{animalDetails.species && ', '}  {animalDetails.species}<br />
                            <table className="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td className='p-0'>Kor:</td>
                                        <td className='p-0'>{calcAge(animalDetails.birthDate)}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Ivar:</td>
                                        <td className='p-0'>{(animalDetails.dateOfSpaying || animalDetails.isSpayed) && 'ivartalanított '}{animalDetails.sex}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Ivartalanítás időpontja:</td>
                                        <td className='p-0'>{animalDetails.dateOfSpaying ? moment(animalDetails.dateOfSpaying).format('YYYY. MM. DD.') : (animalDetails.isSpayed ? 'ismeretlen' : '-')}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Szín:</td>
                                        <td className='p-0'>{animalDetails.color}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Chipszám:</td>
                                        <td className='p-0'>{animalDetails.idOfMicrochip}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Oltási könyv száma:</td>
                                        <td className='p-0'>{animalDetails.idOfVaccinationBook}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Útlevél száma:</td>
                                        <td className='p-0'>{animalDetails.passportId}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Törzskönyv száma:</td>
                                        <td className='p-0'>{animalDetails.pedigree}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td className='p-0'>
                            Tulajdonos adatai: <br />
                            {ownerDetails.ownerName}<br />
                            <table className="table table-borderless ms-0 me-0">
                                <tbody>
                                    <tr>
                                        <td className='p-0'>Cím:</td>
                                        <td className='p-0'>{ownerDetails.zipCode + ' ' + ownerDetails.city + ', ' + ownerDetails.address}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Mobil:</td>
                                        <td className='p-0'>{ownerDetails.mobileNumber}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Telefon:</td>
                                        <td className='p-0'>{ownerDetails.phoneNumber}</td>
                                    </tr>
                                    <tr>
                                        <td className='p-0'>Email:</td>
                                        <td className='p-0'>{ownerDetails.email}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>


        )
    };

    const handleClose = () => {
        const modal = Modal.getOrCreateInstance(document.getElementById('printBoxModal'))
        modal.hide();
        setAnonymized(false);
        setMedicalRecordPrintList(medicalRecordList);
    }

    useEffect(() => {
        const modalObj = document.getElementById('printBoxModal');
        modalObj.addEventListener('hide.bs.modal', function (event) {
            setMedicalRecordPrintList(medicalRecordList);
        });
    }, [medicalRecordList])

    return (
        <div className="modal fade d-print-none" id="printBoxModal" tabIndex="-1" aria-labelledby="printBoxModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <div className='me-2'>
                            <p className='mb-0 text-secondary'><small>Kezdődátum:</small></p>
                            <input
                                className="form-control"
                                name='start'
                                id='start'
                                type='date'
                                value={startDate}
                                onChange={changeInterval}
                                max="9999-12-31"
                            />
                        </div>
                        <div className='me-3'>
                            <p className='mb-0 text-secondary'><small>Végdátum:</small></p>
                            <input
                                className="form-control"
                                name='end'
                                id='end'
                                type='date'
                                value={endDate}
                                onChange={changeInterval}
                                max="9999-12-31"
                            />
                        </div>
                        <div className='me-3'>
                            <p className='mb-0 text-secondary'><small>Képek csatolva: </small></p>
                            <input className="form-check-input" type="checkbox" name="attacheImages" checked={attacheImages} onChange={() => setAttacheImages(!attacheImages)} />
                        </div>
                        <div className='me-3'>
                            <p className='mb-0 text-secondary'><small>PDF-ek csatolva: </small></p>
                            <input className="form-check-input" type="checkbox" name="attachePdfs" checked={attachePdfs} onChange={() => setAttachePdfs(!attachePdfs)} />
                        </div>
                        <div>
                            <p className='mb-0 text-secondary'><small>Anonimizálás: </small></p>
                            <input className="form-check-input" type="checkbox" name="attachePdfs" checked={anonymized} onChange={() => setAnonymized(!anonymized)} />
                        </div>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={() => console.log('meghívva')}></button>
                    </div>
                    <div className="modal-body">
                        {header(animalDetails, ownerDetails)}
                        <div>
                            Kórelőzmény
                        </div>
                        <hr />
                        <ul className="list-group list-group-flush">
                            {medicalRecordPrintList.map((record, index) => {
                                return (
                                    <li className="list-group-item" key={record.id}>
                                        <div className="d-flex align-items-end justify-content-between">
                                            <div>
                                                {moment(record.dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}
                                            </div>
                                            <div>
                                                <button type="button" className="btn btn-primary btn-sm rounded-circle" onClick={() => deleteItem(index)}><FontAwesomeIcon icon={faTrash} /></button>
                                            </div>
                                        </div>
                                        <div >
                                            <div>
                                                {record.vaccination && record.vaccination}
                                                {record.anamnesis && replaceLineBreaks(record.anamnesis)}
                                                {record.symptoms ? replaceLineBreaks(record.symptoms) : null}
                                                {record.therapy ? replaceLineBreaks(record.therapy) : null}
                                                {record.otherDetails ? replaceLineBreaks(record.otherDetails) : null}
                                            </div>
                                        </div>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div className="d-flex align-items-end justify-content-end  p-2">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button type="button" className="btn btn-outline-secondary btn-sm" onClick={callPrint}><FontAwesomeIcon icon={faFilePdf} /> Tovább</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}