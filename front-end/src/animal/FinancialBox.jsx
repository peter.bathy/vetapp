import React from 'react';
import { axiosCall, handleError } from '../utils/ApiService';
import { toast } from 'react-toastify';
import { faCheck, faMoneyBillTrendUp, faChartPie } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import moment from 'moment';

export default function FinancialBox({ ownerDetails, debt, setDebt }) {

    const [debtOrPayBackValue, setDebtOrPayBackValue] = React.useState(0);

    const onDebtChange = event => {
        setDebtOrPayBackValue(event.target.value);
    }

    const clearDebt = () => {
        let currentValue = debt * -1;

        const bodyParameters = {
            ownerId: ownerDetails.ownerId,
            transactionDate: moment().format('YYYY-MM-DD'),
            debtValue: currentValue,
        };
        axiosCall.post('/owner-debt/save',
            bodyParameters
        )
            .then(() => {
                setDebt(0);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a tartozás-változás mentése közben'))
            })
    }

    const saveDebtOrPayback = (transactionType) => {

        if (debtOrPayBackValue !== 0) {
            let currentValue = debtOrPayBackValue;

            if (transactionType !== 'debt') {
                currentValue = (debtOrPayBackValue * -1);
            }
            const bodyParameters = {
                ownerId: ownerDetails.ownerId,
                transactionDate: moment().format('YYYY-MM-DD'),
                debtValue: currentValue,
            };

            axiosCall.post('/owner-debt/save',
                bodyParameters
            )
                .then(() => {
                    setDebt(Number(debt) + Number(currentValue));
                    setDebtOrPayBackValue(0);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a tartozás-változás mentése közben'))
                })
        }
    }

    return (
        <div className="modal" id="financialModal" tabIndex="-1">
            <div className="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">

                        <div className="input-group input-group-sm">
                            <input type="number" min="0" className="form-control mb-2" placeholder="" value={debtOrPayBackValue} onChange={onDebtChange} />
                            <div className="input-group-append">
                                <span className="input-group-text mb-2">Ft.</span>
                            </div>
                        </div>
                        {debt !== 0 ?
                            <>
                                <button type="button" className="btn btn-primary me-1 rounded-pill" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-dismiss="modal" onClick={() => saveDebtOrPayback('payback')}><FontAwesomeIcon icon={faChartPie} /> Résztörlesztés felvitele</button>
                            </>
                            :
                            <>
                            </>
                        }
                        <button type="button" className="btn me-1 rounded-pill btn-danger" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-dismiss="modal" onClick={() => saveDebtOrPayback('debt')}><FontAwesomeIcon icon={faMoneyBillTrendUp} /> Tartozás felvitele</button><br />
                        {debt !== 0 ?
                            <>
                                <hr />
                                <button type="button" className="btn btn-success me-1 rounded-pill" style={{ '--bs-btn-padding-y': '.25rem', '--bs-btn-padding-x': '.5rem', '--bs-btn-font-size': '.75rem' }} data-bs-dismiss="modal" onClick={clearDebt}><FontAwesomeIcon icon={faCheck} /> Teljes összeg kiegyenlítve</button>
                            </>
                            :
                            <>
                            </>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}