import React, { useEffect } from 'react';
import { pdfjs, Document, Page } from 'react-pdf';
import 'react-pdf/dist/esm/Page/AnnotationLayer.css';
import 'react-pdf/dist/esm/Page/TextLayer.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { toast } from 'react-toastify';
import { ACCESS_TOKEN } from "../utils/constants";
import { getWithExpiry } from '../utils/utils';
import { axiosCall, handleError } from '../utils/ApiService';
import { faCompress, faCompressAlt, faCompressArrowsAlt, faExpand, faExpandAlt, faExpandArrowsAlt, faMagnifyingGlassMinus, faMagnifyingGlassPlus, faRedo, faTrash, faUndo } from '@fortawesome/free-solid-svg-icons';
import { Modal } from 'bootstrap';

pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/build/pdf.worker.min.js`;

export default function AttachementViewer({ closeAttachementViewer, viewerContent }) {
    const [numPages, setNumPages] = React.useState(null);
    const [currentFileIndex, setCurrentFileIndex] = React.useState(0);
    const [imgWidth, setImageWidth] = React.useState(100);
    const [deleteIntent, setDeleteIntent] = React.useState(false);
    const [rotationValue, setRotationValue] = React.useState(0);
    const [expandedViewer, setExpandedViewer] = React.useState(false);

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
    };

    const isPdf = path => {
        if (path) {
            return path.split(".").pop() === 'pdf';
        }
    };

    const close = (updateList) => {
        closeAttachementViewer(updateList);
        setCurrentFileIndex(0);
        setDeleteIntent(false);
        setImageWidth(100);
        setRotationValue(0);
        setExpandedViewer(false);
    };

    const rotateImage = (direction) => {
        let newValue = rotationValue;
        if (direction === "clockwise") {
            newValue += 90;
        } else {
            newValue -= 90;
        }

        if (newValue > 270) {
            newValue = 0;
        } else if (newValue < 0) {
            newValue = 270;
        }

        setRotationValue(newValue);
    };



    const deleteFile = filePath => {
        let fileName = filePath.split('/').pop();

        axiosCall.get('/medicalrecord/attachement/delete/' + fileName
        )
            .then(response => {
                const modal = Modal.getOrCreateInstance(document.getElementById('attachementViewerModal'))
                modal.hide();

                close(true);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    };

    useEffect(() => {

        const modalObj = document.getElementById('attachementViewerModal');
        modalObj.addEventListener('hide.bs.modal', function (event) {
            setCurrentFileIndex(0);
            setDeleteIntent(false);
            setImageWidth(100);
            setRotationValue(0);
            setExpandedViewer(false);
        });
    }, [])

    return (
        <div className="modal fade" id="attachementViewerModal" tabIndex="-1" aria-labelledby="attachementViewerModalLabel" aria-hidden="true">
            <div className={"modal-dialog modal-dialog-scrollable modal-dialog-centered " + (expandedViewer ? "modal-fullscreen" : "modal-xl")}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="attachementViewerModalLabel">Csatolmány megjelenítő</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={() => close(false)}></button>
                    </div>
                    <div className="modal-body">
                        {isPdf(viewerContent.filePaths[currentFileIndex]) ?
                            <Document
                                file={{
                                    url: viewerContent.filePaths[currentFileIndex],
                                    httpHeaders: { Authorization: 'Bearer ' + getWithExpiry(ACCESS_TOKEN) },
                                }}
                                onLoadSuccess={onDocumentLoadSuccess}
                            >
                                {Array.from(
                                    new Array(numPages),
                                    (el, index) => (
                                        <Page
                                            scale={1.5}
                                            key={`page_${index + 1}`}
                                            pageNumber={index + 1}
                                        />
                                    ),
                                )}
                            </Document>
                            :
                            <>
                                <div style={{ width: imgWidth + '%' }}>
                                    <img src={viewerContent.filePaths[currentFileIndex]} alt='' width='100%' height='auto' style={{ transform: 'rotate(' + rotationValue + 'deg' }} />
                                </div>
                            </>
                        }
                    </div>
                    <div className="d-flex align-items-end justify-content-between  p-2">
                        {!isPdf(viewerContent.filePaths[currentFileIndex]) ?
                            <div className="btn-group" role="group" aria-label="Basic example">
                                {imgWidth < 300 && <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => setImageWidth(imgWidth + 50)}><FontAwesomeIcon icon={faMagnifyingGlassPlus} /></button>}
                                {imgWidth > 100 && <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => setImageWidth(imgWidth - 50)}><FontAwesomeIcon icon={faMagnifyingGlassMinus} /></button>}
                                <button type="button" className="btn btn-outline-secondary btn-sm ms-2" onClick={() => rotateImage("clockwise")}><FontAwesomeIcon icon={faRedo} /></button>
                                <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => rotateImage("counterclockwise")}><FontAwesomeIcon icon={faUndo} /></button>
                                {expandedViewer && <button type="button" className="btn btn-outline-secondary btn-sm ms-2" onClick={() => setExpandedViewer(false)}><FontAwesomeIcon icon={faCompressAlt} /></button>}
                                {!expandedViewer && <button type="button" className="btn btn-outline-secondary btn-sm ms-2" onClick={() => setExpandedViewer(true)}><FontAwesomeIcon icon={faExpandAlt} /></button>}
                            </div>
                            :
                            <span> </span>
                        }
                        {viewerContent.filePaths.length > 1 &&
                            <ul className="pagination pagination-sm m-0">
                                <li  >
                                    <button className="btn btn-light btn-sm mx-1" aria-label="Previous" onClick={() => setCurrentFileIndex(currentFileIndex - 1)} disabled={currentFileIndex === 0}>
                                        <span aria-hidden="true">&laquo;</span>
                                    </button>
                                </li>
                                {viewerContent.filePaths.map((v, index) => {
                                    return (
                                        <button key={index} className={'btn btn-sm mx-1 ' + (currentFileIndex === index ? 'btn-primary' : 'btn-light')} aria-label="Next" onClick={() => setCurrentFileIndex(index)}>
                                            <span >{index + 1}</span>
                                        </button>
                                    )
                                })}
                                <li >
                                    <button className="btn btn-light btn-sm mx-1" aria-label="Next" onClick={() => setCurrentFileIndex(currentFileIndex + 1)} disabled={currentFileIndex === viewerContent.filePaths.length - 1}>
                                        <span aria-hidden="true">&raquo;</span>
                                    </button>
                                </li>
                            </ul>
                        }
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {deleteIntent ?
                                <button type="button" className="btn btn-outline-danger btn-sm" onClick={() => deleteFile(viewerContent.filePaths[currentFileIndex])}><FontAwesomeIcon icon={faTrash} /> Törlés</button>
                                :
                                <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => setDeleteIntent(true)}><FontAwesomeIcon icon={faTrash} /></button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}