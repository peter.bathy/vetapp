import React, { useEffect } from 'react';
import { axiosCall, handleError } from '../utils/ApiService';
import { toast } from 'react-toastify';
import { faEnvelope, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Modal } from 'bootstrap';

export default function WeightBox({ ownerDetails, setOwnerDetails }) {

    const emailInput = React.createRef();
    const [emailIsValid, setEmailIsValid] = React.useState(true);

    const updatePermission = (permissionGiven) => {

        if ((ownerDetails.enableVaccinationReminder !== true && checkDataIsValid()) || (ownerDetails.enableVaccinationReminder === true)) {
            const bodyParameters = {
                ownerId: ownerDetails.ownerId,
                ownerName: ownerDetails.ownerName,
                zipCode: ownerDetails.zipCode,
                city: ownerDetails.city,
                address: ownerDetails.address,
                mobileNumber: ownerDetails.mobileNumber,
                phoneNumber: ownerDetails.phoneNumber,
                email: emailInput?.current?.value || ownerDetails.email,
                enableVaccinationReminder: permissionGiven,
                ownerNotification: ownerDetails.ownerNotification,
            };

            ownerDetails.enableVaccinationReminder = permissionGiven;

            if (permissionGiven === true) {
                axiosCall.post('/email/vaccination-reminder-registration',
                    bodyParameters
                )
                    .then(() => {
                        callOwnerDataSave(bodyParameters);
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt a levélküldés közben'));
                    });
            } else {
                callOwnerDataSave(bodyParameters);
            }
        }

        function callOwnerDataSave(bodyParameters) {
            axiosCall.post('/owner/save',
                bodyParameters
            )
                .then(response => {
                    setOwnerDetails(response.data);
                    let myModalEl = document.getElementById('vaccinationReminderModal');
                    let modal = Modal.getInstance(myModalEl);
                    modal.hide();
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a tulajdonos adatainak mentése közben'));
                });
        }
    }

    useEffect(() => {
        let myModalEl = document.getElementById('vaccinationReminderModal')
        myModalEl.addEventListener('shown.bs.modal', function (event) {
            setEmailIsValid(true);
        })
    }, []);

    const checkDataIsValid = () => {

        let result = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(emailInput.current.value);

        if (result === false) {
            setEmailIsValid(false)
        } else {
            setEmailIsValid(true)
        }

        return result;
    }

    return (
        <div className="modal fade" id="vaccinationReminderModal" tabIndex="-1" aria-labelledby="vaccinationReminderModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="vaccinationReminderModalLabel">Oltási emlékeztető kiküldésének beállításai</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">

                        {emailIsValid ? null :
                            <div className="alert alert-warning" role="alert">
                                Hiányzó vagy hibás e-mail cím!
                            </div>
                        }

                        {ownerDetails.enableVaccinationReminder === true ?
                            <p>
                                A hozzájárulás megvonásával a továbbiakban nem kap a tulajdonos értesítést az oltás lejárata előtt.
                            </p>
                            :
                            <div>
                                <p>
                                    Az értesítések bekapcsolásával az alábbi címre megy egy megerősítő-, illetve a jövőben az emlékeztető üzenet.
                                </p>
                                <form autoComplete='off'>
                                    <input
                                        className="form-control"
                                        id="email"
                                        ref={emailInput}
                                        autoFocus
                                        name='email'
                                        type='email'
                                        defaultValue={ownerDetails.email}
                                    />
                                </form>
                            </div>
                        }
                    </div>
                    <div className="d-flex justify-content-end  p-2">
                        {ownerDetails.enableVaccinationReminder === true &&
                            <button type="button" className="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal" onClick={() => updatePermission(false)}><FontAwesomeIcon icon={faTrash} /> Jövőbeni értesítők törlése</button>
                        }
                        {(ownerDetails.enableVaccinationReminder === false || ownerDetails.enableVaccinationReminder === null) &&
                            <button type="button" className="btn btn-outline-secondary btn-sm" onClick={() => updatePermission(true)}><FontAwesomeIcon icon={faEnvelope} /> Jövőbeni értesítők bekapcsolása</button>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}