import React, { useMemo } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFlask, faImages, faLaptopMedical, faPaperclip, faTrash, faUpload, faXRay } from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { FileUploader } from "react-drag-drop-files";

const fileTypes = ["PDF", "JPG", "JPEG", "WEBP", "BMP", "PNG", "GIF"];

export default function AttachementBox(props) {

    const [files, setFiles] = React.useState([]);
    const [fileError, setFileError] = React.useState("");
    const [attachementType, setAttachementType] = React.useState('');

    const uploadFiles = () => {

        const formData = new FormData();
        for (let i = 0; i < files.length; i++) {
            formData.append('file', files[i])
        }
        formData.append("attachementType", attachementType);
        formData.append("medicalRecordId", props.historyDetailId);

        axiosCall.post('/medicalrecord/saveAttachement',
            formData,
            {
                headers: {
                    "Content-Type": "multipart/form-data",
                }
            }
        )
            .then(() => {
                toast.success('A csatolmány mentése sikerült')
                closeBox(true);
            }).catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }

    const closeBox = (refreshMedicalRecordList) => {
        setAttachementType('');
        if (refreshMedicalRecordList) {
            props.closeAttachementBox();
        }
        setFiles([]);
        setFileError("");
    }

    const handleChange = (uploadableFiles) => {
        setFiles(uploadableFiles);
        let tempError = "";
        let sumSize = 0;

        Array.from(uploadableFiles).forEach(file => {
            if (file.size >= 10485760) {
                tempError = "fileSize"
            }
            sumSize += file.size;
        });

        if (sumSize >= 52428800) {
            tempError = "packageSize"
        }

        if (tempError) {
            setFileError(tempError);
        }
    };

    const deleteItem = (index) => {
        console.log(index);

        let tempFiles = [...files];

        tempFiles.splice(index, 1)

        let tempError = "";
        let sumSize = 0;

        Array.from(tempFiles).forEach(file => {
            if (file.size >= 10485760) {
                tempError = "fileSize"
            }
            sumSize += file.size;
        });

        if (sumSize >= 52428800) {
            tempError = "packageSize"
        }

        if (tempError) {
            setFileError(tempError);
        } else {
            setFileError("");
        }

        setFiles(tempFiles);
    }



    return (
        <div className="modal fade" id="attachementUploaderModal" tabIndex="-1" aria-labelledby="attachementUploaderModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="attachementUploaderModalLabel">Csatolmány feltöltő</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={closeBox}></button>
                    </div>
                    <div className="modal-body">
                        {attachementType ?
                            <>
                                {files.length === 0 ?
                                    <FileUploader handleChange={handleChange} name="files" types={fileTypes} maxSize="20" multiple={true} />
                                    :
                                    <ul className="list-group">
                                        {Array.from(files).map((file, index) => {
                                            return (
                                                <li key={file} className={"list-group-item " + (file.size >= 10485760 ? "list-group-item-danger" : "")}>
                                                    <div className="d-flex align-items-end align-items-center justify-content-between ">
                                                        <div>
                                                            <span>{file.name}</span><br />
                                                            <small className='text-secondary'>{(file.size / 1048576).toFixed(2)} MB</small> {file.size >= 10485760 ? <small className='text-danger fw-bold'> Túl nagy fájlméret!</small> : ""}
                                                        </div>
                                                        <div>
                                                            <button type="button" className="btn btn-light btn-sm" onClick={() => deleteItem(index)}><FontAwesomeIcon icon={faTrash} /></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            )
                                        }
                                        )}
                                    </ul>
                                }
                            </>
                            :
                            <>
                                <div className="container-fluid ps-0 pe-0">
                                    <div className="row gx-6">
                                        <div className="col-sm-4">
                                            <div className="d-grid gap-2">
                                                <button type="button" className="btn btn-light shadow-sm p-3 my-3 rounded" onClick={() => setAttachementType('lab')}>
                                                    <h1 className='text-center'>
                                                        <FontAwesomeIcon icon={faFlask} />
                                                    </h1>
                                                    <p className='text-center text-secondary'>
                                                        labor
                                                    </p>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="d-grid gap-2">
                                                <button type="button" className="btn btn-light shadow-sm p-3 my-3 rounded" onClick={() => setAttachementType('xray')}>
                                                    <h1 className='text-center'>
                                                        <FontAwesomeIcon icon={faXRay} />
                                                    </h1>
                                                    <p className='text-center text-secondary'>
                                                        röntgen
                                                    </p>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="d-grid gap-2">
                                                <button type="button" className="btn btn-light shadow-sm p-3 my-3 rounded" onClick={() => setAttachementType('photo')}>
                                                    <h1 className='text-center'>
                                                        <FontAwesomeIcon icon={faImages} />
                                                    </h1>
                                                    <p className='text-center text-secondary'>
                                                        kép
                                                    </p>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="d-grid gap-2">
                                                <button type="button" className="btn btn-light shadow-sm p-3 my-3 rounded" onClick={() => setAttachementType('other')}>
                                                    <h1 className='text-center'>
                                                        <FontAwesomeIcon icon={faPaperclip} />
                                                    </h1>
                                                    <p className='text-center text-secondary'>
                                                        egyéb
                                                    </p>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="d-grid gap-2">
                                                <button type="button" className="btn btn-light shadow-sm p-3 my-3 rounded" onClick={() => setAttachementType('ultrasound')}>
                                                    <h1 className='text-center'>
                                                        <FontAwesomeIcon icon={faLaptopMedical} />
                                                    </h1>
                                                    <p className='text-center text-secondary'>
                                                        ultrahang
                                                    </p>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                    <div className="d-flex justify-content-end  p-2">
                        {(files.length > 0 && !fileError) ?
                            <button type="button" className="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal" onClick={uploadFiles}><FontAwesomeIcon icon={faUpload} /> Feltöltés</button>
                            :
                            null
                        }
                        {(fileError) ?
                            <div class="alert alert-danger w-100 text-center" role="alert">
                                {fileError === "fileSize" ?
                                    "Egy vagy több fájl mérete meghaladja a limitet! (10 MB)"
                                    :
                                    "A feltöltendő fájlok összesített fájlmérete meghaladja a limitet! (50 MB)"
                                }
                            </div>
                            :
                            null
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}