import moment from "moment";

const TOKEN_KEY = 'accessToken';

export const login = () => {
    localStorage.setItem(TOKEN_KEY, 'TestLogin');
}

export const logout = () => {
    localStorage.removeItem(TOKEN_KEY);
}

export const getWithExpiry = key => {

    const itemStr = localStorage.getItem(key)
    if (!itemStr) {
        return null
    }
    const item = JSON.parse(itemStr)
    const now = new Date()
    if (now.getTime() > item.expiry) {
        localStorage.removeItem(key)
        return null
    }
    return item.value
}

export const isLogin = () => {

    return (getWithExpiry(TOKEN_KEY) ? true : false)
}

export const setWithExpiry = (key, value, ttl) => {
    const now = new Date()

    const item = {
        value: value,
        expiry: now.getTime() + ttl,
    }
    localStorage.setItem(key, JSON.stringify(item))
}

export const replaceLineBreaks = oldStr => {
    return oldStr.split('\n').map(str => <span key={str}>{str}<br /></span>);
}

export const calcAge = (birthDate, customDate) => {

    let now = moment(new Date());
    if (customDate) {
        now = moment(customDate);
    }
    const end = moment(birthDate);
    let duration = moment.duration(now.diff(end));

    if (moment(birthDate).isAfter(moment().subtract(1, 'year'))) {
        if (duration.asMonths() < 1) {
            return Math.floor(duration.asWeeks()) + ' hetes'
        } else {
            return Math.floor(duration.asMonths()) + ' hónapos'
        }
    } else {
        return Math.floor(duration.asYears()) + ' éves'
    }
}

export const addAutoResize = () => {

    document.querySelectorAll('[data-autoresize]').forEach((element) => {
        element.style.resize = 'none';
        element.style.boxSizing = 'border-box';
        function updateHeight(el) {
            element.style.height = '';
            element.style.height = el.scrollHeight + 'px';
        }
        setTimeout(() => {
            updateHeight(element);
        }, 200)

        element.addEventListener('input', () => updateHeight(element));
        element.removeAttribute('data-autoresize');
    });
}