import axios from "axios";
import { ACCESS_TOKEN } from "./constants";
import { getWithExpiry } from './utils';

export const axiosCall = axios.create({
    baseURL: "http://localhost:5003/api",
    headers: {
        "Content-type": "application/json"
    }
});

// Set the AUTH token for any request
axiosCall.interceptors.request.use(function (config) {
    const token = getWithExpiry(ACCESS_TOKEN);
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    return config;
});

export const handleError = (error, customErrorMessage) => {

    let message = 'Hiba történt a program futása közben';

    if (error.response) {
        if (error.response.status === 401) {
            window.location.href = "/signin"
        }
        if (customErrorMessage) {
            message = customErrorMessage;
        } else {
            message = error.response.data.error ? error.response.data.error : error.response.data.message;
            console.log(error.response)
        }
    }
    console.log(error);
    return message;
}
