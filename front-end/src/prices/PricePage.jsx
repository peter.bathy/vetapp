import React, { useEffect } from 'react';
import {toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';

const compare = (a, b) => {
    if (a.name < b.name) {
        return -1;
    }
    if (a.name > b.name) {
        return 1;
    }
    return 0;
}

export default function PricePage(props) {
    const [nameSearchValue, setNameSearchValue] = React.useState('');
    
    const [priceList, setPriceList] = React.useState([{
        id: 1,
        name: '',
        sellPrice: 0,
        baseQuantityWithUnit: '1 db',
        comment: '',
        keyWords: [],
    },
    ]);
    const [filteredList, setFilteredList] = React.useState(priceList);

    useEffect(() => {

        axiosCall.get('/goods/active-goods'
        )
            .then(response => {
                let list = response.data.sort(compare);
                setPriceList(list);
                setFilteredList(list);
            })
            .catch(error => {
                toast.error(handleError(error))
            });

    }, []);

    const handleChange = event => {
        let key = event.target.name;
        let value = event.target.value;

        if (key === 'name') {
            setNameSearchValue(value)
        }
    }

    useEffect(() => {

        if (nameSearchValue.length === 0) {
            setFilteredList(priceList);
        } else {
            setFilteredList(priceList.filter(item => item.name.toLowerCase().includes(nameSearchValue.toLowerCase())))
        }
    }, [nameSearchValue]);

    return (
        <React.Fragment>
            <div >
                <div>
                    <div className="d-flex align-items-end justify-content-between">
                        <span className="mb-0 h5">Árlista</span>

                    </div>
                    <hr className='mt-2' />

                    <table className="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <input
                                        className="form-control"
                                        key='name'
                                        name='name'
                                        type='text'
                                        placeholder='Név'
                                        value={nameSearchValue}
                                        onChange={handleChange}
                                    />
                                </th>
                                <th scope="col">Ár</th>
                                <th scope="col">Megjegyzés</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredList.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{item.name}</td>
                                        <td>{item.sellPrice} Ft. / {item.baseQuantityWithUnit}</td>
                                        <td>{item.comment}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div >
        </React.Fragment >
    )
}