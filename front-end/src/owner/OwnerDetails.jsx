import React, { useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { calcAge } from '../utils/utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCross } from '@fortawesome/free-solid-svg-icons';

export default function OwnerDetails() {

    const { id } = useParams();
    const history = useNavigate();

    const [ownerDetails, setOwnerDetails] = React.useState({
        animals: [],
        email: "",
        mobileNumber: "",
        zipCode: '',
        city: '',
        address: "",
        ownerId: 0,
        ownerName: '',
        ownerNotification: "",
        phoneNumber: ""
    });

    useEffect(() => {
        const getDataHandler = () => {
            axiosCall.get('/owner/withlist/' + id
            )
                .then(response => {
                    setOwnerDetails(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                })
        };
        getDataHandler();
    }, [id]);

    const goToAnimal = (animal) => {
        let newHistoryList = [];

        if (localStorage.getItem('searchHistory')) {
            newHistoryList = JSON.parse(localStorage.getItem('searchHistory'));
        }

        let index = newHistoryList.findIndex(a => a.animalId === animal.animalId);

        if (index >= 0) {
            newHistoryList.splice(index, 1);
            newHistoryList.unshift(animal);
        } else {
            newHistoryList.unshift(animal);
        }

        if (newHistoryList.length > 10) {
            newHistoryList.pop();
        }

        localStorage.setItem('searchHistory', JSON.stringify(newHistoryList));
        history("/animal/" + animal.animalId);
    }

    return (
        <div>
            <div className="d-flex align-items-end justify-content-between">
                <div>
                    <span className="mb-0 h5"> {ownerDetails.ownerName}</span><br />
                    <span className="mb-0 text-secondary"> {ownerDetails.zipCode + ' ' + ownerDetails.city + ', ' + ownerDetails.address}{ownerDetails.mobileNumber ? ', mobil: ' + ownerDetails.mobileNumber : null}{ownerDetails.phoneNumber ? ', telefon: ' + ownerDetails.phoneNumber : null}{ownerDetails.email ? ', email: ' + ownerDetails.email : null}</span>
                </div>
            </div>
            <hr className='mt-2 mb-4' />
            <div className="container-fluid ps-0 pe-0">
                <div className="row g-3 row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-xxl-6">
                    {ownerDetails.animals.map((animal, index) =>
                        <div className="col" key={index}>
                            <button className="btn btn-light h-100 w-100 shadow-sm text-start" type="button" onClick={() => goToAnimal(animal)} >
                                <span className='fs-5'>
                                    {animal.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>} {animal.animalName} &nbsp; - &nbsp;
                                </span>
                                <span className='text-secondary fs-6'>
                                    {animal.animalId}
                                    <br />
                                    {animal.breed}{animal.species && ', '} {animal.species}, {!animal.dateOfDeath && calcAge(animal.birthDate)}
                                </span>
                            </button>
                        </div>
                    )}
                    <div className="col">
                        <Link className="btn btn-light h-100 w-100 shadow-sm" type="button" to={'/edit?animalid=0&ownerid=' + id + '&chipnumber=0'}><span className='fs-1'>+</span></Link>
                    </div>
                </div>
            </div>
        </div >
    )
}