import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {toast } from 'react-toastify';
import moment from 'moment';
import { axiosCall, handleError } from '../utils/ApiService';
import { faCheck, faCross, faIdCard, faListUl, faPaw, faQuestion, faTowerBroadcast, faUser, faUserPlus, faVenusMars } from '@fortawesome/free-solid-svg-icons';
import { useEffect } from 'react';

export default function ChangeList() {

    const [changeList, setChangeList] = React.useState([]);
    const [limitValue, setLimitValue] = React.useState(0);
    const [oldChangesList, setOldChangesList] = React.useState([]);
    

    const getDatas = useCallback(() => {

        axiosCall.get('/petvetdata')
            .then(response => {
                setChangeList(response.data);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }, []);

    useEffect(() => {
        getDatas();
    }, [getDatas])

    const iconSelector = changeType => {
        switch (changeType) {
            case 'CHIP':
                return <><FontAwesomeIcon icon={faTowerBroadcast} /> Chip beültetés</>;
            case 'DEATH':
                return <><FontAwesomeIcon icon={faCross} /> Elhullás</>;
            case 'SPAYING':
                return <><FontAwesomeIcon icon={faVenusMars} /> Ivartalanítás</>;
            case 'PASSPORT':
                return <><FontAwesomeIcon icon={faIdCard} /> Útlevél</>;
            case 'ANIMALDATACHANGE':
                return <><FontAwesomeIcon icon={faPaw} /> Adatváltozás</>;
            case 'OWNERDATACHANGE':
                return <><FontAwesomeIcon icon={faUser} /> Adatváltozás</>;
            case 'NEWOWNER':
                return <><FontAwesomeIcon icon={faUserPlus} /> Új tulaj</>;
            default:
                return <><FontAwesomeIcon icon={faQuestion} /> Egyéb</>
        }
    }

    const getChangedDatas = index => {

        let currentData = changeList[index];

        axiosCall.get('/animal/' + currentData.animalId,
        )
            .then(response => {
                currentData.animalDetails = response.data;
                axiosCall.get('/owner/' + currentData.ownerId,
                )
                    .then(response => {
                        currentData.ownerDetails = response.data;

                        let newList = [...changeList];

                        newList.splice(index, 1, currentData);


                        setChangeList(newList)
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    })
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
    }

    const descDate = (a, b) => {
        if (a.registrationDate < b.registrationDate) {
            return -1;
        }
        if (a.registrationDate > b.registrationDate) {
            return 1;
        }
        return 0;
    }

    const getOldChangedDatas = () => {
        if (limitValue > 0) {
            axiosCall.get('/petvetdata/old-changes/' + limitValue,
            )
                .then(response => {
                    console.log(response.data);
                    setOldChangesList(response.data.sort(descDate));
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                })
        }
    }

    useEffect(() => {
        getOldChangedDatas();
    }, [limitValue])

    const saveDataChanges = (index) => {

        let currentChange = changeList[index];

        const bodyParameters = {
            id: currentChange.id,
            animalName: currentChange.animalName,
            idOfMicrochip: currentChange.idOfMicrochip,
            animalId: currentChange.animalId,
            ownerName: currentChange.ownerName,
            ownerId: currentChange.ownerId,
            changeType: currentChange.changeType,
            eventDate: currentChange.eventDate,
            registrationDate: moment().format("YYYY-MM-DD"),
        };

        axiosCall.post('/petvetdata/save',
            bodyParameters
        )
            .then(() => {
                getDatas();
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a változások mentése közben'))
            })
    }

    return (

        <div>
            <div className="d-flex align-items-end justify-content-between">
                <span className="mb-0 h5">PetvetData adatváltozások</span>
            </div>
            <hr className='mt-2' />
            <button type="button" className="btn btn-outline-secondary me-2 mb-3 w-100" onClick={() => setLimitValue(limitValue + 5)}>Korábbiak betöltése</button>
            {oldChangesList.map((item, index) => {
                return (
                    <div className="card mb-3" key={index}>
                        <div className="card-body">
                            <div className="d-flex align-items-end justify-content-between">
                                <div>
                                    <small className="rounded-pill bg-secondary text-light p-2 me-2">{iconSelector(item.changeType)} </small> <span>{item.ownerName + ' - ' + item.animalName + ", chipszám: " + item.idOfMicrochip + ", azonosító: " + item.animalId}</span>
                                </div>
                                <div>
                                    <span>{moment(item.eventDate).format("YYYY. MM. DD.")}</span>, felvitel: <span >{moment(item.registrationDate).format("YYYY. MM. DD.")}</span>
                                </div>
                            </div>

                        </div>
                    </div>
                )
            })}
            {changeList.map((item, index) => {
                return (
                    <div className="card mb-3" key={index}>
                        <div className="card-body">
                            <div className="d-flex align-items-end justify-content-between">
                                <div>
                                    <small className="rounded-pill bg-secondary text-light p-2 me-2">{iconSelector(item.changeType)} </small> <span>{item.ownerName + ' - ' + item.animalName + ", chipszám: " + item.idOfMicrochip + ", azonosító: " + item.animalId}</span>
                                </div>
                                <div>
                                    <span className='me-3'>{moment(item.eventDate).format("YYYY. MM. DD.")}</span>
                                    <button type="button" className="btn btn-primary btn-sm me-2 rounded-circle" data-bs-toggle="collapse" data-bs-target={"#moreDataCollapse" + index} aria-expanded="false" aria-controls={"#moreDataCollapse" + index} onClick={() => getChangedDatas(index)}><FontAwesomeIcon icon={faListUl} /></button>
                                    <button type="button" className="btn btn-primary btn-sm rounded-circle" onClick={() => saveDataChanges(index)}><FontAwesomeIcon icon={faCheck} /></button>
                                </div>
                            </div>
                            <div className="collapse" id={"moreDataCollapse" + index}>
                                {item.animalDetails ?
                                    <div className="container-fluid ps-0 pe-0 mt-4">
                                        <div className="row gx-4">
                                            <div className="col-6">
                                                <div className="d-flex align-items-end justify-content-between">
                                                    <div>
                                                        Tulajdonos adatai
                                                    </div>
                                                </div>
                                                <hr className='mt-1' />
                                                <div className="container-fluid ps-0 pe-0">
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Tulajdonos neve:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.ownerDetails.ownerName}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Cím:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.ownerDetails.zipCode + ' ' + item.ownerDetails.city + ', ' + item.ownerDetails.address}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Mobil:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.ownerDetails.mobileNumber}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Telefon:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.ownerDetails.phoneNumber}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Email:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.ownerDetails.email}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Megjegyzés:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p className='text-danger'>
                                                                {item.ownerDetails.ownerNotification}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="d-flex align-items-end justify-content-between">
                                                    <div>
                                                        Állat adatai
                                                    </div>
                                                </div>
                                                <hr className='mt-1' />
                                                <div className="container-fluid ps-0 pe-0">
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Állat neve:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.animalName}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Faj, fajta:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.breed}{item.animalDetails.species && ', '}  {item.animalDetails.species}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                {item.animalDetails.dateOfDeath ? 'Halál dátuma:' : 'Kor:'}
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.dateOfDeath ?
                                                                    moment(item.animalDetails.dateOfDeath).format("YYYY. MM. DD.")
                                                                    :
                                                                    <>
                                                                        {item.animalDetails.birthDateIsEstimated && '~ '}{moment(item.animalDetails.birthDate).format('YYYY. MM. DD.')}
                                                                    </>
                                                                }
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Ivar:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.dateOfSpaying && 'ivartalanított '}{item.animalDetails.sex}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Szín:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.color}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Chipszám:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.idOfMicrochip}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Oltási könyv száma:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.idOfVaccinationBook}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Útlevél száma:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.passportId}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Törzskönyv száma:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.pedigree}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Rendelői azonosító:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p>
                                                                {item.animalDetails.animalId}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row gx-0">
                                                        <div className="col-4">
                                                            <p className='text-secondary'>
                                                                Megjegyzés:
                                                            </p>
                                                        </div>
                                                        <div className="col-8">
                                                            <p className='text-danger'>
                                                                {item.animalDetails.comment}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <div className="spinner-border text-secondary mt-3" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                )
            })}
        </div >
    )
}