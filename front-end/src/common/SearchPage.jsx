import React, { useEffect } from 'react';
import { toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCross, faFileCircleXmark, faMagnifyingGlass, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { Link, useNavigate } from 'react-router-dom';

export default function SearchPage(props) {

    const history = useNavigate();
    const [searchValues, setSearchValues] = React.useState({
        ownerName: "",
        address: "",
        email: "",
        animalName: "",
        breed: "",
        species: "",
        color: "",
        idOfVaccinationBook: "",
        passportId: "",
    });
    const [ownerList, setOwnerList] = React.useState([]);
    const [speciesList, setSpeciesList] = React.useState([]);
    const [breedList, setBreedList] = React.useState([]);
    const [searchFired, setSearchFired] = React.useState(false);
    const [listLoading, setListLoading] = React.useState(false);
    const [animalCounter, setAnimalCounter] = React.useState(0);

    const handleSearchValueChange = event => {
        const key = event.target.name;
        const value = event.target.value;

        let newValues = { ...searchValues }

        newValues[key] = value;

        if (key === 'species') {
            newValues.breed = '';
            if (value === '') {
                setBreedList([])
            } else {
                let species = speciesList.find(s => s.speciesName === value);
                const newBreedList = species.breeds.sort(nameCompare);
                setBreedList(newBreedList)
            }

        }

        setSearchValues(newValues);
    }

    const nameCompare = (a, b) => {
        if (a.speciesName < b.speciesName) {
            return -1;
        }
        if (a.speciesName > b.speciesName) {
            return 1;
        }
        return 0;
    }

    const search = () => {
        setListLoading(true);
        setAnimalCounter(0);
        const bodyParameters = {
            ...searchValues
        };

        axiosCall.post('/animal/detailed-search',
            bodyParameters
        )
            .then((response) => {
                let animalCounter = 0;
                response.data.forEach(owner => {
                    animalCounter += owner.animalList.length;
                })
                setOwnerList(response.data);
                setSearchFired(true);
                setAnimalCounter(animalCounter);
                setListLoading(false);
            })
            .catch(error => {
                toast.error(handleError(error, 'Hiba történt a keresés közben'));
                setListLoading(false)
            })
    }

    const goToAnimal = (animal) => {

        let newHistoryList = [];

        if (localStorage.getItem('searchHistory')) {
            newHistoryList = JSON.parse(localStorage.getItem('searchHistory'));
        }

        let index = newHistoryList.findIndex(a => a.animalId === animal.animalId);

        if (index >= 0) {
            newHistoryList.splice(index, 1);
            newHistoryList.unshift(animal);
        } else {
            newHistoryList.unshift(animal);
        }

        if (newHistoryList.length > 10) {
            newHistoryList.pop();
        }

        localStorage.setItem('searchHistory', JSON.stringify(newHistoryList));
        history("/animal/" + animal.animalId);
    }

    useEffect(() => {

        const getBreedList = () => {
            axiosCall.get('/breed'
            )
                .then(response => {
                    const speciesList = response.data;
                    setSpeciesList(speciesList);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt a fajták betöltése közben'))
                })
        }
        getBreedList();

    }, []);

    return (
        <div>
            <div className="d-flex align-items-end justify-content-between">
                <span className="mb-0 h5">Részletes keresés</span>
            </div>
            <hr className='mt-2' />
            <div className="container-fluid ps-0 pe-0">
                <div className="row gx-4">
                    <div className="col-xl-6">
                        <div className='p-3 bg-white rounded'>
                            <p className='mb-0 text-secondary'><small>Tulajdonos neve</small></p>
                            <input
                                className="form-control"
                                name='ownerName'
                                value={searchValues.ownerName}
                                onChange={handleSearchValueChange}
                            />
                            <p className='mb-0 text-secondary'><small>Cím</small></p>
                            <input
                                className="form-control"
                                name='address'
                                value={searchValues.address}
                                onChange={handleSearchValueChange}
                            />
                            <p className='mb-0 text-secondary'><small>E-mail cím</small></p>
                            <input
                                className="form-control"
                                name='email'
                                value={searchValues.email}
                                onChange={handleSearchValueChange}
                            />
                            <p className='mb-0 text-secondary'><small>Állat neve</small></p>
                            <input
                                className="form-control"
                                name='animalName'
                                value={searchValues.animalName}
                                onChange={handleSearchValueChange}
                            />
                            <p className='mb-0 text-secondary'><small>Faj</small></p>
                            <select
                                className="form-select"
                                name='species'
                                value={searchValues.species}
                                onChange={handleSearchValueChange}
                            >
                                <option value=""></option>
                                {
                                    speciesList.map((option, index) => {
                                        return (
                                            <option key={index} value={option.speciesName}>{option.speciesName}</option>
                                        )
                                    })
                                }
                            </select>
                            <p className='mb-0 text-secondary'><small>Fajta</small></p>
                            <select
                                className="form-select"
                                name='breed'
                                value={searchValues.breed}
                                onChange={handleSearchValueChange}
                                disabled={searchValues.species === ""}
                            >
                                {
                                    breedList.map((option, index) => {
                                        return (
                                            <option key={index} value={option.breedName}>{option.breedName}</option>
                                        )
                                    })
                                }
                            </select>
                            <p className='mb-0 text-secondary'><small>Oltási könyv száma</small></p>
                            <input
                                className="form-control"
                                name='idOfVaccinationBook'
                                value={searchValues.idOfVaccinationBook}
                                onChange={handleSearchValueChange}
                            />
                            <p className='mb-0 text-secondary'><small>Útlevél száma</small></p>
                            <input
                                className="form-control"
                                name='passportId'
                                value={searchValues.passportId}
                                onChange={handleSearchValueChange}
                            />
                            <button type="button" className="btn btn-primary mt-3" onClick={search}>Keresés</button>
                        </div>

                    </div>
                    <div className="col-xl-6">
                        {animalCounter > 200 ?
                            <div className='alert alert-info'>
                                Túl sok találat, pontosítsd a keresési paramétereket!
                            </div>
                            :
                            null
                        }

                        <div className='bg-white rounded'>

                            {listLoading ?
                                <div class="position-relative p-5">
                                    <div class="position-absolute top-50 start-50 translate-middle">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="position-absolute top-50 start-50 translate-middle p-4">
                                        <div class="spinner-border text-secondary text-opacity-50" style={{ 'width': '3rem', 'height': '3rem' }} role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                                :
                                <>
                                    {searchFired ?
                                        <div>
                                            {ownerList.length === 0 ?
                                                <div className='p-5 text-center'>
                                                    <h1 className='text-secondary text-center'>
                                                        <FontAwesomeIcon icon={faFileCircleXmark} />
                                                    </h1>
                                                    <p className='text-secondary text-center'>
                                                        Nincs keresési feltételeknek megfelelő találat.
                                                    </p>
                                                </div>
                                                :
                                                <></>
                                            }
                                            <ul className="list-group">
                                                {ownerList.map((owner, index) => {
                                                    return (
                                                        <>
                                                            {
                                                                owner.animalList.length > 0 ?
                                                                    <li key={owner} className="list-group-item">
                                                                        <span className='text-secondary'><FontAwesomeIcon icon={faMagnifyingGlass} /></span> {owner.ownerName + ', ' + owner.zipCode + ' ' + owner.city + ', ' + owner.address}<br />
                                                                        <div className='mt-1'>
                                                                            {owner.animalList.map((animal, index) => {
                                                                                return (
                                                                                    <button key={index} type="button" className="btn btn-outline-primary me-2 mb-2 btn-sm rounded-pill" onClick={() => goToAnimal(animal)}> {animal.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>}  {animal.animalName + ', ' + animal.breed + ' ' + animal.species} </button>
                                                                                )
                                                                            })}
                                                                        </div>
                                                                    </li>
                                                                    :
                                                                    null
                                                            }
                                                        </>
                                                    )
                                                })}
                                                <li className="list-group-item">
                                                    <div className='mt-1'>
                                                        <Link role="button" to='/edit?animalid=0&ownerid=0&chipnumber=0' className="btn btn-outline-secondary me-2 btn-sm rounded-pill" ><FontAwesomeIcon icon={faUserPlus} /> Új hozzáadása </Link>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        :
                                        <div className='p-5 text-center'>
                                            <h1 className='text-secondary text-center'>
                                                <FontAwesomeIcon icon={faMagnifyingGlass} />
                                            </h1>
                                            <p className='text-secondary text-center'>
                                                A kereséshez írd be a részleteket és a "Keresés" gombot nyomd meg.
                                            </p>
                                        </div>
                                    }
                                </>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}