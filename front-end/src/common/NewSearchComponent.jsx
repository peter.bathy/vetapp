import React from 'react';
import {toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { Link, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCross, faHistory, faMagnifyingGlass, faMagnifyingGlassPlus, faPaw, faUser, faUserPlus } from '@fortawesome/free-solid-svg-icons';

export default function NewSearchComponent(props) {
    const history = useNavigate();
    
    const searchHistory = (JSON.parse(localStorage.getItem('searchHistory')) ? JSON.parse(localStorage.getItem('searchHistory')) : []);
    const [searchList, setSearchList] = React.useState([]);
    const [ownerList, setOwnerList] = React.useState([]);
    const [searchWord, setSearchWord] = React.useState('');
    const [searchMode, setSearchMode] = React.useState('owner');
    const [open, setOpen] = React.useState(false);

    const handleSearchModeChange = () => {
        setSearchWord('');
        setOwnerList([]);
        setSearchList([]);
        if (searchMode === 'owner') {
            setSearchMode('animal');
        } else {
            setSearchMode('owner');
        }
        document.getElementById("searchField").focus();
        setTimeout(() => {
            setOpen(true);
        }, "215")
    };

    const handleClose = () => {
        setTimeout(() => {
            setSearchWord('');
            setSearchList([]);
            setOwnerList([]);
            setOpen(false);
        }, "200")
    }

    const handleFocus = () => {
        setOpen(true);
    }

    const handleChange = (event) => {

        let value = event.target.value;

        setSearchWord(value);
        if (searchMode === 'animal') {
            if (value) {
                const smallSearchWord = String(value).toLocaleLowerCase();
                axiosCall.get('/animal/search/' + smallSearchWord)
                    .then(response => {
                        setSearchList(response.data);
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    });

                if (value.length === 15) {
                    let normalizedValue = value.replaceAll('ö', '0')
                    axiosCall.get('/animal/search/' + normalizedValue)
                        .then(response => {
                            if (response.data.length === 0) {
                                window.location.href = window.location.origin + '/edit?animalid=0&ownerid=0&chipnumber=' + normalizedValue;
                            } else {
                                goToAnimal(response.data[0]);
                            }
                        })
                        .catch(error => {
                            toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                        });
                }
            } else {
                setSearchList([]);
            }
        } else {
            if (value) {

                const smallSearchWord = String(value).toLocaleLowerCase();

                axiosCall.get('/owner/search/' + smallSearchWord)
                    .then(response => {
                        setOwnerList(response.data);
                    })
                    .catch(error => {
                        toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                    });
            } else {
                setOwnerList([]);
            }
        }
    }

    const compare = (a, b) => {
        if (a.animalId < b.animalId) {
            return 1;
        }
        if (a.animalId > b.animalId) {
            return -1;
        }
        return 0;
    }

    const goToAnimal = (animal) => {

        let newHistoryList = [];

        if (localStorage.getItem('searchHistory')) {
            newHistoryList = JSON.parse(localStorage.getItem('searchHistory'));
        }

        let index = newHistoryList.findIndex(a => a.animalId === animal.animalId);

        if (index >= 0) {
            newHistoryList.splice(index, 1);
            newHistoryList.unshift(animal);
        } else {
            newHistoryList.unshift(animal);
        }

        if (newHistoryList.length > 10) {
            newHistoryList.pop();
        }

        localStorage.setItem('searchHistory', JSON.stringify(newHistoryList));
        handleClose();
        history("/animal/" + animal.animalId);
    }

    const goToOwner = (id) => {
        handleClose();
        history("/owner/" + id);
    }

    const addNewAnimalToOwner = id => {
        handleClose();
        history("/edit?animalid=0&ownerid=" + id + '&chipnumber=0')
    }

    document.addEventListener('keydown', function (e) {
        if (e.key === 'Enter') {
            if (e.target.nodeName === 'INPUT' && e.target.type === 'text') {
                e.preventDefault();
                return false;
            }
        }
    }, true);


    return (
        <div className="popperNavbarAnchor">
            <form autoComplete='off'>
                <div className="input-group input-group-sm" id='searchBox'>
                    <button className="btn btn-light btn-sm" type="button" onClick={handleSearchModeChange}>{searchMode === 'owner' ? <FontAwesomeIcon icon={faPaw} /> : <FontAwesomeIcon icon={faUser} />}</button>
                    <button className="btn btn-light be-0  btn-sm" disabled type="button">{searchMode === 'owner' ? <FontAwesomeIcon icon={faUser} /> : <FontAwesomeIcon icon={faPaw} />}<span className='fs-6'> <FontAwesomeIcon icon={faMagnifyingGlass} /></span></button>
                    <input type="text"
                        className="form-control search-bar-input"
                        id='searchField'
                        aria-label="Sizing example input"
                        placeholder="Keresés"
                        onFocus={handleFocus}
                        onChange={handleChange}
                        value={searchWord}
                        onBlur={handleClose}
                        
                    />
                </div>
            </form>
            {open ?
                <div className={'card shadow mb-5 border-top-0 border-bottom-0 popperNavbarAnchorBody'} >
                    <div className="list-group list-group-flush">
                        {(searchList.length !== 0 || ownerList.length !== 0) ?
                            <ul className="list-group">
                                {ownerList.map((owner, index) => {
                                    return (
                                        <li key={index} className="list-group-item">
                                            <span className='text-secondary'><FontAwesomeIcon icon={faMagnifyingGlass} /></span> {owner.ownerName + ', ' + owner.zipCode + ' ' + owner.city + ', ' + owner.address}<br />
                                            <div className='mt-1'>
                                                {owner.animalList.sort(compare).slice(0, 2).map((animal, index) => {
                                                    return (
                                                        <button key={index} type="button" className="btn btn-outline-primary me-2 btn-sm rounded-pill" onClick={() => goToAnimal(animal)}> {animal.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>}  {animal.animalName + ', ' + animal.breed + ' ' + animal.species} </button>
                                                    )
                                                })}
                                                {
                                                    owner.animalList.length > 2 ?
                                                        <button type="button" className="btn btn-outline-primary me-2 btn-sm rounded-pill" onClick={() => goToOwner(owner.ownerId)}> {'+' + Number(owner.animalList.length - 2)} </button>
                                                        :
                                                        <button type="button" className="btn btn-outline-primary me-2 btn-sm rounded-pill" onClick={() => addNewAnimalToOwner(owner.ownerId)}> új állat</button>
                                                }
                                            </div>
                                        </li>
                                    )
                                })}
                                {searchList.map((animal, index) => {
                                    return (
                                        <li key={index} className="list-group-item">
                                            <span className='text-secondary'> <FontAwesomeIcon icon={faMagnifyingGlass} /></span> {animal.ownerName + ', ' + animal.zipCode + ' ' + animal.city + ', ' + animal.address}<br />
                                            <div className='mt-1'>
                                                <button type="button" className="btn btn-outline-primary me-2 btn-sm rounded-pill" onClick={() => goToAnimal(animal)}> {animal.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>}  {animal.animalName + ', ' + animal.breed + ' ' + animal.species + ' - ' + animal.animalId} </button>
                                            </div>
                                        </li>
                                    )
                                })}
                                <li className="list-group-item">
                                    <div className='mt-1'>
                                        <Link role="button" to='/edit?animalid=0&ownerid=0&chipnumber=0' className="btn btn-outline-secondary me-2 btn-sm rounded-pill" onClick={handleClose}><FontAwesomeIcon icon={faUserPlus} /> Új hozzáadása </Link>
                                         <Link role="button" to='/detailed-search' className="btn btn-outline-secondary me-2 btn-sm rounded-pill" onClick={handleClose}><FontAwesomeIcon icon={faMagnifyingGlassPlus} /> Részletes keresés </Link>
                                    </div>
                                </li>
                            </ul>
                            :
                            <ul className="list-group">
                                {searchWord ?
                                    <li className="list-group-item">
                                        <div className='mt-1'>
                                            <Link role="button" to='/edit?animalid=0&ownerid=0&chipnumber=0' className="btn btn-outline-secondary me-2 btn-sm rounded-pill" onClick={handleClose}><FontAwesomeIcon icon={faUserPlus} /> Új hozzáadása </Link>
                                            <Link role="button" to='/detailed-search' className="btn btn-outline-secondary me-2 btn-sm rounded-pill" onClick={handleClose}><FontAwesomeIcon icon={faMagnifyingGlassPlus} /> Részletes keresés </Link>
                                        </div>
                                    </li>
                                    :
                                    <>
                                        {searchHistory.map((animal, index) => {
                                            return (
                                                <li key={index} className="list-group-item">
                                                    <div className='mt-1'>
                                                        <span className='text-secondary'><FontAwesomeIcon icon={faHistory} /></span>  <button type="button" className="btn btn-outline-primary me-2 btn-sm rounded-pill" onClick={() => goToAnimal(animal)}> {animal.animalName + ', ' + animal.breed + ' ' + animal.species + ' - ' + animal.animalId} </button>
                                                    </div>
                                                </li>
                                            );
                                        })}
                                    </>
                                }
                            </ul>
                        }
                    </div>
                </div >
                :
                null
            }
        </div >
    )
}