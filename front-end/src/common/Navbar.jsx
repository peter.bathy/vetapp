import { faBars, faGear } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import Clock from '../common/Clock';
import NewSearchComponent from './NewSearchComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import smallLogo from '../allatdoki-logo-small.png';

export default function Navbar(props) {

    return (
        <React.Fragment>
            <nav className="navbar navbar-dark fixed-top p-0 shadow" style={{ "backgroundColor": "#0597F2" }}>
                <div className="container-fluid">
                    <div className='d-flex'>
                        <>
                        <img className="navbarLogo" src={smallLogo} alt="logo" />
                        <span className="navbar-brand text-white ms-2 mb-0 h1 d-none d-lg-block">Vetapp</span>
                        </>
                        
                        <button className="btn text-light me-3 d-block d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                            <FontAwesomeIcon icon={faBars} />
                        </button>
                    </div>
                    <NewSearchComponent />
                    <div className="d-none d-sm-block">
                        <div className="dropdown d-inline-block ">
                            <button className="btn btn-sm btn-outline-light rounded-pill mb-2 me-3" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <FontAwesomeIcon icon={faGear} />
                            </button>
                            <ul className="dropdown-menu shadow">
                                <li><a className="dropdown-item" href="/settings/datastructure">Adatok</a></li>
                                <li><a className="dropdown-item" href="/settings/goods">Termékek</a></li>
                                <li><a className="dropdown-item" href="/settings/species">Faj/fajta</a></li>
                            </ul>
                        </div>
                        <div className='d-inline-block'>
                            <Clock />
                        </div>
                    </div>
                </div>
            </nav>
        </React.Fragment >
    );
};

