import React from 'react';
import {toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCross, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';

export default function AnimalSearchComponent({ addAnimal }) {

    
    const [open, setOpen] = React.useState(false);
    const [searchList, setSearchList] = React.useState([]);
    const [searchWord, setSearchWord] = React.useState('');

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setTimeout(() => {
            setOpen(false)
            setSearchWord('')
        }, "200")
    }

    const handleChange = (event) => {
        setSearchWord(event.target.value);

        if (event.target.value) {

            const smallSearchWord = String(event.target.value).toLocaleLowerCase();

            axiosCall.get('/animal/search-full/' + smallSearchWord)
                .then(response => {
                    setSearchList(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                });
        } else {
            setSearchList([]);
        }
    }

    const addAnimalFunction = (animal) => {
        handleClose();
        addAnimal(animal);
    }

    return (
        <>
            <div className="input-group input-group-sm" id='searchBox'>
                <button className="btn btn-primary be-0 btn-sm" disabled type="button"> <FontAwesomeIcon icon={faMagnifyingGlass} /></button>
                    <input type="text"
                        className="form-control"
                        id='searchField'
                        aria-label="Sizing example input"
                        placeholder="Keresés"
                        onFocus={handleClick}
                        onChange={handleChange}
                        value={searchWord}
                        onBlur={handleClose}
                    />
            </div>
            {open ?
                <div className={'card shadow mb-5 border-top-0 border-bottom-0 popperNavbarAnchorBody'} >
                    <div className="list-group list-group-flush">
                        {searchList.length !== 0 ?
                            <ul className="list-group">
                                {searchList.slice(0, 20).map((animal, index) => {
                                    return (
                                        <li key={index} className="list-group-item">
                                            <span className='text-secondary'> <FontAwesomeIcon icon={faMagnifyingGlass} /></span> {animal.ownerName + ', ' + animal.zipCode + ' ' + animal.city + ', ' + animal.address}<br />
                                            <div className='mt-1'>
                                                <button type="button" className="btn btn-outline-primary me-2 btn-sm rounded-pill" onClick={() => addAnimalFunction(animal)}> {animal.dateOfDeath && <small><FontAwesomeIcon icon={faCross} /></small>}  {animal.animalName + ', ' + animal.breed + ' ' + animal.species + ' - ' + animal.animalId} </button>
                                            </div>
                                        </li>
                                    )
                                })}
                            </ul>
                            :
                            <ul className="list-group">
                                {searchWord ?
                                    <li className="list-group-item">
                                        <div className='mt-1'>
                                            <p>Nincs találat a keresési kifejezés alapján.</p>
                                        </div>
                                    </li>
                                    :
                                    <li className="list-group-item">
                                        <div className='mt-1'>
                                            <p>Kereséshez adjuk meg a gazdi nevét vagy az állat azonosítóját.</p>
                                        </div>
                                    </li>
                                }
                            </ul>
                        }
                    </div>
                </div >
                :
                null
            }
        </>
    )
}