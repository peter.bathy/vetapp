import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressCard, faCalendarDays, faClipboardList, faEnvelope, faHome, faLaptopMedical, faMedkit, faNotesMedical, faSyringe } from '@fortawesome/free-solid-svg-icons';
import { Offcanvas } from 'bootstrap';

export default function SidebarMenu() {

    const hideSideBarMenu = () => {
        const offcanvas = Offcanvas.getOrCreateInstance(document.getElementById('offcanvasExample'))
        offcanvas.hide();
    }

    return (
        <div>
            <ul className="nav flex-column">
                <li className="nav-item ms-2 mt-2 border-bottom">
                    <Link className="nav-link link-secondary" to="/" onClick={hideSideBarMenu}><span className='fs-6 fa-fw'><FontAwesomeIcon icon={faHome} className='me-3' /></span> Főoldal</Link>
                </li>
                {/*<li className="nav-item ms-2 mt-2 border-bottom">
                    <Link className="nav-link link-secondary" to="/outpatient" onClick={hideSideBarMenu}><span className='fs-6 fa-fw'><FontAwesomeIcon icon={faMedkit} className='me-3' /></span> Ambuláns lapok</Link>
                </li>*/}
                <li className="nav-item ms-2 border-bottom">
                    <Link className="nav-link link-secondary" to="/calendar" onClick={hideSideBarMenu}><span className='fs-6 fa-fw'><FontAwesomeIcon icon={faCalendarDays} className='me-3' /></span> Naptár</Link>
                </li>
                <li className="nav-item ms-2 border-bottom">
                    <Link className="nav-link link-secondary" to="/prices" onClick={hideSideBarMenu}><span className='fs-6 fa-fw'><FontAwesomeIcon icon={faClipboardList} className='me-3' /></span> Árlista</Link>
                </li>
                <li className="nav-item ms-2 border-bottom">
                    <Link className="nav-link link-secondary dropdown-toggle" data-bs-toggle="collapse" to="#notificationMenu" aria-expanded="false" aria-controls="notificationMenu"><span className='fs-6 fa-fw'><FontAwesomeIcon icon={faNotesMedical} className='me-3' /></span> Értesítők</Link>
                </li>
            </ul>
            <div className="collapse" id="notificationMenu">
                <ul className="nav flex-column">
                    <li className="nav-item ms-3">
                        <Link className="nav-link link-secondary" to="/postcards" onClick={hideSideBarMenu}><span className='ps-3 fs-6 fa-fw'><FontAwesomeIcon icon={faEnvelope} className='me-3' /></span> Levelezőlapok</Link>
                    </li>
                </ul>
            </div>
            <ul className="nav flex-column">
                <li className="nav-item ms-2 border-bottom">
                    <Link className="nav-link link-secondary dropdown-toggle" data-bs-toggle="collapse" to="#petvetdataMenu" aria-expanded="false" aria-controls="petvetdataMenu"><span className='fs-6 fa-fw'><FontAwesomeIcon icon={faLaptopMedical} className='me-3' /></span> Petvetdata</Link>
                </li>
            </ul>
            <div className="collapse" id="petvetdataMenu">
                <ul className="nav flex-column">
                    <li className="nav-item ms-3">
                        <Link className="nav-link link-secondary" to="/vaccination/expired" onClick={hideSideBarMenu}><span className='ps-3 fs-6 fa-fw'><FontAwesomeIcon icon={faSyringe} className='me-3' /></span> Oltások</Link>
                    </li>
                    <li className="nav-item ms-3">
                        <Link className="nav-link link-secondary" to="/data-changes" onClick={hideSideBarMenu}><span className='ps-3 fs-6 fa-fw'><FontAwesomeIcon icon={faAddressCard} className='me-3' /></span> Adatváltozások</Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}