import React from 'react';
import { useNavigate } from "react-router-dom";

export default function NotFound() {
    let navigate = useNavigate();

    return (
        <div className="d-flex flex-column align-items-center justify-content-center vh-100 bg-primary">
            <h1 className="display-1 fw-bold text-white">404</h1>
            <h6 className="display-6 fw-bold text-white">A keresett oldal nem található</h6>
            <button type="button" className="btn btn-light mt-3" onClick={() => {
                navigate(`/`)
            }}>Kezdőoldalra</button>
        </div>
    )
}