import { Navigate, useLocation } from 'react-router-dom'
import React from 'react';
import { isLogin } from '../utils/utils';
import Navbar from './Navbar';
import SidebarMenu from './SidebarMenu';
import { ToastContainer } from 'react-toastify';

export function RequireAuth({ children }) {
    const isAuthenticated = isLogin();
    const location = useLocation();

    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    if (isAuthenticated) {
        return (
            <React.Fragment>
                <div className={'d-print-none'}>
                    <Navbar handleDrawerToggle={handleDrawerToggle} />
                </div>
                <div className="offcanvas offcanvas-start" tabIndex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                    <div className="offcanvas-header">
                        <h5 className="offcanvas-title" id="offcanvasExampleLabel">Vetapp menü</h5>
                        <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div className="offcanvas-body">
                        <SidebarMenu />
                    </div>
                </div>
                <div className="position-relative" style={{ 'backgroundColor': '#eeeeee', 'paddingTop': '46px' }}>
                    <div className="d-none d-lg-block position-absolute d-print-none" style={{ 'width': '247px' }}>
                        <SidebarMenu />
                    </div>
                    <div className="d-block main-content px-4 py-3 bg-light">
                        {children}
                    </div>
                </div>
                <ToastContainer />
            </React.Fragment>
        )
    } else {

        return <Navigate to="/signin" state={{ from: location }} />;
    }


}