import React, { useEffect } from 'react';
import moment from 'moment';

export default function Clock() {

    const [time, setTime] = React.useState(moment());

    useEffect(() => {
        const time = setInterval(() => {
            setTime(moment());
        }, 20000);
        return () => clearInterval(time);
    }, []);

    return (
        <p className='fs-2 mb-0 lh-2 text-white'>
            {moment(time).format('HH:mm')}
        </p>
    )
}