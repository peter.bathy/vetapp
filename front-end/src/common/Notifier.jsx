import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressCard, faAt, faCalendarDays, faCartFlatbed, faClipboardList, faDatabase, faEnvelope, faGear, faHome, faLaptopMedical, faListDots, faNotesMedical, faNoteSticky, faSyringe } from '@fortawesome/free-solid-svg-icons';
import { Toast } from 'bootstrap';

export default function Notifier({toastList}) {

  const [list, setList] = useState(toastList);

  useEffect(() => {
    setList(toastList);
}, [toastList, list]);

  return (
    <div className="toast-container position-fixed bottom-0 end-0 p-3 " data-bs-delay="10000">
      <div id="liveToast" className="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div className="d-flex">
          <div className="toast-body">
            Hello, world! This is a toast message.
          </div>
          <button type="button" className="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
      </div>
    </div>
  )
}
