import React, { useEffect } from 'react';
import moment from 'moment';
import { utils, writeFile } from 'xlsx';
import {toast } from 'react-toastify';
import { axiosCall, handleError } from '../utils/ApiService';

export default function VaccinationDataDownload(props) {
    const [vaccines, setVaccines] = React.useState([]);

    const chooseManufacturer = vaccineName => {
        let vaccineIndex = vaccines.findIndex(vaccine => vaccine.vaccineName === vaccineName);

        if (vaccineIndex === -1) {
            return 'A vakcina nem szerepel a gyógyszerlistában';
        } else {
            return vaccines[vaccineIndex].manufacturer;
        }
    }


    const getSerialNumber = vaccineName => {
        const vaccineNumbers = [...props.serialNumbers]

        let index = vaccineNumbers.findIndex(element => element.vaccinationName === vaccineName);

        return vaccineNumbers[index].serialNumber;
    }

    const downloadExcel = () => {

        let excelDatas = [];

        props.animalList.forEach(animal => {

            let dataElement = {
                "idOfMicrochip": animal.numberOfMicrochip,
                "tatoo": "",
                "nameOfVaccine": animal.vaccinationName,
                "manufacturerOfVaccine": chooseManufacturer(animal.vaccinationName),
                "productNumber": getSerialNumber(animal.vaccinationName),
                "dateOfVccination": animal.vaccinationDate,
                "expiryDate": animal.vaccinationEnd,
            }
            excelDatas.push(dataElement);

        });

        const worksheet = utils.json_to_sheet(excelDatas);
        const workbook = utils.book_new();
        utils.sheet_add_aoa(worksheet, [['Mikrochipszám', 'Tetoválás', 'Vakcina neve', 'Vakcina gyártója', 'Gyártási szám', 'Oltás dátuma', 'Lejárat dátuma']], { origin: "A1" })
        utils.book_append_sheet(workbook, worksheet, "Sheet1");
        const filename = "tomeges_oltasfelvitel-" + moment().format('YY-MM-DD-HH-mm');
        writeFile(workbook, filename + ".xls");
    };

    useEffect(() => {
        const getVaccineList = () => {
            axiosCall.get('/goods/vaccines'
            )
                .then(response => {
                    setVaccines(response.data);
                })
                .catch(error => {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                })
        }
        getVaccineList();
    }, []);

    return (
        <div>
            <button onClick={downloadExcel}  type="button" className="btn btn-secondary">Táblázat letöltése</button>
        </div>
    )
}