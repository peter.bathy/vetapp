import React from 'react';
import VaccinationDataListing from './VaccinationDataListing';
import VaccinationDateselector from './VaccinationDateselector';
import '../App.css';
import VaccinationSerialNumbers from './VaccinationSerialNumbers';
import VaccinationDataDownload from './VaccinationDataDownload';
import { faArrowRight, faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const steps = ['Kezdődátum kiválasztása', 'Lista ellenőrzése', 'Gyártási számok megadása', 'Letöltés'];

export default function Vaccination() {
    const [activeStep, setActiveStep] = React.useState(0);
    const [animalList, setAnimalList] = React.useState([]);
    const [serialNumbers, setSerialNumbers] = React.useState([]);

    const handleNext = () => {
        setActiveStep(activeStep + 1);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

    const deleteItem = (index) => {

        const newList = [...animalList];
        newList.splice(index, 1);
        setAnimalList(newList);
    }

    const getStepContent = (step) => {
        switch (step) {
            case 0:
                return <VaccinationDateselector handleNext={handleNext} setAnimalList={setAnimalList} setSerialNumbers={setSerialNumbers}
                />;
            case 1:
                return <VaccinationDataListing animalList={animalList} setAnimalList={setAnimalList} deleteItem={deleteItem} setSerialNumbers={setSerialNumbers}
                />;
            case 2:
                return <VaccinationSerialNumbers animalList={animalList} setAnimalList={setAnimalList} serialNumbers={serialNumbers} setSerialNumbers={setSerialNumbers}
                />;
            case 3:
                return <VaccinationDataDownload animalList={animalList} serialNumbers={serialNumbers}
                />;
            default:
                throw new Error('Unknown step');
        }
    }

    return (
        <div>
            <div className={'d-print-none'}>
                <div className="d-flex align-items-end justify-content-between">
                    <div>
                        <span className="mb-0 h5"> Oltások</span><br />
                        <span className="mb-0 text-secondary"> {steps[activeStep]}</span>
                    </div>
                    {activeStep > 0 && activeStep < 3 &&
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button type="button" className="btn btn-outline-secondary btn-sm" onClick={handleBack} ><FontAwesomeIcon icon={faArrowLeft} /> Vissza</button>
                            <button type="button" className="btn btn-outline-secondary btn-sm" onClick={handleNext}><FontAwesomeIcon icon={faArrowRight} /> Tovább</button>
                        </div>
                    }
                </div>
                <hr className='mt-2 mb-4' />
            </div>
            {getStepContent(activeStep)}
        </div>
    )
}