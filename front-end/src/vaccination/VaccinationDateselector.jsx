import React, { useEffect } from 'react';
import moment from 'moment';
import 'moment/locale/hu';
import { toast } from 'react-toastify';
import { calcAge } from '../utils/utils';
import { axiosCall, handleError } from '../utils/ApiService';

export default function VaccinationDateselector(props) {

    const [currentMonth, setCurrentMonth] = React.useState(moment().startOf('month').format('YYYY-MM-DD'))
    const [daysInMonth, setDaysInMonth] = React.useState([]);

    useEffect(() => {

        const fillDateArray = () => {

            moment.locale('hu');

            let monthsArray = [];

            for (let i = 0; i < moment(currentMonth).daysInMonth(); i++) {

                let currentDate = moment(currentMonth).add(i, 'day');

                if (moment(currentDate).format('YYYY-MM-DD') <= moment().format('YYYY-MM-DD')) {
                    monthsArray.push({
                        givenDay: moment(currentDate).format('YYYY-MM-DD'),
                        dayNumber: i + 1
                    })
                }
            }
            setDaysInMonth(monthsArray);
        };

        fillDateArray();
    }, [currentMonth]);

    const setDuration = animal => {

        const now = moment(animal.vaccinationDate);
        const end = moment(animal.animalDateOfBirth);
        let duration = moment.duration(now.diff(end));

        return duration.asMonths() < 8 ? 0.5 : 1;
    }


    const downloadGivenMonthList = (chosenDay) => {

        const bodyParameters = {
            fromDate: moment(chosenDay).format('YYYY-MM-DD') + ' 00:00:00'
        };

        axiosCall.post('/medicalrecord/vaccination/new',
            bodyParameters
        )
            .then(response => {
                let animalList = [];

                animalList = response.data;

                animalList.forEach(animal => {
                    animal.age = calcAge(animal.animalDateOfBirth, animal.vaccinationDate)
                    animal.numberOfMicrochip = animal.numberOfMicrochip.replaceAll(/\s/g, '');
                    animal.vaccinationDuration = setDuration(animal)
                    animal.vaccinationDate = moment(animal.vaccinationDate).format('YYYY.MM.DD')
                    animal.vaccinationEnd = animal.vaccinationDuration === 0.5 ? moment(animal.vaccinationDate).add(6, 'month').format('YYYY.MM.DD') : moment(animal.vaccinationDate).add(12, 'month').format('YYYY.MM.DD')
                })
                props.setAnimalList(animalList);
                props.handleNext()
            }).catch(error => {
                if (error.response.status === 404) {
                    toast.error('Nincs találat a megadott időponttól')
                } else {
                    toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
                }

            })
    }

    const handleBack = () => {
        const newDate = moment(currentMonth).subtract(1, 'month');

        setCurrentMonth(newDate);
    }

    const handleNext = () => {
        const newDate = moment(currentMonth).add(1, 'month');

        setCurrentMonth(newDate);
    }

    return (
        <div>
            <div className="container-fluid ps-0 pe-0">
                <div className="row g-3">
                    <div className="col-12 col-sm-3">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button type="button" className="btn btn-outline-secondary btn-sm"
                                onClick={handleBack}
                            >Előző </button>
                            {moment().startOf('month').format('YYYY-MM-DD') !== moment(currentMonth).format('YYYY-MM-DD') &&
                                <button type="button" className="btn btn-outline-secondary btn-sm"
                                    onClick={handleNext}
                                >Következő </button>
                            }
                        </div>
                    </div>
                    <div className="col-12 col-sm-6">
                        <h5 className='text-center'>
                            {moment(currentMonth).format('YYYY. MMMM')}
                        </h5>
                    </div>
                    <div className="col-12 col-sm-3">
                    </div>
                </div>
            </div>
            <hr className='mt-2 mb-4' />
            <div className="container-fluid ps-0 pe-0">
                <div className="row g-3">
                    {daysInMonth.map((day, index) =>
                        <div className="col-6 col-sm-3 col-md-2 col-lg-1" key={index}>
                            <button className="btn btn-light h-100 w-100 shadow-sm text-center" type="button" onClick={() => downloadGivenMonthList(day.givenDay)} >
                                <h5>
                                    {day.dayNumber}
                                </h5>
                            </button>
                        </div>
                    )}
                </div >
            </div >
        </div >
    )
}