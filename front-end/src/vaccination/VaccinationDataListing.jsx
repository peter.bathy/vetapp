import React from 'react';
import { toast } from 'react-toastify';
import moment from 'moment';
import { axiosCall, handleError } from '../utils/ApiService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfo, faListUl, faTrash } from '@fortawesome/free-solid-svg-icons';

export default function VaccinationDataListing(props) {

    const [treatmentDetails, setTreatmentDetails] = React.useState({
        animalName: '',
        lastTreatments: [
            {
                dateOfTreatment: '2021.01.01. - 00:00',
                treatmentDetails: ''
            },
            {
                dateOfTreatment: '2021.01.01. - 00:00',
                treatmentDetails: ''
            }, {
                dateOfTreatment: '2021.01.01. - 00:00',
                treatmentDetails: ''
            }
        ]
    })

    const handleClose = () => {
        setTreatmentDetails({
            animalName: '',
            lastTreatments: [
                {
                    dateOfTreatment: '2021.01.01. - 00:00',
                    treatmentDetails: ''
                },
                {
                    dateOfTreatment: '2021.01.01. - 00:00',
                    treatmentDetails: ''
                }, {
                    dateOfTreatment: '2021.01.01. - 00:00',
                    treatmentDetails: ''
                }
            ]
        })
    };

    const getLastTreatment = animal => {

        axiosCall.get('/medicalrecord/lastrecords/' + animal.animalId
        )
            .then(response => {
                setTreatmentDetails({
                    animalName: animal.animalName,
                    lastTreatments: response.data
                });

            }).catch(error => {
                toast.error(handleError(error, 'Hiba történt az adatok betöltése közben'))
            })
            .finally(
            //setLoading(false)
        )
    }

    const handleChange = (event, animal) => {

        const index = props.animalList.findIndex(currentAnimal => currentAnimal.animalId === animal.animalId)
        
        console.log(Number(event.target.value) === 0.5)

        let newAnimalValues = {...props.animalList[index]};
        let newList = [...props.animalList];
        let newEnd = Number(event.target.value) === 0.5 ? moment(animal.vaccinationDate).add(6, 'months').format('YYYY.MM.DD') : moment(animal.vaccinationDate).add(12, 'months').format('YYYY.MM.DD')

        newAnimalValues.vaccinationEnd = newEnd;
        newAnimalValues.vaccinationDuration = Number(event.target.value);

        newList.splice(index, 1, newAnimalValues);

        props.setAnimalList(newList);
    }

    return (
        <div>
            <p className='mb-2'>
                A lista elemeire kattintva meg lehet nézni az utolsó kórelőzményt a kukára kattintva pedig törölhetőek:
            </p>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Állat</th>
                        <th scope="col">Kor</th>
                        <th scope="col">Oltás neve</th>
                        <th scope="col">Érvényesség hossza</th>
                        <th scope="col">Oltás dátuma</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {props.animalList.map((animal, index) => {
                        return (
                            <tr key={index}>
                                <td>
                                    {animal.ownerName + ' / ' + animal.animalName + ', ' + animal.breed + ' ' + animal.species + ' - ' + animal.animalId}<br />
                                    {animal.numberOfMicrochip}
                                </td>
                                <td>
                                    {animal.age}
                                </td>
                                <td>
                                    {animal.vaccinationName}
                                </td>
                                <td>
                                    <select
                                        className="form-select"
                                        name='vaccinationDuration'
                                        key='vaccinationDuration'
                                        id='vaccinationDuration'
                                        value={animal.vaccinationDuration}
                                        onChange={(event) => handleChange(event, animal)}
                                    >
                                        <option value='0.5'>6 hónap</option>
                                        <option value='1'>1 év</option>
                                    </select>
                                </td>
                                <td>
                                    {animal.vaccinationDate}
                                </td>
                                <td>
                                    <button type='button' className="btn btn-light me-2" data-bs-toggle="modal" data-bs-target='#treatmentDetailsModal' onClick={() => getLastTreatment(animal)}><FontAwesomeIcon icon={faInfo} /> </button>
                                    <button type='button' className="btn btn-light" onClick={() => props.deleteItem(index)}><FontAwesomeIcon icon={faTrash} /> </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div className="modal fade" id="treatmentDetailsModal" tabIndex="-1" aria-labelledby="treatmentDetailsModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="treatmentDetailsModalLabel">{treatmentDetails.animalName}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}></button>
                        </div>
                        <div className="modal-body">
                            <div className='mb-3'>
                                <strong>{moment(treatmentDetails.lastTreatments[0].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                <hr className='mt-1' />
                                {treatmentDetails.lastTreatments[0].vaccination &&
                                    <div>
                                        {treatmentDetails.lastTreatments[0].vaccination}
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].anamnesis &&
                                    <div>
                                        <small className='text-secondary'>Anamnézis</small><br />
                                        <span className='ps-2'> {treatmentDetails.lastTreatments[0].anamnesis}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].symptoms &&
                                    <div>
                                        <small className='text-secondary'>Szimptóma</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].symptoms}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].therapy &&
                                    <div>
                                        <small className='text-secondary'>Kezelés</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].therapy}</span>
                                    </div>
                                }
                                {treatmentDetails.lastTreatments[0].otherDetails &&
                                    <div>
                                        <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                        <span className='ps-2'>{treatmentDetails.lastTreatments[0].otherDetails}</span>
                                    </div>
                                }
                                <div className="collapse" id={"moreTreatmentCollapse"} >
                                    {treatmentDetails.lastTreatments[1] &&
                                        <div className='mt-3'>
                                            <strong>{moment(treatmentDetails.lastTreatments[1].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                            <hr className='mt-2' />
                                            {treatmentDetails.lastTreatments[1].vaccination &&
                                                <div>
                                                    {treatmentDetails.lastTreatments[1].vaccination}
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].anamnesis &&
                                                <div>
                                                    <small className='text-secondary'>Anamnézis</small><br />
                                                    <span className='ps-2'> {treatmentDetails.lastTreatments[1].anamnesis}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].symptoms &&
                                                <div>
                                                    <small className='text-secondary'>Szimptóma</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].symptoms}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].therapy &&
                                                <div>
                                                    <small className='text-secondary'>Kezelés</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].therapy}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[1].otherDetails &&
                                                <div>
                                                    <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[1].otherDetails}</span>
                                                </div>
                                            }
                                        </div>}
                                    {treatmentDetails.lastTreatments[2] &&
                                        <div className='mt-3'>
                                            <strong> {moment(treatmentDetails.lastTreatments[2].dateOfTreatment).format('YYYY. MM. DD.  HH:mm')}</strong>
                                            <hr className='mt-2' />
                                            {treatmentDetails.lastTreatments[2].vaccination &&
                                                <div>
                                                    {treatmentDetails.lastTreatments[2].vaccination}
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].anamnesis &&
                                                <div>
                                                    <small className='text-secondary'>Anamnézis</small><br />
                                                    <span className='ps-2'> {treatmentDetails.lastTreatments[2].anamnesis}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].symptoms &&
                                                <div>
                                                    <small className='text-secondary'>Szimptóma</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].symptoms}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].therapy &&
                                                <div>
                                                    <small className='text-secondary'>Kezelés</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].therapy}</span>
                                                </div>
                                            }
                                            {treatmentDetails.lastTreatments[2].otherDetails &&
                                                <div>
                                                    <small className='text-secondary'>Megjegyzés, javaslat</small><br />
                                                    <span className='ps-2'>{treatmentDetails.lastTreatments[2].otherDetails}</span>
                                                </div>
                                            }
                                        </div>}
                                </div>
                            </div>
                            <div className="d-flex align-items-end justify-content-end">
                                <div>
                                    <button type="button" className="btn btn-outline-secondary btn-sm mt-2" data-bs-toggle="collapse" data-bs-target={"#moreTreatmentCollapse"} aria-expanded="false" aria-controls={"#moreTreatmentCollapse"}><FontAwesomeIcon icon={faListUl} /> Előzmény</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </div>
    )
}