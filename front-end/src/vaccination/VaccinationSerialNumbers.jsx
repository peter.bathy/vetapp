import React, { useEffect } from 'react';

export default function VaccinationSerialNumbers({ serialNumbers, setSerialNumbers, animalList }) {

    useEffect(() => {
        let vaccinationList = [];

        animalList.forEach(animal => {
            if (!vaccinationList.some(tag => tag.vaccinationName === animal.vaccinationName)) {
                vaccinationList.push({
                    vaccinationName: animal.vaccinationName
                });
            }
        })
        setSerialNumbers(vaccinationList);
    }, [setSerialNumbers, animalList]);

    const handleChange = (event, index) => {

        let vaccinationList = [...serialNumbers];

        let newValue = vaccinationList[index];

        newValue.serialNumber = event.target.value;

        vaccinationList.splice(index, 1, newValue);

        setSerialNumbers(vaccinationList);
    }

    return (
        <div>
            <div className="container-fluid ps-0 pe-0">
                <div className="row gx-4">
                    <div className="col-2">
                    </div>
                    <div className="col-4">
                        <p>vakcina</p>
                    </div>
                    <div className="col-4">
                        <p>sorozatszám</p>
                    </div>
                    <div className="col-2">
                    </div>
                </div>
            </div>
            <hr className='mt-0 mb-4' />
            <div className="container-fluid ps-0 pe-0">

                {serialNumbers.map((serialnumber, index) => {
                    return (
                        <div className="row gx-4" key={index}>
                            <div className="col-2">
                            </div>
                            <div className="col-4">
                                {serialnumber.vaccinationName}
                            </div>
                            <div className="col-4">
                                <form noValidate>
                                    <input
                                        id="serialNumber"
                                        className="form-control "
                                        defaultValue=''
                                        name="serialNumber"
                                        autoComplete="serialNumber"
                                        onChange={(event) => handleChange(event, index)}
                                    />
                                </form>
                            </div>
                            <div className="col-2">
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    )
}